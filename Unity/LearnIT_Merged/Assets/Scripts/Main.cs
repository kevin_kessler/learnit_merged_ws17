﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;

public static class Main {

    //Use this to add new games to the game selection
    public static void main()
    {
        CreateInformationTheory();
        CreateComplexity();
        CreateNumbersystemsGame();
        CreateModuloVersusGame();
        CreateBooleanAlgebraGame();
        CreateSubnetGame();
        CreateCustomGame();
    }

    private static void CreateCustomGame()
    {
        Game custom = new Game(Global.STRING_CUSTOM_TITLE);
        custom.AddGameMode(new GameModeTheory(TheoryLoader.THEORY_CUSTOM, Global.STRING_THEORY_NAME, string.Format(Global.STRING_THEORY_DESCRIPTION, Global.STRING_CUSTOM_TITLE), Global.PATH_ICON_THEORY, SceneLoader.SCENE_THEORY));
        custom.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_CUSTOM, Global.STRING_CUSTOM_QUIZ_NAME, Global.STRING_CUSTOM_QUIZ_DESCRIPTION, Global.PATH_ICON_QUIZ, SceneLoader.SCENE_QUIZ, Global.PATH_TUTORIAL_QUIZ_SINGLE_PLAYER));
        custom.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_CUSTOM, Global.STRING_CUSTOM_QUIZ_MP_NAME, Global.STRING_CUSTOM_QUIZ_MP_DESCRIPTION, Global.PATH_ICON_QUIZ_MP, SceneLoader.SCENE_QUIZ_MP, Global.PATH_TUTORIAL_QUIZ_MULTI_PLAYER, MPQuizPlayer.PREFAB_PATH));
    }

    private static void CreateInformationTheory()
    {
        Game it = new Game(Global.STRING_IT_TITLE);

        //sp game modes
        it.AddGameMode(new GameModeTheory(TheoryLoader.THEORY_IT, Global.STRING_THEORY_NAME, string.Format(Global.STRING_THEORY_DESCRIPTION, Global.STRING_IT_TITLE), Global.PATH_ICON_THEORY, SceneLoader.SCENE_THEORY));
        it.AddGameMode(new GameMode(Global.STRING_IT_NUMBER_SURVIVAL_NAME, Global.STRING_IT_NUMBER_SURVIVAL_DESCRIPTION, Global.PATH_ICON_IT_NUMBER_GUESSING_SURVIVAL, SceneLoader.SCENE_IT_NUMBER_SURVIVAL, Global.PATH_TUTORIAL_NUMBER_SURVIVAL));
        it.AddGameMode(new GameMode(Global.STRING_IT_NUMBER_ARCADE_NAME, Global.STRING_IT_NUMBER_ARCADE_DESCRIPTION, Global.PATH_ICON_IT_NUMBER_GUESSING_ARCADE, SceneLoader.SCENE_IT_NUMBER_ARCADE, Global.PATH_TUTORIAL_NUMBER_ARCADE));
        it.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_IT, Global.STRING_IT_QUIZ_NAME, Global.STRING_IT_QUIZ_DESCRIPTION, Global.PATH_ICON_QUIZ, SceneLoader.SCENE_QUIZ, Global.PATH_TUTORIAL_QUIZ_SINGLE_PLAYER));

        //mp game modes
        it.AddGameMode(new GameMode(Global.STRING_IT_NUMBER_SURVIVAL_MP_NAME, Global.STRING_IT_NUMBER_SURVIVAL_MP_DESCRIPTION, Global.PATH_ICON_IT_NUMBER_GUESSING_MP_KOOP, SceneLoader.SCENE_IT_NUMBER_SURVIVAL_MP, Global.PATH_TUTORIAL_NUMBER_COOP, MPITNumberPlayer.PREFAB_PATH));
        it.AddGameMode(new GameMode(Global.STRING_IT_NUMBER_BLIND_SURVIVAL_MP_NAME, Global.STRING_IT_NUMBER_BLIND_SURVIVAL_MP_DESCRIPTION, Global.PATH_ICON_IT_NUMBER_GUESSING_MP_BLIND, SceneLoader.SCENE_IT_NUMBER_BLIND_SURVIVAL_MP, Global.PATH_TUTORIAL_NUMBER_COOP_BLIND, MPITNumberPlayer.PREFAB_PATH));
        it.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_IT, Global.STRING_IT_QUIZ_MP_NAME, Global.STRING_IT_QUIZ_MP_DESCRIPTION, Global.PATH_ICON_QUIZ_MP, SceneLoader.SCENE_QUIZ_MP, Global.PATH_TUTORIAL_QUIZ_MULTI_PLAYER, MPQuizPlayer.PREFAB_PATH));
    }

    private static void CreateComplexity()
    {
        //possible colors for graph colouring
        Color[] two_colors = new Color[2] { Color.red, Color.green };
        Color[] three_colors = new Color[3] { Color.red, Color.green, Color.blue };
        Color[] four_colors = new Color[4] { Color.red, Color.green, Color.blue, Color.yellow };

        //graph data
        List<GCGraphData> simple = new List<GCGraphData>();
        simple.Add(new GCGraphData("gc00", 1024, 3, 20f, two_colors));
        simple.Add(new GCGraphData("gc01", 1024, 3, 20f, two_colors));
        simple.Add(new GCGraphData("gc02", 1024, 3, 25f, two_colors));
        simple.Add(new GCGraphData("gc03", 1024, 4, 30f, three_colors));
        simple.Add(new GCGraphData("gc04", 1024, 5, 30f, three_colors));

        List<GCGraphData> logos = new List<GCGraphData>();
        logos.Add(new GCGraphData("gc_logo_chrome", 5, 3, 20f, four_colors));
        logos.Add(new GCGraphData("gc_logo_superman", 10, 3, 25f, two_colors));
        logos.Add(new GCGraphData("gc_logo_bmw", 3, 4, 30f, three_colors));
        logos.Add(new GCGraphData("gc_logo_batman", 5, 4, 30f, three_colors));
        logos.Add(new GCGraphData("gc_logo_captain", 2, 5, 40f, three_colors));

        List<GCGraphData> simple_maps = new List<GCGraphData>();
        simple_maps.Add(new GCGraphData("gc_cs0", 6, 3, 20f, three_colors));
        simple_maps.Add(new GCGraphData("gc_cs1", 7, 3, 20f, two_colors));
        simple_maps.Add(new GCGraphData("gc_cs2", 6, 4, 25f, three_colors));
        simple_maps.Add(new GCGraphData("gc_cs3", 6, 4, 30f, four_colors));

        List<GCGraphData> complex_maps = new List<GCGraphData>();
        complex_maps.Add(new GCGraphData("gc_map_saar", 2, 2, 20f, three_colors));
        complex_maps.Add(new GCGraphData("gc_map_ger", 2, 4, 60f, four_colors));
        complex_maps.Add(new GCGraphData("gc_map_afg", 2, 6, 100f, four_colors));
        complex_maps.Add(new GCGraphData("gc_map_usa", 2, 8, 140f, four_colors));

        List<GCGraphData> all_maps = new List<GCGraphData>();
        all_maps.AddRange(simple_maps);
        all_maps.AddRange(complex_maps);

        List<GCGraphData> master = new List<GCGraphData>();
        master.Add(new GCGraphData("gc_max", 3, 25, 300f, four_colors));

        //determine 2 color and 4 color data entries, for coop mode
        List<GCGraphData> coop_two_color = new List<GCGraphData>();
        List<GCGraphData> coop_four_color = new List<GCGraphData>();
        foreach (GCGraphData data in GCGraphData.GetAllGraphData())
        {
            int numOfColors = data.GetUsableColors().Length;
            if (numOfColors == 2)
                coop_two_color.Add(data);
            else if (numOfColors == 4)
                coop_four_color.Add(data);
        }

        //Game and Game Modes
        Game cp = new Game(Global.STRING_CP_TITLE);

        //singleplayer modes
        cp.AddGameMode(new GameModeTheory(TheoryLoader.THEORY_CP, Global.STRING_THEORY_NAME, string.Format(Global.STRING_THEORY_DESCRIPTION, Global.STRING_CP_TITLE), Global.PATH_ICON_THEORY, SceneLoader.SCENE_THEORY));
        cp.AddGameMode(new GameModeCP(simple, Global.STRING_CP_GC_EASY, Global.STRING_CP_GC_EASY_DESCRIPTION, Global.PATH_ICON_CP_GC_EASY, SceneLoader.SCENE_CP_GC_SURVIVAL, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeCP(logos, Global.STRING_CP_GC_INTERMEDIATE, Global.STRING_CP_GC_INTERMEDIATE_DESCRIPTION, Global.PATH_ICON_CP_GC_INTERMEDIATE, SceneLoader.SCENE_CP_GC_SURVIVAL, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeCP(simple_maps, Global.STRING_CP_GC_MEDIUM, Global.STRING_CP_GC_MEDIUM_DESCRIPTION, Global.PATH_ICON_CP_GC_MEDIUM, SceneLoader.SCENE_CP_GC_ARCADE, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeCP(complex_maps, Global.STRING_CP_GC_HARD, Global.STRING_CP_GC_HARD_DESCRIPTION, Global.PATH_ICON_CP_GC_HARD, SceneLoader.SCENE_CP_GC_ARCADE, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeCP(master, Global.STRING_CP_GC_EXTREME, Global.STRING_CP_GC_EXTREME_DESCRIPTION, Global.PATH_ICON_CP_GC_EXTREME, SceneLoader.SCENE_CP_GC_ARCADE, Global.PATH_TUTORIAL_GC, null));
        cp.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_CP, Global.STRING_CP_QUIZ_NAME, Global.STRING_CP_QUIZ_DESCRIPTION, Global.PATH_ICON_QUIZ, SceneLoader.SCENE_QUIZ, Global.PATH_TUTORIAL_QUIZ_SINGLE_PLAYER));

        //duell modes
        cp.AddGameMode(new GameModeCP(simple, Global.STRING_CP_GC_EASY_MP, Global.STRING_CP_GC_EASY_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_EASY_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(logos, Global.STRING_CP_GC_INTERMEDIATE_MP, Global.STRING_CP_GC_INTERMEDIATE_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_INTERMEDIATE_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(all_maps, Global.STRING_CP_GC_HARD_MP, Global.STRING_CP_GC_HARD_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_HARD_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(master, Global.STRING_CP_GC_EXTREME_MP, Global.STRING_CP_GC_EXTREME_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_EXTREME_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(null, Global.STRING_CP_GC_RANDOM_MP, Global.STRING_CP_GC_RANDOM_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_RANDOM_MP, SceneLoader.SCENE_CP_GC_MP, Global.PATH_TUTORIAL_GC_DUELL, MPGCPlayer.PREFAB_PATH));

        //coop modes
        cp.AddGameMode(new GameModeCP(coop_two_color, Global.STRING_CP_GC_COOP_ONE_MP, Global.STRING_CP_GC_COOP_ONE_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_COOP_ONE, SceneLoader.SCENE_CP_GC_COOP_MP, Global.PATH_TUTORIAL_GC_COOP, MPGCPlayerCoop.PREFAB_PATH));
        cp.AddGameMode(new GameModeCP(coop_four_color, Global.STRING_CP_GC_COOP_TWO_MP, Global.STRING_CP_GC_COOP_TWO_DESCRIPTION_MP, Global.PATH_ICON_CP_GC_COOP_TWO, SceneLoader.SCENE_CP_GC_COOP_MP, Global.PATH_TUTORIAL_GC_COOP, MPGCPlayerCoop.PREFAB_PATH));

        //quiz duell
        cp.AddGameMode(new GameModeQuiz(QuizLoader.QUIZ_CP, Global.STRING_CP_QUIZ_MP_NAME, Global.STRING_CP_QUIZ_MP_DESCRIPTION, Global.PATH_ICON_QUIZ_MP, SceneLoader.SCENE_QUIZ_MP, Global.PATH_TUTORIAL_QUIZ_MULTI_PLAYER, MPQuizPlayer.PREFAB_PATH));
    }

    private static void CreateModuloVersusGame()
    {
        Game moduloGame = new Game("Modulogruppen");

        //MULTIPLAYER

        //VERSUS
        string description = "In diesem Modus kämpfst du gegen einen anderen Mitspieler. Wer die meisten Felder in seiner Spielerfarbe einfärben konnte, gewinnt!";
        moduloGame.AddGameMode(
            new GameModeModulo(
                1,
                "Modulo Versus Best Of 1",
                description,
                "Images/Icons/ModuloLogo",
                "MPModuloVersusBo",
                null,
                "Prefabs/Network/MPModuloPlayer"));

        moduloGame.AddGameMode(
            new GameModeModulo(
                3,
                "Modulo Versus Best Of 3",
                description,
                "Images/Icons/ModuloLogo",
                "MPModuloVersusBo",
                null,
                "Prefabs/Network/MPModuloPlayer"));

        moduloGame.AddGameMode(
            new GameModeModulo(
                5,
                "Modulo Versus Best Of 5",
                description,
                "Images/Icons/ModuloLogo",
                "MPModuloVersusBo",
                null,
                "Prefabs/Network/MPModuloPlayer"));


    }

    private static void CreateNumbersystemsGame()
    {
        Game convertGame = new Game("Zahlen umrechnen");

        GameModeNumberSystem.NumberSystemData binaryToDecimal = new GameModeNumberSystem.NumberSystemData(2, 10, Global.BINARY_TEXT, Global.DECIMAL_TEXT, 1.12f, 13.05f);
        GameModeNumberSystem.NumberSystemData octalToDecimal = new GameModeNumberSystem.NumberSystemData(8, 10, Global.OCTAL_TEXT, Global.DECIMAL_TEXT, 2.16f, 1.85f);
        GameModeNumberSystem.NumberSystemData hexToDecimal = new GameModeNumberSystem.NumberSystemData(16, 10, Global.HEX_TEXT, Global.DECIMAL_TEXT, 1.68f, 9.25f);
        GameModeNumberSystem.NumberSystemData decimalToBinary = new GameModeNumberSystem.NumberSystemData(10, 2, Global.DECIMAL_TEXT, Global.BINARY_TEXT, 1.68f, 9.25f);
        GameModeNumberSystem.NumberSystemData decimalToOctal = new GameModeNumberSystem.NumberSystemData(10, 8, Global.DECIMAL_TEXT, Global.OCTAL_TEXT, 1.68f, 9.25f);
        GameModeNumberSystem.NumberSystemData decimalToHex = new GameModeNumberSystem.NumberSystemData(10, 16, Global.DECIMAL_TEXT, Global.HEX_TEXT, 1.68f, 9.25f);

        //SINGLEPLAYER
        convertGame.AddGameMode(
            new GameModeNumberSystem(
                binaryToDecimal,
                "Binär -> Dezimal Arcade",
                "Ziel ist es, alle vorgegebenen Binärzahlen in Dezimalzahlen umzurechnen. In diesem Modus hast du nur wenige Versuche, aber keinen Zeitdruck.",
                "Images/Icons/LogoBinaerDecimal",
                "SPNumberSystemArcade",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                octalToDecimal,
                "Oktal -> Dezimal Arcade",
                "Ziel ist es, alle vorgegebenen Oktalzahlen in Dezimalzahlen umzurechnen. In diesem Modus hast du nur wenig Versuche, aber keinen Zeitdruck.",
                "Images/Icons/LogoOktalDecimal",
                "SPNumberSystemArcade",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                hexToDecimal,
                "Hexadezimal -> Dezimal Arcade",
                "Ziel ist es, alle vorgegebenen Hexadezimalzahlen in Dezimalzahlen umzurechnen. In diesem Modus hast du nur wenig Versuche, aber keinen Zeitdruck",
                "Images/Icons/LogoHexDecimal",
                "SPNumberSystemArcade",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToBinary,
                "Dezimal -> Binär Arcade",
                "Ziel ist es, alle vorgegebenen Dezimalzahlen in Binärzahlen umzurechnen. In diesem Modus hast du nur wenig Versuche, aber keinen Zeitdruck.",
                "Images/Icons/LogoDecimalBinaer",
                "SPNumberSystemArcade",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToOctal,
                "Dezimal -> Oktal Arcade",
                "Ziel ist es, alle vorgegebenen Dezimalzahlen in Oktalzahlen umzurechnen. in diesem Modus hast du nur wenig Versuche, aber keinen Zeitdruck.",
                "Images/Icons/LogoDecimalOktal",
                "SPNumberSystemArcade",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToHex,
                "Dezimal -> Hexadezimal Arcade",
                "Ziel ist es, alle vorgegebenen Dezimalzahlen in Hexadezimalzahlen umzurechnen. In diesem Modus hast du nur wenig versuche, aber keinen Zeitdruck.",
                "Images/Icons/LogoDecimalHex",
                "SPNumberSystemArcade",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                binaryToDecimal,
                "Binär -> Dezimal Survival",
                "Ziel ist es, alle vorgegebenen Binärzahlen in Dezimalzahlen umzurechnen. In diesem Modus kämpst du gegen die Zeit!",
                "Images/Icons/LogoBinaerDecimal",
                "SPNumberSystemSurvival",
                null,
                null
            ));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                octalToDecimal,
                "Oktal -> Dezimal Survival",
                "Ziel ist es, alle vorgegebenen Oktalzahlen in Dezimalzahlen umzurechnen. In diesem Modus kämpfst du gegen die Zeit!",
                "Images/Icons/LogoOktalDecimal",
                "SPNumberSystemSurvival",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                hexToDecimal,
                "Hexadezimal -> Dezimal Survival",
                "Ziel ist es, alle vorgegebenen Hexadezimalzahlen in Dezimalzahlen umzurechnen. In diesem  Modus kämpfst du gegen die Zeit!",
                "Images/Icons/LogoHexDecimal",
                "SPNumberSystemSurvival",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToBinary,
                "Dezimal -> Binär Survival",
                "Ziel ist es, alle vorgegebenen Dezimalzahlen in Binärzahlen umzurechnen. In diesem Modus kämpfst du gegen die Zeit!",
                "Images/Icons/LogoDecimalBinaer",
                "SPNumberSystemSurvival",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToOctal,
                "Dezimal -> Oktal Survival",
                "Ziel ist es, alle vorgegebenen Dezimalzahlen in Oktalzahlen umzurechnen. In diesem Modus kämpfst du gegen die Zeit!",
                "Images/Icons/LogoDecimalOktal",
                "SPNumberSystemSurvival",
                null,
                null));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToHex,
                "Dezimal -> Hexadezimal Survival",
                "Ziel ist es, alle vorgegebenen Dezimalzahlen in Hexadezimalzahhlen umzurechnen. In diesem Modus kämpfst du gegen die Zeit!",
                "Images/Icons/LogoDecimalHex",
                "SPNumberSystemSurvival",
                null,
                null));

        //SINGLEPLAYER ZWEIERKOMPLEMENT
        convertGame.AddGameMode(
            new GameModeNumberSystem(
                binaryToDecimal,
                "Zweierkomplement",
                "Ziel ist es, alle vorgegebenen Binärzahlen im Zweierkomplement in Dezimalzahlen umzurechnen. In diesem Modus hast du nur wenige Versuche, aber keinen Zeitdruck.",
                "Images/Icons/zweierLogo",
                "SPTwosComplementArcade",
                null,
                null));

        //MULTIPLAYER
        convertGame.AddGameMode(
            new GameModeNumberSystem(
                binaryToDecimal,
                "Binär -> Dezimal Duell",
                "In diesem Modus kämpst du gegen einen anderen Mitspieler. Wer schneller ist, gewinnt!",
                "Images/Icons/LogoBinaerDecimal", 
                "MPNumberSystemsScore",
                null,
                "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                octalToDecimal,
                "Oktal -> Dezimal Duell",
                "In diesem Modus kämpst du gegen einen anderen Mitspieler. Wer schneller ist, gewinnt!",
                "Images/Icons/LogoOktalDecimal",
                "MPNumberSystemsScore",
                null,
                "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                hexToDecimal,
                "Hexadezimal -> Dezimal Duell",
                "In diesem Modus kämpst du gegen einen anderen Mitspieler. Wer schneller ist, gewinnt!",
                "Images/Icons/LogoHexDecimal",
                "MPNumberSystemsScore",
                null,
                "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToBinary,
                "Dezimal -> Binär Duell",
                "In diesem Modus kämpst du gegen einen anderen Mitspieler. Wer schneller ist, gewinnt!",
                "Images/Icons/LogoDecimalBinaer",
                "MPNumberSystemsScore",
                null,
                "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToOctal,
                "Dezimal -> Oktal Duell",
                "In diesem Modus kämpst du gegen einen anderen Mitspieler. Wer schneller ist, gewinnt!",
                "Images/Icons/LogoDecimalOktal",
                "MPNumberSystemsScore",
                null,
                "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToHex,
                "Dezimal -> Hexadezimal Duell",
                "In diesem Modus kämpst du gegen einen anderen Mitspieler. Wer schneller ist, gewinnt!",
                "Images/Icons/LogoDecimalHex",
                "MPNumberSystemsScore",
                null,
                "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                binaryToDecimal,
                "Binär -> Dezimal Tauziehen",
                "In diesem Modus kämpfst du gegen einen anderen Mitspieler. Wer als erster 3 Punkte Vorsprung hat gewinnt!",
                "Images/Icons/LogoBinaerDecimal",
                "MPNumberSystemsTow",
                null,
                "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
             new GameModeNumberSystem(
                 octalToDecimal,
                 "Oktal -> Dezimal Tauziehen",
                 "In diesem Modus kämpfst du gegen einen anderen Mitspieler. Wer als erster 3 Punkte Vorsprung hat gewinnt!",
                 "Images/Icons/LogoBinaerDecimal",
                 "MPNumberSystemsTow",
                 null,
                 "Prefabs /Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
             new GameModeNumberSystem(
                 hexToDecimal,
                 "Hexadezimal -> Dezimal Tauziehen",
                 "In diesem Modus kämpfst du gegen einen anderen Mitspieler. Wer als erster 3 Punkte Vorsprung hat gewinnt!",
                 "Images/Icons/LogoBinaerDecimal",
                 "MPNumberSystemsTow",
                 null,
                 "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
             new GameModeNumberSystem(
                 decimalToBinary,
                 "Dezimal -> Binär Tauziehen",
                 "In diesem Modus kämpfst du gegen einen anderen Mitspieler. Wer als erster 3 Punkte Vorsprung hat gewinnt!",
                 "Images/Icons/LogoBinaerDecimal",
                 "MPNumberSystemsTow",
                 null,
                 "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
            new GameModeNumberSystem(
                decimalToOctal,
                "Dezimal -> Oktal Tauziehen",
                "In diesem Modus kämpfst du gegen einen anderen Mitspieler. Wer als erster 3 Punkte Vorsprung hat gewinnt!",
                "Images/Icons/LogoBinaerDecimal",
                "MPNumberSystemsTow",
                null,
                "Prefabs/Network/MPNumberSystemsPlayer"));

        convertGame.AddGameMode(
             new GameModeNumberSystem(
                 decimalToHex,
                 "Dezimal -> Hexadezimal Tauziehen",
                 "In diesem Modus kämpfst du gegen einen anderen Mitspieler. Wer als erster 3 Punkte Vorsprung hat gewinnt!",
                 "Images/Icons/LogoBinaerDecimal",
                 "MPNumberSystemsTow",
                 null,
                 "Prefabs/Network/MPNumberSystemsPlayer"));
        
        //COOP

        convertGame.AddGameMode(
             new GameMode("Coop",
             "Ihr spielt zusammen und versucht die vorgegebene Zahl in die geforderten Systeme umzurechnen.",
             "Images/Icons/LogoCOOP",
             "MPNumberSystemsCOOP",
             null,
             "Prefabs/Network/MPNumberSystemsCoopPlayer"));

    }

    private static void CreateSubnetGame()
    {
        Game subnet = new Game("Subnetze");


        //Theorie Modus für Subnetze
        subnet.AddGameMode(
             new GameModeTheory(
                 "subnet_theory", // Theory XML
                 "Subnetze Theorie", //Title
                 "Lernt alles über Subnetze was ihr braucht um bei Quiz zu Punkten", //Description
                 "Images/Icons/book", //Path to Mode Icon
                 "Theory Scene" //Mode Scene Name
        ));

        //Singleplayer
        subnet.AddGameMode(
            new GameModeSubnet(
                GameModeSubnet.TurnMode.TIME,     //turn mode
                GameModeSubnet.Difficulty.EASY,     //difficulty
                GameModeSubnet.PlayMode.SINGLEPLAYER,
                5,                      //number of questions
                5,                      //number of ips per questions
                "Subnetze [Zeit] [Leicht]",     //title
                "Dem Spieler wird ein Subnetz vorgegeben und er muss in der vorgegebenen Zeit eine IP finden, die sich im selben Subnetz befindet.",     //description
                "Images/Icons/subnet",     //Path to mode icon
                "Subnet Scene",     //Mode Scene Name
                 null //"Video/quizsp" //Path to Mode Tutorial Video
        ));

        subnet.AddGameMode(
            new GameModeSubnet(
                GameModeSubnet.TurnMode.TIME,         //turn mode
                GameModeSubnet.Difficulty.MASTER,       //difficulty
                GameModeSubnet.PlayMode.SINGLEPLAYER,
                5,                      //number of questions
                5,                      //number of ips per questions
                "Subnetze [Zeit] [Schwer]",     //title
                "Dem Spieler wird ein Subnetz vorgegeben und er muss in der vorgegebenen Zeit eine IP finden, die sich im selben Subnetz befindet.",     //description
                "Images/Icons/subnet",     //Path to mode icon
                "Subnet Scene",     //Mode Scene Name
                 null //"Video/quizsp" //Path to Mode Tutorial Video
        ));

        subnet.AddGameMode(
            new GameModeSubnet(
                GameModeSubnet.TurnMode.TRIES,        //turn mode
                GameModeSubnet.Difficulty.EASY,         //difficulty
                GameModeSubnet.PlayMode.SINGLEPLAYER,
                5,                      //number of questions
                5,                      //number of ips per questions
                "Subnetze [Versuche] [Leicht]",     //title
                "Dem Spieler wird ein Subnetz vorgegeben und er muss mit den vorgegebenen Versuchen eine IP finden, die sich im selben Subnetz befindet.",     //description
                "Images/Icons/subnet",     //Path to mode icon
                "Subnet Scene",     //Mode Scene Name
                 null //"Video/quizsp" //Path to Mode Tutorial Video
        ));

        subnet.AddGameMode(
            new GameModeSubnet(
                GameModeSubnet.TurnMode.TRIES,        //turn mode
                GameModeSubnet.Difficulty.MASTER,       //difficulty
                GameModeSubnet.PlayMode.SINGLEPLAYER,
                5,                      //number of questions
                5,                      //number of ips per questions
                "Subnetze [Versuche] [Schwer]",     //title
                "Dem Spieler wird ein Subnetz vorgegeben und er muss mit den vorgegebenen Versuchen eine IP finden, die sich im selben Subnetz befindet.",     //description
                "Images/Icons/subnet",     //Path to mode icon
                "Subnet Scene",     //Mode Scene Name
                null //"Video/quizsp" //Path to Mode Tutorial Video
        ));

        //Singleplayer Quiz Mode für Subnetze
        subnet.AddGameMode(
            new GameModeQuiz(
                "subnet_quiz", //quiz xml
                "Subnetze Quiz", //title
                "Ein kleines Quiz zu Thema Subnetze", //description
                "Images/Icons/quiz", //Path to Mode Icon 
                "Quiz Scene", //Mode Scene Name
                "Video/quizsp" //Path to Mode Tutorial Video
        ));

        //Multiplayer
        subnet.AddGameMode(
            new GameModeSubnet(
                GameModeSubnet.TurnMode.NULL,        //turn mode
                GameModeSubnet.Difficulty.EASY,       //difficulty
                GameModeSubnet.PlayMode.PVP,
                5,                      //number of questions
                5,                      //number of ips per questions
                "Subnetze Duell",     //title
                "In diesem Modus kämpfst du gegen einen anderen Mitspieler. Wer schneller alle Fragen Korrekt beantwortet, gewinnt!",     //description
                "Images/Icons/subnet_mp",     //Path to mode icon
                "MP Subnet Scene",     //Mode Scene Name
                null, //"Video/quizmp",
                "Prefabs/Network/MPSubnetPlayer"
        ));

        subnet.AddGameMode(
            new GameModeSubnet(
                GameModeSubnet.TurnMode.NULL,        //turn mode
                GameModeSubnet.Difficulty.EASY,       //difficulty
                GameModeSubnet.PlayMode.COOP,
                5,                      //number of questions
                5,                      //number of ips per questions
                "Subnetze Kooperativ",     //title
                "In diesem Modus spielst du mit einem anderen Mitspieler zusammen. Ihr müsst in der vorgegeben Zeit alle Aufgaben beantworten.",     //description
                "Images/Icons/subnet_mp",     //Path to mode icon
                "MP Subnet Scene",     //Mode Scene Name
                null, //"Video/quizmp",
                "Prefabs/Network/MPSubnetPlayer"
        ));

        //Quiz
        subnet.AddGameMode(
            new GameModeQuiz(
                "subnet_quiz", //quiz xml
                "Subnetze Quiz - Duell", //title
                "Quiz zum Thema Subnetze.", //description
                "Images/Icons/quiz_mp", //Path to Mode Icon 
                "MP Quiz Scene", //Mode Scene Name
                "Video/quizmp", //Path to Mode Tutorial Video
                "Prefabs/Network/MPQuizPlayer" //path to multiplayer prefab
        ));
    }

    private static void CreateBooleanAlgebraGame()
    {
        Game boolescheAlgebra = new Game("Boolesche Algebra");

        /**
         * Schaltnetze Singleplayer
         */
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
           "Schaltnetze [Zeit][Einfach]",
           "Wie viele Schaltnetze schaffst du zu lösen bevor die Zeit abläuft?\n"
           + "richtig = +" + BooleanEquationOnTimeController.RIGHT_ANSWER_IN_SECONDS + " Sekunden\n"
           + "falsch = " + BooleanEquationOnTimeController.FALSE_ANSWER_IN_SECONDS + " Sekunden\n"
           + "überspringen = " + BooleanEquationOnTimeController.SKIP_ANSWER_IN_SECONDS + " Sekunden\n"
           + "\nOperatoren: AND, OR",
           "Images/Icons/bool",
           "BoolEquationOnTimeScene",
           Difficulty.EASY,
            Variant.SCHALTNETZ,
           null,
           null
           ));
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Schaltnetze [Versuche][Einfach]",
            "Wie viele Schaltnetze schaffst du mit " + BooleanEquationOnTryController.GetStartTries(Difficulty.EASY) + " Versuchen zu lösen?\n"
            + "\nOperatoren: AND, OR",
            "Images/Icons/bool",
            "BoolEquationOnTryScene",
            Difficulty.EASY,
            Variant.SCHALTNETZ,
            null,
            null
            ));

        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Schaltnetze [Zeit][Mittel]",
            "Wie viele Schaltnetze schaffst du zu lösen bevor die Zeit abläuft?\n"
            + "richtig = +" + BooleanEquationOnTimeController.RIGHT_ANSWER_IN_SECONDS + " Sekunden\n"
            + "falsch = " + BooleanEquationOnTimeController.FALSE_ANSWER_IN_SECONDS + " Sekunden\n"
            + "überspringen = " + BooleanEquationOnTimeController.SKIP_ANSWER_IN_SECONDS + " Sekunden\n"
            + "\nOperatoren: NAND, NOR",
            "Images/Icons/bool",
            "BoolEquationOnTimeScene",
            Difficulty.MODERATE,
            Variant.SCHALTNETZ,
            null,
            null
            ));
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Schaltnetze [Versuche][Mittel]",
            "Wie viele Schaltnetze schaffst du mit " + BooleanEquationOnTryController.GetStartTries(Difficulty.MODERATE) + " Versuchen zu lösen?\n"
            + "\nOperatoren: NAND, NOR",
            "Images/Icons/bool",
            "BoolEquationOnTryScene",
            Difficulty.MODERATE,
            Variant.SCHALTNETZ,
            null,
            null
            ));

        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Schaltnetze [Zeit][Schwer]",
            "Wie viele Schaltnetze schaffst du zu lösen bevor die Zeit abläuft?\n"
            + "richtig = +" + BooleanEquationOnTimeController.RIGHT_ANSWER_IN_SECONDS + " Sekunden\n"
            + "falsch = " + BooleanEquationOnTimeController.FALSE_ANSWER_IN_SECONDS + " Sekunden\n"
            + "überspringen = " + BooleanEquationOnTimeController.SKIP_ANSWER_IN_SECONDS + " Sekunden"
            + "\nOperatoren: AND, OR, NAND, NOR",
            "Images/Icons/bool",
            "BoolEquationOnTimeScene",
            Difficulty.HARD,
            Variant.SCHALTNETZ,
            null,
            null
            ));
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Schaltnetze [Versuche][Schwer]",
            "Wie viele Schaltnetze schaffst du mit " + BooleanEquationOnTryController.GetStartTries(Difficulty.HARD) + " Versuchen zu lösen?\n"
            + "\nOperatoren: AND, OR, NAND, NOR",
            "Images/Icons/bool",
            "BoolEquationOnTryScene",
            Difficulty.HARD,
            Variant.SCHALTNETZ,
            null,
            null
            ));

        /**
         * Boolean Terms Singleplayer
         */
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Terme [Zeit][Einfach]",
            "Wie viele Terme schaffst du zu lösen bevor die Zeit abläuft?\n"
            + "richtig = +" + BooleanEquationOnTimeController.RIGHT_ANSWER_IN_SECONDS + " Sekunden\n"
            + "falsch = " + BooleanEquationOnTimeController.FALSE_ANSWER_IN_SECONDS + " Sekunden\n"
            + "überspringen = " + BooleanEquationOnTimeController.SKIP_ANSWER_IN_SECONDS + " Sekunden\n"
            + "\nOperatoren: AND, OR",
            "Images/Icons/bool",
            "BoolEquationOnTimeScene",
            Difficulty.EASY,
            Variant.TERM,
            null,
            null
            ));
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Terme [Versuche][Einfach]",
            "Wie viele Terme schaffst du mit " + BooleanEquationOnTryController.GetStartTries(Difficulty.EASY) + " Versuchen zu lösen?\n"
            + "\nOperatoren: AND, OR",
            "Images/Icons/bool",
            "BoolEquationOnTryScene",
            Difficulty.EASY,
            Variant.TERM,
            null,
            null
            ));

        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Terme [Zeit][Mittel]",
            "Wie viele Terme schaffst du zu lösen bevor die Zeit abläuft?\n"
            + "richtig = +" + BooleanEquationOnTimeController.RIGHT_ANSWER_IN_SECONDS + " Sekunden\n"
            + "falsch = " + BooleanEquationOnTimeController.FALSE_ANSWER_IN_SECONDS + " Sekunden\n"
            + "überspringen = " + BooleanEquationOnTimeController.SKIP_ANSWER_IN_SECONDS + " Sekunden\n"
            + "\nOperatoren: NAND, NOR",
            "Images/Icons/bool",
            "BoolEquationOnTimeScene",
            Difficulty.MODERATE,
            Variant.TERM,
            null,
            null
            ));
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Terme [Versuche][Mittel]",
            "Wie viele Terme schaffst du mit " + BooleanEquationOnTryController.GetStartTries(Difficulty.MODERATE) + " Versuchen zu lösen?\n"
            + "\nOperatoren: NAND, NOR",
            "Images/Icons/bool",
            "BoolEquationOnTryScene",
            Difficulty.MODERATE,
            Variant.TERM,
            null,
            null
            ));

        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Terme [Zeit][Schwer]",
            "Wie viele Terme schaffst du zu lösen bevor die Zeit abläuft?\n"
            + "richtig = +" + BooleanEquationOnTimeController.RIGHT_ANSWER_IN_SECONDS + " Sekunden\n"
            + "falsch = " + BooleanEquationOnTimeController.FALSE_ANSWER_IN_SECONDS + " Sekunden\n"
            + "überspringen = " + BooleanEquationOnTimeController.SKIP_ANSWER_IN_SECONDS + " Sekunden"
            + "\nOperatoren: AND, OR, NAND, NOR",
            "Images/Icons/bool",
            "BoolEquationOnTimeScene",
            Difficulty.HARD,
            Variant.TERM,
            null,
            null
            ));
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Terme [Versuche][Schwer]",
            "Wie viele Terme schaffst du mit " + BooleanEquationOnTryController.GetStartTries(Difficulty.HARD) + " Versuchen zu lösen?\n"
            + "\nOperatoren: AND, OR, NAND, NOR",
            "Images/Icons/bool",
            "BoolEquationOnTryScene",
            Difficulty.HARD,
            Variant.TERM,
            null,
            null
            ));

        /**
         * Tic Tac Toe Multiplayer
         */
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Tic Tac Toe [PvP][Einfach]",
            "Schlage deinen Herausforderer im booleschen Tic Tac Toe!\n"
            + "Operatoren: AND, OR",
            "Images/Icons/ttt",
            "TicTacToeScene",
            Difficulty.EASY,
            null,
            "Prefabs/MPTicTacToePlayer"
        ));
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Tic Tac Toe [PvP][Mittel]",
            "Schlage deinen Herausforderer im booleschen Tic Tac Toe!\n"
            + "Operatoren: NAND, NOR",
            "Images/Icons/ttt",
            "TicTacToeScene",
            Difficulty.MODERATE,
            null,
            "Prefabs/MPTicTacToePlayer"
        ));
        boolescheAlgebra.AddGameMode(new BooleanGameMode(
            "Tic Tac Toe [PvP][Schwer]",
            "Schlage deinen Herausforderer im booleschen Tic Tac Toe!\n"
            + "Operatoren: AND, OR, NAND, NOR",
            "Images/Icons/ttt",
            "TicTacToeScene",
            Difficulty.HARD,
            null,
            "Prefabs/MPTicTacToePlayer"
        ));
    }
}

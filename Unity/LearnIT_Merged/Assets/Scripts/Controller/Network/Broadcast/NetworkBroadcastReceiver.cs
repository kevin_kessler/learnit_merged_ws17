﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.Networking;

public class NetworkBroadcastReceiver : NetworkBroadcastService {

    private event UnityAction<string, int, string> OnBroadcastReceive;

    public void AddOnBroadcastReceiveAction(UnityAction<string, int, string> onBroadcastReceiveAction)
    {
        OnBroadcastReceive += onBroadcastReceiveAction;
    }

    public override bool Initialize()
    {
        if (!NetworkTransport.IsStarted)
        {
            NetworkTransport.Init();
        }

        msgBuffer = new byte[MAX_BROADCAST_MSG_SIZE];

        ConnectionConfig cc = new ConnectionConfig();
        cc.AddChannel(QosType.Unreliable);
        defaultTopology = new HostTopology(cc, 1);

        return true;
    }

    // listen for broadcasts
    public override bool StartService()
    {
        if (IsRunning())
        {
            //Debug.LogWarning("NetworkBroadcastReceiver Start: already started");
            return false;
        }

        //open socket
        hostId = NetworkTransport.AddHost(defaultTopology, broadcastPort);
        if (hostId == INVALID_HOST_ID)
        {
            Debug.LogError("NetworkBroadcastReceiver Start: addHost failed");
            return false;
        }

        byte error;
        NetworkTransport.SetBroadcastCredentials(hostId, broadcastKey, broadcastVersion, broadcastSubVersion, out error);

        Debug.Log("NetworkBroadcastReceiver started: listening");
        return true;
    }

    public override bool StopService()
    {
        if (!IsRunning())
            return false;

        //close socket
        NetworkTransport.RemoveHost(hostId);
        hostId = INVALID_HOST_ID;

        Array.Clear(msgBuffer, 0, msgBuffer.Length);

        Debug.Log("Stopped NetworkBroadcastReceiver");
        return true;
    }

    //check for received broadcasts
    void Update()
    {
        if (!IsRunning())
            return;

        int connectionId;
        int channelId;
        int receivedSize;
        byte error;
        NetworkEventType networkEvent = NetworkEventType.DataEvent;

        do
        {
            //check if there is a broadcast message in the queue
            networkEvent = NetworkTransport.ReceiveFromHost(hostId, out connectionId, out channelId, msgBuffer, MAX_BROADCAST_MSG_SIZE, out receivedSize, out error);
            if (networkEvent == NetworkEventType.BroadcastEvent)
            {
                //fetch broadcast message and purge buffer of bytes from previous messages. 
                //note: the purging (clear) could be ommited if performance is affected, since BytesToString copies only "receivedSize" bytes from this message. 
                NetworkTransport.GetBroadcastConnectionMessage(hostId, msgBuffer, MAX_BROADCAST_MSG_SIZE, out receivedSize, out error);
                Array.Clear(msgBuffer, receivedSize - 1, msgBuffer.Length - receivedSize);

                //fetch sender information
                string senderAddr;
                int senderPort;
                NetworkTransport.GetBroadcastConnectionInfo(hostId, out senderAddr, out senderPort, out error);

                //handle broadcast message and make sure to only copy the bytes received by the current message (ommit empty ones)
                string msgData = BytesToString(msgBuffer, receivedSize);
                ReceiveBroadcast(senderAddr, senderPort, msgData);
            }
        } while (networkEvent != NetworkEventType.Nothing);
    }

    private void ReceiveBroadcast(string fromAddress, int fromPort, string data)
    {
        if (null != OnBroadcastReceive)
        {
            OnBroadcastReceive.Invoke(fromAddress, fromPort, data);
            //Debug.Log("Receiving from "+fromAddress + ": " + data);  
        }


        /*
        var items = data.Split(':');
        if (items.Length == 3 && items[0] == "NetworkManager")
        {
            
            if (NetworkManager.singleton != null && NetworkManager.singleton.client == null)
            {
                NetworkManager.singleton.networkAddress = items[1];
                NetworkManager.singleton.networkPort = Convert.ToInt32(items[2]);
                NetworkManager.singleton.StartClient();
            }
            
        }
         * */
    }
}

﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public abstract class NetworkBroadcastService : MonoBehaviour {

    public const int MAX_BROADCAST_MSG_SIZE = 512;
    protected const int INVALID_HOST_ID = -1;

    // config data
    [SerializeField]
    public int broadcastPort = NetworkController.BROADCAST_PORT;

    [SerializeField]
    public int broadcastKey = 1000;

    [SerializeField]
    public int broadcastVersion = 1;

    [SerializeField]
    public int broadcastSubVersion = 1;

    // runtime data
    protected int hostId = INVALID_HOST_ID;
    protected HostTopology defaultTopology;
    protected byte[] msgBuffer = null;

    /// <summary>
    /// Copys the amount of bytes specified by "sizeToCopy" from the given bytes array
    /// to a char array and returns it as string.
    /// If no "sizeToCopy" is specified, the whole bytes array will be copied.
    /// </summary>
    /// <param name="bytes">Data to parse to string</param>
    /// <param name="sizeToCopy">Number of bytes to parse to string. Beginning at bytes[0] up to bytes[sizeToCopy].</param>
    /// <returns></returns>
    protected static string BytesToString(byte[] bytes, int sizeToCopy = -1)
    {
        if (sizeToCopy < 0)
            sizeToCopy = bytes.Length;

        char[] chars = new char[sizeToCopy / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, sizeToCopy);
        return new string(chars);
    }

    protected static byte[] StringToBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    public void SetBroadcastPort(int port)
    {
        broadcastPort = port;
    }

    public void SetBroadcastCredentials(int key, int version, int subversion)
    {
        broadcastKey = key;
        broadcastVersion = version;
        broadcastSubVersion = subversion;
    }

    public bool IsRunning()
    {
        return hostId != INVALID_HOST_ID;
    }

    /*
    public bool RestartService()
    {
        if (IsRunning())
        {
            return StopService() && StartService();
        }
            
        return false;
    }*/

    public abstract bool Initialize();
    public abstract bool StartService();
    public abstract bool StopService();
    
}

﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkBroadcastSender : NetworkBroadcastService {

    [SerializeField]
    public string dataToBroadcast = "This is a broadcasted message";

    public bool SetBroadcastData(string dataToBroadcast)
    {
        if (dataToBroadcast.Length >= MAX_BROADCAST_MSG_SIZE)
        {
            Debug.LogError("NetworkBroadcastSender Initialize - dataToBroadcast is too large. max is " + MAX_BROADCAST_MSG_SIZE);
            return false;
        }

        this.msgBuffer = StringToBytes(dataToBroadcast);
        this.dataToBroadcast = dataToBroadcast;
        //Debug.Log("set broadcast data " + dataToBroadcast + " --- " + BytesToString(msgBuffer));
        return true;
    }

    public override bool Initialize()
    {
        SetBroadcastData(dataToBroadcast);
        ConnectionConfig cc = new ConnectionConfig();
        cc.AddChannel(QosType.Unreliable);
        defaultTopology = new HostTopology(cc, 1);

        return true;
    }

    public override bool StartService()
    {
   
        if (IsRunning())
        {
            Debug.LogWarning("NetworkBroadcastSender Start: can not start, already started");
            return false;
        }

        if (null == msgBuffer)
        {
            Debug.LogError("NetworkBroadcastSender Start: can not start, no message to broadcast was set");
            return false;
        }

        if (Application.internetReachability != NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            Debug.LogError("NetworkBroadcastSender Start: can not start, not connected to network");
            return false;
        }

        //open socket
        hostId = NetworkTransport.AddHost(defaultTopology);
        if (hostId == INVALID_HOST_ID)
        {
            Debug.LogError("NetworkBroadcastSender Start: can not start, addHost failed");
            return false;
        }

        //invoke sending in intervals
        byte err;
        if (!NetworkTransport.StartBroadcastDiscovery(hostId, broadcastPort, broadcastKey, broadcastVersion, broadcastSubVersion, msgBuffer, msgBuffer.Length, 1000, out err))
        {
            Debug.LogError("NetworkBroadcastSender StartBroadcast failed err: " + err);
            return false;
        }

        Debug.Log("NetworkBroadcastSender started! broadcasting message: " + BytesToString(msgBuffer));

        return true;
    }

    public override bool StopService()
    {
        if (!IsRunning())
        {
            Debug.LogError("NetworkBroadcastSender Stop: service not started");
            return false;
        }

        //stop broadcast
        NetworkTransport.StopBroadcastDiscovery();
        //close socket
        NetworkTransport.RemoveHost(hostId);
        hostId = INVALID_HOST_ID;

        Debug.Log("Stopped NetworkBroadcastSender. IsBroadcastDiscoveryRunning = " + NetworkTransport.IsBroadcastDiscoveryRunning());
        return true;
    }

    public IEnumerator RestartServiceRoutine()
    {
        if (IsRunning())
        {
            StopService();

            //wait for service to be stopped, before starting again
            while (NetworkTransport.IsBroadcastDiscoveryRunning())
                yield return null;

            StartService();
        }
    }
}

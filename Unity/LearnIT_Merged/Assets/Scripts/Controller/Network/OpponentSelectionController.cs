﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System.Linq;

public class OpponentSelectionController : MonoBehaviour, INetworkChangedHandler {

    private UIToggleListDialog opponentSelectionDialogUI;
    private Sprite isPlayingSprite;

    private ReadOnlyCollection<NetworkClientInfo> availableOpponents;
    private NetworkClientInfo selectedOpponent = null;

    private NetworkDiscoveryService discoveryService;

    private event UnityAction<NetworkClientInfo> onSelectionSubmitted;

    void Start()
    {
        opponentSelectionDialogUI = UIElement.Instantiate<UIToggleListDialog>(this.transform, "UIOppenentSelection");
        opponentSelectionDialogUI.Setup(
            Global.STRING_DIALOG_CHOOSE_OPPONENT_TITLE,
            Global.STRING_DIALOG_CHOOSE_OPPONENT_LIST_TITLE,
            Global.STRING_CHALLENGE,
            OnOpponentSelectionClose,
            OnOpponentSelected,
            OnSelectionSubmitted
            );

        isPlayingSprite = Resources.Load<Sprite>(Global.PATH_ICON_GAMEPAD);
    }

    public void SetNetworkDiscoveryService(NetworkDiscoveryService discoveryService)
    {
        //unregister from old service
        if (null != this.discoveryService)
            this.discoveryService.RemoveNetworkChangedHandler(this);

        this.discoveryService = discoveryService;

        if (null == discoveryService)
        {
            this.discoveryService = null;
            this.availableOpponents = null;
            this.selectedOpponent = null;
            return;
        }

        //register on new service for network updates
        this.discoveryService = discoveryService;
        this.discoveryService.RegisterNetworkChangedHandler(this);
        this.availableOpponents = this.discoveryService.GetDiscoveredClients();
    }

    public void ShowOpponentSelectionDialog()
    {
        discoveryService.StartListening();
        UpdateOpponentSelectionDialogUI();
        opponentSelectionDialogUI.Show();
    }

    public void AddOnSelectionSubmittedHandler(UnityAction<NetworkClientInfo> onSelectionSubmittedHandler)
    {
        if (null != onSelectionSubmittedHandler)
            onSelectionSubmitted += onSelectionSubmittedHandler;
    }

    public void ClearOnSelectionSubmittedHandlers()
    {
        onSelectionSubmitted = null;
    }

    private void UpdateOpponentSelectionDialogUI()
    {
        //deselect
        if (null != selectedOpponent && selectedOpponent.IsPlaying())
            selectedOpponent = null;

        //remove old toggle entries
        opponentSelectionDialogUI.ClearToggles();

        //create new toggle entries
        List<DictionaryEntry> namesToIsSelectable = GetNamesToIsSelectableOfAvailableOpponents();
        int selectedOpponendIndex = GetIndexOfSelectedOpponent();
        for(int i=0; i < namesToIsSelectable.Count; i++){
            string playerName = (string)namesToIsSelectable[i].Key;
            bool isSelectable = (bool)namesToIsSelectable[i].Value;
            opponentSelectionDialogUI.AddToggle(playerName, isSelectable, isSelectable ? null : isPlayingSprite, i == selectedOpponendIndex);
        }
    }

    //triggered when a toggle of the dialog is clicked
    private void OnOpponentSelected(int index)
    {
        selectedOpponent = availableOpponents[index];
    }

    //triggered when the submit button of the dialog is clicked
    private void OnSelectionSubmitted(int index)
    {
        opponentSelectionDialogUI.Hide();
        discoveryService.StopListening();

        selectedOpponent = availableOpponents[index];
        if (null != onSelectionSubmitted)
            onSelectionSubmitted.Invoke(selectedOpponent);
    }

    private void OnOpponentSelectionClose()
    {
        selectedOpponent = null;
        opponentSelectionDialogUI.Hide();
        discoveryService.StopListening();
    }

    public void OnClientJoined()
    {
        UpdateAvailableOpponents();
    }

    public void OnClientUpdated()
    {
        UpdateAvailableOpponents();
    }

    public void OnClientLeft()
    {
        //deselect selected client, if it was him that left
        if (null != selectedOpponent && !discoveryService.GetDiscoveredClients().Contains(selectedOpponent))
            selectedOpponent = null;

        UpdateAvailableOpponents();
    }

    private void UpdateAvailableOpponents()
    {
        if (null == discoveryService)
            return;

        availableOpponents = discoveryService.GetDiscoveredClients();
        UpdateOpponentSelectionDialogUI();
    }

    private List<DictionaryEntry> GetNamesToIsSelectableOfAvailableOpponents()
    {
        List<DictionaryEntry> opponentNamesToIsPlaying = new List<DictionaryEntry>();
        if (null == availableOpponents || availableOpponents.Count <= 0)
            return opponentNamesToIsPlaying;

        foreach (NetworkClientInfo clientInfo in availableOpponents){
            string name = clientInfo.GetPlayerName();
            bool isPlaying = clientInfo.IsPlaying();
            opponentNamesToIsPlaying.Add(new DictionaryEntry(name, !isPlaying));
        }

        return opponentNamesToIsPlaying;
    }

    private int GetIndexOfSelectedOpponent()
    {
        if (null == availableOpponents || availableOpponents.Count <= 0 || null == selectedOpponent)
            return -1;

        return availableOpponents.IndexOf(selectedOpponent);
    }

    void OnDestroy()
    {
        //unregister for network updates
        SetNetworkDiscoveryService(null);
    }

    //if any other level is loaded while the selection is open -> close it
    void OnLevelWasLoaded(int levelIndex)
    {
        OnOpponentSelectionClose();
    }
}

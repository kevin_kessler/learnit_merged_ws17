﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.Networking;
using PeerMsgTypes;

public class PeerStateInLobby : PeerState {

    private GameMode gameModeToPlay;
    private LobbyController lobby;
    private bool isHostingLobby;
    private string hostAdress;
    private string previousScene;

    private UILoadingIndicator enteringLobbyIndicator;
    private UIDecisionDialog leaveLobbyDialog;

    public PeerStateInLobby(Peer localPeer, GameMode gameModeToPlay)
        :base(localPeer)
    {
        Utils.DebugUtils.AssertNotNull(Utils.DebugUtils.GetMemberName(()=>gameModeToPlay), gameModeToPlay);

        this.gameModeToPlay = gameModeToPlay;
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();

        //indicate entering lobby
        enteringLobbyIndicator = UIElement.Instantiate<UILoadingIndicator>();
        enteringLobbyIndicator.SetIndicationText(Global.STRING_INDICATE_ENTERING_LOBBY);
        enteringLobbyIndicator.SetIsCancelAble(false);
        enteringLobbyIndicator.Show();

        //prepare leaveLobby confirmation dialog
        leaveLobbyDialog = UIElement.Instantiate<UIDecisionDialog>(null, "LeaveLobbyDecisionDialog");
        leaveLobbyDialog.Setup(
                Global.STRING_DIALOG_LEAVE_LOBBY_TITLE,
                Global.STRING_DIALOG_LEAVE_LOBBY_DESCRIPTION,
                null,
                null,
                null
            );
        Object.DontDestroyOnLoad(leaveLobbyDialog.gameObject);

        //if we received the invite, we will host the lobby. 
        if (localPeer.GetPreviousState().GetType() == typeof(PeerStateInviteReceived))
        {
            isHostingLobby = true;
            hostAdress = "localhost";
        }
        //else we will join the opponents lobby
        else if (localPeer.GetPreviousState().GetType() == typeof(PeerStateInvitePending))
        {
            isHostingLobby = false;
            hostAdress = localPeer.GetClient().connection.address;
            localPeer.SendRequest(ReadyToJoinLobbyMsg.ID, new ReadyToJoinLobbyMsg());
        }
        else
        {
            //TODO: implement transition rules and check on transition if valid
            Debug.LogError("Invalid State transition");
            localPeer.TransitionTo(new PeerStateIdle(localPeer));
            return;
        }  
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        if (null != enteringLobbyIndicator)
        {
            enteringLobbyIndicator.Hide();
            Object.Destroy(enteringLobbyIndicator.gameObject);
        }

        if(null!=leaveLobbyDialog)
        {
            leaveLobbyDialog.Hide();
            Object.Destroy(leaveLobbyDialog.gameObject);
        }
    }

    public override PeerState OnReadyToJoinLobbyMsg(NetworkMessage netMsg)
    {
        base.OnReadyToJoinLobbyMsg(netMsg);
        //client is ready to join lobby. -> shutdown peer and host lobby
        localPeer.StartCoroutine(EnterLobby(isHostingLobby));   
        return this;
    }

    public override PeerState OnClientLeftServer(NetworkMessage netMsg)
    {
        base.OnClientLeftServer(netMsg);
        //server peer shut down. -> lobby has been hosted -> join lobby
        localPeer.StartCoroutine(EnterLobby(isHostingLobby));
        return this;
    }
    
    private IEnumerator EnterLobby(bool asHost)
    {
        //wait one frame to finish processing of messages
        yield return null;

        //shut down peer connection in order to be able to host / join lobby
        localPeer.ShutDown();
        while (NetworkServer.active || localPeer.IsConnectedAsClient())
            yield return null;

        //load lobby scene in order to access lobbycontroller
        previousScene = Application.loadedLevelName;
        yield return Application.LoadLevelAsync(SceneLoader.SCENE_LOBBY);
        lobby = Object.FindObjectOfType<LobbyController>();
        lobby.networkPort = NetworkController.LOBBY_PORT;
        lobby.Setup(gameModeToPlay);

        lobby.connectionConfig.FragmentSize = 1024;
        lobby.connectionConfig.MaxSentMessageQueueSize = ushort.MaxValue;

        if (asHost){
            leaveLobbyDialog.AddOnAcceptAction(() => { StopHosting(false); });
            lobby.onLobbyLeaveButton += leaveLobbyDialog.Show;

            //if one of the players leaves the lobby, force stop hosting
            lobby.onLobbyServerDisconnect += () => { StopHosting(true); };
        }
        else{
            leaveLobbyDialog.AddOnAcceptAction(() => { LeaveLobby(false); });
            lobby.onLobbyLeaveButton += leaveLobbyDialog.Show;

            //when server/lobby shuts down or we are kicked while we are connected: force leave lobby
            lobby.onLobbyClientDisconnect += () => { LeaveLobby(true); };
        }

        //join or host lobby
        if (!asHost)
            yield return localPeer.StartCoroutine(
                JoinLobby(hostAdress, (success) => 
                { 
                    if(!success) 
                        localPeer.TransitionTo(new PeerStateIdle(localPeer)); 
                }));
        else
            yield return localPeer.StartCoroutine(HostLobby());

        //we entered lobby -> hide entering indicator
        yield return null;
        if(null!=enteringLobbyIndicator)
            enteringLobbyIndicator.Hide();
    }

    //callback(bool) -> whether successfully joined
    private IEnumerator JoinLobby(string hostAddress, UnityAction<bool> onFinishCallback = null)
    {
        //join lobby as new client
        lobby.networkAddress = hostAddress;
        lobby.StartClient();

        //wait for connection to be established TODO: use constant
        float connectingTime = 0;
        float connectingTimeout = 5;
        while (!lobby.IsClientConnected() && connectingTime < connectingTimeout)
        {
            connectingTime += Time.deltaTime;
            yield return null;
        }

        Debug.Log(logPrefix + "Attempted to join Lobby. Successful: " + lobby.IsClientConnected());

        if (null != onFinishCallback) onFinishCallback.Invoke(lobby.IsClientConnected());

    }

    private IEnumerator HostLobby()
    {
        Debug.Log(logPrefix + "Hosting Lobby");

        //host the lobby and join as localclient
        lobby.StartHost();
        while (!NetworkServer.active)
            yield return null;
    }

    private void LeaveLobby(bool forcedByLobby){
        localPeer.StartCoroutine(LeaveLobbyRoutine(forcedByLobby));
    }
    private void StopHosting(bool forcedByLobby)
    {
        localPeer.StartCoroutine(StopHostingRoutine(forcedByLobby));
    }

    //forcedByLobby = should be true, when the lobby made us to leave and false if the user decided to leave
    //when true, a toast tells the user that the lobby closed
    private IEnumerator LeaveLobbyRoutine(bool forcedByLobby)
    {
        Debug.Log(logPrefix + "LeaveLobby start");

        //disconnect from lobby. lobby object will be destroyed in PeerStateIdle
        //since it would not be destroyed correctly here.
        lobby.StopClient();
        while (lobby.IsClientConnected())
            yield return null;

        yield return Application.LoadLevelAsync(previousScene);
        localPeer.StartUp();
        localPeer.TransitionTo(new PeerStateIdle(localPeer));

        //show message if we have been forced to leave
        if (forcedByLobby)
        {
            Debug.Log(logPrefix + "Forced to leave lobby");
            UIToastFlasher.CreateAndDisplayToast(localPeer, Color.white, Global.COLOR_FLASH_RED, Global.STRING_INDICATE_PLAYER_LEFT_LOBBY);
        }

        Debug.Log(logPrefix + "LeaveLobby end");
    }

    //forcedByLobby = should be true, when the lobby made us to stop and false if the user decided to stop
    //when true, a toast tells the user that the lobby closed
    private IEnumerator StopHostingRoutine(bool forcedByLobby)
    {
        Debug.Log(logPrefix + "StopHosting start");

        lobby.StopHost();
        //NetworkServer.Shutdown();
        while (NetworkServer.active)
            yield return null;

        yield return localPeer.StartCoroutine(LeaveLobbyRoutine(forcedByLobby));

        Debug.Log(logPrefix + "StopHosting end");
    }

}

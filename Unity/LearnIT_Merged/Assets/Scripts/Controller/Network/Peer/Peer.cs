﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;
using PeerMsgTypes;
using System.Collections;

public class Peer : MonoBehaviour
{
#pragma warning disable 0414
    private static bool isInstantiated = false;
#pragma warning restore 0414

    private PeerState previousState;
    private PeerState currentState;

    private int peerPort;
    private NetworkClient localClient;
    private HostTopology topology;
    private GameMode currentGameMode;
    private LocalClientInfo localClientInfo;

    public void Init(int peerPort, LocalClientInfo localClientInfo)
    {
        //we want this to be initialized only once, but not global access
        Debug.Assert(!isInstantiated);
        isInstantiated = true;

        this.peerPort = peerPort;
        this.localClientInfo = localClientInfo;

        //allow only one connection at a time
        ConnectionConfig cc = new ConnectionConfig();
        cc.AddChannel(QosType.Reliable);
        topology = new HostTopology(cc, 1);

        StartUp();
    }

    private void InitServer()
    {
        if (NetworkServer.active)
        {
            ShutDownServer();
        }

        NetworkServer.Configure(topology);

        // start the server
        NetworkServer.Listen(peerPort);

        // system msgs
        NetworkServer.RegisterHandler(MsgType.Connect, (netMsg) => { HandleStateTransition(currentState.OnServerReceivedClient(netMsg)); });
        NetworkServer.RegisterHandler(MsgType.Disconnect, (netMsg) => { HandleStateTransition(currentState.OnServerLostClient(netMsg)); });
        NetworkServer.RegisterHandler(MsgType.Error, (netMsg) => { HandleStateTransition(currentState.OnServerError(netMsg)); });

        // custom msgs
        NetworkServer.RegisterHandler(MatchInvitationRequest.ID, (netMsg) => { HandleStateTransition(currentState.OnMatchInvitationRequest(netMsg)); });
        NetworkServer.RegisterHandler(CancelMatchInvitationRequest.ID, (netMsg) => { HandleStateTransition(currentState.OnCancelMatchInvitationRequest(netMsg)); });
        NetworkServer.RegisterHandler(ReadyToJoinLobbyMsg.ID, (netMsg) => { HandleStateTransition(currentState.OnReadyToJoinLobbyMsg(netMsg)); });

    }

    private void InitClient()
    {
        if (localClient != null)
        {
            ShutDownClient();
        }

        localClient = new NetworkClient();
        localClient.Configure(topology);

        // system msgs
        localClient.RegisterHandler(MsgType.Connect, (netMsg) => { HandleStateTransition(currentState.OnClientJoinedServer(netMsg)); });
        localClient.RegisterHandler(MsgType.Disconnect, (netMsg) => { HandleStateTransition(currentState.OnClientLeftServer(netMsg)); });
        localClient.RegisterHandler(MsgType.Error, (netMsg) => { HandleStateTransition(currentState.OnClientError(netMsg)); });

        // custom msgs
        localClient.RegisterHandler(MatchInvitationResponse.ID, (netMsg) => { HandleStateTransition(currentState.OnMatchInvitationResponse(netMsg)); });
        localClient.RegisterHandler(CancelMatchInvitationResponse.ID, (netMsg) => { HandleStateTransition(currentState.OnCancelMatchInvitationResponse(netMsg)); });
    }

    public void StartUp()
    {
        InitServer();
        InitClient();

        //begin with the idle state
        PeerState initialState = new PeerStateIdle(this);
        if (currentState == null){
            currentState = initialState;
            currentState.OnStateEnter();
        }
        else
            TransitionTo(initialState);
    }

    public void ShutDown()
    {
        ShutDownClient();
        ShutDownServer();
    }

    public void ShutDownClient()
    {
        if (localClient != null)
        {
            localClient.Disconnect();
            localClient.Shutdown();
            localClient = null;
        }
    }

    public void ShutDownServer()
    {
        if (NetworkServer.active)
        {
            NetworkServer.Shutdown();
        }
    }

    public NetworkClient GetClient()
    {
        return localClient;
    }

    public bool IsConnectedAsClient()
    {
        if (null == localClient)
            return false;
        return localClient.isConnected;
    }

    //callback(bool) -> successful or not
    public IEnumerator ConnectToServer(string serverIpAddress, GameMode gameModeToPlay, UnityAction<bool> onFinishCallback = null)
    {
        
        //only allow to connect as a client, if we are in idle state
        if (!(currentState is PeerStateIdle))
        {
            Debug.LogError("Can not connect: not in idle state.");
            if (null != onFinishCallback) onFinishCallback.Invoke(false);
            yield break;
        }

        if (localClient.isConnected)
        {
            Debug.LogError("Can not connect: already connected");
            if (null != onFinishCallback) onFinishCallback.Invoke(false);
            yield break;
        }

        currentGameMode = gameModeToPlay;
        localClient.Connect(serverIpAddress, peerPort);

        //wait for connection to be established TODO: use constant
        float connectingTime = 0;
        float connectingTimeout = 5;
        while (!localClient.isConnected && connectingTime < connectingTimeout)
        {
            connectingTime += Time.deltaTime;
            yield return null;
        }

        if (null != onFinishCallback) onFinishCallback.Invoke(localClient.isConnected);
        yield break;
    }

    public bool DisconnectFromServer()
    {
        //todo: maybe check states which are allowed to disconnect?

        if (!localClient.isConnected)
        {
            //Debug.LogWarning("Can not disconnect: Already disconnected.");
            return false;
        }

        localClient.Disconnect();
        return true;
    }

    public void ResetClient()
    {
        ShutDownClient();
        InitClient();
    }

    public bool SendRequest(short msgType, MessageBase request)
    {
        if (!localClient.isConnected || null == localClient.connection)
        {
            Debug.LogWarning("Can not send request: not connected.");
            return false;
        }

        return localClient.Send(msgType, request);
    }

    public bool SendResponse(short msgType, MessageBase response, NetworkMessage msgToRespondTo)
    {
        if (null == msgToRespondTo || null == msgToRespondTo.conn)
        {
            Debug.LogWarning("Can not send response: requesting client not connected.");
            return false;
        }

        return msgToRespondTo.conn.Send(msgType, response);
    }

    public void SendResponse(short msgType, MessageBase response, NetworkConnection connToRespondTo)
    {
        connToRespondTo.Send(msgType, response);
    }

    public GameMode GetCurrentGameMode()
    {
        return currentGameMode;
    }

    public LocalClientInfo GetLocalClientInfo()
    {
        return localClientInfo;
    }

    //used for when the transition depends on local user action instead of receiving a message
    public void TransitionTo(PeerState transitionState)
    {
        //TODO: check if is valid transition
        HandleStateTransition(transitionState);
    }

    private void HandleStateTransition(PeerState transitionState)
    {
        if (null == transitionState || transitionState.GetType() == currentState.GetType() )
            return;

        previousState = currentState;
        previousState.OnStateExit();

        currentState = transitionState;
        currentState.OnStateEnter();

        //if we are not in idle state, we are busy -> mark as playing
        localClientInfo.SetIsPlaying(!(currentState is PeerStateIdle));
    }

    public PeerState GetPreviousState()
    {
        return previousState;
    }
}

﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using PeerMsgTypes;

public abstract class PeerState {

    protected string logPrefix;
    protected Peer localPeer;

    public PeerState(Peer localPeer)
    {
        this.localPeer = localPeer;
        this.logPrefix = "[" + this.GetType().Name + "]: ";
    }

    public virtual void OnStateEnter()
    {
        Debug.Log(logPrefix + "Entered State");
    }

    public virtual void OnStateExit()
    {
        Debug.Log(logPrefix + "Left State");
    }

    /////////////////////////////////////////////////////////////////////// custom message handlers begin
    

    public virtual PeerState OnMatchInvitationRequest(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "Received " + typeof(MatchInvitationRequest).Name);
        return this;
    }

    public virtual PeerState OnCancelMatchInvitationRequest(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "Received " + typeof(CancelMatchInvitationRequest).Name);
        return this;
    }

    public virtual PeerState OnReadyToJoinLobbyMsg(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "Received " + typeof(ReadyToJoinLobbyMsg).Name);
        return this;
    }

    public virtual PeerState OnMatchInvitationResponse(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "Received " + typeof(MatchInvitationResponse).Name);
        return this;
    }

    public virtual PeerState OnCancelMatchInvitationResponse(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "Received " + typeof(CancelMatchInvitationResponse).Name);
        return this;
    }

    /////////////////////////////////////////////////////////////////////// custom message handlers end

    ///////////////////////////////////////////////////////////////////////system handlers begin
    public virtual PeerState OnServerReceivedClient(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "As Server: Received a client");
        return this;
    }

    
    public virtual PeerState OnServerLostClient(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "As Server: Lost a client");
        return this;
    }

    public virtual PeerState OnServerError(NetworkMessage netMsg)
    {
        UnityEngine.Networking.NetworkSystem.ErrorMessage errMsg = netMsg.ReadMessage<UnityEngine.Networking.NetworkSystem.ErrorMessage>();
        Debug.LogError(logPrefix + "ServerError: " + errMsg.errorCode);
        return this;
    }

    public virtual PeerState OnClientJoinedServer(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "As Client: Joined a server");
        return this;
    }

    public virtual PeerState OnClientLeftServer(NetworkMessage netMsg)
    {
        Debug.Log(logPrefix + "As Client: Left a server");
        return this;
    }

    public virtual PeerState OnClientError(NetworkMessage netMsg)
    {
        UnityEngine.Networking.NetworkSystem.ErrorMessage errMsg = netMsg.ReadMessage<UnityEngine.Networking.NetworkSystem.ErrorMessage>();
        Debug.LogError(logPrefix + "ClientError: " + errMsg.errorCode);
        return this;
    }
    ///////////////////////////////////////////////////////////////////////system handlers end
}

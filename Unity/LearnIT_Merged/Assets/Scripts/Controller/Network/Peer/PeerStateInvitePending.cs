﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using PeerMsgTypes;

public class PeerStateInvitePending : PeerState {

    private UILoadingIndicator waitingForInvitationResponseIndicator;
    private GameMode requestedGameMode;

    public PeerStateInvitePending(Peer localPeer, GameMode requestedGameMode)
        :base(localPeer)
    {
        this.requestedGameMode = requestedGameMode;
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();

        //loading indicator
        waitingForInvitationResponseIndicator = UIElement.Instantiate<UILoadingIndicator>();
        waitingForInvitationResponseIndicator.SetIndicationText(Global.STRING_INDICATE_WAITING_FOR_INVITATION_RESPONSE);
        waitingForInvitationResponseIndicator.ClearOnCancelActions();
        waitingForInvitationResponseIndicator.AddOnCancelAction(() =>
        {
            //we dont go into idle state here, because we wait for ack of this request first
            RequestCancelInvitation();
        });

        waitingForInvitationResponseIndicator.Show();
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        waitingForInvitationResponseIndicator.Hide();
        Object.Destroy(waitingForInvitationResponseIndicator.gameObject);
    }


    public override PeerState OnClientLeftServer(NetworkMessage netMsg)
    {
        base.OnClientLeftServer(netMsg);
        return new PeerStateIdle(localPeer);
    }

    public override PeerState OnClientError(NetworkMessage netMsg)
    {
        base.OnClientError(netMsg);
        ToastInvitationDeclinedMessage();
        return new PeerStateIdle(localPeer);
    }

    public override PeerState OnMatchInvitationResponse(NetworkMessage netMsg)
    {
        base.OnMatchInvitationResponse(netMsg);

        MatchInvitationResponse response = netMsg.ReadMessage<MatchInvitationResponse>();
        bool accepted = response.accepted;
        if (accepted)
        {
            //transition to lobby
            return new PeerStateInLobby(localPeer, requestedGameMode);
        }
        else
        {
            //show declined message
            ToastInvitationDeclinedMessage();
            return new PeerStateIdle(localPeer);
        }
    }

    public override PeerState OnCancelMatchInvitationResponse(NetworkMessage netMsg)
    {
        base.OnCancelMatchInvitationResponse(netMsg);
        return new PeerStateIdle(localPeer);
    }

    //if we get invited from another player while we are in a pending invite, refuse it
    public override PeerState OnMatchInvitationRequest(NetworkMessage netMsg)
    {
        base.OnMatchInvitationRequest(netMsg);
        MatchInvitationResponse response = new MatchInvitationResponse();
        response.accepted = false;
        localPeer.SendResponse(MatchInvitationResponse.ID, response, netMsg);
        return this;
    }

    private void RequestCancelInvitation()
    {
        CancelMatchInvitationRequest request = new CancelMatchInvitationRequest();
        request.requestingPlayerName = localPeer.GetLocalClientInfo().GetPlayerName();
        localPeer.SendRequest(CancelMatchInvitationRequest.ID, request);
    }

    private void ToastInvitationDeclinedMessage()
    {
        waitingForInvitationResponseIndicator.Hide();
        UIToastFlasher.CreateAndDisplayToast(localPeer, Color.white, Global.COLOR_FLASH_RED, Global.STRING_INDICATE_MATCH_DECLINED);
    }
}

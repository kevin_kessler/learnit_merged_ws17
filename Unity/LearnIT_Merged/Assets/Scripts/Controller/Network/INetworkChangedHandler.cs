﻿using UnityEngine;
using System.Collections;

public interface INetworkChangedHandler {

    void OnClientJoined();
    void OnClientLeft();
    void OnClientUpdated();

}

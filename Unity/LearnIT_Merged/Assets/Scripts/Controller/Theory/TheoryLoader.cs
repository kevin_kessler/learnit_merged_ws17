﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using UnityEngine;
using System.Text.RegularExpressions;
using Utils;
using System.Collections;

public class TheoryLoader
{
    private static readonly string LOG_PREFIX = "[" + typeof(TheoryLoader).Name + "]: ";
    private static readonly string THEORY_DIRECTORY = Application.persistentDataPath + Path.AltDirectorySeparatorChar + "theory" + Path.AltDirectorySeparatorChar;
    public const string THEORY_IT = "informationtheory_theory"; //must be equal to the xml file name (without extension)
    public const string THEORY_CP = "complexity_theory"; //must be equal to the xml file name (without extension)
    public const string THEORY_CUSTOM = "custom_theory"; //must be equal to the xml file name (without extension)

    private static Dictionary<string, TheoryBook> theoryByName = new Dictionary<string, TheoryBook>();

    public static TheoryBook LoadTheoryBookByName(string theoryName)
    {
        TheoryBook book;
        if (string.IsNullOrEmpty(theoryName))
            return null;

        if (!theoryByName.ContainsKey(theoryName))
        {
            book = LoadTheoryBook(theoryName);
            theoryByName.Add(theoryName, book);
        }
        else
        {
            book = theoryByName[theoryName];
        }

        return book;
    }

    /// <summary>
    /// Loads a theory book from the xmlfile with the given name (without extension)
    /// And sets the generated book to the dictionary while using the given name as key
    /// </summary>
    private static TheoryBook LoadTheoryBook(string theoryName)
    {
        XmlDocument quizXML = LoadTheoryBookXML(theoryName);
        if (null == quizXML)
            return null;

        TheoryBook book = new TheoryBook();

        //parse all "page" Elements and add them to the book object
        XmlNodeList pageElements = quizXML.SelectNodes("TheoryBook/Page");
        foreach (XmlNode pageElement in pageElements)
        {
            //iterate over page children elements and add them to page object
            TheoryPage currentPage = new TheoryPage();
            FillTheoryPage(currentPage, pageElement.GetEnumerator());
            book.AddPage(currentPage);
        }

        return book;
    }

    private static void FillTheoryPage(TheoryPage page, IEnumerator pageChildren)
    {
        while(pageChildren.MoveNext())
        {
            XmlNode currentNode = (XmlNode)pageChildren.Current;
            if(string.IsNullOrEmpty(currentNode.InnerText))
                continue;

            PageElement.ElementType type = PageElement.ElementType.NONE;
            if (currentNode.LocalName.Equals("Text"))
                type = PageElement.ElementType.TEXT;
            else if (currentNode.LocalName.Equals("Image"))
                type = PageElement.ElementType.IMAGE;
            else if (currentNode.LocalName.Equals("Heading"))
                type = PageElement.ElementType.HEADING;
            else if (currentNode.LocalName.Equals("Caption"))
                type = PageElement.ElementType.CAPTION;

            if (type == PageElement.ElementType.NONE)
                continue;

            //create elementy by type and add attributes of node to it
            PageElement element = null;
            Dictionary<string, string> attributes = new Dictionary<string, string>();
            foreach (XmlAttribute attribute in currentNode.Attributes)
                attributes.Add(attribute.Name, attribute.Value);

            //create specific image element
            if (type == PageElement.ElementType.IMAGE){
                //parse widthScale
                float widthScale = 1;
                string widthAttributeValue = attributes["width"];
                if (null != widthAttributeValue)
                    float.TryParse(widthAttributeValue, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out widthScale);

                //create element
                element = new PageImage(currentNode.InnerText, widthScale, attributes);
            }

            //create general element
            else
                element = new PageElement(type, currentNode.InnerText, attributes);

            page.AddElement(element);
        }
    }

    /// <summary>
    /// Loads the theory book xml file with the given filename (without extension) and returns it.
    /// Returns null if file not found;
    /// </summary>
    private static XmlDocument LoadTheoryBookXML(string theoryFileName)
    {
        if (!Directory.Exists(THEORY_DIRECTORY))
            Directory.CreateDirectory(THEORY_DIRECTORY);

        if (!File.Exists(THEORY_DIRECTORY + theoryFileName + ".xml"))
            CreateTheoryBookFiles(theoryFileName);

        XmlDocument doc = null;
        string xmlText = AssetUtils.LoadTextFromFile(THEORY_DIRECTORY + theoryFileName + ".xml");
        if (null != xmlText)
        {
            doc = new XmlDocument();
            doc.LoadXml(xmlText);
        }

        return doc;
    }

    private static void CreateTheoryBookFiles(string theoryFileName)
    {
        string ressQuizDir = "Theory" + Path.AltDirectorySeparatorChar;
        TextAsset xmlText = Resources.Load<TextAsset>(ressQuizDir + theoryFileName);
        if(null==xmlText)
            return;

        //write xml content to persistent datapath
        File.WriteAllText(THEORY_DIRECTORY + theoryFileName + ".xml", xmlText.text, Encoding.UTF8);

        //identify used images and copy them as well
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xmlText.text);
        XmlNodeList imagePathNodes = doc.SelectNodes("TheoryBook/Page/Image");
        foreach(XmlNode imgPathNode in imagePathNodes)
        {
            if(string.IsNullOrEmpty(imgPathNode.InnerText))
                continue;

            string imageName = Path.GetFileNameWithoutExtension(imgPathNode.InnerText);
            Texture2D image = Resources.Load<Texture2D>(ressQuizDir + imageName);
            if (null != image)
            {
                string fullImageName = THEORY_DIRECTORY + imageName + ".png";
                if (!File.Exists(fullImageName))
                    File.WriteAllBytes(fullImageName, image.EncodeToPNG());
            }
        }

        Debug.Log(LOG_PREFIX + "Created TheoryBook Files for '" + theoryFileName + "' at " + THEORY_DIRECTORY);
    }

    public static Sprite LoadPageImageFromFile(string imageName)
    {
        return AssetUtils.LoadSpriteFromFile(THEORY_DIRECTORY, imageName);
    }
}


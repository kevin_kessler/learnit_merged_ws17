﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using GuessingHint = ITNumberGuessingRound.GuessingHint;

public abstract class ITNumberController : SPGameController {

    //UI
    protected UINumberGuessing numberGuessingUI;
    private UICustomSlider gameSliderUI;
    protected UIImageFlasher screenFlasherUI;
    protected UITextFlasher textFlasherUI;
    private UIDialog helpDialog;

    protected ITNumberGuessingRound currentRound;
    protected override void Init()
    {
        //init ui
        numberGuessingUI = UIElement.Instantiate<UINumberGuessing>(this.transform);
        numberGuessingUI.SetTitleText(GetGameTitle());
        screenFlasherUI = UIElement.Instantiate<UIImageFlasher>(this.transform);
        screenFlasherUI.transform.SetAsLastSibling();
        textFlasherUI = UIElement.Instantiate<UITextFlasher>(this.transform);
        textFlasherUI.transform.SetAsLastSibling();
        gameSliderUI = InitGameSlider();
        gameSliderUI.OnFinished += OnSliderFinished;
        helpDialog = GetHelpDialog();

        numberGuessingUI.AddPauseButtonAction(PauseGame);
        numberGuessingUI.AddGuessButtonAction(Guess);
        numberGuessingUI.SetGameSlider(gameSliderUI);
        numberGuessingUI.GetNumPadUI().AddOnAnyKeyPressedAction(audioPlayer.PlayKeyPadSound);
        numberGuessingUI.Hide();
	}

    protected override void OnGameLoaded()
    {
        ITNumberGuessingRound firstRound = GetFirstRound();
        if (null == firstRound)
        {
            FinishGame("Spielrunden konnten nicht geladen werden.");
            return;
        }

        audioPlayer.PlayNumberGuessingMusic();

        SetCurrentRound(firstRound);
        numberGuessingUI.Show();
    }

    protected abstract UICustomSlider InitGameSlider();
    protected abstract string GetGameTitle();

    protected abstract UIDialog GetHelpDialog();

    private void Guess()
    {
        //check guessed value and give feedback
        string guessedValue = numberGuessingUI.GetInputText();
        GuessingHint hint = currentRound.CheckGuess(guessedValue);
        switch (hint)
        {
            case GuessingHint.CORRECT:
                OnCorrectGuess();
                break;
            case GuessingHint.TOO_HIGH:
                numberGuessingUI.AddGuessItem(currentRound.GetNumOfGuesses(), guessedValue, true);
                OnTooHighGuess();
                break;
            case GuessingHint.TOO_LOW:
                numberGuessingUI.AddGuessItem(currentRound.GetNumOfGuesses(), guessedValue, false);
                OnTooLowGuess();
                break;
            case GuessingHint.WRONG_INPUT:
            default:
                ShowWrongInputDialog();
                break;
        }

        //clear input field for convenience
        numberGuessingUI.ClearInputText();
    }

    protected abstract void OnCorrectGuess();
    protected abstract void OnTooHighGuess();
    protected abstract void OnTooLowGuess();

    protected abstract ITNumberGuessingRound GetNextRound();
    protected abstract ITNumberGuessingRound GetFirstRound();
    protected abstract void UpdateRoundSpecificUI(ITNumberGuessingRound round);
    protected void NextRound()
    {
        SetCurrentRound(GetNextRound());
    }

    protected void SetCurrentRound(ITNumberGuessingRound round)
    {
        currentRound = round;
        UpdateRoundSpecificUI(round);
        Debug.Log("Number to Guess: " + currentRound.GetNumberToGuess());
    }

    protected virtual void ShowWrongNumberFeedback()
    {
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));
        audioPlayer.PlayWrongSound();
    }

    protected virtual void ShowNextRoundFeedback(string textToShow)
    {
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_GREEN));
        StartCoroutine(textFlasherUI.Flash(Global.COLOR_FLASH_GREEN, textToShow));
        audioPlayer.PlayCorrectSound();
    }

    protected virtual void ShowGameWonFeedback()
    {
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_GREEN));
        //TODO: some animation?
        audioPlayer.PlayGameWinSound();
    }

    protected virtual void ShowGameOverFeedback()
    {
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));
        //TODO: some animation?
        audioPlayer.PlayGameOverSound();
    }

    UIInfoDialog wrongInputDialog;
    private void ShowWrongInputDialog()
    {
        if(null == wrongInputDialog){
            wrongInputDialog = UIElement.Instantiate<UIInfoDialog>();
            wrongInputDialog.Setup(
                Global.STRING_DIALOG_WRONG_INPUT_TITLE,
                string.Format(Global.STRING_IT_DIALOG_NUMBER_ARCADE_WRONG_INPUT_DESCRIPTION, 0, int.MaxValue),
                Resources.Load<Sprite>(Global.PATH_ICON_ERROR)
                );
        }

        wrongInputDialog.Show();
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION BEGIN //////////////////////////////////
    public override void OnHelp()
    {
        helpDialog.Show();
    }

    public override bool HasHelpOption()
    {
        return true;
    }
    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION END //////////////////////////////////

    public abstract void OnSliderFinished();

    protected override void OnGameFinished()
    {
    }
}

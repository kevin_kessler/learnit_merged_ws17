﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using GuessingHint = ITNumberGuessingRound.GuessingHint;
using System.IO;

public class MPITNumberController : MPGameController
{
    private const int SURVIVAL_DURATION = 30; //in seconds
    private const int SURVIVAL_BONUS_TIME = 2; //in seconds, will be multiplied by level progress
    
    //Game Specific UI
    private UINumberGuessing numberGuessingUI;
    private UITimeSlider gameSliderUI;
    private UIImageFlasher screenFlasherUI;
    private UITextFlasher textFlasherUI;

    //Game Specific Logic
    [SyncVar(hook="OnCurrentRoundInformationChange")]
    private RoundInformation currentRoundInformation;
    private ITNumberGuessingRound currentRound;
    private int roundCounter = 1;
    bool isBlindMode = false;

    private struct RoundInformation
    {
        public int rangeStart;
        public int rangeEnd;
        public int numberToGuess;
        public int roundCounter;
        public float remainingTime;

        public RoundInformation(ITNumberGuessingRound round, int roundCounter, float remainingTime)
        {
            this.rangeStart = round.GetRangeStart();
            this.rangeEnd = round.GetRangeEnd();
            this.numberToGuess = round.GetNumberToGuess();
            this.roundCounter = roundCounter;
            this.remainingTime = remainingTime;
        }
    }

    //////////////////////////////////////////////////////Shared Logic Begin (Client and Server)
    protected override void InitBothSides()
    {
        //server needs slider in order to control when the game ends and in order to sync remaining time between clients
        //client needs slider for visual representation of remaining time
        if (null == gameSliderUI)
        {
            gameSliderUI = InitGameSlider();
            gameSliderUI.Pause();
        }

        //blind mode is active when the blind mode scene has been loaded.
        isBlindMode = Application.loadedLevelName.Equals(SceneLoader.SCENE_IT_NUMBER_BLIND_SURVIVAL_MP);
    }

    protected override void InitServerSide()
    {
        //only server listens for timer finish
        gameSliderUI.OnFinished += OnSliderFinished;

        //generate first round on server
        currentRound = GetFirstRound();
        currentRoundInformation = new RoundInformation(currentRound, roundCounter, gameSliderUI.GetCurrentValue());
    }

    bool localClientIsInitialized = false;
    protected override void InitClientSide()
    {
        //init UI for client
        numberGuessingUI = UIElement.Instantiate<UINumberGuessing>(this.transform);
        numberGuessingUI.SetTitleText(GetGameTitle());
        numberGuessingUI.SetGameSlider(gameSliderUI);
        numberGuessingUI.SetPauseButtonImage(Resources.Load<Sprite>(Global.PATH_ICON_EXIT));
        numberGuessingUI.AddPauseButtonAction(lobby.InvokeLeaveLobbyButton);
        numberGuessingUI.Hide();
        screenFlasherUI = UIElement.Instantiate<UIImageFlasher>(this.transform);
        screenFlasherUI.transform.SetAsLastSibling();
        screenFlasherUI.Hide();
        textFlasherUI = UIElement.Instantiate<UITextFlasher>(this.transform);
        textFlasherUI.transform.SetAsLastSibling();
        textFlasherUI.Hide();

        //only clients can guess
        numberGuessingUI.AddGuessButtonAction(TryToGuess);
        numberGuessingUI.GetNumPadUI().AddOnAnyKeyPressedAction(audioPlayer.PlayKeyPadSound);

        //set synced currentRoundInformation on clients that joined
        localClientIsInitialized = true;
        OnCurrentRoundInformationChange(currentRoundInformation);
    }

    protected override void OnMatchStarted()
    {
        numberGuessingUI.Show();
        screenFlasherUI.Show();
        textFlasherUI.Show();
        gameSliderUI.Continue();
        audioPlayer.PlayNumberGuessingMusic();
    }

    protected override void OnMatchFinished()
    {
        screenFlasherUI.Hide();
        textFlasherUI.Hide();
        gameSliderUI.Pause();
        audioPlayer.StopBgMusic();
    }

    private UITimeSlider InitGameSlider()
    {
        gameSliderUI = UIElement.Instantiate<UITimeSlider>();
        gameSliderUI.Reset(SURVIVAL_DURATION);
        return gameSliderUI;
    }

    private string GetGameTitle()
    {
        return isBlindMode ? Global.STRING_IT_NUMBER_BLIND_SURVIVAL_MP_NAME : Global.STRING_IT_NUMBER_SURVIVAL_MP_NAME;
    }
    //////////////////////////////////////////////////////Shared Logic End (Client and Server)

    //////////////////////////////////////////////////////Server Logic Begin

    [Server]
    protected override void OnAllPlayersReadyToBeginMatch()
    {
        StartMatch();
    }

    [Server]
    protected override int GetCountdownForLobby()
    {
        return 20;
    }

    [Server]
    public void Guess(string guessedValue, uint guessingPlayerId)
    {
        //check guessed value and give feedback
        GuessingHint hint = currentRound.CheckGuess(guessedValue);
        switch (hint)
        {
            case GuessingHint.CORRECT:
                OnCorrectGuess(guessedValue, guessingPlayerId);
                break;
            case GuessingHint.TOO_HIGH:
                OnTooHighGuess(guessedValue, guessingPlayerId);
                break;
            case GuessingHint.TOO_LOW:
                OnTooLowGuess(guessedValue, guessingPlayerId);
                break;
            case GuessingHint.WRONG_INPUT:
            default:
                //nothing to do. wrong input dialog is shown on client side upon sending the guess
                break;
        }
    }

    [Server]
    private void OnCorrectGuess(string guessedValue, uint guessingPlayerId)
    {
        //calculate bonusTime
        float bonusTime = roundCounter * SURVIVAL_BONUS_TIME;
        float currentTime = gameSliderUI.GetCurrentValue();
        if (bonusTime + currentTime > SURVIVAL_DURATION)
            bonusTime = SURVIVAL_DURATION - currentTime;

        //advance to next round
        NextRound(bonusTime);

        //update player statistics
        MPITNumberPlayer guessingPlayer = (MPITNumberPlayer)GetGamePlayer(guessingPlayerId);
        DebugUtils.AssertNotNull(DebugUtils.GetMemberName(() => guessingPlayer), guessingPlayer);
        guessingPlayer.IncrementNumOfGuesses();
        guessingPlayer.IncrementNumOfCorrectGuesses();
        guessingPlayer.AddBonusTime(bonusTime);
        foreach(MPGamePlayer player in gamePlayers.Values){
            ((MPITNumberPlayer)player).IncrementReachedLevel();
        }
    }

    [Server]
    private void OnTooHighGuess(string guessedValue, uint guessingPlayerId)
    {
        RpcAddGuessItem(currentRound.GetNumOfGuesses(), guessedValue, true, guessingPlayerId);
        RpcShowWrongNumberFeedback();
    }

    [Server]
    private void OnTooLowGuess(string guessedValue, uint guessingPlayerId)
    {
        RpcAddGuessItem(currentRound.GetNumOfGuesses(), guessedValue, false, guessingPlayerId);
        RpcShowWrongNumberFeedback();
    }

    [Server]
    private ITNumberGuessingRound GetFirstRound()
    {
        return ITNumberGuessingRound.GenerateRoundByLevelProgress(1);
    }

    [Server]
    private ITNumberGuessingRound GetNextRound()
    {
        roundCounter++;
        return ITNumberGuessingRound.GenerateRoundByLevelProgress(roundCounter);
    }

    [Server]
    private void NextRound(float bonusTime)
    {
        gameSliderUI.AddTime(bonusTime);

        //advance to next round and tell clients to do so as well by invoking the hook on currentRoundInformation
        int correctNumber = currentRound.GetNumberToGuess();
        currentRound = GetNextRound();
        currentRoundInformation = new RoundInformation(currentRound, roundCounter, gameSliderUI.GetCurrentValue());

        //display next round feedback on clients
        RpcShowNextRoundFeedback(string.Format(
            "\"{0}\""+System.Environment.NewLine 
            + "Level {1}" + System.Environment.NewLine 
            + "+{2}s", correctNumber, roundCounter, Mathf.RoundToInt(bonusTime)));
    }

    [Server]
    private void OnSliderFinished()
    {
        //TODO show blink animation before ends -> maybe in slider script itself
        RpcShowTimeIsUpFeedback();
        FinishMatch();
        //TODO: time is up logic
    }
    
    //////////////////////////////////////////////////////Server Logic End

    //////////////////////////////////////////////////////Client Logic Begin
    [Client]
    private void TryToGuess()
    {
        string guessedValue = numberGuessingUI.GetInputText();

        //client side input validation to show wrong input dialog if wrong input was delivered
        bool isValidGuess = currentRound.CheckGuess(guessedValue) != ITNumberGuessingRound.GuessingHint.WRONG_INPUT;
        if (isValidGuess)
        {
            //input okay -> forward guess from localplayer to server
            ((MPITNumberPlayer)GetLocalGamePlayer()).CmdGuess(guessedValue);
        }
        else
        {
            ShowWrongInputDialog();
        }

        //clear input field for convenience
        numberGuessingUI.ClearInputText();
    }


    [Client]
    private void OnCurrentRoundInformationChange(RoundInformation newRoundInformation)
    {
        if (!localClientIsInitialized)
            return;

        Debug.Log(LOG_PREFIX + "OnCurrentRoundInformationChange");

        //update to retrieved round info
        roundCounter = newRoundInformation.roundCounter;
        ITNumberGuessingRound newRound = new ITNumberGuessingRound(newRoundInformation.rangeStart, newRoundInformation.rangeEnd);
        newRound.SetNumberToGuess(newRoundInformation.numberToGuess);
        currentRound = newRound;
        numberGuessingUI.SetRoundInfo(currentRound, roundCounter.ToString());

        //sync timer
        //bool localTimeIsAhead = gameSliderUI.GetCurrentValue() > newRoundInformation.remainingTime;
        //float difference = Mathf.Abs(gameSliderUI.GetCurrentValue() - newRoundInformation.remainingTime);
        //gameSliderUI.AddTime(localTimeIsAhead ? -difference : difference);
        gameSliderUI.SetTime(newRoundInformation.remainingTime);
        Debug.Log(LOG_PREFIX + "Number to Guess: " + currentRound.GetNumberToGuess());
    }

    [ClientRpc]
    private void RpcAddGuessItem(int numOfGuesses, string guessedValue, bool tooHigh, uint playerId)
    {
        //determine player that made the guess to access its playercolor and update player stats
        MPITNumberPlayer guessingPlayer = (MPITNumberPlayer)GetGamePlayer(playerId);
        DebugUtils.AssertNotNull(DebugUtils.GetMemberName(() => guessingPlayer), guessingPlayer);
        guessingPlayer.IncrementNumOfGuesses();

        //local players dont see their own guess items in blind mode
        if (isBlindMode && guessingPlayer.isLocalPlayer)
            return;

        UIGuessItem addedItem = numberGuessingUI.AddGuessItem(numOfGuesses, guessedValue, tooHigh);
        addedItem.SetAttemptNumberBackgroundColor(guessingPlayer.GetPlayerColor());
    }

    [ClientRpc]
    private void RpcShowWrongNumberFeedback()
    {
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));
        audioPlayer.PlayWrongSound();
    }

    [ClientRpc]
    public void RpcShowTimeIsUpFeedback()
    {
        //TODO: play alarm / troete
        //TODO: show red flash

        //for now...
        ShowGameOverFeedback();
    }

    [Client]
    private void ShowGameOverFeedback()
    {
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));
    }

    [ClientRpc]
    private void RpcShowNextRoundFeedback(string textToShow)
    {
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_GREEN));
        StartCoroutine(textFlasherUI.Flash(Global.COLOR_FLASH_GREEN, textToShow));
        audioPlayer.PlayCorrectSound();
    }

    UIInfoDialog wrongInputDialog;
    [Client]
    public void ShowWrongInputDialog()
    {
        if (null == wrongInputDialog)
        {
            wrongInputDialog = UIElement.Instantiate<UIInfoDialog>();
            wrongInputDialog.Setup(
                Global.STRING_DIALOG_WRONG_INPUT_TITLE,
                string.Format(Global.STRING_IT_DIALOG_NUMBER_ARCADE_WRONG_INPUT_DESCRIPTION, 0, int.MaxValue),
                Resources.Load<Sprite>(Global.PATH_ICON_ERROR)
                );
        }

        wrongInputDialog.Show();
    }

    [Client]
    protected override UIGameOverview InitGameOverview()
    {
        UIMPITNumberOverview gameOverviewUI = UIElement.Instantiate<UIMPITNumberOverview>(this.transform);
        return gameOverviewUI;
    }
    //////////////////////////////////////////////////////Client Logic End
}

﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;
using System.IO;

public class MPITNumberPlayer : MPGamePlayer {

    public static readonly string PREFAB_PATH = Global.PATH_PREFABS_DIR + "Network" + Path.AltDirectorySeparatorChar + "MPITNumberPlayer";

    private UIMPITNumberPlayer numberPlayerUI;
    private MPITNumberController numberGameController;

    [SyncVar(hook = "SetNumOfGuesses")]
    private int numOfGuesses;

    [SyncVar(hook = "SetNumOfCorrectGuesses")]
    private int numOfCorrectGuesses;

    [SyncVar(hook = "SetEarnedBonusTime")]
    private float earnedBonusTime;

    [SyncVar(hook = "SetReachedLevel")]
    private int reachedLevel;

    protected override void InitClientSide()
    {
        SetNumOfGuesses(numOfGuesses);
        SetNumOfCorrectGuesses(numOfCorrectGuesses);
        SetEarnedBonusTime(earnedBonusTime);
        SetReachedLevel(reachedLevel);
    }

    protected override UIMPGamePlayer InitPlayerUI()
    {
        numberPlayerUI = UIElement.Instantiate<UIMPITNumberPlayer>(this.transform);
        return numberPlayerUI;
    }

    public void SetNumOfGuesses(int num)
    {
        this.numOfGuesses = num;

        if(null!=numberPlayerUI)
            numberPlayerUI.SetNumOfGuesses(num);
    }

    public void SetNumOfCorrectGuesses(int num)
    {
        this.numOfCorrectGuesses = num;
        if (null != numberPlayerUI)
            numberPlayerUI.SetNumOfCorrectGuesses(num);
    }

    public void SetEarnedBonusTime(float bonusTime)
    {
        this.earnedBonusTime = bonusTime;
        if (null != numberPlayerUI)
            numberPlayerUI.SetBonusTime(bonusTime);
    }

    public void SetReachedLevel(int lvl)
    {
        this.reachedLevel = lvl;
        if (null != numberPlayerUI)
            numberPlayerUI.SetLevel(lvl);
    }

    public int GetNumOfGuesses()
    {
        return numOfGuesses;
    }

    public int GetNumOfCorrectGuesses()
    {
        return numOfCorrectGuesses;
    }

    public float GetEarnedBonusTime()
    {
        return earnedBonusTime;
    }

    public int GetReachedLevel()
    {
        return reachedLevel;
    }

    public void IncrementNumOfGuesses()
    {
        SetNumOfGuesses(numOfGuesses + 1);
    }

    public void IncrementNumOfCorrectGuesses()
    {
        SetNumOfCorrectGuesses(numOfCorrectGuesses + 1);
    }

    public void IncrementReachedLevel()
    {
        SetReachedLevel(reachedLevel + 1);
    }

    public void AddBonusTime(float bonusTime)
    {
        SetEarnedBonusTime(earnedBonusTime + bonusTime);
    }

    [Command]
    public void CmdGuess(string guessedValue)
    {
        if(null==numberGameController)
            numberGameController = (MPITNumberController)gameController;

        numberGameController.Guess(guessedValue, netId.Value);
    }
}

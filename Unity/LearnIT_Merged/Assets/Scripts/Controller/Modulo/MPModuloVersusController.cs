﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Reflection;
using System.Collections.Generic;
using System;

public class MPModuloVersusController : MPGameController, GridButtonListener, NumpadArrowKeyListener
{
	private enum Operator{ADD,MULTIPLY};

    private int roundsToWin = 1;

	UIModulo uiModulo;
    MPUINumberSystemsOverview gameOverview;
	MPModuloPlayer player;
	private UIScoreBar bar;
	private int level = 1;
	private int modulo = 5;
	private GridInputField currentField;
	private int currentPosition = 0;
	private GridInputField currentEnemyField;
	private Operator mode = Operator.ADD;
    private SubmittedResult[] results;
	System.Random rand = new System.Random();

	/**
	 * This method changes the existing grid to the size (modulo + 1) X (modulo + 1)
	 * and fills it with fields.
	 */
    [Client]
	private void BuildGrid()
	{
		FieldInfo fieldIsColumn = typeof(AutoGridLayout).GetField ("m_IsColumn", BindingFlags.NonPublic | BindingFlags.Instance);
		fieldIsColumn.SetValue (uiModulo.GetGrid (), true);
		FieldInfo fieldColumn = typeof(AutoGridLayout).GetField ("m_Column", BindingFlags.NonPublic | BindingFlags.Instance);
		fieldColumn.SetValue (uiModulo.GetGrid (), modulo + 1);
		uiModulo.GetGrid ().CalculateLayoutInputHorizontal ();

		GridInputField operatorField = UIElement.Instantiate<GridInputField> (uiModulo.GetGrid().transform);
		operatorField.GetInputField().interactable = false;

		switch(mode){
			
		case Operator.ADD:
			operatorField.GetInputField().text = "+";
			break;
		case Operator.MULTIPLY:
			operatorField.GetInputField().text = "*";
			break;
		}

        uiModulo.SetModuloText(modulo.ToString());

		int size = modulo + 1;

		for (int i = 1; i < size; i++) {
			GridInputField input = UIElement.Instantiate<GridInputField> (uiModulo.GetGrid ().transform);
		
			input.GetInputField().text = (i - 1).ToString ();
		}

		for (int i = size; i < size * size; i++) {
			if (i % size == 0) {
				GridInputField input = UIElement.Instantiate<GridInputField> (uiModulo.GetGrid ().transform);
				input.GetInputField().text = (i / size - 1).ToString ();
			} else {
				int x = i % size -1;
				int y = i / size - 1;
				GridInputField input = UIElement.Instantiate<GridInputField> (uiModulo.GetGrid ().transform);
				input.SetNumpad(uiModulo.GetNumpad());
				ColorBlock block = input.GetInputField ().colors; 
				block.disabledColor = new Color (1.0f, 1.0f, 1.0f);
				input.SetEnabled (true);
				input.SetListener (this);
				input.Position = (y * modulo + x);
				input.GetInputField ().text = "";
				if (x == 0 && y == 0) {
					uiModulo.setNumpadInputField (input.GetInputField ());
					input.SelectFieldListener ();
				}
			}
		}
	}

	/**
	 * Removes all elements form the grid.
	 */
    [Client]
    private void ClearGrid()
    {
        foreach (GridInputField field in uiModulo.GetGrid().GetComponentsInChildren<GridInputField>())
        {
            Destroy(field.gameObject);         

        }
    }

	protected override void InitBothSides ()
	{
		
	}

	protected override void InitServerSide ()
	{
        roundsToWin = ((GameModeModulo)lobby.GetGameModeToPlay()).GetRoundsToWin();
        modulo = rand.Next(2, 9);
        results = new SubmittedResult[modulo * modulo];
	}

	protected override void InitClientSide ()
	{
		uiModulo = UIElement.Instantiate<UIModulo> ();
		bar = UIElement.Instantiate<UIScoreBar>();
		uiModulo.setSlider (bar);
		
        uiModulo.GetHeader().SetLevelText(Global.ROUND_TEXT);
		

        uiModulo.GetSubmitButton().onClick.AddListener(OnSubmitListener);

        uiModulo.GetNumpad().AddOnSubmitAction(OnSubmitListener);
		uiModulo.GetNumpad().SetArrowKeyListener(this);

		uiModulo.AddPauseButtonAction(TriggerLeaveGame);
    }

	[Client]
	private void TriggerLeaveGame(){
		((MPModuloPlayer)GetLocalGamePlayer ()).CmdLeaveGame ();
	}

    [Client]
    protected override UIGameOverview InitGameOverview()
    {
        MPUINumberSystemsOverview gameOverviewUI = UIElement.Instantiate<MPUINumberSystemsOverview>(this.transform);
        gameOverview = gameOverviewUI;
        return gameOverviewUI;
    }

    [Server]
    public void LeaveGame()
    {
        lobby.InvokeReturnToLobby();
    }

    [ClientRpc]
	private void RpcBuildGui(Operator op, int modulo, int level)
    {
        this.modulo = modulo;
        this.mode = op;
        uiModulo.GetHeader().SetLevelCountText(level.ToString());
        ClearGrid();
        BuildGrid();
    }

	/**
	 * Sends the current cursorPosition and the current InputFields content to the server
	 */
    [Client]
    private void OnSubmitListener()
    {
        ((MPModuloPlayer)GetLocalGamePlayer()).CmdSubmitResult(currentField.GetInputField().text, currentField.Position);
    }

    [ClientRpc]
    private void RpcInitPlayer()
    {
        player = (MPModuloPlayer)GetLocalGamePlayer();
    }

	/**
	 * Sets the colors and names according to the selected names and colors in the gui.
	 */
    [ClientRpc]
    private void RpcInitNamesAndColors()
    {
        ArrayList players = new ArrayList();
        foreach (KeyValuePair<uint, MPGamePlayer> mpPlayer in gamePlayers)
        {
            players.Add(mpPlayer.Value);
        }
        player = (MPModuloPlayer)GetLocalGamePlayer();

        if (players[0] == player)
        {
            uiModulo.Slider.LocalName = ((MPModuloPlayer)players[0]).GetPlayerName();
            uiModulo.Slider.EnemyName = ((MPModuloPlayer)players[1]).GetPlayerName();
            uiModulo.Slider.PlayerLocalColor = ((MPModuloPlayer)players[0]).GetPlayerColor();
            uiModulo.Slider.PlayerEnemyColor = ((MPModuloPlayer)players[1]).GetPlayerColor();
        }
        else
        {
            uiModulo.Slider.LocalName = ((MPModuloPlayer)players[1]).GetPlayerName();
            uiModulo.Slider.EnemyName = ((MPModuloPlayer)players[0]).GetPlayerName();
            uiModulo.Slider.PlayerLocalColor = ((MPModuloPlayer)players[1]).GetPlayerColor();
            uiModulo.Slider.PlayerEnemyColor = ((MPModuloPlayer)players[0]).GetPlayerColor();
        }
    }

	/**
	 * Triggers the clients to initialize their guis.
	 */
    [Server]
	protected override void OnAllPlayersReadyToBeginMatch ()
	{
        RpcInitPlayer();
        RpcInitNamesAndColors();
        RpcBuildGui(Operator.ADD, modulo, level);
        StartMatch ();	
	}

	/**
	 * Builds the overview according to the results of the game.
	 */
    [Client]
    protected override void OnMatchFinished()
    {
        UIScoreBar scoreBar = UIElement.Instantiate<UIScoreBar>(gameOverview.SliderHolder.transform);
        scoreBar.ScoreEnemy = uiModulo.Slider.ScoreEnemy;
        scoreBar.ScoreLocal = uiModulo.Slider.ScoreLocal;
        scoreBar.EnemyName = uiModulo.Slider.EnemyName;
        scoreBar.LocalName = uiModulo.Slider.LocalName;
        scoreBar.PlayerEnemyColor = uiModulo.Slider.PlayerEnemyColor;
        scoreBar.PlayerLocalColor = uiModulo.Slider.PlayerLocalColor;
        scoreBar.SetScoreTextColor(new Color32(0, 69, 149, 255));
        gameOverview.Slider = scoreBar;
        if (scoreBar.ScoreLocal > scoreBar.ScoreEnemy)
        {
            gameOverview.WinnerText.text = Global.YOU_WIN_TEXT;
            gameOverview.WinnerText.color = (Color.green);
        }
        else
        {
            gameOverview.WinnerText.text = Global.YOU_LOSE_TEXT;
            gameOverview.WinnerText.color = (Color.red);
        }
    }


	/**
	 * Checks if the submitted result is correct.
	 * If false: Signals the clients that the submitted result was false;
	 * If true: Signals the clients that the submitted result was correct and broadcasts it to all clients.
	 */
	[Server]
	public void OnResultReceived(SubmittedResult submittedResult){

        int x = submittedResult.GetPosition() % modulo;
        int y = submittedResult.GetPosition() / modulo;
        int serverResult = 0;

        switch (mode)
        {
            case Operator.ADD:
                serverResult = (x + y) % modulo;
                break;
            case Operator.MULTIPLY:
                serverResult = (x * y) % modulo;
                break;
        }

        if(serverResult.ToString() == submittedResult.GetResult())
        {
			if (results [submittedResult.GetPosition ()] == null) {
				results [submittedResult.GetPosition ()] = submittedResult;
				RpcSendFeedBack (submittedResult.GetSubmitter (), submittedResult.GetPosition (), submittedResult.GetResult ());
                foreach(SubmittedResult result in results)
                {
                    if (result == null)
                        return;
                }
                RoundFinished();
			}
        }
        else
        {
            RpcSendFeedBack(submittedResult.GetSubmitter(), submittedResult.GetPosition(), null);
        }
    
	}

	/**
	 * Recalculates the stats and decides then if the round is over.
	 * If the round is over it checks if the match is over.
	 * If not data for a new round is generated and the clients data is updated.
	 */
    [Server]
    private void RoundFinished()
    {
        //add points for this round
        foreach (KeyValuePair<uint, MPGamePlayer> mpPlayer in gamePlayers)
		{
			((MPModuloPlayer)mpPlayer.Value).RoundScore = 0;
            foreach (SubmittedResult result in results)
            {
				if (mpPlayer.Key == result.GetSubmitter ()) {
					((MPModuloPlayer)mpPlayer.Value).RoundScore++;
				}

            }
			if (((MPModuloPlayer)mpPlayer.Value).RoundScore > (modulo * modulo / 2.0f))
			{
				((MPModuloPlayer)mpPlayer.Value).Score++;
				RpcUpdateScore(((MPModuloPlayer)mpPlayer.Value).netId.Value);
				level++;
				if (((MPModuloPlayer)mpPlayer.Value).Score >= roundsToWin) {
					FinishMatch ();
				}
				break;
			}else if(((MPModuloPlayer)mpPlayer.Value).RoundScore == (modulo * modulo / 2.0f))
			{
				break;
			}
        }
        

        mode = (Operator) rand.Next(0, Enum.GetNames(typeof(Operator)).Length);
        modulo = rand.Next(2, 9);
        results = new SubmittedResult[modulo * modulo];
        RpcBuildGui(mode, modulo, level);
        
    }

	/**
	 * Increments the score of the supplied player by 1;
	 */
    [ClientRpc]
    private void RpcUpdateScore(uint playerID)
    {
        if(GetLocalGamePlayer().netId.Value == playerID)
        {
            uiModulo.Slider.ScoreLocal++;
        }
        else
        {
            uiModulo.Slider.ScoreEnemy++;
        }
    }

	/**
	 * Flashes the screen read for the clients when a wrong result is submitted.
	 * If the right result is submitted it updates the gui of the clients with the new result and colors the inputfield with the submitters playercolor.
	 * @param submitter: the id of the player that send the result.
	 * @param position: the position in the grid that the result was submitted for (null if not correct).
	 * @param result: the result submitted (null if not correct).
	 */
    [ClientRpc]
	private void RpcSendFeedBack(uint submitter, int position, string result)
    {
		if (!string.IsNullOrEmpty(result))
        {
			foreach (GridInputField content in uiModulo.GetGrid().GetComponentsInChildren<GridInputField>())
            {
				if (content.Position == position) {
					ColorBlock colorBlock = content.GetInputField ().colors;
					colorBlock.disabledColor = gamePlayers [submitter].GetPlayerColor ();
					content.GetInputField ().colors = colorBlock;
					content.GetButton ().interactable = false;
					content.GetInputField ().text = result;
					if (content == currentField)
					{
						currentField = null;
						uiModulo.GetNumpad().SetKeyboardInputAllowed(false);
					}
				}
            }
        }
        else
        {
            if (submitter == GetLocalGamePlayer().netId.Value)
            {
                //FLASH RED
				StartCoroutine(uiModulo.GetScreenFlasher().Flash(Global.COLOR_FLASH_RED));
				foreach (GridInputField content in uiModulo.GetGrid().GetComponentsInChildren<GridInputField>())
				{
					if (content.Position == position) {
						content.GetInputField ().text = "";
					}
				}
           
            }
        }
        
    }

	/**
	 * This method updates the selected field to the given field and deselects the previous selected field.
	 */
	[Client]
	public void Listen(GridInputField field)
	{
		uiModulo.GetNumpad().SetKeyboardInputAllowed(true);
		if (currentField != null) {

			currentField.Deselect ();
            currentField.SetGlowing(false);
		}
		currentField = field;
		currentPosition = currentField.Position;
        currentField.SetGlowing(true);
		player.CmdSendSelectionChanged (currentPosition);
	}

	/**
	 * Sends update about a players current cursorposition to the clients
	 */
	[Server]
	public void SendSelectionChanged(uint sender, int position)
	{
		RpcSelectionChanged(sender, position);
	}

	/**
	 * Updates the enemy cursorposition if the sender isnt the current player. Otherwise does nothing.
	 * @param sender: The id of the player that changed his cursor position.
	 * @param position: The position the cursor was changed to.
	 */
	[ClientRpc]
	private void RpcSelectionChanged(uint sender, int position)
	{
		Debug.Log("receive selection changed! :" + position);
		if (this.player.netId.Value != sender) {
			if (currentEnemyField != null) {
				currentEnemyField.SetCursorEnemy (false);
			}
			foreach (GridInputField content in uiModulo.GetGrid().GetComponentsInChildren<GridInputField>()) {
				if (content.Position == position) {
					currentEnemyField = content;
					currentEnemyField.SetCursorEnemy (true);
					return;
				}
			}
		}
	}

	/**
	 * Moves the players cursor up. Wraps around when in the top row.
	 */
	public void UpEvent()
	{
		SelectToInputField ("UP");
	}

	/**
	 * Moves the players cursor up by 1 field. Wraps around when in the top row.
	 * @param position: the current cursor position.
	 */
	private int NextUp(int position)
	{
		return (modulo * modulo + position - modulo) % (modulo * modulo);
	}

	/**
	 * Moves the players cursor down. Wraps around when in the bottom row.
	 */
	public void DownEvent()
	{
		SelectToInputField ("DOWN");
	}

	/**
	 * Moves the players cursor down by 1 field. Wraps around when in the bottom row.
	 * @param position: the current cursor position.
	 */
	private int NextDown(int position)
	{
		return (position + modulo) % (modulo * modulo);
	}

	/**
	 * Moves the players cursor to the left by 1 field. Wraps around when in the most left column.
	 */
	public void LeftEvent()
	{
		SelectToInputField ("LEFT");
	}

	/**
	 * Moves the players cursor to the left by 1 field. Wraps around when in the most left column.
	 * @param position: the current cursor position.
	 */
	private int NextLeft(int position)
	{
		return (position - 1 + modulo) % modulo + position - position % modulo;
	}

	/**
	 * Moves the players cursor to the right by 1 field. Wraps around when in the most right column.
	 */
	public void RightEvent()
	{
		SelectToInputField ("RIGHT");
	}

	/**
	 * Moves the players cursor to the right by 1 field. Wraps around when in the most right column.
	 * @param position: the current cursor position.
	 */
	private int NextRight(int position)
	{
		return (position + 1) % modulo + position - position % modulo;
	}

	/**
	 * Moves the players cursor into the given direction by 1 field or wraps around when it cant move into that direction any further.
	 * @param direction: the direction to move at.
	 */
	private void SelectToInputField(string direction)
	{
		int position = currentPosition;
		switch (direction)
		{
		case "UP":
			position = NextUp (position);
			break;
		case "DOWN":
			position = NextDown (position);
			break;
		case "LEFT":
			position = NextLeft (position);
			break;
		case "RIGHT":
			position = NextRight (position);
			break;
		default:
			Debug.LogError ("Unknown direction: " + direction);
			break;
		}
		foreach (GridInputField content in uiModulo.GetGrid().GetComponentsInChildren<GridInputField>()) {
			if (content.Position == position) {
				DisableGlowOnField (currentPosition);
				if (currentField != null) {
					currentField.Deselect ();
				}
				if (content.GetButton ().interactable) {
					content.SelectFieldListener ();
				} else {
					content.SetGlowing (true);
				}

				currentPosition = position;
				player.CmdSendSelectionChanged (position);
				break;
			}
		}
	}

	/**
	 * Hides the cursor image on the field represented by the given position.
	 * @param position: The position of the field to remove the cursor from.
	 */
    private void DisableGlowOnField(int position)
    {
        foreach (GridInputField content in uiModulo.GetGrid().GetComponentsInChildren<GridInputField>())
        {
            if (content.Position == position)
            {
                content.SetGlowing(false);
            }
        }
    }

    protected override void OnMatchStarted()
    {
    }
}
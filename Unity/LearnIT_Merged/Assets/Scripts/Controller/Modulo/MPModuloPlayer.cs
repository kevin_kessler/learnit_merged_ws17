﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MPModuloPlayer : MPGamePlayer
{

    MPModuloVersusController controller;
    private int score = 0;
    private int roundScore = 0;

    [Client]
    protected override void InitClientSide()
    {
        controller = (MPModuloVersusController)gameController;
    }

	/**
	 * Call this method to update the cursors position on the other client.
	 * @param position: The new CursorPosition.
	 */
	[Command]
	public void CmdSendSelectionChanged(int position)
	{
		controller.SendSelectionChanged (this.netId.Value, position);
	}

	/**
	 * Sends the input for a cell to the server.
	 * @param result: the content for the cell.
	 * @param position: the cells position.
	 */
    [Command]
    public void CmdSubmitResult(string result, int position)
    {
        SubmittedResult submittedResult = new SubmittedResult(this.netId.Value, result, position);
        controller.OnResultReceived(submittedResult);
    }

	[Command]
	public void CmdLeaveGame(){
		controller.LeaveGame ();
	}

	/**
	 * The total score for this player.
	 */
    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
        }
    }

	/**
	 * This is used to keep track of the points the player scored in this level.
	 * This needs to be reseted when changing the level.
	 */
    public int RoundScore
    {
        get
        {
            return roundScore;
        }
        set
        {
            roundScore = value;
        }
    }
}

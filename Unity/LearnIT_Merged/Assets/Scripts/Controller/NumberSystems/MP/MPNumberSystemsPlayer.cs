﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MPNumberSystemsPlayer : MPGamePlayer
{
	MPNumberControllerSuper controller;
    private int score;

	[Client]
	protected override void InitClientSide ()
	{
		controller = (MPNumberControllerSuper)gameController;
        score = 0;
	}


    [Command]
	public void CmdSubmitResult(string message, uint playerID)
	{
		controller.OnResultReceived (message, playerID);
	}

	[Command]
	public void CmdLeaveGame(){
		controller.LeaveGame ();
	}

    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
        }
    }
}


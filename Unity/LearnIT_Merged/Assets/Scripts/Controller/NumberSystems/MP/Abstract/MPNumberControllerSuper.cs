﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public abstract class MPNumberControllerSuper : MPGameController
{
	protected MPUINumberSystems mpNumberSystemsUI;

	MPNumberSystemsPlayer player;

    MPUINumberSystemsOverview gameOverview;

	private float startTime;

	private NumberCreator numberCreator;

	private string leftText;
	private string rightText;
	protected int level = 1;

	private int numberSystemLeft = 2;
	private int numberSystemRight = 10;
	private float difficultyScaling = 1.12F;
	private float stretchScaling = 13.05F;
	protected string numberSystemLeftName = Global.BINARY_TEXT;
	protected string numberSystemRightName = Global.DECIMAL_TEXT;

	protected NumberCreator GetNumberCreator ()
	{
		return new NumberCreator (numberSystemLeft, numberSystemRight);
	}

	/**
	 * Calculates the number of digits based on the current level, the difficultyScaling and the strechscaling.
	 */
	[Server]
	protected int GetDigits()
	{
		return (int)(Mathf.Log(level + stretchScaling - 1)/Mathf.Log(difficultyScaling) + 1 - Mathf.Log(stretchScaling)/Mathf.Log(difficultyScaling));
	}

	protected override void InitBothSides ()
	{
        if (lobby.GetGameModeToPlay() is GameModeNumberSystem)
        {
            GameModeNumberSystem mode = (GameModeNumberSystem)lobby.GetGameModeToPlay();
            GameModeNumberSystem.NumberSystemData numberSystem = mode.GetNumberSystem();

            numberSystemLeft = numberSystem.GetNumberSystemLeft();
            numberSystemRight = numberSystem.GetNumberSystemRight();
            difficultyScaling = numberSystem.GetDifficultyScaling();
            stretchScaling = numberSystem.GetStretchScaling();
            numberSystemLeftName = numberSystem.GetNumberSystemLeftName();
            numberSystemRightName = numberSystem.GetNumberSystemRightName();
        }

		numberCreator = GetNumberCreator ();		
	}

	[Server]
	protected override void InitServerSide ()
	{
	}

	/**
	 * Method to supply a custom slider dependant on the implementation.
	 */
	protected abstract UIScoreBar CreateSlider ();

	[Client]
    protected override void InitClientSide()
    {
        //init GUI
        mpNumberSystemsUI = UIElement.Instantiate<MPUINumberSystems>();
		mpNumberSystemsUI.SetSlider (CreateSlider ());
		mpNumberSystemsUI.CreateNumPad (numberCreator);

        Button submitButton = mpNumberSystemsUI.GetSubmitButon();
        mpNumberSystemsUI.GetHeader().SetLevelCountText(level.ToString());

        submitButton.onClick.AddListener(OnSubmitListener);
        mpNumberSystemsUI.GetNumPad().AddOnSubmitAction(OnSubmitListener);

       // mpNumberSystemsUI.GetHeader().RemovePauseButton();
		mpNumberSystemsUI.SetLeftSystemName (numberSystemLeftName);
		mpNumberSystemsUI.SetRightSystemName (numberSystemRightName);
		mpNumberSystemsUI.GetHeader ().SetTitleText (CreateTitleText ());

		mpNumberSystemsUI.AddPauseButtonaction (TriggerLeaveGame);
    }

	/**
	 * This method transmits the current user-input to the server
	 */
	[Client]
	private void TriggerLeaveGame(){
		((MPNumberSystemsPlayer)GetLocalGamePlayer ()).CmdLeaveGame ();
	}

	[Server]
	public void LeaveGame()
	{
		lobby.InvokeReturnToLobby();
	}

	[Client]
	private void OnSubmitListener(){

		InputField inputDecimal = mpNumberSystemsUI.GetInputFieldRight();
		string message = inputDecimal.text;
		player.CmdSubmitResult(message, player.netId.Value);
	}
	

	[Client]
	protected override void OnMatchStarted ()
	{
        
    }

	/**
	 * Fills the overview with the results of the game.
	 */
	[Client]
	protected override void OnMatchFinished ()
	{
        //create a copy of the scoreBar and display it in the GameOverview
		UIScoreBar scoreBar = UIElement.Instantiate<UIScoreBar> (gameOverview.SliderHolder.transform);
		scoreBar.ScoreEnemy = mpNumberSystemsUI.GetSlider ().ScoreEnemy;
		scoreBar.ScoreLocal = mpNumberSystemsUI.GetSlider ().ScoreLocal;
		scoreBar.EnemyName = mpNumberSystemsUI.GetSlider ().EnemyName;
		scoreBar.LocalName = mpNumberSystemsUI.GetSlider ().LocalName;
		scoreBar.PlayerEnemyColor = mpNumberSystemsUI.GetSlider ().PlayerEnemyColor;
		scoreBar.PlayerLocalColor = mpNumberSystemsUI.GetSlider ().PlayerLocalColor;
		scoreBar.SetScoreTextColor (Global.STANDARD_BLUE);
		gameOverview.Slider = scoreBar;
        if(scoreBar.ScoreLocal > scoreBar.ScoreEnemy)
        {
            gameOverview.WinnerText.text = Global.YOU_WIN_TEXT;
            gameOverview.WinnerText.color = (Color.green);
        }
        else
        {
            gameOverview.WinnerText.text = Global.YOU_LOSE_TEXT;
            gameOverview.WinnerText.color = (Color.red);
        }
	}

	/**
	 * Checks whether the submitted result is correct.
	 * If wrong causes the screen of the submitter flash red.
	 * Else it will increment the level, create new numbers, increment the submitters score, distribute the winner and the score and check if the match is finished.
	 */
    [Server]
    public void OnResultReceived(string message, uint playerID)
    {
        int gain = 1;
        if (message == rightText)
        {
			foreach (KeyValuePair<uint, MPGamePlayer> mpPlayer in gamePlayers)
			{
				if (((MPNumberSystemsPlayer)mpPlayer.Value).netId.Value == playerID)
				{
					((MPNumberSystemsPlayer)mpPlayer.Value).Score += gain;
					level++;
					createNumber ();
					RpcDistributeWinnerAndScore (playerID, gain, leftText, level);
					CheckMatchFinished ();
					break;
				}
			}
        }
        else
        {
            RpcSendWrongAnswer(playerID);
        }
    }

	[Server]
	protected abstract void CheckMatchFinished ();

	/**
	 * Updates the displayed score, the displayed level and the number to be converted.
	 */
	[ClientRpc]
	public void	RpcDistributeWinnerAndScore(uint playerID, int gain, string binaryString, int level)
	{
		if (player.netId.Value == playerID) {
			mpNumberSystemsUI.GetSlider ().ScoreLocal += gain;
			StartCoroutine(mpNumberSystemsUI.GetScreenFlasher().Flash(Global.COLOR_FLASH_GREEN));
		
		}
		else
		{
			mpNumberSystemsUI.GetSlider ().ScoreEnemy += gain;
			StartCoroutine(mpNumberSystemsUI.GetScreenFlasher().Flash(Global.COLOR_FLASH_RED));
		}
		mpNumberSystemsUI.GetInputFieldRight ().text = "";
		mpNumberSystemsUI.GetInputFieldLeft ().text = binaryString;
		mpNumberSystemsUI.GetHeader ().SetLevelText (Global.LEVEL_TEXT);
		mpNumberSystemsUI.GetHeader ().SetLevelCountText (level.ToString ());

	}

	/**
	 * Flashes the screen red and clears the inputField.
	 */
	[ClientRpc]
	public void RpcSendWrongAnswer(uint playerID)
	{
		if (player.netId.Value == playerID)
		{
			StartCoroutine(mpNumberSystemsUI.GetScreenFlasher().Flash(Global.COLOR_FLASH_RED));
			mpNumberSystemsUI.GetInputFieldRight ().text = "";
		}
	}

	/**
	 * Generates a new number and result.
	 */
	[Server]
	protected void createNumbers()
	{
		string[] result = numberCreator.createNumbers (GetDigits());
		leftText = result [0];
		rightText = result [1];       
	}

    [Server]
    protected override void OnAllPlayersReadyToBeginMatch()
    {
        RpcInitGui();
        createNumber ();
		StartMatch ();
	}
		
	[Server]
	public void createNumber()
	{
		createNumbers ();
		RpcSendNumber (leftText);
	}

	[ClientRpc]
	public void RpcInitGui()
	{
		ArrayList players = new ArrayList ();
		foreach (KeyValuePair<uint, MPGamePlayer> mpPlayer in gamePlayers)
		{
			players.Add (mpPlayer.Value);
		}
        player = (MPNumberSystemsPlayer)GetLocalGamePlayer();

		if (players [0] == player) {
			mpNumberSystemsUI.GetSlider ().LocalName = ((MPNumberSystemsPlayer)players [0]).GetPlayerName ();
			mpNumberSystemsUI.GetSlider ().EnemyName = ((MPNumberSystemsPlayer)players [1]).GetPlayerName ();
			mpNumberSystemsUI.GetSlider ().PlayerLocalColor = ((MPNumberSystemsPlayer)players [0]).GetPlayerColor ();
			mpNumberSystemsUI.GetSlider ().PlayerEnemyColor = ((MPNumberSystemsPlayer)players [1]).GetPlayerColor ();
		} else
		{
			mpNumberSystemsUI.GetSlider ().LocalName = ((MPNumberSystemsPlayer)players [1]).GetPlayerName ();
			mpNumberSystemsUI.GetSlider ().EnemyName = ((MPNumberSystemsPlayer)players [0]).GetPlayerName ();
			mpNumberSystemsUI.GetSlider ().PlayerLocalColor = ((MPNumberSystemsPlayer)players [1]).GetPlayerColor ();
			mpNumberSystemsUI.GetSlider ().PlayerEnemyColor = ((MPNumberSystemsPlayer)players [0]).GetPlayerColor ();
		}

	}


    [ClientRpc]
	public void RpcSendNumber(string binaryNumber)
	{
		mpNumberSystemsUI.GetInputFieldLeft ().text = binaryNumber;
	}

    [Client]
    protected override UIGameOverview InitGameOverview()
    {
		MPUINumberSystemsOverview gameOverviewUI = UIElement.Instantiate<MPUINumberSystemsOverview>(this.transform);
		gameOverview = gameOverviewUI;
        return gameOverviewUI;
    }

	[Client]
	protected abstract string CreateTitleText ();
}
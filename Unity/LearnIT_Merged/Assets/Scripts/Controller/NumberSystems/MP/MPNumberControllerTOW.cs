﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class MPNumberControllerTOW : MPNumberControllerSuper
{
	private ArrayList players = null;

	[Server]
	protected override void CheckMatchFinished()
	{
		if (players == null)
		{
			players = new ArrayList ();
			foreach(KeyValuePair<uint, MPGamePlayer> pair in gamePlayers)
			{
				players.Add (pair.Value);
			}
		}

		if(Mathf.Abs(((MPNumberSystemsPlayer)players[0]).Score - ((MPNumberSystemsPlayer)players[1]).Score) >= Global.TOW_WINNING_LEAD)
		{
			FinishMatch();
		}
	}

	[Client]
	protected override string CreateTitleText()
	{
		return numberSystemLeftName + " -> " + numberSystemRightName + Global.TOW_TEXT;
	}

	[Client]
	protected override UIScoreBar CreateSlider ()
	{
		return UIElement.Instantiate<UIScoreBarRope> ();
	}

	[Client]
	public override void Update ()
	{
		base.Update ();
		((UIScoreBarRope)this.mpNumberSystemsUI.GetSlider ()).UpdateScrollBar();
	}
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class MPNumberController : MPNumberControllerSuper
{
	[Server]
	protected override void CheckMatchFinished()
	{
		foreach (KeyValuePair<uint, MPGamePlayer> pair in gamePlayers)
		{
			if (((MPNumberSystemsPlayer) (pair.Value)).Score >= Global.DUEL_MAX_SCORE)
			{
				FinishMatch();
			}
		}
	}

	[Client]
	protected override string CreateTitleText()
	{
		return numberSystemLeftName + " -> " + numberSystemRightName + Global.DUEL_TEXT;
	}

	[Client]
	protected override UIScoreBar CreateSlider ()
	{
		return UIElement.Instantiate<UIScoreBar> ();
	}
}
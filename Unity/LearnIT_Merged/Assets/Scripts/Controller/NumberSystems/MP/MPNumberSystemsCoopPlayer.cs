﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class MPNumberSystemsCoopPlayer : MPGamePlayer
{
    private MPNumberSystemsCoopController controller;

    [Command]
    public void CmdSubmitResult(string message)
    {
        controller.OnResultReceived(message, this.netId.Value);
    }

    [Client]
    protected override void InitClientSide()
    {
        controller = (MPNumberSystemsCoopController)gameController;
    }

	[Command]
	public void CmdRequestSwap()
	{
		controller.OnSwapRequest(this.netId.Value);
	}

	[Command]
	public void CmdSendInputChange (string newText)
	{
		controller.SendInputChange(this.netId.Value, newText);

	}

	[Command]
	public void CmdLeaveGame(){
		controller.LeaveGame ();
	}
}
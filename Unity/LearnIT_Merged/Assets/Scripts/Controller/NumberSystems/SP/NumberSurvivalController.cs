﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System;


public class NumberSurvivalController : NumberControllerSuper
{

	private UITimeSlider slider;
	private new UIInfoDialog helpDialog;
	protected int startTime  = Global.START_TIME;
	protected int timeBonus = Global.TIME_BONUS;


	private string GetEndText(){
		return string.Format(Global.LEVEL_REACHED_TEXT,level);
	}

    protected override UICustomSlider InitGameSlider()
	{
		slider = UIElement.Instantiate<UITimeSlider>();
		slider.Reset (startTime);
		slider.Pause ();
		return slider;
	}

	protected override void ResetSlider(){
		slider.Reset (slider.GetCurrentValue() + timeBonus + level/2);
	}

	public override void OnSliderFinished()
	{
        ShowWrongInputFeedback(Global.WIN_TEXT, GetEndText(), numberSystemsUI.GetAnswers(), Resources.Load<Sprite> (Global.PATH_ICON_WIN));
	}

	protected override void OnGameLoaded(){
		base.OnGameLoaded ();
		slider.Continue ();
	}

	protected override void UpdateLevel()
	{
		SetGameLevel ();
		ResetSlider ();
	}

	protected override void OnCorrectGuess()
	{
			level++;
			UpdateLevel ();
			slider.AddTime(timeBonus + level/2);
			ShowNextRoundFeedback (Global.LEVEL_TEXT + " " + level);

	}

	protected override void OnFalseGuess()
	{
		ShowWrongNumberFeedback ();
	}

	protected override string GetGameTitle()
	{
		return Global.GAME_TITLE_SURVIVAL;
	}

	protected override void SetGameLevel ()
	{
		numberSystemsUI.SetGameLevel (level);
	}


	protected override UIDialog GetHelpDialog()
	{
		if (null == helpDialog)
		{
			helpDialog = UIElement.Instantiate<UIInfoDialog>(transform);
			helpDialog.Setup(
				Global.STRING_HELP,
                //TODO
				"????",
				Resources.Load<Sprite>(Global.PATH_ICON_QUESTIONMARK)
			);
		}
		return helpDialog;
	}

	public override void OnRestartGame ()
	{
		base.OnRestartGame ();
		this.level = 1;
		this.UpdateLevel ();
		this.slider.Reset (startTime + timeBonus);
		base.createNumber ();
		numberSystemsUI.GetInputFieldRight ().text = "";
	}

	protected override string CreateTitleText()
	{
		return numberSystemLeftName + " -> " + numberSystemRightName + " " + Global.SURVIVAL_TEXT;
	}
}
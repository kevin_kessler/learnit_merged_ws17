﻿using UnityEngine;
using System.Collections;

public class TwosComplementArcardeController : NumberArcadeController
{
	protected override void Init()
	{
		

	}

    protected override void OnGameLoaded()
    {
        numberCreator = this.GetNumberCreator();
        numberSystemsUI = UIElement.Instantiate<UINumberSystemsNegativeWithHistory>(this.transform);
        ((UINumberSystemsNegativeWithHistory)numberSystemsUI).CreateNumPad(numberCreator);
        numberSystemsUI.GetSubmitButton().onClick.AddListener(onSubmitListener);
        createNumber();

        gameSliderUI = InitGameSlider();
        gameSliderUI.OnFinished += OnSliderFinished;

        numberSystemsUI.SetGameSlider(gameSliderUI);
        numberSystemsUI.SetTitleText(GetGameTitle());
        numberSystemsUI.GetNumPad().AddOnSubmitAction(onSubmitListener);
        numberSystemsUI.AddPauseButtonAction(PauseGame);
        numberSystemsUI.SetLeftNumberSystemName(numberSystemLeftName);
        numberSystemsUI.SetRightNumberSystemName(numberSystemRightName);
        numberSystemsUI.SetTitleText(CreateTitleText());
        UpdateLevel();

        numberSystemsUI.SetGameSlider(gameSliderUI);
        helpDialog = GetHelpDialog();
    }

    protected override NumberCreator GetNumberCreator()
	{
        NumberCreator creator = base.GetNumberCreator();
		creator.setModeToNegative (true);
		return creator;
	}
}
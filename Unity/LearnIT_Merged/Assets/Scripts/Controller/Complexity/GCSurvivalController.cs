﻿using UnityEngine;
using System.Collections;

public class GCSurvivalController : GCController {

    private UITimeSlider timeSlider;
    private UIInfoDialog winDialog;
    private UIInfoDialog loseDialog;

    protected override UICustomSlider GetGameSlider()
    {
        if (null == timeSlider)
            timeSlider = UIElement.Instantiate<UITimeSlider>();

        return timeSlider;
    }

    protected override void OnNewCurrentGraph()
    {
        timeSlider.Reset(currentGraph.GetSolveDuration());
        timeSlider.Continue();
    }

    protected override void OnWrongColor()
    {
        //nothing to do
    }

    protected override void OnGraphCorrect()
    {
        timeSlider.Pause();
    }

    protected override void OnGameSliderFinished()
    {
        if (null == loseDialog)
        {
            loseDialog = UIElement.Instantiate<UIInfoDialog>();
            loseDialog.Setup(
                Global.STRING_DIALOG_LOSE_TITLE,
                Global.STRING_CP_GC_DIALOG_TIME_UP_DESCRIPTION,
                Resources.Load<Sprite>(Global.PATH_ICON_LOSE),
                FinishGame
                );
        }
        audioPlayer.StopBgMusic();
        audioPlayer.PlayGameOverSound();
        loseDialog.Show();
    }

    protected override void OnGameWon()
    {
        if (null == winDialog)
        {
            winDialog = UIElement.Instantiate<UIInfoDialog>();
            winDialog.Setup(
                Global.STRING_DIALOG_WIN_TITLE,
                string.Format(Global.STRING_CP_GC_DIALOG_ARCADE_WIN_DESCRIPTION, GameMenuController.GetLastChosenMode().GetName()),
                Resources.Load<Sprite>(Global.PATH_ICON_WIN),
                FinishGame
                );
        }

        winDialog.Show();
    }

    public override void OnQuitGame()
    {
        timeSlider.Pause();
        base.OnQuitGame();
    }
}

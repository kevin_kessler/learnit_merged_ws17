﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using Utils;

public abstract class GCController : SPGameController {

    private GCLoader gcLoader;

    private List<GCGraph> graphs;
    protected GCGraph currentGraph;
    private Color currentColor = Color.white;
    private bool nodesAreFlickering = false; //true while the flicker animation is playing to prevent further click processing
    private bool advancingToNextGraph = false; //true while setting next graph to prevent further click processing

    private Texture2D paintBrushTexture;
    private UIGraphColoring gcUI;
    private UIImageDialog solutionDialog;
    private UIDecisionDialog helpDialog;
    private UIImageFlasher screenFlasherUI;
    private UICustomSlider gameSlider;

    protected override void Init()
    {
        gcLoader = gameObject.AddComponent<GCLoader>();
        loadingIndicator.SetShowProgressBar(true);

        gcUI = UIElement.Instantiate<UIGraphColoring>();
        gcUI.SetOnGraphImageClickListener(OnGraphImageClick);
        gcUI.SetOnColorButtonClickListener(OnColorButtonClick);
        gcUI.SetOnEraseButtonClickListener(() => { OnColorButtonClick(currentGraph.GetDefaultNodeColor()); });
        gcUI.SetOnClearButtonClickListener(OnClearButtonClick);
        gcUI.SetTitle(GameMenuController.GetLastChosenMode().GetName());
        gcUI.Hide();

        helpDialog = UIElement.Instantiate<UIDecisionDialog>();
        helpDialog.SetDialogTitle(Global.STRING_HELP);
        helpDialog.SetInfoText(Global.STRING_CP_GC_HELP_TEXT);
        helpDialog.SetInfoIcon(Resources.Load<Sprite>(Global.PATH_ICON_QUESTIONMARK));
        helpDialog.SetAcceptButtonText(Global.STRING_SHOW_SOLUTION);
        helpDialog.SetDismissButtonText(Global.STRING_CLOSE);
        helpDialog.Hide();
        helpDialog.AddOnAcceptAction(helpDialog.Hide);
        helpDialog.AddOnCloseAction(helpDialog.Hide);

        solutionDialog = UIElement.Instantiate<UIImageDialog>();
        solutionDialog.SetDialogTitle(Global.STRING_SOLUTION);
        solutionDialog.Hide();
        helpDialog.AddOnAcceptAction(solutionDialog.Show);

        gameSlider = GetGameSlider();
        gameSlider.OnFinished += OnGameSliderFinished;
        gcUI.SetGameSlider(gameSlider);
        gcUI.AddPauseButtonAction(PauseGame);
        screenFlasherUI = UIElement.Instantiate<UIImageFlasher>(this.transform);

        graphs = new List<GCGraph>();
	}

    protected abstract UICustomSlider GetGameSlider();
    protected abstract void OnNewCurrentGraph();
    protected abstract void OnGraphCorrect();
    protected abstract void OnWrongColor();
    protected abstract void OnGameSliderFinished();
    protected abstract void OnGameWon();

    protected override void OnGameFinished()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        foreach (GCGraph graph in graphs)
        {
            graph.ResetColorOfAllNodes();
        }
        audioPlayer.StopBgMusic();
    }

    void SetCursorColor(Color newColor)
    {
        if (null == paintBrushTexture)
            paintBrushTexture = Resources.Load<Texture2D>(Global.PATH_CURSOR_PAINTBRUSH);

        Texture2D newCursor = AssetUtils.CopyTexture(paintBrushTexture);
        AssetUtils.ReplaceTextureColor(newCursor, Color.white, newColor);
        Vector2 cursorSpot = new Vector2(0, newCursor.height);
        Cursor.SetCursor(newCursor, cursorSpot, CursorMode.Auto);
    }

    protected override IEnumerator LoadGame()
    {
        //load graphs of the current game mode
        GameModeCP currentGameMode = (GameModeCP)GameMenuController.GetLastChosenMode();
        List<GCGraphData> graphData = currentGameMode.GetGraphData();

        for (int i = 0; i < graphData.Count; i++)
        {
            GCGraphData data = graphData[i];
            string graphName = data.GetGraphImageName();

            if (!gcLoader.GraphIsLoaded(graphName))
            {
                StartCoroutine(gcLoader.LoadGraph(data));
                yield return null;

                while (gcLoader.IsLoading())
                {
                    float currentLoadingProgress = gcLoader.GetLoadingProgress();
                    float overallProgress = (i + currentLoadingProgress) / graphData.Count;
                    loadingIndicator.UpdateProgress(overallProgress);
                    yield return null;
                }
            }
            GCGraph loadedGraph = gcLoader.GetGraphByName(graphName);
            if (null == loadedGraph)
                continue;

            graphs.Add(loadedGraph);
        }
    }

    protected override void OnGameLoaded()
    {
        if (graphs.Count < 1)
        {
            FinishGame("Keine Graphen gefunden.");
            return;
        }

        SetCurrentGraph(graphs[0]);
        gcUI.Show();
        audioPlayer.PlayGCMusic();
    }

    private void NextGraph()
    {
        int currentIndex = graphs.IndexOf(currentGraph);
        currentIndex++;
        if (currentIndex >= graphs.Count)
        {
            OnGameWon();
            return;
        }

        SetCurrentGraph(graphs[currentIndex]);
    }

    private void SetCurrentGraph(GCGraph graph)
    {
        //TODO: maybe reset current graph / unload sprite to save memory
        currentGraph = graph;
        gcUI.SetGraph(currentGraph);
        gcUI.SetLevelProgressText(graphs.IndexOf(graph)+1, graphs.Count);

        //update help dialog / solution
        solutionDialog.SetImage(gcLoader.LoadGraphSolution(currentGraph.GetName()));

        //autoselect first color
        currentColor = graph.GetUsableColors()[0];
        SetCursorColor(currentColor);
        gcUI.SetColorButtonActive(currentColor);

        OnNewCurrentGraph();
    }

    private void OnColorButtonClick(Color clickedColor)
    {
        SetCursorColor(clickedColor);
        currentColor = clickedColor;
    }

    private void OnClearButtonClick()
    {
        currentGraph.ResetColorOfAllNodes();
        gameSlider.Reset();
    }

    private void OnGraphImageClick(PointerEventData data, RectTransform imageRect)
    {
        if (nodesAreFlickering || advancingToNextGraph)
            return;

        int xPos;
        int yPos;
        GetImageClickCoords(data, imageRect, out xPos, out yPos);

        GCNode clickedNode = currentGraph.GetNodeByPosition(xPos, yPos);
        if (null == clickedNode)
        {
            Debug.LogWarning("Could not find node at "+xPos+"/"+yPos);
            return;
        }

        //erase?
        Color defaultNodeColor = currentGraph.GetDefaultNodeColor();
        if(currentColor == defaultNodeColor)
        {
            currentGraph.ResetColorOfNode(clickedNode);
            return;
        }

        //check color of neighbors to determine whether this action is allowed
        List<GCNode> neighborsWithSameColor = new List<GCNode>();
        foreach (GCNode neighbor in clickedNode.GetNeighbors())
        {
            if (neighbor.GetColor() != currentColor)
                continue;

            //forbidden action! neighbor owns this color!
            neighborsWithSameColor.Add(neighbor);
        }

        if (neighborsWithSameColor.Count > 0)
        {
            StartCoroutine(OnWrongColorRoutine(clickedNode, currentColor, neighborsWithSameColor));
            return;
        }

        //action allowed --> colorize node and check if finished
        currentGraph.ColorizeNode(clickedNode, currentColor);
        foreach (GCNode node in currentGraph.GetNodes())
            if (node.GetColor() == defaultNodeColor)
                return;
        
        //graph completely colored
        StartCoroutine(OnGraphCorrectRoutine());
    }

    private IEnumerator OnGraphCorrectRoutine()
    {
        advancingToNextGraph = true;
        OnGraphCorrect();
        yield return StartCoroutine(ShowCorrectFeedback());
        while (nodesAreFlickering)
            yield return null;
        NextGraph();
        advancingToNextGraph = false;
    }

    private IEnumerator OnWrongColorRoutine(GCNode clickedNode, Color appliedColor, List<GCNode> neighborsWithSameColor)
    {
        yield return StartCoroutine(ShowWrongFeedback(clickedNode, appliedColor, neighborsWithSameColor));
        OnWrongColor();
    }

    

    private void GetImageClickCoords(PointerEventData data, RectTransform imageRect, out int xPos, out int yPos)
    {
        //determine clicked coords realtive to the image rect displayed on screen
        Vector2 localCursor;
        var pos1 = data.position;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(imageRect, pos1, null, out localCursor))
        {
            xPos = -1;
            yPos = -1;
            return;
        }
        Rect rectBounds = imageRect.rect;
        xPos = (int)(localCursor.x);
        yPos = (int)(localCursor.y);
        xPos += (int)rectBounds.width / 2;
        yPos += (int)rectBounds.height / 2;

        //screen image size may differ from actual image size. adapt screen image click to real image click.
        //do this by determining percentual position of click, and applying this percentage to the real image.
        float xPerc = xPos / rectBounds.width;
        float yPerc = yPos / rectBounds.height;
        xPos = Mathf.RoundToInt(currentGraph.GetImage().texture.width * xPerc);
        yPos = Mathf.RoundToInt(currentGraph.GetImage().texture.height * yPerc);
    }

    private IEnumerator ShowCorrectFeedback()
    {
        audioPlayer.PlayCorrectSound();
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_GREEN));
        yield return new WaitForSeconds(1);
    }

    private IEnumerator ShowWrongFeedback(GCNode clickedNode, Color appliedColor, List<GCNode> neighborsWithSameColor)
    {
        audioPlayer.PlayWrongSound();
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));

        
        if(neighborsWithSameColor.Count < 1)
            yield break;

        //highlight neighborsWithSameColor and temporarily show clicked node in selected color
        Color originalColor = clickedNode.GetColor();
        currentGraph.ColorizeNode(clickedNode, appliedColor);
        yield return StartCoroutine(FlickerNodes(neighborsWithSameColor, currentGraph.GetDefaultNodeColor(), appliedColor));
        currentGraph.ColorizeNode(clickedNode, originalColor);
    }

    private IEnumerator FlickerNodes(List<GCNode> nodes, Color flickerColorOne, Color flickerColorTwo)
    {
        nodesAreFlickering = true;

        //remember colors to re-apply after flickering finished
        Color[] originalColors = new Color[nodes.Count];
        for(int i=0; i<nodes.Count; i++)
        {
            originalColors[i] = nodes[i].GetColor();
        }

        //toggle between flicker colors
        for (int i = 0; i < 8; i++)
        {
            Color highlightColor = i % 2 == 0 ? flickerColorOne : flickerColorTwo;
            for (int j=0; j<nodes.Count; j++)
            {
                currentGraph.ColorizeNode(nodes[j], highlightColor);
            }
            yield return new WaitForSeconds(0.15f);
        }

        //re-apply colors
        for (int i = 0; i < nodes.Count; i++)
        {
            currentGraph.ColorizeNode(nodes[i], originalColors[i]);
        }

        nodesAreFlickering = false;
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION BEGIN //////////////////////////////////
    public override void OnRestartGame()
    {
        OnClearButtonClick();
        OnResumeGame();
    }

    public override void OnHelp()
    {
        //show help dialog
        helpDialog.Show();
    }

    public override bool HasHelpOption()
    {
        return true;
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION END //////////////////////////////////
}

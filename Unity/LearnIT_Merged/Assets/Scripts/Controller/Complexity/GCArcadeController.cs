﻿using UnityEngine;
using System.Collections;

public class GCArcadeController : GCController {


    private UICountSlider countSlider;
    private UIInfoDialog winDialog;
    private UIInfoDialog loseDialog;

    protected override UICustomSlider GetGameSlider()
    {
        if (null == countSlider)
            countSlider = UIElement.Instantiate<UICountSlider>();

        return countSlider;
    }

    protected override void OnNewCurrentGraph()
    {
        countSlider.Reset(currentGraph.GetAllowdNumberOfAttempts());
        countSlider.SetAppendingText(" / " + currentGraph.GetAllowdNumberOfAttempts());
        countSlider.UpdateSlider();
    }

    protected override void OnWrongColor()
    {
        countSlider.Decrement();
    }

    protected override void OnGraphCorrect()
    {
        //nothing to do
    }

    protected override void OnGameSliderFinished()
    {
        if (null == loseDialog)
        {
            loseDialog = UIElement.Instantiate<UIInfoDialog>();
            loseDialog.Setup(
                Global.STRING_DIALOG_LOSE_TITLE,
                Global.STRING_CP_GC_DIALOG_ARCADE_LOSE_DESCRIPTION,
                Resources.Load<Sprite>(Global.PATH_ICON_LOSE),
                FinishGame
                );
        }
        audioPlayer.StopBgMusic();
        audioPlayer.PlayGameOverSound();
        loseDialog.Show();
    }

    protected override void OnGameWon()
    {
        if (null == winDialog)
        {
            winDialog = UIElement.Instantiate<UIInfoDialog>();
            winDialog.Setup(
                Global.STRING_DIALOG_WIN_TITLE,
                string.Format(Global.STRING_CP_GC_DIALOG_ARCADE_WIN_DESCRIPTION, GameMenuController.GetLastChosenMode().GetName()),
                Resources.Load<Sprite>(Global.PATH_ICON_WIN),
                FinishGame
                );
        }

        winDialog.Show();
    }
}

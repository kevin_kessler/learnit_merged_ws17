﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.Networking;

public class MPSubnetPlayer : MPGamePlayer
{
    public static readonly string PREFAB_PATH = Global.PATH_PREFABS_DIR + "Network" + Path.AltDirectorySeparatorChar + "MPSubnetPlayer";

    private UISubnetPlayer subnetPlayerUI;
    private MPSubnetController subnetController;

    [SyncVar(hook = "SetNumOfQuestions")]
    private int numOfQuestions;

    [SyncVar(hook = "SetNumOfCorrectAnswers")]
    private int numOfCorrectAnswers;

    [SyncVar(hook = "SetUsedTimeToSolve")]
    private float usedTimeToSolve;

    [SyncVar]
    private bool receivedAllQuestions = false;

    [SyncVar]
    private bool answeredAllQuestions = false;

    private bool[] activeQuestionState = new bool[4];

    protected override void InitClientSide()
    {
        SetNumOfQuestions(numOfQuestions);
        SetNumOfCorrectAnswers(numOfCorrectAnswers);
    }

    protected override UIMPGamePlayer InitPlayerUI()
    {
        subnetPlayerUI = UIElement.Instantiate<UISubnetPlayer>(this.transform);
        return subnetPlayerUI;
    }

    public MPSubnetController GetMPSubnetController()
    {
        if (subnetController == null)
            subnetController = (MPSubnetController)gameController;

        return subnetController;
    }

    public void SetNumOfQuestions(int numOfQuestions)
    {
        this.numOfQuestions = numOfQuestions;

        if (null != subnetPlayerUI)
            subnetPlayerUI.SetTotalNumberOfQuestions(numOfQuestions);
    }

    public void SetNumOfCorrectAnswers(int numOfCorrectAnswers)
    {
        this.numOfCorrectAnswers = numOfCorrectAnswers;

        if (null != subnetPlayerUI)
            subnetPlayerUI.SetNumberOfCorrectAnswers(numOfCorrectAnswers);
    }

    public void SetUsedTimeToSolve(float usedTimeToSolve)
    {
        this.usedTimeToSolve = usedTimeToSolve;
        if (null != subnetPlayerUI)
            subnetPlayerUI.SetUsedTimeToSolveText(usedTimeToSolve);
    }

    public void IncreaseUsedTimeToSolve(float timeToAdd)
    {
        SetUsedTimeToSolve(usedTimeToSolve + timeToAdd);
    }

    public int GetNumOfQuestions()
    {
        return numOfQuestions;
    }

    public int GetNumOfCorrectAnswers()
    {
        return numOfCorrectAnswers;
    }

    public bool ReceivedAllQuestions()
    {
        return receivedAllQuestions;
    }

    public bool AnsweredAllQuestions()
    {
        return answeredAllQuestions;
    }

    public void IncrementNumOfCorrectAnswers()
    {
        SetNumOfCorrectAnswers(numOfCorrectAnswers + 1);
    }

    public void QuestionPartValid(int part)
    {
        activeQuestionState[part] = true;
    }

    public bool QuestionIsAnswered()
    {
        foreach (bool b in activeQuestionState)
        {
            if (!b)
            {
                return false;
            }
        }

        activeQuestionState = new bool[4];
        return true;
    }

    [Command]
    public void CmdReceivedAllQuestions()
    {
        receivedAllQuestions = true;
        GetMPSubnetController().OnClientReceivedAllQuestions(this);
    }

    [Command]
    public void CmdAnsweredAllQuestions()
    {
        answeredAllQuestions = true;
        GetMPSubnetController().OnClientAnsweredAllQuestions(this);
    }

    [Command]
    public void CmdClicked(int value, int part, int sListID, int questionIndex, float sliderValue)
    {
        GetMPSubnetController().OnIPPartClicked(value, part, sListID, questionIndex, sliderValue, this);
    }

    [Command]
    public void CmdEndTurn(int questionIndex)
    {
        GetMPSubnetController().TurnEndPVP(questionIndex, false, 0, this);
    }
}
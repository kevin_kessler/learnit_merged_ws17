﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class SubnetController : SPGameController
{
    private const int WAIT_AFTER_TURN = 5;

    private Subnet subnet;

    private UISubnet gameUI;
    private UISubnetOverview subnetOverviewUI;
    private UISubnetPlayer singlePlayerUI;
    private UICustomSlider slider;
    private UIInfoDialog helpDialog;

    private GameModeSubnet.TurnMode turnMode;

    private int turnTime = 60;
    private int addValue;
    private int removeValue;
    private float timeToSolve;

    private int triesPerLevel = 10;

    protected override void Init()
    {
        turnMode = ((GameModeSubnet)GameMenuController.GetLastChosenMode()).GetActiveTurnMode();
        gameUI = UIElement.Instantiate<UISubnet>(transform);
        gameUI.Hide();

        switch (turnMode)
        {
            case GameModeSubnet.TurnMode.TIME:
                slider = UIElement.Instantiate<UITimeSlider>();
                ((UITimeSlider) slider).Pause();
                slider.Hide();
                break;

            case GameModeSubnet.TurnMode.TRIES:
                slider = UIElement.Instantiate<UICountSlider>();
                slider.Hide();
                break;

            default:
                break;
        }

        slider.OnFinished += OnTurnEnd;
    }

    protected override IEnumerator LoadGame()
    {
        subnet = ((GameModeSubnet)GameMenuController.GetLastChosenMode()).GetSubnet();
        yield break;
    }

    protected override void OnGameLoaded()
    {
        gameUI.Setup(slider, subnet.GetActiveQuestion(), subnet.GetQuestionCount(), OnIPPartClicked);
        gameUI.SetTitleText(GameMenuController.GetLastChosenMode().GetName());
        gameUI.AddPauseButtonAction(PauseGame);
        gameUI.SetAllScrollListsInteractable();

        subnetOverviewUI = UIElement.Instantiate<UISubnetOverview>(transform);
        singlePlayerUI = UIElement.Instantiate<UISubnetPlayer>();
        singlePlayerUI.SetPlayerName(GameManager.GetInstance().GetSettingsController().GetPlayerInfo().GetPlayerName());
        singlePlayerUI.SetTotalNumberOfQuestions(subnet.GetQuestionCount());
        subnetOverviewUI.AddPlayerUI(singlePlayerUI);
        subnetOverviewUI.UseAsSinglePlayerOverview();
        subnetOverviewUI.SetBackButtonAction(FinishGame);
        subnetOverviewUI.InitQuestionResults(subnet.GetAllQuestions());
        subnetOverviewUI.Hide();

        gameUI.Show();

        switch (turnMode)
        {
            case GameModeSubnet.TurnMode.TIME:
                turnTime = turnTime / (1 + (int)((GameModeSubnet)GameMenuController.GetLastChosenMode()).GetActiveDifficulty());
                addValue = turnTime / 20;
                removeValue = turnTime / 10;
                slider.Reset(turnTime);
                slider.Show();
                ((UITimeSlider) slider).Continue();
                break;
            case GameModeSubnet.TurnMode.TRIES:
                triesPerLevel -= 2 * (int) ((GameModeSubnet)GameMenuController.GetLastChosenMode()).GetActiveDifficulty();
                addValue = 1;
                removeValue = 1;
                ((UICountSlider)slider).SetAppendingText(" / " + triesPerLevel);
                slider.Reset(triesPerLevel);
                slider.Show();
                break;
            default:
                break;
        }

    }

    private void OnTurnEnd()
    {

        if (subnet.GetActiveQuestion().GetAnsweredState() == SubnetQuestion.AnswerState.NOT_ANSWERED)
        {
            subnet.GetActiveQuestion().WrongAnswered();
            gameUI.UpdateProgressMarker(subnet.ActiveQuestionIndex(), false);
        }

        subnetOverviewUI.UpdateQuestionResult(subnet.ActiveQuestionIndex(), subnet.GetActiveQuestion().GetAnsweredState());

        if (turnMode == GameModeSubnet.TurnMode.TIME)
        {
            timeToSolve += turnTime - slider.GetCurrentValue();
        }

        gameUI.PauseScrollLists();
        Debug.Log("Turn end!");
        if (subnet.ActiveQuestionIndex() + 1 == subnet.GetQuestionCount())
        {
            OnSubnetGameFinished();
            return;
        }

        switch (turnMode)
        {
            case GameModeSubnet.TurnMode.TIME:
                StartCoroutine(OnTurnEndTime());
                break;
            case GameModeSubnet.TurnMode.TRIES:
                StartCoroutine(OnTurnEndTries());
                break;
            default:
                break;
        }
    }

    private IEnumerator OnTurnEndTime()
    {
        ((UITimeSlider)slider).Pause();

        yield return StartCoroutine(gameUI.EndOfTurnFlash(subnet.GetActiveQuestion().GetCorrectIPAddress().ToString(), subnet.GetActiveQuestion().GetAnsweredState()));

        subnet.NextQuestion();
        ((UITimeSlider)slider).Reset(turnTime - (subnet.ActiveQuestionIndex()) * 5);
        gameUI.UpdateQuestion(subnet.ActiveQuestionIndex(), subnet.GetActiveQuestion(), OnIPPartClicked);
        ((UITimeSlider)slider).Continue();
        gameUI.SetAllScrollListsInteractable();
    }

    private IEnumerator OnTurnEndTries()
    {

        yield return StartCoroutine(gameUI.EndOfTurnFlash(subnet.GetActiveQuestion().GetCorrectIPAddress().ToString(), subnet.GetActiveQuestion().GetAnsweredState()));

        subnet.NextQuestion();
        triesPerLevel -= 1;
        ((UICountSlider)slider).SetAppendingText(" / " + triesPerLevel);
        slider.Reset(triesPerLevel);
        gameUI.UpdateQuestion(subnet.ActiveQuestionIndex(), subnet.GetActiveQuestion(), OnIPPartClicked);
        gameUI.SetAllScrollListsInteractable();
    }

    private void OnSubnetGameFinished()
    {
        if (turnMode == GameModeSubnet.TurnMode.TIME)
        {
            ((UITimeSlider)slider).Pause();
            singlePlayerUI.SetUsedTimeToSolveText(timeToSolve);
        }

        subnetOverviewUI.Show();
    }

    private void OnIPPartClicked(int value, int part, int sListID)
    {
        if (subnet.IsValidInput(value, part))
        {
            //Debug.Log("Is valid.");
            gameUI.GetScrollList(sListID).InputIsValid(value);

            if (subnet.GetActiveQuestion().GetAnsweredState() == SubnetQuestion.AnswerState.CORRECT_ANSWERED)
            {
                gameUI.UpdateProgressMarker(subnet.ActiveQuestionIndex(), true);
                OnTurnEnd();
                return;
            }

            switch (turnMode)
            {
                case GameModeSubnet.TurnMode.TIME:
                    gameUI.PositiveFlash(addValue.ToString(), turnMode);
                    ((UITimeSlider)slider).AddTime(addValue);
                    break;
                case GameModeSubnet.TurnMode.TRIES:
                    if (((UICountSlider)slider).GetCurrentValue() < triesPerLevel)
                    {
                        gameUI.PositiveFlash(addValue.ToString(), turnMode);
                        ((UICountSlider)slider).Increment();
                    }
                    break;
                default:
                    break;
            }

        }

        else
        {
            //Debug.Log("Is not valid.");
            gameUI.GetScrollList(sListID).InputIsNotValid(value);
            gameUI.NegativFlash(removeValue.ToString(), turnMode);
            switch (turnMode)
            {
                case GameModeSubnet.TurnMode.TIME:
                    ((UITimeSlider)slider).AddTime(-(removeValue));
                    break;
                case GameModeSubnet.TurnMode.TRIES:
                    ((UICountSlider)slider).Decrement();
                    break;
                default:
                    break;
            }

        }
    }

    protected UIDialog GetHelpDialog()
    {
        if (null == helpDialog)
        {
            helpDialog = UIElement.Instantiate<UIInfoDialog>(transform);
            helpDialog.Setup(
                Global.STRING_HELP,
                Global.STRING_SUBNET_HELP_TEXT,
                Resources.Load<Sprite>(Global.PATH_ICON_QUESTIONMARK)
            );
        }
        return helpDialog;
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION BEGIN //////////////////////////////////

    public override void OnHelp()
    {
        GetHelpDialog().Show();
    }

    public override bool HasHelpOption()
    {
        return true;
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION END //////////////////////////////////

    protected override void OnGameFinished()
    {
        subnet.Reset();
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using Utils;
using System.Collections.Generic;

[RequireComponent(typeof(NetworkTransmitter))]
public class MPSubnetController : MPGameController
{
    //for question transmission
    private NetworkTransmitter networkTransmitter;
    private int totalReceivedQuestionBytes = 0;
    private int totalExpectedQuestionBytes = 0;

    //Game Specific Logic
    private SubnetQuestion[] serverQuestions;
    private SubnetQuestion[] clientQuestions;
    private int clientQuestionIndex;
    private MPSubnetPlayer localSubnetPlayer;
    private List<uint> activeScrollLists = new List<uint>();

    private Subnet subnet;

    private UIMPSubnet gameUI;
    private UISubnetOverview subnetOverviewUI;
    private UITimeSlider slider;

    private GameModeSubnet gameMode;
    private GameModeSubnet.PlayMode playMode;

    private int turnTime = 60;
    private int addValue;
    private int removeValue;
    private int timeToSolve;

    //////////////////////////////////////////////// Shared Logic Begin

    protected override void InitBothSides()
    {
        int reliableSequencedChannel = -1;
        for (int i = 0; i < lobby.channels.Count; i++)
            if (lobby.channels[i] == QosType.ReliableSequenced)
                reliableSequencedChannel = i;

        if (reliableSequencedChannel != LobbyController.CHANNEL_RELIABLE_SEQUENCED)
        {
            Debug.LogError(LOG_PREFIX + "Can not send SubnetQuestionData to client. Qos Channel 'ReliableSequenced' not found at LobbyController.CHANNEL_RELIABLE_SEQUENCED="
                + LobbyController.CHANNEL_RELIABLE_SEQUENCED + "... reliableSequencedChannel=" + reliableSequencedChannel);
        }

        networkTransmitter = GetComponent<NetworkTransmitter>();

        gameMode = ((GameModeSubnet)lobby.GetGameModeToPlay());
        playMode = gameMode.GetActivePlayMode();

        activeScrollLists.Add(0);
        activeScrollLists.Add(0);
        activeScrollLists.Add(0);
        activeScrollLists.Add(0);

        turnTime = turnTime / (1 + (int)gameMode.GetActiveDifficulty());
        addValue = turnTime / (10 * (1 + (int)gameMode.GetActiveDifficulty()));
        removeValue = turnTime / (10 / (1 + (int)gameMode.GetActiveDifficulty()));

        slider = UIElement.Instantiate<UITimeSlider>();
        turnTime = turnTime / (1 + (int)gameMode.GetActiveDifficulty());
        addValue = turnTime / (10 * (1 + (int)gameMode.GetActiveDifficulty()));
        removeValue = turnTime / (10 / (1 + (int)gameMode.GetActiveDifficulty()));

        slider.Reset(turnTime);
        slider.Pause();
        slider.Hide();
        if (playMode == GameModeSubnet.PlayMode.PVP)
        {
            slider.OnFinished += TryEndTurn;
        }

    }

    protected override void InitServerSide()
    {
        if (null == networkTransmitter)
        {
            Debug.LogError(LOG_PREFIX + "Did not find NetworkTransmitter. Can not transmit QuestionData. Returning to lobby.");
            lobby.InvokeReturnToLobby();
            return;
        }

        if (playMode == GameModeSubnet.PlayMode.COOP)
        {
            slider.OnFinished += TurnEnd;
        }
    }

    protected override void InitClientSide()
    {
        gameUI = UIElement.Instantiate<UIMPSubnet>(transform);
        gameUI.Hide();

        loadingGameIndicator.SetShowProgressBar(true);
        loadingGameIndicator.SetIndicationText(Global.STRING_INDICATE_LOADING_SUBNET);

        networkTransmitter.OnDataCompletelyReceived += OnReceiveQuestion;
        networkTransmitter.OnDataFragmentReceived += OnReceiveQuestionFragment;
    }

    public MPSubnetPlayer GetLocalSubnetPlayer()
    {
        if (localSubnetPlayer == null)
            localSubnetPlayer = (MPSubnetPlayer)GetLocalGamePlayer();
        return localSubnetPlayer;
    }

    //////////////////////////////////////////////// Shared Logic End

    //////////////////////////////////////////////// Server Logic Begin

    [Server]
    protected override void OnAllPlayersReadyToBeginMatch()
    {
        Debug.Log(LOG_PREFIX + "OnServerAllPlayersReadyToBeginMatch");

        //players are ready. generate subnet and send questionts to the clients
        GameModeSubnet subnetMode = lobby.GetGameModeToPlay() is GameModeSubnet ? (GameModeSubnet)lobby.GetGameModeToPlay() : null;
        if (subnetMode != null)
            subnet = subnetMode.GetSubnet();
        if (subnet != null)
            serverQuestions = subnet.GetAllQuestions();
        if (serverQuestions == null || serverQuestions.Length < 1)
        {
            Debug.LogWarning(LOG_PREFIX + "Could not load Subnet Data. Returning to lobby.");
            UIToastFlasher.CreateAndDisplayToast(this, Color.white, Global.COLOR_FLASH_RED, "Subnet konnte nicht geladen werden.");
            lobby.InvokeReturnToLobby();
            return;
        }

        if (playMode == GameModeSubnet.PlayMode.COOP)
        {
            int i = 0;
            foreach (uint key in gamePlayers.Keys)
            {
                activeScrollLists[i * 2] = key;
                activeScrollLists[i * 2 + 1] = key;
                i++;
            }

            ShuffleActiveScrollLists();
        }
        StartCoroutine(DistributeSubnetQuestions());
    }

    [Server]
    private IEnumerator DistributeSubnetQuestions()
    {
        Debug.Log(LOG_PREFIX + "Preparing data of SubnetQuestions to distribute to clients ...");

        byte[][] questionDataToSend = new byte[serverQuestions.Length][];
        int[] sizes = new int[serverQuestions.Length];
        for (int i = 0; i < serverQuestions.Length; i++)
        {
            questionDataToSend[i] = serverQuestions[i].SerializeToByteArray();
            sizes[i] = questionDataToSend[i].Length;
        }

        //tell clients how many questions they have to expect from server, and how big each question is
        RpcPrepareToReceiveSubnetQuestions(sizes);
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            ((MPSubnetPlayer)gp).SetNumOfQuestions(serverQuestions.Length);
        }

        Debug.Log(LOG_PREFIX + "Starting to distribute data of SubnetQuestions to clients ...");

        yield return null;

        //share generated questions with all clients
        for (int i = 0; i < questionDataToSend.Length; i++)
        {
            yield return StartCoroutine(networkTransmitter.SendBytesToClientsRoutine(i, questionDataToSend[i]));
        }
    }

    [Server]
    public void OnClientReceivedAllQuestions(MPSubnetPlayer subnetPlayer)
    {
        Debug.Log(LOG_PREFIX + "OnClientReceivedAllQuestions - " + subnetPlayer);

        //check if all clients received all questions
        bool allReceivedEverything = true;
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            MPSubnetPlayer sp = (MPSubnetPlayer)gp;
            if (!sp.ReceivedAllQuestions())
            {
                allReceivedEverything = false;
                break;
            }
        }

        if (allReceivedEverything)
            OnAllClientsReceivedAllQuestions();
    }

    [Server]
    public void OnAllClientsReceivedAllQuestions()
    {
        Debug.Log(LOG_PREFIX + "OnClientReceivedAllQuestions");

        StartMatch();
    }

    [Server]
    public void OnClientAnsweredAllQuestions(MPSubnetPlayer subnetPlayer)
    {
        Debug.Log(LOG_PREFIX + "OnClientAnsweredAllQuestions - " + subnetPlayer);

        //check if all clients answered all questions
        bool allAnsweredEverything = true;
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            MPSubnetPlayer sp = (MPSubnetPlayer)gp;
            if (!sp.AnsweredAllQuestions())
            {
                allAnsweredEverything = false;
                break;
            }
        }

        if (allAnsweredEverything)
            OnAllClientsAnsweredAllQuestions();
    }

    [Server]
    public void OnAllClientsAnsweredAllQuestions()
    {
        Debug.Log(LOG_PREFIX + "OnAllClientsAnsweredAllQuestions");

        subnet.Reset();

        FinishMatch();
    }

    [Server]
    private void ShuffleActiveScrollLists()
    {
        activeScrollLists.Shuffle();

        RpcRecievePlayerIDs(activeScrollLists[0], activeScrollLists[1], activeScrollLists[2], activeScrollLists[3]);
    }

    [Server]
    private void TurnEnd()
    {
        TurnEndCOOP(subnet.ActiveQuestionIndex(), false, turnTime);
    }

    [Server]
    public void TurnEndPVP(int questionIndex, bool correct, float time, MPGamePlayer player)
    {
        ((MPSubnetPlayer)player).IncreaseUsedTimeToSolve(turnTime - time);

        if (correct)
        {
            RpcOnCorrectAnswer(questionIndex, player.netId.Value);
        }

        else
        {
            RpcOnWrongAnswer(questionIndex, player.netId.Value);
        }

        Debug.Log("Turn end!");

        if (((MPSubnetPlayer)player).AnsweredAllQuestions())
        {
            return;
        }

        RpcTurnEndPVP(questionIndex, correct, player.netId.Value);

    }

    [Server]
    public void TurnEndCOOP(int questionIndex, bool correct, float time)
    {
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            ((MPSubnetPlayer)gp).IncreaseUsedTimeToSolve(turnTime - time);
        }

        if (correct)
        {
            RpcOnCorrectAnswerCOOP(questionIndex);
        }

        else
        {
            RpcOnWrongAnswerCOOP(questionIndex);
        }

        Debug.Log("Turn end!");

        if (subnet.ActiveQuestionIndex() >= subnet.GetQuestionCount() - 1)
        {
            RpcOnSubnetCOOPFinished();
            return;
        }

        ShuffleActiveScrollLists();
        subnet.NextQuestion();
        turnTime -= subnet.ActiveQuestionIndex() * 2;
        slider.Reset(turnTime);
        slider.Continue();
        RpcTurnEndCOOP(questionIndex, turnTime, correct);

    }

    [Server]
    public void OnIPPartClicked(int value, int part, int sListID, int questionIndex, float time, MPGamePlayer clickingPlayer)
    {
        if (playMode == GameModeSubnet.PlayMode.COOP && activeScrollLists[sListID] != clickingPlayer.netId.Value)
        {
            return;
        }

        switch (playMode)
        {
            case GameModeSubnet.PlayMode.SINGLEPLAYER:
                break;
            case GameModeSubnet.PlayMode.PVP:
                if (serverQuestions[questionIndex].IsValidInput(value, part))
                {
                    RpcInputValidPVP(value, part, sListID, clickingPlayer.netId.Value);
                    ((MPSubnetPlayer)clickingPlayer).QuestionPartValid(part);
                    if (((MPSubnetPlayer)clickingPlayer).QuestionIsAnswered())
                    {
                        TurnEndPVP(questionIndex, true, time, clickingPlayer);
                        return;
                    }
                }

                else
                {
                    RpcInputNotValidPVP(value, sListID, clickingPlayer.netId.Value);
                }
                break;

            case GameModeSubnet.PlayMode.COOP:
                if (subnet.IsValidInput(value, part))
                {
                    RpcInputValidCOOP(value, part, sListID);
                    if (subnet.GetActiveQuestion().GetAnsweredState() == SubnetQuestion.AnswerState.CORRECT_ANSWERED)
                    {
                        TurnEndCOOP(subnet.ActiveQuestionIndex(), true, slider.GetCurrentValue());
                        return;
                    }

                }

                else
                {
                    RpcInputNotValidCOOP(value, part, sListID);
                }

                    break;
            default:
                break;
        }
    }

    //////////////////////////////////////////////// Server Logic End

    //////////////////////////////////////////////// Client Logic Begin


    [ClientRpc]
    private void RpcPrepareToReceiveSubnetQuestions(int[] questionSizes)
    {
        foreach (int size in questionSizes)
            totalExpectedQuestionBytes += size;

        clientQuestions = new SubnetQuestion[questionSizes.Length];

        Debug.Log(LOG_PREFIX + "RpcInitSubnetQuestions size=" + questionSizes.Length + "totalsize=" + totalExpectedQuestionBytes);
    }

    [Client]
    private void TryToClick(int value, int part, int sListID)
    {
        GetLocalSubnetPlayer().CmdClicked(value, part, sListID, clientQuestionIndex, slider.GetCurrentValue());
    }

    [Client]
    private void TryEndTurn()
    {
        GetLocalSubnetPlayer().CmdEndTurn(clientQuestionIndex);
    }

    [Client]
    private void OnReceiveQuestion(int questionIndex, byte[] questionBytes)
    {
        //already received this question?
        if (null != clientQuestions[questionIndex])
            return;

        Debug.Log(LOG_PREFIX + "Completely Received Question at index=" + questionIndex);
        clientQuestions[questionIndex] = ObjectSerializationExtension.Deserialize<SubnetQuestion>(questionBytes);

        //received first question? --> setup ui
        if (questionIndex == 0)
            switch (playMode)
            {
                case GameModeSubnet.PlayMode.SINGLEPLAYER:
                    break;
                case GameModeSubnet.PlayMode.PVP:
                    SetupSubnetPVPUI();
                    break;
                case GameModeSubnet.PlayMode.COOP:
                    SetupSubnetCOOPUI();
                    break;
                default:
                    break;
            }

        //check if we received all questions
        bool receivedAll = true;
        for (int i = 0; i < clientQuestions.Length; i++)
        {
            if (null == clientQuestions[i])
            {
                receivedAll = false;
                break;
            }
        }

        //notify server, when this client received all questions -> tell him we are ready to play
        if (receivedAll)
        {
            subnetOverviewUI.InitQuestionResults(clientQuestions);

            if (playMode == GameModeSubnet.PlayMode.COOP)
            {
                uint tmp = 0;
                foreach (uint ui in gamePlayers.Keys)
                {
                    if (ui != GetLocalGamePlayer().netId.Value)
                    {
                        tmp = ui;
                    }
                }

                subnetOverviewUI.RemovePlayerUI(gamePlayers[tmp].GetUI());
            }

            loadingGameIndicator.SetIndicationText(Global.STRING_INDICATE_WAITING_FOR_PLAYERS);
            loadingGameIndicator.SetShowProgressBar(false);
            GetLocalSubnetPlayer().CmdReceivedAllQuestions();
        }
    }

    [Client]
    private void OnReceiveQuestionFragment(int questionIndex, byte[] questionFragment)
    {
        UpdateReceivingProgress(questionFragment.Length);
    }

    [Client]
    private void UpdateReceivingProgress(int receivedBytes)
    {
        totalReceivedQuestionBytes += receivedBytes;
        loadingGameIndicator.UpdateProgress(totalReceivedQuestionBytes / (float)totalExpectedQuestionBytes);
    }

    [Client]
    private void SetupSubnetPVPUI()
    {
        gameUI.Setup(slider, clientQuestions[0], clientQuestions.Length, TryToClick);
        gameUI.SetTitleText(((GameModeSubnet)lobby.GetGameModeToPlay()).GetName());
        gameUI.SetAllScrollListsInteractable();
        slider.Show();

        //the MPSubnetUI currently supports only 2 players.
        //If there are more than one remote player, their visuals will be overriden
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            if (gp.isLocalPlayer)
                gameUI.SetupLocalPlayerVisuals(gp);
            else
                gameUI.SetupRemotePlayerVisuals(gp);
        }
    }

    [Client]
    private void SetupSubnetCOOPUI()
    {
        gameUI.Setup(slider, clientQuestions[0], clientQuestions.Length, TryToClick);
        gameUI.SetTitleText(((GameModeSubnet)lobby.GetGameModeToPlay()).GetName());
        UpdateScrollListColor();

        slider.Show();

        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            if (gp.isLocalPlayer)
                gameUI.SetupLocalPlayerVisuals(gp);
            else
                gameUI.SetupRemotePlayerVisuals(gp);
        }
    }

    [ClientRpc]
    private void RpcRecievePlayerIDs(uint list1, uint list2, uint list3, uint list4)
    {
        activeScrollLists[0] = list1;
        activeScrollLists[1] = list2;
        activeScrollLists[2] = list3;
        activeScrollLists[3] = list4;
    }

    [Client]
    private void UpdateScrollListColor()
    {
        gameUI.SetAllScrollListsNonInteractable();
        for (int i = 0; i < activeScrollLists.Count; i++)
        {
            gameUI.SetScrollListColor(i, GetGamePlayer(activeScrollLists[i]).GetPlayerColor());
            if (activeScrollLists[i] == GetLocalGamePlayer().netId.Value)
            {
                gameUI.SetScrollListInteractable(i);
            }
        }
    }

    [ClientRpc]
    private void RpcSyncTime(float time)
    {
        slider.Reset(time);
    }

    [ClientRpc]
    private void RpcOnCorrectAnswer(int questionIndex, uint answeringPlayerId)
    {
        StartCoroutine(OnCorrectAnswer(questionIndex, answeringPlayerId));
    }

    [ClientRpc]
    private void RpcOnWrongAnswer(int questionIndex, uint answeringPlayerId)
    {
        StartCoroutine(OnWrongAnswer(questionIndex, answeringPlayerId));
    }

    [ClientRpc]
    private void RpcOnCorrectAnswerCOOP(int questionIndex)
    {
        StartCoroutine(OnCorrectAnswerCOOP(questionIndex));
    }

    [ClientRpc]
    private void RpcOnWrongAnswerCOOP(int questionIndex)
    {
        StartCoroutine(OnWrongAnswerCOOP(questionIndex));
    }

    [Client]
    private IEnumerator OnCorrectAnswer(int questionIndex, uint answeringPlayerId)
    {
        MPGamePlayer answeringPlayer = GetGamePlayer(answeringPlayerId);

        //remote player answered a question? only update result, don't intervene our game
        if (!answeringPlayer.isLocalPlayer)
        {
            gameUI.UpdateOpponentProgressMarker(questionIndex, true);
            subnetOverviewUI.UpdateQuestionResult(questionIndex, SubnetQuestion.AnswerState.CORRECT_ANSWERED, (UISubnetPlayer)answeringPlayer.GetUI());
            yield break;
        }

        //show and wait for local feedback, then advance to next question
        slider.Pause();
        gameUI.UpdateProgressMarker(questionIndex, true);
        subnetOverviewUI.UpdateQuestionResult(questionIndex, SubnetQuestion.AnswerState.CORRECT_ANSWERED, (UISubnetPlayer)answeringPlayer.GetUI());
        yield return StartCoroutine(ShowCorrectFeedback());
    }

    [Client]
    private IEnumerator OnWrongAnswer(int questionIndex, uint answeringPlayerId)
    {
        MPGamePlayer answeringPlayer = GetGamePlayer(answeringPlayerId);

        //remote player answered a question? only update result, don't intervene our game
        if (!answeringPlayer.isLocalPlayer)
        {
            gameUI.UpdateOpponentProgressMarker(questionIndex, false);
            subnetOverviewUI.UpdateQuestionResult(questionIndex, SubnetQuestion.AnswerState.WRONG_ANSWERED, (UISubnetPlayer)answeringPlayer.GetUI());
            yield break;
        }

        //show and wait for local feedback, then advance to next question
        slider.Pause();
        gameUI.UpdateProgressMarker(questionIndex, false);
        subnetOverviewUI.UpdateQuestionResult(questionIndex, SubnetQuestion.AnswerState.WRONG_ANSWERED, (UISubnetPlayer)answeringPlayer.GetUI());
        yield return StartCoroutine(ShowWrongFeedback());
    }

    [Client]
    private IEnumerator ShowCorrectFeedback()
    {
        audioPlayer.PlayCorrectSound();
        yield return null;
    }

    [Client]
    private IEnumerator ShowWrongFeedback()
    {
        audioPlayer.PlayWrongSound();
        yield return null;
    }

    [Client]
    private IEnumerator OnCorrectAnswerCOOP(int questionIndex)
    {
        slider.Pause();
        gameUI.UpdateProgressMarker(questionIndex, true);
        gameUI.UpdateOpponentProgressMarker(questionIndex, true);
        subnetOverviewUI.UpdateQuestionResult(questionIndex, SubnetQuestion.AnswerState.CORRECT_ANSWERED);
        yield return StartCoroutine(ShowCorrectFeedback());
    }

    [Client]
    private IEnumerator OnWrongAnswerCOOP(int questionIndex)
    {
        slider.Pause();
        gameUI.UpdateProgressMarker(questionIndex, false);
        gameUI.UpdateOpponentProgressMarker(questionIndex, false);
        subnetOverviewUI.UpdateQuestionResult(questionIndex, SubnetQuestion.AnswerState.WRONG_ANSWERED);
        yield return StartCoroutine(ShowWrongFeedback());
    }

    [ClientRpc]
    private void RpcTurnEndPVP(int questionIndex, bool answerState, uint answeringPlayerId)
    {
        StartCoroutine(OnTurnEndPVP(questionIndex, answerState, answeringPlayerId));
    }

    [Client]
    private IEnumerator OnTurnEndPVP(int questionIndex, bool answerState, uint answeringPlayerId)
    {
        MPGamePlayer answeringPlayer = GetGamePlayer(answeringPlayerId);
        SubnetQuestion.AnswerState state = SubnetQuestion.AnswerState.WRONG_ANSWERED;

        if (answerState)
            state = SubnetQuestion.AnswerState.CORRECT_ANSWERED;

        if (!answeringPlayer.isLocalPlayer)
        {
            yield break;
        }

        gameUI.PauseScrollLists();

        yield return StartCoroutine(gameUI.EndOfTurnFlash(clientQuestions[questionIndex].GetCorrectIPAddress().ToString(), state)); ;

        NextQuestion();
    }

    [ClientRpc]
    private void RpcTurnEndCOOP(int questionIndex, float newTime, bool answerState)
    {
        StartCoroutine(OnTurnEndCOOP(questionIndex, newTime, answerState));
    }

    [Client]
    private IEnumerator OnTurnEndCOOP(int questionIndex, float newTime, bool answerState)
    {
        SubnetQuestion.AnswerState state = SubnetQuestion.AnswerState.WRONG_ANSWERED;

        if (answerState)
            state = SubnetQuestion.AnswerState.CORRECT_ANSWERED;

        gameUI.PauseScrollLists();
        slider.Reset(newTime);

        yield return StartCoroutine(gameUI.EndOfTurnFlash(clientQuestions[questionIndex].GetCorrectIPAddress().ToString(), state));

        NextQuestionCOOP();
    }

    [ClientRpc]
    private void RpcInputValidPVP(int value, int part, int sListID, uint answeringPlayerId)
    {
        StartCoroutine(OnInputValidPVP(value, part, sListID, answeringPlayerId));
    }

    [ClientRpc]
    private void RpcInputNotValidPVP(int value, int sListID, uint answeringPlayerId)
    {
        StartCoroutine(OnInputNotValidPVP(value, sListID, answeringPlayerId));
    }

    [Client]
    private IEnumerator OnInputValidPVP(int value, int part, int sListID, uint answeringPlayerId)
    {
        MPGamePlayer answeringPlayer = GetGamePlayer(answeringPlayerId);

        if (!answeringPlayer.isLocalPlayer)
        {
            yield break;
        }

        gameUI.GetScrollList(sListID).InputIsValid(value);
        gameUI.PositiveFlash(addValue.ToString());
        slider.AddTime(addValue);
    }

    [Client]
    private IEnumerator OnInputNotValidPVP(int value, int sListID, uint answeringPlayerId)
    {
        MPGamePlayer answeringPlayer = GetGamePlayer(answeringPlayerId);

        if (!answeringPlayer.isLocalPlayer)
        {
            yield break;
        }

        gameUI.GetScrollList(sListID).InputIsNotValid(value);
        gameUI.NegativFlash(removeValue.ToString());
        slider.AddTime(-removeValue);
    }

    [ClientRpc]
    private void RpcInputValidCOOP(int value, int part, int sListID)
    {
        gameUI.GetScrollList(sListID).InputIsValid(value);
        gameUI.PositiveFlash(addValue.ToString());
        slider.AddTime(addValue);
    }

    [ClientRpc]
    private void RpcInputNotValidCOOP(int value, int part, int sListID)
    {
        gameUI.GetScrollList(sListID).InputIsNotValid(value);
        gameUI.NegativFlash(removeValue.ToString());
        slider.AddTime(-removeValue);
    }

    [Client]
    private void NextQuestion()
    {
        ++clientQuestionIndex;
        if (clientQuestionIndex >= clientQuestions.Length)
        {
            OnSubnetPVPFinished();
            return;
        }
        
        gameUI.SetQuestion(clientQuestions[clientQuestionIndex]);
        gameUI.SetProgressText(clientQuestionIndex, clientQuestions.Length);
        gameUI.UpdateQuestion(clientQuestionIndex, clientQuestions[clientQuestionIndex], TryToClick);
        turnTime -= clientQuestionIndex * 2;
        slider.Reset(turnTime);
        slider.Continue();
    }

    [Client]
    private void NextQuestionCOOP()
    {
        ++clientQuestionIndex;
        if (clientQuestionIndex >= clientQuestions.Length)
        {
            OnSubnetCOOPFinished();
            return;
        }

        UpdateScrollListColor();
        gameUI.SetQuestion(clientQuestions[clientQuestionIndex]);
        gameUI.SetProgressText(clientQuestionIndex, clientQuestions.Length);
        gameUI.UpdateQuestion(clientQuestionIndex, clientQuestions[clientQuestionIndex], TryToClick);
        slider.Continue();
    }

    [Client]
    private void OnSubnetPVPFinished()
    {
        
        slider.Pause();
        subnetOverviewUI.Show();

        //reset answers, in case another round is played
        foreach (SubnetQuestion qq in clientQuestions)
            qq.Reset();

        //notify server that client finished quiz
        GetLocalSubnetPlayer().CmdAnsweredAllQuestions();
    }

    [ClientRpc]
    private void RpcOnSubnetCOOPFinished()
    {
        OnSubnetCOOPFinished();
    }

    [Client]
    private void OnSubnetCOOPFinished()
    {
        slider.Pause();
        gameUI.PauseScrollLists();
        subnetOverviewUI.Show();

        GetLocalSubnetPlayer().CmdAnsweredAllQuestions();
    }

    [Client]
    protected override UIGameOverview InitGameOverview()
    {
        subnetOverviewUI = UIElement.Instantiate<UISubnetOverview>(transform);
        subnetOverviewUI.SetController(playMode);

        return subnetOverviewUI;
    }

    [Client]
    protected override void OnMatchStarted()
    {
        gameUI.Show();
        slider.Continue();
    }

    protected override void OnMatchFinished()
    {
        
    }

    //////////////////////////////////////////////// Client Logic End

}

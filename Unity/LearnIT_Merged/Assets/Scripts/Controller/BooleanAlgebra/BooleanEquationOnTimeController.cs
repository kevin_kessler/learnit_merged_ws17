﻿/**
 * The controller to play on time
 */
public class BooleanEquationOnTimeController : BooleanEquationController {
       
    public static readonly int RIGHT_ANSWER_IN_SECONDS = 8;
    public static readonly int FALSE_ANSWER_IN_SECONDS = -4;
    public static readonly int SKIP_ANSWER_IN_SECONDS = -6;
    protected int startTimeInSeconds;
    protected UITimeSlider timer;

    protected override void Init()
    {
        startTimeInSeconds = GetStartTime(base.degree);
        base.Init();
        timer = UITimeSlider.Instantiate<UITimeSlider>();
        timer.Reset(startTimeInSeconds);
    }

    public static int GetStartTime(Difficulty degree)
    {
        switch (degree)
        {
            case Difficulty.EASY:
                return 40;
            case Difficulty.MODERATE:
                return 40;
            case Difficulty.HARD:
                return 60;
            default:
                return 40;
        }
    }

    protected override void OnGameLoaded()
    {
        base.OnGameLoaded();        
    }

    protected override void SetSlider()
    {
        gui.SetSlider(timer);
        timer.Continue();
    }

    protected override void OnSolved()
    {
        gui.FlashCorrect(RIGHT_ANSWER_IN_SECONDS);
        timer.AddTime(RIGHT_ANSWER_IN_SECONDS);
    }

    protected override void OnFail()
    {
        gui.FlashWrong(FALSE_ANSWER_IN_SECONDS);
        timer.AddTime(FALSE_ANSWER_IN_SECONDS);
    }

    protected override void OnSkip()
    {
        gui.FlashWrong(SKIP_ANSWER_IN_SECONDS);
        timer.AddTime(SKIP_ANSWER_IN_SECONDS);
    }

    protected override void ShowHistory()
    {
        timer.Pause();
        gui.ShowHistory();
    }

    protected override void HideHistory()
    {
        timer.Continue();
        gui.HideHistory();
    }

    public override void OnRestartGame()
    {
        base.restartGame();
        timer.Reset(startTimeInSeconds);
        timer.Pause();
        gui.SetSlider(timer);
        OnResumeGame();
        timer.Continue();
    }
}

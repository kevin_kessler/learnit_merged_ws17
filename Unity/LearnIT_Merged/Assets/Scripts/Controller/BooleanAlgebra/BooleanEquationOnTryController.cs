﻿/**
 * The controller to play on Try
 */ 
public class BooleanEquationOnTryController : BooleanEquationController {

    protected int startTries;
    protected int tries;
    protected UICountSlider counter;

    protected override void Init()
    {        
        base.Init();
        startTries = GetStartTries(base.degree);
        tries = startTries;
        counter = UICountSlider.Instantiate<UICountSlider>();                
        counter.Reset(startTries);
        counter.SetAppendingText("/"+ startTries);
    }

    public static int GetStartTries(Difficulty degree)
    {
        switch (degree)
        {
            case Difficulty.EASY:
                return 5;
            case Difficulty.MODERATE:
                return 5;
            case Difficulty.HARD:
                return 10;
            default:
                return 5;
        }
    }

    protected override void OnGameLoaded()
    {
        base.OnGameLoaded();        
    }

    protected override void SetSlider()
    {
        gui.SetSlider(counter);
    }

    protected override void OnSolved()
    {
        gui.FlashCorrect();
    }

    protected override void OnFail()
    {
        gui.FlashWrong();
        counter.Decrement();
    }

    protected override void OnSkip()
    {
        gui.FlashWrong();
        counter.Decrement();
    }

    protected override void ShowHistory()
    {
        gui.ShowHistory();
    }

    protected override void HideHistory()
    {
        gui.HideHistory();
    }

    public override void OnRestartGame()
    {
        base.restartGame();
        counter.Reset(startTries);
        counter.SetAppendingText("/" + startTries);
        gui.SetSlider(counter);
        OnResumeGame();
    }
}

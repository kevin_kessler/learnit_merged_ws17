﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Utils;

public class MPTicTacToeController : MPGameController
{
    public static readonly int NUMBER_OF_OPERANDS = 9;
    public static readonly int NUMBER_OF_OPERATORS = 16;
    public static readonly int NUMBER_OF_MAX_ROUNDS = 3;
    public static readonly int BYTES_PER_ROUND = 0;

    private Difficulty degree;
    private UITimeSlider timer;
    private MPTicTacToeGUI gui;
    private NetworkTransmitter nTransmitter;
    private MPTicTacToeRound[] clientRounds;
    private MPTicTacToePlayer localTTTplayer;
    private BoolOperand currentOperand;
    [SyncVar]
    private int currentRoundIndex = -1;
    [SyncVar]
    private int timeForDecision = 10;    
    [SyncVar]
    private uint player0Id, player1Id;

    private UITicTacToeOverView overview;

    protected override void InitBothSides()
    {
        nTransmitter = GetComponent<NetworkTransmitter>();
    }

    /**
     * Server
     */
    [Server]
    protected override void InitServerSide()
    {
        this.degree = ((BooleanGameMode)lobby.GetGameModeToPlay()).GetDifficulty();
        MPTicTacToeModel.Reset();
    }

    [Server]
    protected override void OnAllPlayersReadyToBeginMatch()
    {
        RpcPrepare(NUMBER_OF_MAX_ROUNDS);

        Dictionary<uint, MPGamePlayer>.Enumerator e = gamePlayers.GetEnumerator();
        e.MoveNext();
        player0Id = e.Current.Value.netId.Value;
        Debug.Log("Player 0: " + player0Id);
        e.MoveNext();
        player1Id = e.Current.Value.netId.Value;
        Debug.Log("Player 1: " + player1Id);
        MPTicTacToeModel.InitMatch(player0Id, player1Id, degree);

        StartCoroutine(SendMatch());
    }

    [Server]
    public void CheckForStart()
    {
        Debug.Log(" test if all players ready, " + gamePlayers.Values.Count+" players to test");
        foreach (MPGamePlayer player in gamePlayers.Values)
            if (!((MPTicTacToePlayer)player).isReady())
                return;
        Debug.Log("all players ready, lets start!");
        StartMatch();
        InitNextRound();
    }

    [Server]
    public void CheckForNextRound()
    {
        foreach (MPGamePlayer player in gamePlayers.Values)
            if (!((MPTicTacToePlayer)player).isReady())
                return;
        InitNextRound();
    }

    [Server]
    public void Decide(uint playerId, int index)
    {
        if (MPTicTacToeModel.GetPlayerIDByOperand(currentRoundIndex, currentOperand) == playerId)
        {
            MPTicTacToeModel.SetOperand(currentRoundIndex, index, playerId);
            RpcDecision(index, currentOperand);

            BooleanEquation[] equations = MPTicTacToeModel.EvaluateRound(MPTicTacToeModel.GetRound(currentRoundIndex));

            foreach (BooleanEquation equation in equations)
                equation.SetSolution(BooleanSolver.Evaluate(equation));

            for (int i = 0; i < equations.Length; i++)
                if (equations[i].GetSolution() != BoolOperand.NULL)
                {
                    Debug.Log("Equation " + i + " = "+equations[i].ToString());
                    RpcColorLines(i, GetPlayerByNetId(MPTicTacToeModel.GetRound(currentRoundIndex).GetPlayerIDByOperand(equations[i].GetSolution())).GetPlayerColor());
                }
                    

            if (MPTicTacToeModel.GetRound(currentRoundIndex).IsEnd())
            {
                int player0Points = 0, player1Points = 0;
                foreach (BooleanEquation equation in equations)
                    if (equation.GetSolution() == MPTicTacToeModel.GetRound(currentRoundIndex).GetOperandByPlayerID(player0Id))
                        player0Points++;
                    else
                        player1Points++;

                bool player0Won = player0Points >= player1Points;
                bool player1Won = player1Points >= player0Points;
                RpcProgress(currentRoundIndex, player0Won, player1Won);

                if (currentRoundIndex < NUMBER_OF_MAX_ROUNDS - 1)
                    RpcNextRoundAvailable();
                else
                    FinishMatch();
            }
            else
            {
                currentOperand = BooleanAlgebra.Negate(currentOperand);
                RpcHighlightTurn(MPTicTacToeModel.GetRound(currentRoundIndex).GetPlayerIDByOperand(currentOperand));
            }
        }
    }

    [Server]
    public void CheckPlayerReadyForNextRound(uint playerId)
    {
        int i = 0;
        foreach (MPTicTacToePlayer player in gamePlayers.Values)
        {
            if (playerId == player.netId.Value)
               RpcActivatePlayerWaiting(i, playerId);

            i++;
        }
    }

    [ClientRpc]
    private void RpcActivatePlayerWaiting(int playerId, uint playerNetId)
    {
        GetLocalTTTPlayer().isWaiting(true);
        if(GetLocalTTTPlayer().netId.Value != playerNetId)
            gui.ActivatePlayerWaiting(playerId);
    }

    [Server]
    public IEnumerator SendMatch()
    {
        byte[][] rounds = new Byte[NUMBER_OF_MAX_ROUNDS][];
        for (int i = 0; i < NUMBER_OF_MAX_ROUNDS; i++)
            rounds[i] = ObjectSerializationExtension.SerializeToByteArray(MPTicTacToeModel.GetRounds()[i]);
        for (int i = 0; i < NUMBER_OF_MAX_ROUNDS; i++)
            yield return StartCoroutine(nTransmitter.SendBytesToClientsRoutine(i, rounds[i]));
    }

    [Server]
    public void InitNextRound()
    {
        currentRoundIndex++;
        currentOperand = MPTicTacToeModel.GetRound(currentRoundIndex).GetStartOperand();
        RpcNextRound(currentRoundIndex);
        RpcHighlightTurn(MPTicTacToeModel.GetRound(currentRoundIndex).GetPlayerIDByOperand(currentOperand));
        foreach (MPGamePlayer player in gamePlayers.Values)
            ((MPTicTacToePlayer)player).isReady(false);
    }

    /**
     * ClientRpc
     */
    [ClientRpc]
    private void RpcPrepare(int numberOfRounds)
    {
        clientRounds = new MPTicTacToeRound[numberOfRounds];
    }

    [ClientRpc]
    private void RpcNextRound(int index)
    {
        Debug.Log("currentRoundIndex in Rpc = " + index);
        GetLocalTTTPlayer().GetController().SetCurrentRoundIndex(index);
        GetLocalTTTPlayer().isWaiting(false);
        InitGui();
    }

    [ClientRpc]
    private void RpcDecision(int labelIndex, BoolOperand op)
    {
        gui.SetOperand(labelIndex, op);
    }

    [ClientRpc]
    private void RpcHighlightTurn(uint playerId)
    {
        if (playerId == player0Id)
            gui.ActivatePlayerOneTurnIndicator();
        else
            gui.ActivatePlayerTwoTurnIndicator();
    }

    [ClientRpc]
    private void RpcNextRoundAvailable()
    {
        gui.EnableNextButton();
    }

    [ClientRpc]
    private void RpcColorLines(int index, Color color)
    {
        gui.SetSolutionIndicatorLineColor(index, color);
    }

    [ClientRpc]
    private void RpcProgress(int roundIndex, bool player0Won, bool player1Won)
    {
        gui.SetPlayerOneProgress(roundIndex, player0Won);
        gui.SetPlayerTwoProgress(roundIndex, player1Won);
    }

    /**
     * Client
     */
    [Client]
    protected override void InitClientSide()
    {
        gui = UIElement.Instantiate<MPTicTacToeGUI>(this.transform);
        
        //gui.SetClickLabelAction(GetLocalTTTPlayer().CmdDecide);
        timer = UITimeSlider.Instantiate<UITimeSlider>();
        //timer.OnFinished += () => { currentOperand = BooleanAlgebra.Negate(currentOperand); };
        timer.Reset(timeForDecision);
        timer.Pause();
        nTransmitter.OnDataCompletelyReceived += OnReceiveRound;
    }

    [Client]
    protected override void OnMatchStarted()
    {
        gui.Show();
        gui.SetHeaderTitle(lobby.GetGameModeToPlay().GetName());
    }

    [Client]
    protected override void OnMatchFinished()
    {
        UIMPGamePlayer playerOne = UIElement.Instantiate<UIMPGamePlayer>();
        playerOne.SetPlayerName(gui.GetPlayerOneName());
        playerOne.SetPlayerColor(gui.GetPlayerOneColor());
        //overview.AddPlayerUI(playerOne);
        overview.SetPlayeOneName(gui.GetPlayerOneName());

        UIMPGamePlayer playerTwo = UIElement.Instantiate<UIMPGamePlayer>();
        playerTwo.SetPlayerName(gui.GetPlayerTwoName());
        playerTwo.SetPlayerColor(gui.GetPlayerTwoColor());
        //overview.AddPlayerUI(playerTwo);
        overview.SetPlayeTwoName(gui.GetPlayerTwoName());

        UIProgressMarker[] playerOneRounds = gui.GetPlayerOneRounds();
        UIProgressMarker[] playerTwoRounds = gui.GetPlayerTwoRounds();

        for(int i = 0; i < playerOneRounds.Length; i++)
        {
            UIRound round = UIElement.Instantiate<UIRound>();
            round.SetRoundNumber(i+1);
            round.SetRoundWon(playerOneRounds[i], playerTwoRounds[i]);
            overview.AddRoundUI(round);
        }

        overview.Show();
    }
        
    [Client]
    private void InitGui()
    {
        gui.GenerateGUI(clientRounds[currentRoundIndex].GetOperators(), GetPlayerByNetId(player0Id), GetPlayerByNetId(player1Id), currentRoundIndex);
        gui.SetButtonActions(GetLocalTTTPlayer().Decide, () => { if(!GetLocalTTTPlayer().isWaiting())GetLocalTTTPlayer().CheckOtherPlayerReadyForNextRound(); GetLocalTTTPlayer().ReadyForNextRound(); });
        gui.AddOnExitAction(PlayerLeaveMatch);
    }

    [Client]
    private void PlayerLeaveMatch()
    {
        GetLocalTTTPlayer().CmdLeaveGame();
    }

    [Server]
    public void LeaveGame()
    {
        lobby.InvokeReturnToLobby();
    }

    [Client]
    private void OnReceiveRound(int id, byte[] round)
    {
        if (clientRounds[id] != null )
            return;

        MPTicTacToeRound roundObject = ObjectSerializationExtension.Deserialize<MPTicTacToeRound>(round);

        for(int i = 0; i < roundObject.GetOperators().Length; i++)
            Debug.Log(roundObject.GetOperators()[i].ToString());

        clientRounds[id] = roundObject;
        bool isReady = true;
        foreach(MPTicTacToeRound r in clientRounds)
            if(r == null)
            {
                isReady = false;
                break;
            }
        if (isReady)
            GetLocalTTTPlayer().CmdReadyToStart();

        Debug.Log("Round received with index = "+id); 
    }

    /**
     * Helper and getter methods
     */

    public MPTicTacToePlayer GetPlayerByNetId(uint netId)
    {
        MPGamePlayer result;
        gamePlayers.TryGetValue(netId, out result);
        return (MPTicTacToePlayer) result;
    }

    public MPTicTacToeRound GetRound(int index)
    {
        Debug.Log(index);
        return clientRounds[index];
    }

    public int GetCurrentRoundIndex()
    {
        return currentRoundIndex;
    }
    
    public MPTicTacToePlayer GetLocalTTTPlayer()
    {
        if (localTTTplayer == null)
            localTTTplayer = (MPTicTacToePlayer)GetLocalGamePlayer();
        return localTTTplayer;
    }

    public void SetCurrentRoundIndex(int index)
    {
        currentRoundIndex = index;
    }
    
    
    protected override UIGameOverview InitGameOverview()
    {
        overview = UIElement.Instantiate<UITicTacToeOverView>(transform);
        overview.Hide();
        
        return overview;
    }
    
    protected override void ShowGameOverviewOnTab()
    {
        
    }
}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/**
 * Variants for the GUI
 */
public enum Variant { TERM, SCHALTNETZ }
/**
* Base controller to set up the game 
*/
public abstract class BooleanEquationController : SPGameController {
    
    public static readonly int MAX_OPERATORS = 5;
    protected int solved = 0;
    protected BooleanEquation actualEquation;
    
    protected BooleanEquationGUI gui;

    protected Difficulty degree;

    protected override void Init()
    {
        this.degree = ((BooleanGameMode)GameMenuController.GetLastChosenMode()).GetDifficulty();

        if(((BooleanGameMode)GameMenuController.GetLastChosenMode()).getVariant() == Variant.TERM)        
            gui = UIElement.Instantiate<BooleanEquationGUI_Terme>(this.transform);        
        else
            gui = UIElement.Instantiate<BooleanEquationGUI_Schaltnetz>(this.transform);        
        
        gui.AddSubmitAction(Submit);
        gui.AddSkipAction(Skip);
        gui.AddHistoryAction(ShowHistory);
    }

    protected override IEnumerator LoadGame()
    {
        yield return null;
    }

    protected override void OnGameFinished()
    {
       
    }

    protected override void OnGameLoaded()
    {
        gui.GenerateGUI( GenerateNextTerm() );
        gui.AddPauseButtonAction(PauseGame);
        gui.AddHistoryBackAction(HideHistory);
        gui.AddClickLabelAction(LabelsClickedControl);
        SetSlider();
        gui.AddOnSliderFinishedAction(OnSliderFinished);
        gui.SetHeaderTitle(GameMenuController.GetLastChosenMode().GetName());
    }

    protected void Submit()
    {
        gui.StopAllCoroutines();
        BooleanEquation userTerm = gui.GetTerm();
        userTerm.SetSolution(BooleanSolver.Evaluate(userTerm));
        if (actualEquation.GetSolution() == userTerm.GetSolution())
        {
            OnSolved();
            gui.SetPoints(++solved);
            gui.AddUserEquation(userTerm, true);
            gui.EnableMasterEquation();           
            gui.GenerateGUI( GenerateNextTerm() );
            gui.AddClickLabelAction(LabelsClickedControl);
        }
        else
        {
            gui.AddUserEquation(userTerm, false);
            OnFail();
        }
    }

    protected void Skip()
    {
        gui.EnableMasterEquation();
        OnSkip();
        if (!gui.GetSlider().IsFinished())
        {
            gui.GenerateGUI(GenerateNextTerm());
            gui.AddClickLabelAction(LabelsClickedControl);
        }
    }

    protected void LabelsClickedControl()
    {
        
        if (gui.LabelsClickedControl()) {
            gui.SetCanSubmit(true);
            gui.GetSubmitButton().GetComponent<Image>().color = new UnityEngine.Color32(0x00, 0x45, 0x95, 0xFF);
        }
    }

    protected BooleanEquation GenerateNextTerm()
    {
        BooleanEquation equation = BooleanGenerator.GetRandomEquation(GetNumberOfOperators(), degree, false);
        actualEquation = equation;
        gui.AddHistoryItem(equation);
        return equation;
    }
     
    protected int GetNumberOfOperators()
    {
        int number = 1+solved/3;
        if (number > MAX_OPERATORS)
            number = MAX_OPERATORS;
        return number;
    }

    protected void OnSliderFinished()
    {
        gui.AddHistoryBackAction(FinishGame);
        gui.EnableMasterEquation();
        ShowHistory();
    }

    protected abstract void SetSlider();
    protected abstract void OnSolved();
    protected abstract void OnFail();
    protected abstract void OnSkip();
    protected abstract void ShowHistory();
    protected abstract void HideHistory();

    private UIInfoDialog helpDialog;
    protected UIDialog GetHelpDialog()
    {
        if (null == helpDialog)
        {
            helpDialog = UIElement.Instantiate<UIInfoDialog>(transform);
            helpDialog.Setup(
                Global.STRING_HELP,
                "Löse die vorgegebene Gleichung nach dem Angezeigten Ergebnis auf!\n\n" +
                "Klicke dazu auf die ovalen Felder um sie mit X oder !X zu besetzen.\n" + 
                "Klicke dann auf 'Bestätigen' um deine Lösung abzuschicken. Wenn du die Gleichung überspringen willst, " +
                "klicke auf 'Nächste'. Wenn du deine bisherigen Versuche ansehen möchtest klicke auf 'Verlauf'.",
                Resources.Load<Sprite>(Global.PATH_ICON_QUESTIONMARK)
            );
        }
        return helpDialog;
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION BEGIN //////////////////////////////////
    protected void restartGame()
    {
        solved = 0;
        gui.ResetGui();
        gui.GenerateGUI(GenerateNextTerm());
        gui.AddHistoryBackAction(HideHistory);
        gui.AddClickLabelAction(LabelsClickedControl);
    }

    public override void OnRestartGame()
    {
        restartGame();
        OnResumeGame();
    }

    public override void OnQuitGame()
    {
        OnResumeGame();
        FinishGame();
    }

    public override void OnHelp()
    {
        GetHelpDialog().Show();
    }

    public override bool HasHelpOption()
    {
        return true;
    }
}

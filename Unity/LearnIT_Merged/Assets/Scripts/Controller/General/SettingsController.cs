﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using UnityEngine.UI;
using Utils;

public class SettingsController : MonoBehaviour
{
    private static readonly string PPREF_CLIENTID = "ClientID";
    private static readonly string PPREF_PLAYERNAME = "PlayerName";
    private static readonly string PPREF_MUSICVOLUME = "MusicVolume";
    private static readonly string PPREF_SFXVOLUME = "SfxVolume";
    private static readonly string PPREF_ISMUTED = "IsMuted";
    private const int MAX_PLAYERNAME_SIZE = 15; //characters

    private UISettingsMenu settingsMenuUI;
    private UIPromptDialog playerNamePromptUI;
    private LocalClientInfo playerInfo;
    private AudioPlayer audioPlayer;

    void Awake()
    {
        settingsMenuUI = UIElement.Instantiate<UISettingsMenu>(this.transform);
        settingsMenuUI.AddOnCloseAction(SaveSettings);
        settingsMenuUI.AddOnCloseAction(settingsMenuUI.Hide);

        playerNamePromptUI = UIElement.Instantiate<UIPromptDialog>(this.transform, "PlayerNamePromptUI");
        playerNamePromptUI.SetDialogTitle(Global.STRING_SETTINGS_EDIT_PLAYERNAME_TITLE);
        playerNamePromptUI.SetInputCharacterLimit(MAX_PLAYERNAME_SIZE);
        playerNamePromptUI.SetInfoIcon(Resources.Load<Sprite>(Global.PATH_ICON_PLAYER));
        playerNamePromptUI.AddOnAcceptAction(SavePlayerName);
        playerNamePromptUI.AddOnAcceptAction(playerNamePromptUI.Hide);
        playerNamePromptUI.AddOnCloseAction(() => 
        {
            //undo changes and close dialog
            playerNamePromptUI.SetInputText(playerInfo.GetPlayerName());
            playerNamePromptUI.Hide();
        });

        //read settings from player prefs
        string clientID = ReadClientID();
        string clientName = ReadPlayerName();
        float musicVolume = ReadVolume(PPREF_MUSICVOLUME);
        float sfxVolume = ReadVolume(PPREF_SFXVOLUME);
        bool isMuted = ReadIsMuted();
        PlayerPrefs.Save();

        //apply values from data to UI
        playerInfo = new LocalClientInfo(clientID, clientName, false);
        playerNamePromptUI.SetInputText(clientName);
        playerNamePromptUI.SetInfoText(string.Format(Global.STRING_SETTINGS_EDIT_PLAYERNAME_DESCRIPTION, clientName));
        settingsMenuUI.SetMusicVolume(musicVolume);
        settingsMenuUI.SetSfxVolume(sfxVolume);
        settingsMenuUI.SetOnMuteHandler((muted) => {
            if (muted)
                AudioPlayer.Mute();
            else
                AudioPlayer.Unmute();
        });
        settingsMenuUI.SetIsMuted(isMuted);
        AudioPlayer.SetMusicVolume(musicVolume);
        AudioPlayer.SetSFXVolume(sfxVolume);

        //play sound when changing volume
        audioPlayer = gameObject.AddComponent<AudioPlayer>();
        settingsMenuUI.AddOnSfxVolumeChangedListener(PlayVolumeSound);
        settingsMenuUI.SetOnSfxVolumeEndDragListener(PlayVolumeSound);

        settingsMenuUI.AddOnMusicVolumeChangedListener(AdjustMusicVolume);
        settingsMenuUI.SetOnMusicVolumeEndDragListener(AdjustMusicVolume);
    }

    private void PlayVolumeSound(float volume)
    {
        if (!settingsMenuUI.SfxSliderIsBeingDragged())
            audioPlayer.PlayVolumeSound(volume);
    }

    private void AdjustMusicVolume(float volume)
    {
        AudioPlayer.SetMusicVolume(volume);
    }

    public void ShowSettingsMenu()
    {
        settingsMenuUI.Show();
    }

    public void ShowPlayerNamePrompt()
    {
        playerNamePromptUI.Show();
    }

    public LocalClientInfo GetPlayerInfo()
    {
        return playerInfo;
    }

    public void SaveSettings()
    {
        //save settings from UI to file
        PlayerPrefs.SetFloat(PPREF_MUSICVOLUME, settingsMenuUI.GetMusicVolume());
        PlayerPrefs.SetFloat(PPREF_SFXVOLUME, settingsMenuUI.GetSFXVolume());
        PlayerPrefs.SetInt(PPREF_ISMUTED, settingsMenuUI.IsMuted() ? 1 : 0);

        //apply values from UI to data
        AudioPlayer.SetMusicVolume(settingsMenuUI.GetMusicVolume());
        AudioPlayer.SetSFXVolume(settingsMenuUI.GetSFXVolume());
        if (settingsMenuUI.IsMuted())
            AudioPlayer.Mute();
        else
            AudioPlayer.Unmute();
    }

    private void SavePlayerName(string name)
    {
        PlayerPrefs.SetString(PPREF_PLAYERNAME, name);
        playerInfo.SetPlayerName(name);
        playerNamePromptUI.SetInfoText(string.Format(Global.STRING_SETTINGS_EDIT_PLAYERNAME_DESCRIPTION, name));
    }

    private string ReadClientID()
    {
        string clientID;
        if (!PlayerPrefs.HasKey(PPREF_CLIENTID)) {
            //generate pseudo unique device identifier, to properly distinguish between clients in a network. 
            clientID = SystemInfo.deviceUniqueIdentifier + "-" + Random.Range(0, int.MaxValue) + "-" + System.DateTime.Now.ToString("yyyyMMddHHmmssff");
            PlayerPrefs.SetString(PPREF_CLIENTID, clientID);
        }
        else {
            clientID = PlayerPrefs.GetString(PPREF_CLIENTID);
        }
        return clientID;
    }

    private string ReadPlayerName()
    {
        string name;
        if (!PlayerPrefs.HasKey(PPREF_PLAYERNAME)){
            name = "Spieler " + Random.Range(0, 1000);
            PlayerPrefs.SetString(PPREF_PLAYERNAME, name);
        }
        else{
            name = PlayerPrefs.GetString(PPREF_PLAYERNAME);
        }
        return name;
    }

    private float ReadVolume(string key)
    {
        float volume = -1;
        if (!PlayerPrefs.HasKey(key)){
            volume = 0.5f;
            PlayerPrefs.SetFloat(key, volume);
        }
        else{
            volume = PlayerPrefs.GetFloat(key);
        }
        return volume;
    }

    private bool ReadIsMuted()
    {
        bool isMuted = true;
        if (!PlayerPrefs.HasKey(PPREF_ISMUTED))
            PlayerPrefs.SetInt(PPREF_ISMUTED, 0);
        else
            isMuted = PlayerPrefs.GetInt(PPREF_ISMUTED) == 0 ? false : true;

        return isMuted;
    }
}
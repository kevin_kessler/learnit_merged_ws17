﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;

public class GameMenuController : MonoBehaviour {

    private static GameMode lastChosenMode = null;
    private LocalClientInfo localClientInfo;

    public static GameMode GetLastChosenMode()
    {
        return lastChosenMode;
    }

    private UIGameMenu gameMenu;
    private UIVideoDialog videoPlayer;
    private SceneLoader sceneLoader;
    private AudioPlayer audioPlayer;

    void Start()
    {
        GameManager.GetInstance().GetPlayerInfo().SetIsPlaying(false);
        sceneLoader = gameObject.AddComponent<SceneLoader>();
        gameMenu = UIElement.Instantiate<UIGameMenu>(this.transform);
        videoPlayer = UIElement.Instantiate<UIVideoDialog>();
        audioPlayer = gameObject.AddComponent<AudioPlayer>();
        audioPlayer.PlayMenuMusic();

        Setup(GameSelectionController.GetLastChosenGame());

        //listen for localClientInfo change, to update name of local player
        localClientInfo = GameManager.GetInstance().GetPlayerInfo();
        localClientInfo.OnModelChanged += OnLocalPlayerNameChanged;
        OnLocalPlayerNameChanged();
    }

    private void Setup(Game game)
    {
        if (null == game)
            return;

        UIGameMenuHeader menuHeader = gameMenu.GetMenuHeader();
        UIGameMenuTabContent spTabContent = gameMenu.GetTabHolder().GetLeftTab().GetTabContent();
        UIGameMenuTabContent mpTabContent = gameMenu.GetTabHolder().GetRightTab().GetTabContent();

        //setup header
        menuHeader.SetTitleText(game.GetName());

        if (!game.GetModes().Contains(lastChosenMode))
            lastChosenMode = null;

        //Get Modes and init respective Modebuttons & Modeinfos
        foreach (GameMode gm in game.GetModes())
        {
            if (gm.IsMultiplayer()){
                InitMode(gm, mpTabContent.GetGameModeList(), mpTabContent.GetGameModeInfo());
            }
            else {
                InitMode(gm, spTabContent.GetGameModeList(), spTabContent.GetGameModeInfo());
            }
        }

        //hide empty tabs
        if (spTabContent.GetGameModeList().GetNumOfToggles() <= 0){
            gameMenu.GetTabHolder().GetLeftTab().Hide();
            gameMenu.GetTabHolder().SetRightTabActive();
        }
        else if (mpTabContent.GetGameModeList().GetNumOfToggles() <= 0){
            gameMenu.GetTabHolder().GetRightTab().Hide();
            gameMenu.GetTabHolder().SetLeftTabActive();
        }
        //or switch tab if mpTab was opened previously
        else if ((lastChosenMode != null && lastChosenMode.IsMultiplayer()))
            gameMenu.GetTabHolder().SetRightTabActive();
        else
            gameMenu.GetTabHolder().SetLeftTabActive();
    }

    private void InitMode(GameMode modeToInit, UISimpleToggleListGroup modeListToAddTo, UIGameMenuModeInfo modeInfoToChangeTo)
    {
        //create toggle button for the mode and set its onValueChanged behaviour
        Toggle modeButtonToInit = modeListToAddTo.CreateAndAddToggle(modeToInit.GetName());
        modeButtonToInit.onValueChanged.AddListener((value) =>
        {
            if (!value)
                return;

            //Update UI
            modeInfoToChangeTo.SetMode(modeToInit);

            //Set Play Button Functionality
            modeInfoToChangeTo.GetPlayButton().onClick.RemoveAllListeners();
            modeInfoToChangeTo.GetPlayButton().onClick.AddListener(() =>
            {
                lastChosenMode = modeToInit;
                //if we have a multiplayer mode, show possible players to play with before loading the scene
                if (modeToInit.IsMultiplayer())
                    GameManager.GetInstance().GetNetworkController().OfferOpponentSelection(modeToInit);
                //else load game scene
                else
                {
                    audioPlayer.StopBgMusic();
                    sceneLoader.LoadScene(modeToInit);
                }
            });

            //Set Tutorial Button Functionality
            modeInfoToChangeTo.GetTutorialButton().onClick.RemoveAllListeners();
            if (!modeToInit.HasTutorial())
                return;

            modeInfoToChangeTo.GetTutorialButton().onClick.AddListener(() =>
            {
                audioPlayer.PauseBgMusic();
                localClientInfo.SetIsPlaying(true);
                videoPlayer.PlayVideo("Tutorial", modeToInit.GetTutorialPath(), OnTutorialEnded);
            });
                
        });
        
        //set first toggleitem of the list to on, all others to off. except if there was one selected before, then set this one to on
        modeButtonToInit.isOn = lastChosenMode != null && lastChosenMode.IsMultiplayer() == modeToInit.IsMultiplayer() 
                                ? modeToInit == lastChosenMode 
                                : modeListToAddTo.GetItemHolder().transform.childCount <= 1;
        
    }

    private void OnTutorialEnded()
    {
        localClientInfo.SetIsPlaying(false);
        audioPlayer.ResumeBgMusic();
    }

    private void OnLocalPlayerNameChanged()
    {
        gameMenu.GetMenuHeader().SetPlayerName(localClientInfo.GetPlayerName());
    }

    void OnDestroy()
    {
        //deregister listener
        if (null != GameManager.GetInstance())
        {
            localClientInfo.OnModelChanged -= OnLocalPlayerNameChanged;
        }
    }
}

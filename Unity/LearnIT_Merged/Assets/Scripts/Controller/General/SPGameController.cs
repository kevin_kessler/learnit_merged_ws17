﻿using UnityEngine;
using System.Collections;

public abstract class SPGameController : MonoBehaviour, IPauseHandler
{
    private SceneLoader sceneLoader;
    protected AudioPlayer audioPlayer;
    protected UILoadingIndicator loadingIndicator;
    private bool isFinishing = false;
    private PauseController pauseController;

	private void Start () {
        GameManager.GetInstance().GetPlayerInfo().SetIsPlaying(true);
        audioPlayer = gameObject.AddComponent<AudioPlayer>();
        sceneLoader = gameObject.AddComponent<SceneLoader>();
        pauseController = gameObject.AddComponent<PauseController>();
        pauseController.SetPauseHandler(this);

        this.transform.position = new Vector3(0, 0, 0);
        loadingIndicator = UIElement.Instantiate<UILoadingIndicator>(this.transform, "LoadingGameIndicator");
        loadingIndicator.SetIndicationText(Global.STRING_INDICATE_LOADING_GAME);
        loadingIndicator.SetShowProgressBar(false);
        loadingIndicator.SetIsCancelAble(true);
        loadingIndicator.AddOnCancelAction(() =>{
            StopAllCoroutines();
            FinishGame();
        });
        loadingIndicator.Hide();

        Init();
        StartCoroutine(WaitForLoading());
	}

    private IEnumerator WaitForLoading()
    {
        //only show loading indicator if LoadGame takes more than a frame
        IEnumerator showRoutine = DelayedIndicatorShow();
        StartCoroutine(showRoutine);

        //start loading
        yield return StartCoroutine(LoadGame());

        //hide indicator
        StopCoroutine(showRoutine);
        loadingIndicator.Hide();
        OnGameLoaded();
    }

    
    private IEnumerator DelayedIndicatorShow()
    {
        yield return null;
        loadingIndicator.Show();
    }

    protected abstract void Init();
    protected abstract IEnumerator LoadGame();
    protected abstract void OnGameLoaded();

    protected abstract void OnGameFinished();

    public void FinishGame()
    {
        isFinishing = true;
        OnGameFinished();
        GameManager.GetInstance().GetPlayerInfo().SetIsPlaying(false);
        sceneLoader.LoadGameMenuScene();
    }

    public void FinishGame(string errorMessage)
    {
        if (!string.IsNullOrEmpty(errorMessage))
            UIToastFlasher.CreateAndDisplayToast(this, Color.white, Global.COLOR_FLASH_RED, errorMessage);
        FinishGame();
    }

    public void PauseGame()
    {
        pauseController.PauseGame();
    }

    void OnDestroy()
    {
        if (!isFinishing)
            FinishGame();
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION BEGIN //////////////////////////////////

    public virtual void OnPauseGame()
    {
        Time.timeScale = 0;
    }

    public virtual void OnResumeGame()
    {
        Time.timeScale = 1;
    }

    public virtual void OnRestartGame()
    {
        OnResumeGame();
    }

    public virtual void OnQuitGame()
    {
        OnResumeGame();
        FinishGame();
    }

    public virtual void OnHelp()
    {
        //nothing to do
    }

    public virtual bool HasHelpOption()
    {
        return false;
    }
    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION END //////////////////////////////////
}

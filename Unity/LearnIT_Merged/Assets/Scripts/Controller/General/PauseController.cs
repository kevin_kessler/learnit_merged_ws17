﻿using UnityEngine;
using UnityEngine.Events;
using Utils;
using System.Collections;
using System;

public class PauseController : MonoBehaviour{

    private UIPauseMenuSP pauseMenuUI;
    private IPauseHandler currentPauseHandler;
    public event UnityAction OnPause;

    void Awake()
    {
        pauseMenuUI = UIElement.Instantiate<UIPauseMenuSP>(this.transform);
        pauseMenuUI.AddOnSettingsAction(GameManager.GetInstance().GetSettingsController().ShowSettingsMenu);
    }

    private void SetOnPause()
    {
        OnPause = null;
        OnPause += currentPauseHandler.OnPauseGame;
        OnPause += pauseMenuUI.Show;
    }

    private void SetOnResume()
    {
        pauseMenuUI.ClearOnResumeActions();
        pauseMenuUI.AddOnResumeAction(currentPauseHandler.OnResumeGame);
        pauseMenuUI.AddOnResumeAction(pauseMenuUI.Hide);

        pauseMenuUI.ClearOnCloseActions();
        pauseMenuUI.AddOnCloseAction(currentPauseHandler.OnResumeGame);
        pauseMenuUI.AddOnCloseAction(pauseMenuUI.Hide);
    }

    private void SetOnRestart()
    {
        pauseMenuUI.ClearOnRestartActions();
        pauseMenuUI.AddOnRestartAction(currentPauseHandler.OnRestartGame);
        pauseMenuUI.AddOnRestartAction(pauseMenuUI.Hide);
    }

    private void SetOnQuit()
    {
        pauseMenuUI.ClearOnQuitActions();
        pauseMenuUI.AddOnQuitAction(currentPauseHandler.OnQuitGame);
        pauseMenuUI.AddOnQuitAction(pauseMenuUI.Hide);
    }

    private void SetOnHelp()
    {
        pauseMenuUI.ClearOnHelpActions();
        pauseMenuUI.AddOnHelpAction(currentPauseHandler.OnHelp);
        pauseMenuUI.ShowHelpButton();
    }

    public void SetPauseHandler(IPauseHandler handler)
    {
        currentPauseHandler = handler;
        SetOnPause();
        SetOnResume();
        SetOnRestart();
        SetOnQuit();

        if(handler.HasHelpOption())
            SetOnHelp();
        else
        {
            pauseMenuUI.ClearOnHelpActions();
            pauseMenuUI.HideHelpButton();
        }
    }

    public void UnSetPauseHandler()
    {
        currentPauseHandler = null;
        OnPause = null;
        pauseMenuUI.ClearOnCloseActions();
        pauseMenuUI.ClearOnResumeActions();
        pauseMenuUI.ClearOnRestartActions();
        pauseMenuUI.ClearOnQuitActions();
        pauseMenuUI.ClearOnHelpActions();
    }

    public void PauseGame()
    {
        DebugUtils.AssertNotNull(DebugUtils.GetMemberName(() => currentPauseHandler), currentPauseHandler);
        OnPause.Invoke();
    }
}

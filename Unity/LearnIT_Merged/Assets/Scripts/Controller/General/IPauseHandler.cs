﻿using UnityEngine;
using System.Collections;

public interface IPauseHandler {

    void OnPauseGame();
    void OnResumeGame();
    void OnRestartGame();
    void OnQuitGame();

    bool HasHelpOption();

    void OnHelp();
}

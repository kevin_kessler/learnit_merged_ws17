﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using UnityEngine;
using System.Text.RegularExpressions;
using Utils;

public class QuizLoader
{
    private static readonly string LOG_PREFIX = "["+typeof(QuizLoader).Name+"]: ";
    private static readonly string QUIZ_DIRECTORY = Application.persistentDataPath + Path.AltDirectorySeparatorChar + "quiz" + Path.AltDirectorySeparatorChar;
    public const string QUIZ_IT = "informationtheory_quiz"; //must be equal to the xml file name (without extension)
    public const string QUIZ_CP = "complexity_quiz"; //must be equal to the xml file name (without extension)
    public const string QUIZ_CUSTOM = "custom_quiz"; //must be equal to the xml file name (without extension)

    private static Dictionary<string, Quiz> quizByName = new Dictionary<string, Quiz>();

    public static Quiz LoadQuizByName(string quizName)
    {
        Quiz quiz;
        if (string.IsNullOrEmpty(quizName))
            return null;

        if (!quizByName.ContainsKey(quizName))
        {
            quiz = LoadQuiz(quizName);
            quizByName.Add(quizName, quiz);
        }
        else
        {
            quiz = quizByName[quizName];
        }

        return quiz;
    }

    /// <summary>
    /// Loads a quiz from the xmlfile with the given name (without extension)
    /// And sets the generated quiz to the dictionary while using the given name as key
    /// </summary>
    private static Quiz LoadQuiz(string quizName)
    {
        XmlDocument quizXML = LoadQuizXML(quizName);
        if (null == quizXML)
            return null;

        //parse "QuestionsPerMatch"
        int questionsPerMatch = -1;
        if (null != quizXML.SelectSingleNode("Quiz/QuestionsPerMatch"))
            int.TryParse(quizXML.SelectSingleNode("Quiz/QuestionsPerMatch").InnerText, out questionsPerMatch);

        //parse "ShuffleQuestions"
        bool shuffleEnabled = true;
        if (null != quizXML.SelectSingleNode("Quiz/ShuffleQuestions") && null != quizXML.SelectSingleNode("Quiz/ShuffleQuestions").Attributes["enabled"])
            bool.TryParse(quizXML.SelectSingleNode("Quiz/ShuffleQuestions").Attributes["enabled"].Value, out shuffleEnabled);

        //parse "DefaultSolveDuration"
        float defaultSolveDuration = -1;
        if (null != quizXML.SelectSingleNode("Quiz/DefaultSolveDuration"))
            float.TryParse(quizXML.SelectSingleNode("Quiz/DefaultSolveDuration").InnerText, out defaultSolveDuration);
        if(defaultSolveDuration<1)
            defaultSolveDuration = 20;

        //create quiz object by parsed values
        Quiz quiz = new Quiz(questionsPerMatch, shuffleEnabled, defaultSolveDuration);

        //parse all "Question" Elements and add them to the quiz object
        XmlNodeList questionElements = quizXML.SelectNodes("Quiz/Question");
        foreach (XmlNode questionElement in questionElements)
        {
            if (null == questionElement["Answers"])
                continue;

            int correctAnswer = -1;
            bool parseOK = int.TryParse(questionElement["Answers"].GetAttribute("correctIndex"), out correctAnswer);
            if (!parseOK || correctAnswer < 0 || correctAnswer > 3)
                continue;

            float solveDuration = -1;
            if (null != questionElement["SolveDuration"])
            float.TryParse(questionElement["SolveDuration"].InnerText, out solveDuration);

            XmlNode textElement = questionElement["Text"];
            string questionText = textElement == null ? null : textElement.InnerText;

            XmlNode imageNameElement = questionElement["ImageName"];
            string imageName = imageNameElement == null ? null : imageNameElement.InnerText;
            imageName = string.IsNullOrEmpty(imageName) ? null : imageName;

            string[] answers = new string[4];
            XmlNode answerElement00 = questionElement.SelectSingleNode("Answers/Zeroth");
            XmlNode answerElement01 = questionElement.SelectSingleNode("Answers/First");
            XmlNode answerElement02 = questionElement.SelectSingleNode("Answers/Second");
            XmlNode answerElement03 = questionElement.SelectSingleNode("Answers/Third");
            answers[0] = answerElement00 == null ? null : answerElement00.InnerText;
            answers[1] = answerElement01 == null ? null : answerElement01.InnerText;
            answers[2] = answerElement02 == null ? null : answerElement02.InnerText;
            answers[3] = answerElement03 == null ? null : answerElement03.InnerText;

            QuizQuestion question = new QuizQuestion(questionText, imageName, answers[0], answers[1], answers[2], answers[3], correctAnswer, solveDuration);
            quiz.AddQuestion(question);
        }

        return quiz;
    }

    /// <summary>
    /// Loads the quiz xml file with the given filename (without extension) and returns it.
    /// Returns null if file not found;
    /// </summary>
    private static XmlDocument LoadQuizXML(string quizFileName)
    {
        if (!Directory.Exists(QUIZ_DIRECTORY))
            Directory.CreateDirectory(QUIZ_DIRECTORY);

        if (!File.Exists(QUIZ_DIRECTORY + quizFileName+".xml"))
            CreateQuizFiles(quizFileName);


        XmlDocument doc = null;
        string xmlText = AssetUtils.LoadTextFromFile(QUIZ_DIRECTORY + quizFileName + ".xml");
        if (null != xmlText)
        {
            doc = new XmlDocument();
            doc.LoadXml(xmlText);
        }

        return doc;
    }

    private static void CreateQuizFiles(string quizFileName)
    {
        string ressQuizDir = "Quiz" + Path.AltDirectorySeparatorChar;
        TextAsset xmlText = Resources.Load<TextAsset>(ressQuizDir + quizFileName);
        if(null==xmlText)
            return;

        //write xml content to persistent datapath
        File.WriteAllText(QUIZ_DIRECTORY + quizFileName + ".xml", xmlText.text, Encoding.UTF8);

        //identify used images and copy them as well
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xmlText.text);
        XmlNodeList imagePathNodes = doc.SelectNodes("Quiz/Question/ImageName");
        foreach(XmlNode imgPathNode in imagePathNodes)
        {
            if(string.IsNullOrEmpty(imgPathNode.InnerText))
                continue;

            string imageName = Path.GetFileNameWithoutExtension(imgPathNode.InnerText);
            Texture2D image = Resources.Load<Texture2D>(ressQuizDir + imageName);

            if (null != image)
            {
                string fullImageName = QUIZ_DIRECTORY + imageName + ".png";
                if (!File.Exists(fullImageName))
                    File.WriteAllBytes(fullImageName, image.EncodeToPNG());
            }
        }

        Debug.Log(LOG_PREFIX + "Created Quiz Files for '" + quizFileName + "' at " + QUIZ_DIRECTORY);
    }

    public static Sprite LoadQuizImageFromFile(string imageName)
    {
        return AssetUtils.LoadSpriteFromFile(QUIZ_DIRECTORY, imageName);
    }

    public static byte[] LoadQuizImageBytesFromFile(string imageName)
    {
        return AssetUtils.LoadBytesFromFile(QUIZ_DIRECTORY, imageName);
    }
}


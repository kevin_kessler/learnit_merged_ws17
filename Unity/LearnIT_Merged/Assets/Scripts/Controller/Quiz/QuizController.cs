﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class QuizController : SPGameController
{
    private QuizQuestion[] quizQuestions;
    private int currentQuestionIndex;

    private bool gameIsFinished = false;

    private UIQuiz quizUI;
    private UIQuizOverview quizOverviewUI;
    private UIQuizPlayer singlePlayerUI;
    private UIImageFlasher screenFlasherUI;
    private UITimeSlider timeSlider;

    protected override void Init()
    {
        currentQuestionIndex = 0;

        //init slider and handle its finish as "wrong answer"
        timeSlider = UITimeSlider.Instantiate<UITimeSlider>();
        timeSlider.OnFinished += () => 
        {
            quizQuestions[currentQuestionIndex].GiveAnswer(-1); 
            StartCoroutine(OnWrongAnswer()); 
        };
        timeSlider.Pause();
        screenFlasherUI = UIElement.Instantiate<UIImageFlasher>(this.transform);
    }

    protected override IEnumerator LoadGame()
    {
        //load quiz questions
        GameMode lastChosenMode = GameMenuController.GetLastChosenMode();
        GameModeQuiz quizMode = lastChosenMode is GameModeQuiz ? (GameModeQuiz)lastChosenMode : null;
        Quiz quiz = null;
        if (null != quizMode)
            quiz = quizMode.GetQuiz();
        if (null != quiz)
            quizQuestions = quiz.GetQuestions();
        yield break;
    }

    protected override void OnGameLoaded()
    {
        if (null == quizQuestions || quizQuestions.Length < 1){
            Debug.LogWarning("[QuizController]: Could not load Quiz Data. Returning to previous scene.");
            FinishGame("Quiz konnte nicht geladen werden. Ist die Quiz XML korrupt?");
            return;
        }

        //init quiz UI and assign slider
        quizUI = UIElement.Instantiate<UIQuiz>(this.transform);
        quizUI.Setup(quizQuestions[currentQuestionIndex], quizQuestions.Length, OnAnswerSelected);
        quizUI.SetGameSlider(timeSlider);
        quizUI.SetTitleText(GameMenuController.GetLastChosenMode().GetName());
        quizUI.AddPauseButtonAction(PauseGame);

        quizOverviewUI = UIElement.Instantiate<UIQuizOverview>(this.transform);
        singlePlayerUI = UIElement.Instantiate<UIQuizPlayer>();
        singlePlayerUI.SetPlayerName(GameManager.GetInstance().GetSettingsController().GetPlayerInfo().GetPlayerName());
        singlePlayerUI.SetTotalNumberOfQuestions(quizQuestions.Length);
        quizOverviewUI.AddPlayerUI(singlePlayerUI);
        quizOverviewUI.UseAsSinglePlayerOverview();
        quizOverviewUI.SetBackButtonAction(FinishGame);
        quizOverviewUI.InitQuestionResults(quizQuestions);
        quizOverviewUI.Hide();

        quizUI.Show();

        timeSlider.Reset(quizQuestions[currentQuestionIndex].GetSolveDuration());
        timeSlider.Continue();

        audioPlayer.PlayQuizMusic();
    }

    void Update()
    {
        ShowGameOverviewOnTab();
    }

    private void OnAnswerSelected(int selectedAnswerIndex)
    {
        QuizQuestion currentQuestion = quizQuestions[currentQuestionIndex];

        if (currentQuestion.IsAnswered())
            return;

        currentQuestion.GiveAnswer(selectedAnswerIndex);
        switch (currentQuestion.GetAnswerState())
        {
            case QuizQuestion.AnswerState.CORRECT_ANSWERED:
                StartCoroutine(OnCorrectAnswer());
                break;
            case QuizQuestion.AnswerState.WRONG_ANSWERED:
                StartCoroutine(OnWrongAnswer());
                break;
            default:
                break;
        }

        //update time to solve quiz
        float usedTimeToSolveQuiz = singlePlayerUI.GetUsedTimeToSolve();
        usedTimeToSolveQuiz += (currentQuestion.GetSolveDuration() - timeSlider.GetCurrentValue());
        singlePlayerUI.SetUsedTimeToSolveText(usedTimeToSolveQuiz);
    }

    private IEnumerator OnCorrectAnswer()
    {
        //show and wait for feedback, then advance to next question
        timeSlider.Pause();
        quizUI.UpdateProgressMarker(currentQuestionIndex, true);
        QuizQuestion currQuestion = quizQuestions[currentQuestionIndex];
        quizOverviewUI.UpdateQuestionResult(currentQuestionIndex, currQuestion.GetAnswerState());
        yield return StartCoroutine(ShowCorrectFeedback());
        NextQuestion();
    }

    private IEnumerator OnWrongAnswer()
    {
        //show and wait for feedback, then advance to next question
        timeSlider.Pause();
        quizUI.UpdateProgressMarker(currentQuestionIndex, false);
        QuizQuestion currQuestion = quizQuestions[currentQuestionIndex];
        quizOverviewUI.UpdateQuestionResult(currentQuestionIndex, currQuestion.GetAnswerState());
        yield return StartCoroutine(ShowWrongFeedback());
        NextQuestion();
    }

    private void NextQuestion()
    {
        currentQuestionIndex++;
        if (currentQuestionIndex >= quizQuestions.Length)
        {
            OnQuizFinished();
            return;
        }

        quizUI.SetQuestion(quizQuestions[currentQuestionIndex]);
        quizUI.SetProgressText(currentQuestionIndex, quizQuestions.Length);
        timeSlider.Reset(quizQuestions[currentQuestionIndex].GetSolveDuration());
        timeSlider.Continue();
    }

    private void OnQuizFinished()
    {
        timeSlider.Pause();
        gameIsFinished = true;
        quizOverviewUI.Show();
        audioPlayer.StopBgMusic();
        audioPlayer.PlayNotificationSound();

        //reset answers, in case another round is played
        foreach (QuizQuestion qq in quizQuestions)
            qq.Reset();
    }

    private IEnumerator ShowCorrectFeedback()
    {
        audioPlayer.PlayCorrectSound();
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_GREEN));
        yield return StartCoroutine(quizUI.HighlightAnswerButton(quizQuestions[currentQuestionIndex]));
    }

    private IEnumerator ShowWrongFeedback()
    {
        QuizQuestion curQuestion = quizQuestions[currentQuestionIndex];
        audioPlayer.PlayWrongSound();
        if (null != curQuestion && curQuestion.GetGivenAnswerIndex() < 0)
            yield return StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));
        else
        {
            StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));
            yield return StartCoroutine(quizUI.HighlightAnswerButton(curQuestion));
        }
    }

    private void ShowGameOverviewOnTab()
    {
        if (gameIsFinished || null == quizOverviewUI)
            return;

        //display overview during match when pressing tab
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            quizOverviewUI.ShowWithoutFooter();
        }

        if (Input.GetKeyUp(KeyCode.Tab))
        {
            quizOverviewUI.Hide();
        }
    }

    private UIInfoDialog helpDialog;
    protected UIDialog GetHelpDialog()
    {
        if (null == helpDialog)
        {
            helpDialog = UIElement.Instantiate<UIInfoDialog>(transform);
            helpDialog.Setup(
                Global.STRING_HELP,
                Global.STRING_QUIZ_HELP_TEXT,
                Resources.Load<Sprite>(Global.PATH_ICON_QUESTIONMARK)
            );
        }
        return helpDialog;
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION BEGIN //////////////////////////////////
    public override void OnRestartGame()
    {
        currentQuestionIndex = 0;
        quizUI.SetQuestion(quizQuestions[currentQuestionIndex]);
        for (int i = 0; i < quizQuestions.Length; i++)
        {
            quizUI.UpdateProgressMarker(i);
        }
        timeSlider.Reset();
        OnResumeGame();
    }

    public override void OnQuitGame()
    {
        OnResumeGame();
        timeSlider.Pause();
        FinishGame();
    }

    public override void OnHelp()
    {
        GetHelpDialog().Show();
    }

    public override bool HasHelpOption()
    {
        return true;
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION END //////////////////////////////////

    protected override void OnGameFinished()
    {
    }
}

﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

namespace Utils
{

    public class AssetUtils
    {
        public static void ReplaceTextureColor(Texture2D textureToWorkOn, Color colorToReplace, Color colorToApply)
        {
            for (int x = 0; x < textureToWorkOn.width; x++)
            {
                for (int y = 0; y < textureToWorkOn.width; y++)
                {
                    Color curColor = textureToWorkOn.GetPixel(x, y);
                    if (ColorsAreSimilar(curColor, colorToReplace))
                        textureToWorkOn.SetPixel(x, y, colorToApply);
                }
            }
            textureToWorkOn.Apply();
        }

        public static bool ColorsAreSimilar(Color c1, Color c2)
        {
            float epsilon = 0.2f;
            bool redIsSimilar = Mathf.Abs(c1.r - c2.r) < epsilon;
            bool greenIsSimilar = Mathf.Abs(c1.g - c2.g) < epsilon;
            bool blueIsSimilar = Mathf.Abs(c1.b - c2.b) < epsilon;
            bool alphaIsSimilar = Mathf.Abs(c1.a - c2.a) < epsilon;
            return redIsSimilar && greenIsSimilar && blueIsSimilar && alphaIsSimilar;
        }

        public static Texture2D CopyTexture(Texture2D original)
        {
            Texture2D copiedImage = new Texture2D(original.width, original.height);
            copiedImage.SetPixels(original.GetPixels());
            copiedImage.Apply();
            return copiedImage;
        }

        public static Sprite LoadSpriteFromFile(string directory, string imageName)
        {
            //Debug.Log("Loading Sprite from File: " + directory + imageName);
            byte[] texData = LoadBytesFromFile(directory, imageName);
            return LoadSpriteFromBytes(ref texData);
        }

        public static Sprite LoadSpriteFromBytes(ref byte[] imageBytes)
        {
            if (null == imageBytes)
                return null;

            Texture2D tex = LoadTextureFromBytes(imageBytes);

            return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        }

        public static Texture2D LoadTextureFromBytes(byte[] textureBytes)
        {
            if (null == textureBytes)
                return null;

            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(textureBytes); //auto resizes dimensions

            return tex;
        }

        public static byte[] LoadBytesFromFile(string directory, string imageName)
        {
            string imagePath = directory + imageName;
            if (!File.Exists(imagePath))
                return null;

            return File.ReadAllBytes(imagePath);
        }

        public static string LoadTextFromFile(string absFilePath)
        {
            try
            {
                string line;
                StreamReader reader = new StreamReader(absFilePath, Encoding.UTF8);
                StringBuilder builder = new StringBuilder();
                using (reader)
                {
                    do
                    {
                        line = reader.ReadLine();
                        if (line != null)
                        {
                            builder.Append(line);
                        }
                    }
                    while (line != null);
                    reader.Close();
                }
                return builder.ToString();
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
                return null;
            }
        }

    }
}

﻿using UnityEngine;
using System.Collections;
using System;

namespace Utils
{
    public class InputUtils
    {

        public static KeyCode GetPressedKey()
        {
            foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyUp(kcode))
                    return kcode;
            }

            return KeyCode.None;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;

public class UISubnet : UIElement
{
    [SerializeField]
    private Text aufgabeIP;
    [SerializeField]
    private Text aufgabeSNM;

    [SerializeField]
    private GameObject sListHolder;
    protected UIScrollList[] ipScrollLists;

    [SerializeField]
    private GameObject headerHolder;
    protected UIGameHeaderWithSlider Header;

    [SerializeField]
    private GameObject progressHolder;
    private UIProgressMarker[] progressMarkers;

    private int[] ipaddress = new int[4];

    private UITextFlasher TFlasher;
    private UIImageFlasher IFlasher;
    private UIToastFlasher ToastFlasher;

    private int numberOfQuestions;

    public virtual void Setup(UICustomSlider slider, SubnetQuestion question, int numberOfQuestions, UnityAction<int, int, int> callback)
    {
        Header = Instantiate<UIGameHeaderWithSlider>(headerHolder.transform);
        Header.SetSlider(slider);

        this.numberOfQuestions = numberOfQuestions;
        progressMarkers = new UIProgressMarker[numberOfQuestions];
        for (int i = 0; i < progressMarkers.Length; i++)
        {
            progressMarkers[i] = Instantiate<UIProgressMarker>(progressHolder.transform);
            progressMarkers[i].SetNumber(i + 1);
        }

        ipScrollLists = new UIScrollList[4];

        for (int i = 0; i < ipScrollLists.Length; i++)
        {
            ipScrollLists[i] = Instantiate<UIScrollList>(sListHolder.transform);
            ipScrollLists[i].SetID(i);
        }

        UpdateQuestion(0, question, callback);

        //Flasher
        TFlasher = Instantiate<UITextFlasher>();
        IFlasher = Instantiate<UIImageFlasher>();
        ToastFlasher = Instantiate<UIToastFlasher>();
        ToastFlasher.SetStayDuration(5);
    }

    private int[] GetIPAddress()
    {
        return ipaddress;
    }

    public UIScrollList GetScrollList(int id)
    {
        return ipScrollLists[id];
    }

    public void SetQuestion(SubnetQuestion question)
    {
        string[] aufgabe = question.GetQuestionText();
        aufgabeIP.text = aufgabe[0];
        aufgabeSNM.text = aufgabe[1];
    }

    public void SetTitleText(string text)
    {
        Header.SetTitleText(text);
    }

    public void SetScrollListButtonText(SubnetQuestion question)
    {
        IPAddress[] ipa = question.GetIPAddresses();
        List<int> tmp = new List<int>();

        int[,] shuffled = new int[ipa[0].GetAddress().Length, ipa.Length];

        for (int i = 0; i < ipa[0].GetAddress().Length; i++)
        {
            for (int j = 0; j < ipa.Length; j++)
            {
                tmp.Add(ipa[j].GetAddress()[i]);
            }
            tmp.Shuffle();
            for (int j = 0; j < tmp.Count; j++)
            {
                shuffled[i, j] = tmp[j];
            }
            tmp.Clear();
        }

        for(int i = 0; i < ipScrollLists.Length; i++)
        {
            for (int j = 0; j < ipa.Length; j++)
            {
                ipScrollLists[i].SetTextOfButton(j, shuffled[i,j].ToString());
            }
        }
    }

    public void UpdateProgressMarker(int questionIndex, bool correct)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= progressMarkers.Length);

        if (correct)
            progressMarkers[questionIndex].SetCorrect();
        else
            progressMarkers[questionIndex].SetWrong();
    }

    public void UpdateProgressMarker(int questionIndex)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= progressMarkers.Length);
        progressMarkers[questionIndex].SetNumber(questionIndex + 1);
    }

    public void UpdateQuestion(int activeQuestionIndex, SubnetQuestion question, UnityAction<int, int, int> callback)
    {
        SetProgressText(activeQuestionIndex, numberOfQuestions);
        
        SetScrollListButtonText(question);
        SetQuestion(question);

        for (int i = 0; i < ipScrollLists.Length; i++)
        {
            ipScrollLists[i].BindButtonListener(i, callback);
            ipScrollLists[i].ContinueScrolling();
        }

    }

    public void StopFlasher()
    {
        TFlasher.StopAllCoroutines();
        StartCoroutine(TFlasher.Flash(" "));
        TFlasher.StopAllCoroutines();

        IFlasher.StopAllCoroutines();
        ToastFlasher.StopAllCoroutines();
    }

    public IEnumerator EndOfTurnFlash(string correctIP, SubnetQuestion.AnswerState answer)
    {
        string flasherText = "Eine mögliche IP wäre " + correctIP + " .";

        StopFlasher();

        if (answer == SubnetQuestion.AnswerState.CORRECT_ANSWERED)
        {
            StartCoroutine(IFlasher.Flash(Global.COLOR_FLASH_GREEN));
            yield return StartCoroutine(ToastFlasher.Flash(Color.white, Global.COLOR_FLASH_GREEN, flasherText));
        }

        else
        {
            StartCoroutine(IFlasher.Flash(Global.COLOR_FLASH_RED));
            yield return StartCoroutine(ToastFlasher.Flash(Color.white, Global.COLOR_FLASH_RED, flasherText));
        }

    }

    public void PositiveFlash(string value)
    {
        StopFlasher();

        StartCoroutine(TFlasher.Flash(Global.COLOR_FLASH_GREEN, "+" + value + " Sekunden"));
        StartCoroutine(IFlasher.Flash(Global.COLOR_FLASH_GREEN));
    }

    public void PositiveFlash(string value, GameModeSubnet.TurnMode mode)
    {
        StopFlasher();

        switch (mode)
        {
            case GameModeSubnet.TurnMode.TIME:
                StartCoroutine(TFlasher.Flash(Global.COLOR_FLASH_GREEN, "+" + value + " Sekunden"));
                StartCoroutine(IFlasher.Flash(Global.COLOR_FLASH_GREEN));
                break;

            case GameModeSubnet.TurnMode.TRIES:
                StartCoroutine(TFlasher.Flash(Global.COLOR_FLASH_GREEN, "+" + value + " Versuch"));
                StartCoroutine(IFlasher.Flash(Global.COLOR_FLASH_GREEN));
                break;
            default:
                break;
        }

    }

    public void NegativFlash(string value)
    {
        StopFlasher();

        StartCoroutine(TFlasher.Flash(Global.COLOR_FLASH_RED, "-" + value + " Sekunden"));
        StartCoroutine(IFlasher.Flash(Global.COLOR_FLASH_RED));
    }

    public void NegativFlash(string value, GameModeSubnet.TurnMode mode)
    {
        StopFlasher();

        switch (mode)
        {
            case GameModeSubnet.TurnMode.TIME:
                StartCoroutine(TFlasher.Flash(Global.COLOR_FLASH_RED, "-" + value + " Sekunden"));
                StartCoroutine(IFlasher.Flash(Global.COLOR_FLASH_RED));
                break;

            case GameModeSubnet.TurnMode.TRIES:
                StartCoroutine(TFlasher.Flash(Global.COLOR_FLASH_RED, "-" + value + " Versuch"));
                StartCoroutine(IFlasher.Flash(Global.COLOR_FLASH_RED));
                break;

            default:
                break;
        }
    }


    public void AddPauseButtonAction(UnityAction pauseButtonAction)
    {
        Header.AddOnPauseAction(pauseButtonAction);
    }

    public void PauseScrollLists()
    {
        foreach (UIScrollList sl in ipScrollLists)
        {
            sl.PauseScrolling();
        }
    }

    public void SetProgressText(int currentQuestionIndex, int amountOfQuestions)
    {
        Header.SetLevelCountText(string.Format("{0} / {1}", currentQuestionIndex + 1, amountOfQuestions));
    }

    public void SetActiveQuestion(SubnetQuestion question)
    {
        SetScrollListButtonText(question);
        SetQuestion(question);
    }

    public void SetScrollListColor(int sListID, Color color)
    {
        ipScrollLists[sListID].SetBackgroundColor(color);
    }

    public void SetAllScrollListsInteractable()
    {
        foreach (UIScrollList sl in ipScrollLists)
        {
            sl.SetInteractable();
        }
    }

    public void SetAllScrollListsNonInteractable()
    {
        foreach (UIScrollList sl in ipScrollLists)
        {
            sl.SetNonInteractable();
        }
    }

    public void SetScrollListInteractable(int sListID)
    {
        ipScrollLists[sListID].SetInteractable();
    }

    public void SetScrollListNonInteractable(int sListID)
    {
        ipScrollLists[sListID].SetNonInteractable();
    }
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UISubnetPlayer : UIMPGamePlayer
{
    [SerializeField]
    private Text usedTimeToSolveText;
    [SerializeField]
    private Text answerdCounter;
    private float usedTimeToSolve;
    private int numberOfQuestions;
    private int numOfCorrectAnswers;

    void Awake()
    {
        SetUsedTimeToSolveText(0);
    }

    public void SetTotalNumberOfQuestions(int numberOfQuestions)
    {
        this.numberOfQuestions = numberOfQuestions;
        numOfCorrectAnswers = 0;
        SetCorrectAnswersText(numOfCorrectAnswers, numberOfQuestions);
    }

    public void IncreaseNumOfCorrectAnswers()
    {
        numOfCorrectAnswers++;
        SetCorrectAnswersText(numOfCorrectAnswers, numberOfQuestions);
    }

    private void SetCorrectAnswersText(int numOfCorrectAnswers, int numOfQuestions)
    {
        answerdCounter.text = string.Format("{0} / {1}", numOfCorrectAnswers, numOfQuestions);
    }

    public void SetUsedTimeToSolveText(float timeToSolve)
    {
        usedTimeToSolve = timeToSolve;
        usedTimeToSolveText.text = Mathf.CeilToInt(timeToSolve).ToString() + " s";
    }

    public float GetUsedTimeToSolve()
    {
        return usedTimeToSolve;
    }

    public void SetNumberOfCorrectAnswers(int numOfCorrectAnswers)
    {
        this.numOfCorrectAnswers = numOfCorrectAnswers;
        SetCorrectAnswersText(numOfCorrectAnswers, numberOfQuestions);
    }
}

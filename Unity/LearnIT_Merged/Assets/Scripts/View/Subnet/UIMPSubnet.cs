﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIMPSubnet : UISubnet {

    [SerializeField]
    private GameObject progressHolderOpponent;
    private UIProgressMarker[] progressMarkersOpponent;

    [SerializeField]
    private Text localPlayerName;
    [SerializeField]
    private Text opponentName;
    [SerializeField]
    private Image localPlayerBackground;
    [SerializeField]
    private Image opponentBackground;

    public override void Setup(UICustomSlider slider, SubnetQuestion question, int numberOfQuestions, UnityAction<int, int, int> callback)
    {
        base.Setup(slider, question, numberOfQuestions, callback);

        RemovePauseButton();

        progressMarkersOpponent = new UIProgressMarker[numberOfQuestions];

        for (int i = 0; i < numberOfQuestions; i++)
        {
            progressMarkersOpponent[i] = Instantiate<UIProgressMarker>();
            progressMarkersOpponent[i].transform.SetParent(progressHolderOpponent.transform, false);
            progressMarkersOpponent[i].SetNumber(i + 1);
        }
    }

    public void UpdateOpponentProgressMarker(int questionIndex, bool correct)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= progressMarkersOpponent.Length);

        if (correct)
            progressMarkersOpponent[questionIndex].SetCorrect();
        else
            progressMarkersOpponent[questionIndex].SetWrong();
    }

    public void UpdateOpponentProgressMarker(int questionIndex)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= progressMarkersOpponent.Length);
        progressMarkersOpponent[questionIndex].SetNumber(questionIndex + 1);
    }

    public void SetupLocalPlayerVisuals(MPGamePlayer localPlayer)
    {
        localPlayerName.text = localPlayer.GetPlayerName();
        localPlayerBackground.color = localPlayer.GetPlayerColor();
    }

    public void SetupRemotePlayerVisuals(MPGamePlayer remotePlayer)
    {
        opponentName.text = remotePlayer.GetPlayerName();
        opponentBackground.color = remotePlayer.GetPlayerColor();
    }



    private void RemovePauseButton()
    {
        Header.RemovePauseButton();
    }
}
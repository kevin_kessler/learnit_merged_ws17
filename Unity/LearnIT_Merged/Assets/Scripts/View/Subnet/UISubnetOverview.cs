﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.ObjectModel;

public class UISubnetOverview : UIGameOverview
{
    [SerializeField]
    private Button solutionButton;
    [SerializeField]
    private Text solutionButtonText;
    [SerializeField]
    private Text colTextCorrect;
    [SerializeField]
    private Text questionTableQuestionText;
    [SerializeField]
    private Text questionTableSolutionText;
    [SerializeField]
    private GameObject questionTablePlayerNameList;
    [SerializeField]
    private GameObject questionTablePlayerNamePrefab;
    [SerializeField]
    private GameObject questionTableResultList;

    private UISubnetResultEntry[] subnetResultEntries;

    private GameModeSubnet.PlayMode playMode;

    protected override void Awake()
    {
        base.Awake();

        colTextCorrect.text = Global.STRING_OVERVIEW_CORRECT;
        questionTableQuestionText.text = Global.STRING_OVERVIEW_QUESTION;
        backToLobbyIndicationText.text = Global.STRING_OVERVIEW_WAITING_FOR_PLAYER;
        solutionButtonText.text = Global.STRING_SHOW_SOLUTION;

        solutionButton.onClick.RemoveAllListeners();
        solutionButton.onClick.AddListener(ToggleSolutions);
    }

    public override void AddPlayerUI(UIMPGamePlayer playerUI)
    {
        base.AddPlayerUI(playerUI);

        GameObject playerNameHolder = Instantiate(questionTablePlayerNamePrefab);
        playerNameHolder.transform.SetParent(questionTablePlayerNameList.transform, false);
        playerNameHolder.SetActive(true);
        Text playerName = playerNameHolder.GetComponent<Text>();
        playerName.text = playerUI.GetPlayerName();
    }

    public override void RemovePlayerUI(UIMPGamePlayer playerUI)
    {
        base.RemovePlayerUI(playerUI);

        if (playMode == GameModeSubnet.PlayMode.COOP)
        {
            for (int i = 0; i < questionTablePlayerNameList.transform.childCount; i++)
            {
                if (playerUI.GetPlayerName().Equals(questionTablePlayerNameList.transform.GetChild(i).GetComponent<Text>().text))
                {
                    Debug.Log("Player: " + playerUI.GetPlayerName());
                    Destroy(questionTablePlayerNameList.transform.GetChild(i).GetComponent<Text>());
                    Destroy(questionTablePlayerNameList.transform.GetChild(i).GetComponent<LayoutElement>());
                    break;
                }
            }
        }

    }

    public void SetController(GameModeSubnet.PlayMode playmode)
    {
        playMode = playmode;
    }

    public void InitQuestionResults(ReadOnlyCollection<SubnetQuestion> questions)
    {
        subnetResultEntries = new UISubnetResultEntry[questions.Count];
        for (int i = 0; i < questions.Count; i++)
        {
            UISubnetResultEntry result = Instantiate<UISubnetResultEntry>();
            result.transform.SetParent(questionTableResultList.transform, false);

            if (playMode == GameModeSubnet.PlayMode.COOP)
            {
                result.SetQuestion(i, questions[i].GetResultQuestionText(), questions[i].GetCorrectIPAddress().ToString(), questions[i].GetAnsweredState(), 1);
            }
            else
            {
            result.SetQuestion(i, questions[i].GetResultQuestionText(), questions[i].GetCorrectIPAddress().ToString(), questions[i].GetAnsweredState(), playerUIs.Count);
            }

            subnetResultEntries[i] = result;
        }
    }

    public void InitQuestionResults(SubnetQuestion[] questions)
    {
        InitQuestionResults(System.Array.AsReadOnly(questions));
    }

    public void UpdateQuestionResult(int questionIndex, SubnetQuestion.AnswerState answer, UISubnetPlayer answeringPlayer)
    {
        int markerIndex = playerUIs.IndexOf(answeringPlayer);

        subnetResultEntries[questionIndex].UpdateMarker(markerIndex, questionIndex, answer);
        if (answer == SubnetQuestion.AnswerState.CORRECT_ANSWERED)
            answeringPlayer.IncreaseNumOfCorrectAnswers();
    }

    public void UpdateQuestionResult(int questionIndex, SubnetQuestion.AnswerState answer)
    {
        subnetResultEntries[questionIndex].UpdateMarker(questionIndex, answer);

        if (answer == SubnetQuestion.AnswerState.CORRECT_ANSWERED)
            for (int i = 0; i < playerUIs.Count; i++)
            {
                ((UISubnetPlayer)playerUIs[i]).IncreaseNumOfCorrectAnswers();
            }
    }

    public void ShowSolutions()
    {
        solutionButtonText.text = Global.STRING_HIDE_SOLUTION;
        questionTableSolutionText.gameObject.SetActive(true);
        for (int i = 0; i < subnetResultEntries.Length; i++)
            subnetResultEntries[i].ShowSolution();
    }

    public void HideSolutions()
    {
        solutionButtonText.text = Global.STRING_SHOW_SOLUTION;
        questionTableSolutionText.gameObject.SetActive(false);
        for (int i = 0; i < subnetResultEntries.Length; i++)
            subnetResultEntries[i].HideSolution();
    }

    private void ToggleSolutions()
    {
        if (questionTableSolutionText.IsActive())
            HideSolutions();
        else
            ShowSolutions();
    }

    public override void Show()
    {
        base.Show();
        solutionButton.gameObject.SetActive(true);
    }

    public override void ShowWithoutFooter()
    {
        base.ShowWithoutFooter();
        solutionButton.gameObject.SetActive(false);
    }
}
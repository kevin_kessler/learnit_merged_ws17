﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UISubnetResultEntry : UIElement
{
    [SerializeField]
    private Text questionNumber;
    [SerializeField]
    private Text questionText;
    [SerializeField]
    private GameObject solutionHolder;
    [SerializeField]
    private Text solutionText;
    [SerializeField]
    private GameObject progressMarkerListHolder;
    [SerializeField]
    private GameObject markerHolderPrefab;
    UIProgressMarker[] progressMarkers;

    public void SetQuestion(int questionIndex, string questionText, string solutionText, SubnetQuestion.AnswerState[] answersInPlayerOrder)
    {
        questionNumber.text = (questionIndex + 1).ToString();
        this.questionText.text = questionText;
        this.solutionText.text = solutionText;

        progressMarkers = new UIProgressMarker[answersInPlayerOrder.Length];
        for (int i = 0; i < progressMarkers.Length; i++)
        {
            progressMarkers[i] = CreateMarker(questionIndex, answersInPlayerOrder[i]);
        }
    }

    public void SetQuestion(int questionIndex, string questionText, string solutionText, SubnetQuestion.AnswerState answer, int numOfMarkers)
    {
        questionNumber.text = (questionIndex + 1).ToString();
        this.questionText.text = questionText;
        this.solutionText.text = solutionText;

        progressMarkers = new UIProgressMarker[numOfMarkers];
        for (int i = 0; i < progressMarkers.Length; i++)
        {
            progressMarkers[i] = CreateMarker(questionIndex, answer);
        }
    }

    private UIProgressMarker CreateMarker(int questionIndex, SubnetQuestion.AnswerState answerState)
    {
        GameObject markerHolder = Instantiate(markerHolderPrefab);
        markerHolder.transform.SetParent(progressMarkerListHolder.transform, false);
        markerHolder.SetActive(true);

        UIProgressMarker progressMarker = Instantiate<UIProgressMarker>(markerHolder.transform);
        UpdateMarker(progressMarker, questionIndex, answerState);

        return progressMarker;
    }

    public void UpdateMarker(int markerIndex, int questionIndex, SubnetQuestion.AnswerState answerState)
    {
        UpdateMarker(progressMarkers[markerIndex], questionIndex, answerState);
    }

    public void UpdateMarker(int questionIndex, SubnetQuestion.AnswerState answerState)
    {
        for (int i = 0; i < progressMarkers.Length; i++)
        {
            UpdateMarker(progressMarkers[i], questionIndex, answerState);
        }
    }

    private void UpdateMarker(UIProgressMarker marker, int questionIndex, SubnetQuestion.AnswerState answerState)
    {
        if (answerState == SubnetQuestion.AnswerState.CORRECT_ANSWERED)
            marker.SetCorrect();
        else if (answerState == SubnetQuestion.AnswerState.WRONG_ANSWERED)
            marker.SetWrong();
        else
            marker.SetNumber(questionIndex + 1);
    }

    public void ShowSolution()
    {
        solutionHolder.SetActive(true);
    }

    public void HideSolution()
    {
        solutionHolder.SetActive(false);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIScrollList : UIElement
{
    private static Color32 BASE_COLOR = new Color32(0, 66, 143, 255);

    [SerializeField]
    private Button[] buttons;

    [SerializeField]
    private GameObject[] listEnds;

    [SerializeField]
    private GameObject listHolder;

    private int sListID;

    private float buttonSize = 0;
    private int[] values;

    private Vector2 movementSpeed;
    private float minSpeed = 50f;
    private float maxSpeed = 90f;
    private bool stopped;

    private bool interactable = false;

    void Awake()
    {
        movementSpeed.y = -Random.Range(minSpeed, maxSpeed);

        SetBackgroundColor(BASE_COLOR);

        values = new int[buttons.Length];
    }

    void Update()
    {
        if (!stopped)
        {
            Movement(Time.deltaTime);
        }
    }

    private void Movement(float deltaTime)
    {
        Transform top = listEnds[0].transform;
        Transform bot = listEnds[1].transform;

        for (int i = 0; i < buttons.Length; i++)
        {
            Transform child = buttons[i].transform;

            if (bot.position.y > child.position.y)
            {
                child.Translate(0, top.position.y - child.position.y, 0);
            }

            else
            {
                child.Translate(movementSpeed * deltaTime);
            }

            ChangeOpacity(child, top, bot);
        }
    }

    private void ChangeOpacity(Transform child, Transform top, Transform bot)
    {
        Color newTextAlpha;
        Color newButtonAlpha;
        float mid = top.position.y - ((top.position.y - bot.position.y) / 2);

        if (child.position.y > mid)
        {
            newTextAlpha = new Color(
                child.GetChild(0).GetComponent<Text>().color.r,
                child.GetChild(0).GetComponent<Text>().color.g,
                child.GetChild(0).GetComponent<Text>().color.b,
                (float)DoubleMap(child.position.y, top.position.y, mid, 0, 1)
                );

            newButtonAlpha = new Color(
                child.GetComponent<Image>().color.r,
                child.GetComponent<Image>().color.g,
                child.GetComponent<Image>().color.b,
                (float)DoubleMap(child.position.y, top.position.y, mid, 0, 1)
                );
        }

        else if (child.position.y < mid)
        {
            newTextAlpha = new Color(
                child.GetChild(0).GetComponent<Text>().color.r,
                child.GetChild(0).GetComponent<Text>().color.g,
                child.GetChild(0).GetComponent<Text>().color.b,
                (float)DoubleMap(child.position.y, mid, bot.position.y, 1, 0)
                );

            newButtonAlpha = new Color(
                child.GetComponent<Image>().color.r,
                child.GetComponent<Image>().color.g,
                child.GetComponent<Image>().color.b,
                (float)DoubleMap(child.position.y, mid, bot.position.y, 1, 0)
                );
        }

        else
        {
            newTextAlpha = new Color(
                child.GetChild(0).GetComponent<Text>().color.r,
                child.GetChild(0).GetComponent<Text>().color.g,
                child.GetChild(0).GetComponent<Text>().color.b,
                1
                );

            newButtonAlpha = new Color(
                child.GetComponent<Image>().color.r,
                child.GetComponent<Image>().color.g,
                child.GetComponent<Image>().color.b,
                1
                );
        }

        child.GetChild(0).GetComponent<Text>().color = newTextAlpha;
        child.GetComponent<Image>().color = newButtonAlpha;
    }

    private float GetButtonSize()
    {
        if (buttonSize == 0 && buttons.Length > 0)
        {
            buttonSize = buttons[0].GetComponent<RectTransform>().rect.height;
        }
        return buttonSize;
    }

    private double DoubleMap(double x, double in_min, double in_max, double out_min, double out_max)
    {
        if (in_max == in_min)
        {
            return 0;
        }
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    public void SetID(int id)
    {
        sListID = id;
    }

    public void SetInteractable()
    {
        interactable = true;
    }

    public void SetNonInteractable()
    {
        interactable = false;
    }

    public void SetBackgroundColor(Color playerColor)
    {
        SetBackgroundPlayerColorActive();
        listHolder.GetComponent<RawImage>().color = playerColor;
    }

    public void SetBackgroundPlayerColorActive()
    {
        if (!listHolder.GetComponent<RawImage>().isActiveAndEnabled)
        {
            listHolder.GetComponent<RawImage>().enabled = true;
        }
    }

    public void SetBackgroundPlayerColorInactive()
    {
        listHolder.GetComponent<RawImage>().enabled = false;
    }

    public void PauseScrolling()
    {
        stopped = true;
        interactable = false;
        foreach (Button b in buttons)
        {
            b.interactable = false;
        }
    }

    public void ContinueScrolling()
    {
        movementSpeed.y = -Random.Range(minSpeed, maxSpeed);
        stopped = false;

        foreach (Button b in buttons)
        {
            b.interactable = true;
        }
    }

    public int[] GetValues()
    {
        return values;
    }

    public Button[] GetButtons()
    {
        return buttons;
    }

    public void InputIsValid(int value)
    {
        PauseScrolling();

        foreach (Button b in buttons)
        {
            Text tmp = b.transform.GetChild(0).GetComponent<Text>();
            if (tmp.text == value.ToString())
            {
                tmp.color = new Color(Color.green.r, Color.green.g, Color.green.b);
            }
        }

    }

    public void InputIsNotValid(int value)
    {
        foreach (Button b in buttons)
        {
            Text tmp = b.transform.GetChild(0).GetComponent<Text>();
            if (tmp.text == value.ToString())
            {
                tmp.color = new Color(Color.red.r, Color.red.g, Color.red.b);
                b.interactable = false;
            }
        }
    }

    public void SetTextOfButton(int row, string text)
    {
        Text tmp = buttons[row].transform.GetChild(0).GetComponent<Text>();
        tmp.text = text;
        tmp.color = Color.white;
        values[row] = int.Parse(tmp.text);
    }

    public void EnterHoverTextColor(Button self)
    {
        if (interactable && self.IsInteractable())
        {
            self.GetComponentInChildren<Text>().color = Color.yellow;
        }
    }

    public void LeaveHoverTextColor(Button self)
    {
        if (interactable && self.IsInteractable())
        {
            self.GetComponentInChildren<Text>().color = Color.white;
        }
    }

    public void BindButtonListener(int part, UnityAction<int, int, int> callback)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            int value = values[i];
            buttons[i].onClick.RemoveAllListeners();
            buttons[i].onClick.AddListener(() => { callback(value, part, sListID); });
        }
    }
}
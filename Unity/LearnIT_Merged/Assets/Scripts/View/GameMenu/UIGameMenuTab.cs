﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Utils;

public class UIGameMenuTab : UIElement {
    private UIGameMenuTabContent tabContent;

    [SerializeField]
    private Button tabButton;

    public UIGameMenuTabContent GetTabContent()
    {
        return tabContent;
    }

    public Button GetTabButton()
    {
        return tabButton;
    }

    void Awake()
    {
        tabContent = UIElement.Instantiate<UIGameMenuTabContent>(this.transform);
        tabContent.transform.SetAsFirstSibling();
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class UIGameMenu : UIElement
{
    [SerializeField] private GameObject menuHeaderHolder;
    private UIGameMenuHeader menuHeader;
    private UIGameMenuTabHolder tabHolder;

    public UIGameMenuHeader GetMenuHeader()
    {
        return menuHeader;
    }

    public UIGameMenuTabHolder GetTabHolder()
    {
        return tabHolder;
    }

    void Awake()
    {
        menuHeader = UIElement.Instantiate<UIGameMenuHeader>(menuHeaderHolder.transform);
        menuHeader.transform.SetAsFirstSibling();

        tabHolder = UIElement.Instantiate<UIGameMenuTabHolder>(this.transform);
    }
}

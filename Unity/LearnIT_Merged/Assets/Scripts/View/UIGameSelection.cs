﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class UIGameSelection : UIElement
{
    private UIGameMenuHeader menuHeader;

    [SerializeField] private GameObject gameButtonsHolder;
    [SerializeField] private GameObject gameButtonPrefab;

    private SceneLoader sceneLoader;

    public UIGameMenuHeader GetMenuHeader()
    {
        return menuHeader;
    }

    void Awake()
    {
        sceneLoader = gameObject.AddComponent<SceneLoader>();
        InitHeader();
    }

    private void InitHeader()
    {
        //setup header prefab
        menuHeader = UIElement.Instantiate<UIGameMenuHeader>(this.transform);
        menuHeader.transform.SetAsFirstSibling();
        menuHeader.SetTitleText(Global.STRING_APPLICATION_TITLE);

        //find the left buttons image child and change its sprite to the quit sprite
        Button leftButton = menuHeader.GetLeftButton();
        Sprite quitSprite = Resources.Load<Sprite>(Global.PATH_ICON_QUIT); ;
        Image[] imageComponents = leftButton.GetComponentsInChildren<Image>();
        foreach (Image img in imageComponents)
        {
            if (leftButton.transform.Equals(img.gameObject.transform.parent))
            {
                img.sprite = quitSprite;
            }
        }

        //change left buttons default behaviour
        leftButton.onClick.RemoveAllListeners();
        leftButton.onClick.AddListener(sceneLoader.QuitApplication);
    }

    public Button AddGame(Game game)
    {
        //instantiate game button
        Button listItem = Instantiate(gameButtonPrefab).GetComponent<Button>();
        DebugUtils.Assert(null != listItem, DebugUtils.GenerateComponentMissingMsg(() => gameButtonPrefab, typeof(Button)));
        listItem.transform.SetParent(gameButtonsHolder.transform, false);

        //set text
        Text buttonText = listItem.GetComponentInChildren<Text>();
        DebugUtils.Assert(null != buttonText, DebugUtils.GenerateComponentInChildrenMissingMsg(() => listItem, typeof(Text)));
        buttonText.text = game.GetName();

        return listItem;
    }
}

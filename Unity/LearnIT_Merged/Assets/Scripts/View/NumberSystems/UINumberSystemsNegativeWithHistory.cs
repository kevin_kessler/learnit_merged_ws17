﻿using UnityEngine;
using System.Collections;

public class UINumberSystemsNegativeWithHistory : UINumberSystemsWithHistory
{
	public override void CreateNumPad(NumberCreator creator)
	{
		if (creator.getRightSystem() > 10)
		{
			numpad = UIElement.Instantiate<UINumPadHexa>(numpadHolder.transform);
		}
		else
		{
			numpad = UIElement.Instantiate<UINumpadNegative>(numpadHolder.transform);
		}
		numpad.SetInputField(rightInputField);
	}
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Events;

public class UIGameEndingDialog : UIInfoDialog {

    [SerializeField]
    private Text headerAsked;
    [SerializeField]
    private Text headerAnswered;
    [SerializeField]
    private Text headerCorrect;
    [SerializeField]
    private GameObject answerListHolder;
    protected List<UIAnswersEntry> answers;

    // Use this for initialization
    protected override void Awake()
    {
        

        base.Awake();
        answers = new List<UIAnswersEntry>();
        headerAnswered.text = Global.ANSWER_TEXT;
        headerAsked.text = Global.ASKED_TEXT;
        headerCorrect.text = Global.CORRECT_TEXT;
    }

    public void AddAnswerUIs(List<UIAnswersEntry> answerUIs)
    {
        foreach(UIAnswersEntry answerUI in answerUIs)
        {
            
            UIAnswersEntry a = UIElement.Instantiate<UIAnswersEntry>(answerListHolder.transform);
            a.transform.SetAsFirstSibling();
            a.AskedNumber.text = answerUI.AskedNumber.text;
            a.AnsweredNumber.text = answerUI.AnsweredNumber.text;
            a.CorrectNumber.text = answerUI.CorrectNumber.text;
            if (a.AnsweredNumber.text == a.CorrectNumber.text)
            {
                a.BackgroundColor = Color.green;
            }
            else
            {
                a.BackgroundColor = Color.red;
            }
        }
    }

    public void Setup(string dialogTitle, string infoText, List<UIAnswersEntry> answerUIs, Sprite infoSprite = null,  UnityAction onCloseCallback = null)
    {
        AddAnswerUIs(answerUIs);
        base.Setup(dialogTitle, infoText, infoSprite, onCloseCallback);
    }
}

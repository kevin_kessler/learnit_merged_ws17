﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class MPUINumberSystems : UIElement {

	[SerializeField] GameObject numpadHolder;
	[SerializeField] Button submitButton;
	[SerializeField] GameObject headerHolder;
	[SerializeField] InputField leftInputField;
	[SerializeField] InputField rightInputField;
	[SerializeField] Text leftNumberSystem;
	[SerializeField] Text rightNumberSystem;

	private UIScoreBar scoreBar;
	private UIGameHeaderWithSlider headerWithSlider;
	private UINumPad numpad;

	private UITextFlasher textFlasher;
	private UIImageFlasher screenFlasher;

	public void SetLeftSystemName(string name)
	{
		leftNumberSystem.text = name;
	}

	public void SetRightSystemName(string name)
	{
		rightNumberSystem.text = name;
	}

	public UINumPad GetNumPad(){
		return numpad;
	}

	public void CreateNumPad(NumberCreator creator)
	{
		if (creator.getRightSystem () > 10)
		{
			numpad = UIElement.Instantiate<UINumPadHexa> (numpadHolder.transform);
		}
		else
		{
			numpad = UIElement.Instantiate<UINumPad> (numpadHolder.transform);
		}
		numpad.SetInputField (rightInputField);
	}
		
	public Button GetSubmitButon(){
		return submitButton;
	}

	public InputField GetInputFieldLeft(){
		return leftInputField;
	}

	public InputField GetInputFieldRight(){
		return rightInputField;
	}

	public UIScoreBar GetSlider()
	{
		return scoreBar;
	}

	public UIGameHeaderWithSlider GetHeader()
	{
		return headerWithSlider;
	}

	public void SetSlider(UIScoreBar slider)
	{
		scoreBar = slider;
		headerWithSlider.SetSlider (scoreBar);
	}

	void Awake()
	{
		headerWithSlider = UIElement.Instantiate<UIGameHeaderWithSlider> (headerHolder.transform);

		textFlasher = UIElement.Instantiate<UITextFlasher> (this.transform);
		textFlasher.transform.SetAsLastSibling ();
		screenFlasher = UIElement.Instantiate<UIImageFlasher> (this.transform);
		screenFlasher.transform.SetAsLastSibling ();

		headerWithSlider.SetTitleText(Global.NUMBERSYSTEMS_VS_TITLE_TEXT);
		headerWithSlider.SetPauseButtonImage(Resources.Load<Sprite>(Global.PATH_ICON_EXIT));
	}
		

	void Update()
	{
		submitButton.interactable = rightInputField.text.Length > 0;
	}

	public void AddPauseButtonaction(UnityAction pauseButtonAction)
	{
		headerWithSlider.AddOnPauseAction (pauseButtonAction);
	}

	public UITextFlasher GetTextFlasher()
	{
		return textFlasher;
	}

	public UIImageFlasher GetScreenFlasher()
	{
		return screenFlasher;
	}


}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class UINumberSystems : UIElement {

    [SerializeField] private GameObject numpadHolder;
    private UINumPad numpad;
    [SerializeField] private InputField leftInputField;
    [SerializeField] private InputField rightInputField;
    [SerializeField] private Button submitButton;
	[SerializeField] private GameObject headerHolder;
	private UIGameHeaderWithSlider gameHeaderWithSliderUI;
	[SerializeField] private Text numberSystemLeftText;
	[SerializeField] private Text numberSystemRightText;





    private UITextFlasher textFlasherUI;
    private UIImageFlasher screenFlasher;


    // Use this for initialization
    void Awake ()
	{
		gameHeaderWithSliderUI = UIElement.Instantiate<UIGameHeaderWithSlider> (headerHolder.transform);
        textFlasherUI = UIElement.Instantiate<UITextFlasher>(this.transform);
        textFlasherUI.transform.SetAsLastSibling();
        screenFlasher = UIElement.Instantiate<UIImageFlasher>(this.transform);
        screenFlasher.transform.SetAsLastSibling();
    }
	
	// Update is called once per frame
	void Update ()
    {
		submitButton.interactable = rightInputField.text.Length > 0;
	}


	public void AddPauseButtonAction(UnityAction pauseButtonAction){
		gameHeaderWithSliderUI.AddOnPauseAction (pauseButtonAction);
	}

	public void SetLeftNumberSystemName(string name)
	{
		this.numberSystemLeftText.text = name;
	}

	public void SetRightNumberSystemName(string name)
	{
		this.numberSystemRightText.text = name;
	}

    public UINumPad GetNumPad()
    {
        return numpad;
    }

	public void CreateNumPad(NumberCreator creator)
	{
		if (creator.getRightSystem () > 10)
		{
			numpad = UIElement.Instantiate<UINumPadHexa> (numpadHolder.transform);
		}
		else
		{
			numpad = UIElement.Instantiate<UINumPad> (numpadHolder.transform);
		}
		numpad.SetInputField(rightInputField);
	}

    public Button GetSubmitButton()
    {
        return submitButton;
    }

    public InputField GetInputFieldLeft()
    {
        return leftInputField;
    }

    public InputField GetInputFieldRight()
    {
        return rightInputField;
    }

    public UIImageFlasher GetScreenFlasher()
    {
        return screenFlasher;
    }

    public UITextFlasher GetTextFlasher()
    {
        return textFlasherUI;
    }

	public void SetGameSlider(UICustomSlider slider)
	{
		gameHeaderWithSliderUI.SetSlider(slider);
	}

	public void SetTitleText( string s){
		gameHeaderWithSliderUI.SetTitleText (s);
	}
		
	public void SetGameLevel(int currentLevel, int maxLevel){
		gameHeaderWithSliderUI.SetLevelText (Global.LEVEL_TEXT);
		gameHeaderWithSliderUI.SetLevelCountText (currentLevel + " / " + maxLevel);
	}

	public void SetGameLevel(int currentLevel){
		gameHeaderWithSliderUI.SetLevelText (Global.LEVEL_TEXT);
		gameHeaderWithSliderUI.SetLevelCountText ("" + currentLevel);
	}
}

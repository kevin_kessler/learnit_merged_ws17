﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class MPUINumberSystemsCOOP : UIElement
{
    [SerializeField]
    GameObject numpadHolder;
    [SerializeField]
    GameObject headerHolder;
    [SerializeField]
    Button submitButton;
	[SerializeField]
	Button swapButton;
    [SerializeField]
    InputField upperInputField;
    [SerializeField]
    InputField leftInputField;
    [SerializeField]
    InputField rightInputField;
    [SerializeField]
    Text upperNumberSystem;
    [SerializeField]
    Text leftNumberSystem;
    [SerializeField]
    Text rightNumberSystem;
	[SerializeField]
	GameObject jokerHolder;
	[SerializeField]
	Image inputDone;
	[SerializeField]
	Image inputWaiting;
	[SerializeField]
	Image inputLocalDone;
	[SerializeField]
	Image inputLocalWrong;
	[SerializeField]
	Image inputLocalSwap;
	[SerializeField]
	Text textWaiting;




	private bool unlocked = true;
	private volatile bool waiting = false;

    private UICustomSlider slider;
    private UIGameHeaderWithSlider headerWithSlider;
    private UINumPad numpad;

    private UITextFlasher textFlasher;
    private UIImageFlasher screenFlasher;

	private MPNumberSystemsCoopController controller;

	public Image GetInputDone(){
		return inputDone;
	}

	public Image GetInputSwap(){
		return inputLocalSwap;
	}

	public Image GetLocalWrong(){
		return inputLocalWrong;
	}

	public Image GetLocalDone(){
		return inputLocalDone;
	}
	public Image GetInputWaiting(){
		return inputWaiting;
	}

	public GameObject GetJokerHolder()
	{
		return jokerHolder;
	}

	public void SetNumberSystemController(MPNumberSystemsCoopController controller)
	{
		this.controller = controller;
	}

	private string lastText = "";

	public bool Unlocked
	{
		get
		{
			return unlocked;
		}
		set
		{
			unlocked = value;
		}
	}

	public bool Waiting
	{
		get
		{
			return waiting;
		}
		set
		{ 
			waiting = value;
		}
	}

	public Text GetTextWaiting(){
		return textWaiting;
	}

    public void SetLeftSystemName(string name)
    {
        leftNumberSystem.text = name;
    }

	public string GetLeftSystemName()
	{
		return leftNumberSystem.text;
	}

    public void SetRightSystemName(string name)
    {
        rightNumberSystem.text = name;
    }

	public string GetRightSystemName()
	{
		return rightNumberSystem.text;
	}

    public void SetUpperSystemName(string name)
    {
        upperNumberSystem.text = name;
    }

    public UINumPad GetNumPad()
    {
        return numpad;
    }

    public void CreateNumPad()
    {
        numpad = UIElement.Instantiate<UINumPadHexa>(numpadHolder.transform);
        numpad.SetInputField(rightInputField);
    }

    public Button GetSubmitButton()
    {
        return submitButton;
    }

	public Button GetSwapButton()
	{
		return swapButton;
	}

    public InputField GetInputFieldLeft()
    {
        return leftInputField;
    }

    public InputField GetInputFieldRight()
    {
        return rightInputField;
    }

    public InputField GetInputFieldTop()
    {
        return upperInputField;
    }

    public UICustomSlider GetSlider()
    {
        return slider;
    }

    public UIGameHeaderWithSlider GetHeader()
    {
        return headerWithSlider;
    }

    public void setSlider(UICustomSlider slider)
    {
        this.slider = slider;
        headerWithSlider.SetSlider(this.slider);
    }

    void Awake()
    {
        headerWithSlider = UIElement.Instantiate<UIGameHeaderWithSlider>(headerHolder.transform);

        textFlasher = UIElement.Instantiate<UITextFlasher>(this.transform);
        textFlasher.transform.SetAsLastSibling();
        screenFlasher = UIElement.Instantiate<UIImageFlasher>(this.transform);
        screenFlasher.transform.SetAsLastSibling();
     
        headerWithSlider.SetTitleText(Global.NUMBERSYSTEMS_COOP_TITLE_TEXT);
        headerWithSlider.SetPauseButtonImage(Resources.Load<Sprite>(Global.PATH_ICON_EXIT));

        CreateNumPad();
    }

    public void AddPauseButtonAction(UnityAction pauseButtonAction)
    {
        headerWithSlider.AddOnPauseAction(pauseButtonAction);
    }

    void Update()
    {
		if (unlocked) {
			submitButton.interactable = rightInputField.text.Length > 0;
			if (lastText != rightInputField.text)
			{
				controller.UpdateInputContent (rightInputField.text);
				lastText = rightInputField.text;
			}
		}
    }

    public UITextFlasher GetTextFlasher()
    {
        return textFlasher;
    }

    public UIImageFlasher GetScreenFlasher()
    {
        return screenFlasher;
    }
}

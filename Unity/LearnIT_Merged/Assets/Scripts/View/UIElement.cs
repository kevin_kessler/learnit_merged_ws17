﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Utils;

public abstract class UIElement : MonoBehaviour
{
    /// <summary>
    /// This requires the name of the prefab residing in PATH_PREFABS_DIR to be equivalent to the specific UIElement class name
    /// and requires the prefab to reside in the PATH_PREFABS_DIR.
    /// The optional name parameter can be specified to instantiate the gameobject with a specific name which will be displayed in hierarchy.
    /// </summary>
    public static T Instantiate<T>(Transform parent = null, string name = null) where T : UIElement
    {
        string prefabPath = Global.PATH_PREFABS_DIR + typeof(T).Name;
        return Instantiate<T>(prefabPath, parent, name);
    }

    public static T Instantiate<T>(string prefabPath, Transform parent = null,  string name = null) where T : UIElement
    {
        //load prefab
        GameObject prefab = Resources.Load<GameObject>(prefabPath);
        if (null == prefab)
        {
            Debug.LogError("Couldn't Instantiate " + typeof(T).Name + " - no prefab was found at " + prefabPath);
            return null;
        }

        return Instantiate<T>(prefab, parent, name);
    }

    public static T Instantiate<T>(GameObject prefab, Transform parent = null, string name = null) where T : UIElement
    {
        //instantiate
        T uiElement = Instantiate(prefab).GetComponent<T>();

        DebugUtils.Assert(null != uiElement, DebugUtils.GenerateComponentMissingMsg(() => prefab, typeof(T)));

        if (null != name)
            uiElement.name = name;

        if (null != parent)
            uiElement.transform.SetParent(parent, false);

        return uiElement;
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}

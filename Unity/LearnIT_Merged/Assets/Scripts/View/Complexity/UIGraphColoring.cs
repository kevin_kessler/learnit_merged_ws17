﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;
using System.Collections;
using UnityEngine.EventSystems;

public class UIGraphColoring : UIElement
{
    //Header
    [SerializeField] private GameObject headerHolder;
    protected UIGameHeaderWithSlider gameHeaderWithSliderUI;

    [SerializeField] private Image graphImage;
    private UnityAction<PointerEventData, RectTransform> onGraphImageClick;
    private UnityAction<Color> onColorButtonClick;
    private UnityAction onClearButtonClick;
    private UnityAction onEraseButtonClick;

    //Footer
    [SerializeField] protected GameObject colorButtonHolder;
    [SerializeField] private Button colorButtonPrefab;
    [SerializeField] private Color colorButtonTextureColorToReplace;
    [SerializeField] private Color colorButtonActiveColor;
    [SerializeField] private Color colorButtonInactiveColor;
    [SerializeField] protected Button eraseButton;
    [SerializeField] protected Button clearButton;
    protected Dictionary<Color,Button> colorButtons = new Dictionary<Color,Button>();

    void Awake()
    {
        gameHeaderWithSliderUI = UIElement.Instantiate<UIGameHeaderWithSlider>(headerHolder.transform);
        gameHeaderWithSliderUI.SetTitleText(Global.STRING_CP_TITLE);
        eraseButton.onClick.AddListener(() => { if (null != onEraseButtonClick) onEraseButtonClick(); SetColorButtonActive(eraseButton, colorButtons); });

        if(null!=clearButton)
            clearButton.onClick.AddListener(() => { if (null != onClearButtonClick) onClearButtonClick(); });
    }

    public void SetTitle(string title)
    {
        gameHeaderWithSliderUI.SetTitleText(title);
    }

    public virtual void SetGraph(GCGraph graph)
    {
        graphImage.sprite = graph.GetImage();

        //clear color buttons and add new ones
        colorButtons.Clear();
        foreach (Transform child in colorButtonHolder.transform)
        {
            if(child.gameObject.activeSelf)
                Destroy(child.gameObject);
        }

        colorButtons.Add(graph.GetDefaultNodeColor(), eraseButton);
        foreach (Color color in graph.GetUsableColors())
        {
            Button colorButton = CreateColorButton(color);
            colorButton.transform.SetParent(colorButtonHolder.transform, false);
            colorButtons.Add(color, colorButton);
        }
    }

    protected Button CreateColorButton(Color color)
    {
        Button colorButton = Instantiate(colorButtonPrefab);
        colorButton.gameObject.SetActive(true);
        colorButton.onClick.AddListener(() => { if (null != onColorButtonClick) onColorButtonClick(color); SetColorButtonActive(color); });

        Image[] buttonImages = colorButton.GetComponentsInChildren<Image>();
        Image buttonImage = null;
        foreach (Image img in buttonImages)
            if (img.gameObject.name.Equals("ColorButtonImage"))
                buttonImage = img;

        if(null == buttonImage)
            return colorButton;

        Texture2D copiedImage = AssetUtils.CopyTexture(buttonImage.sprite.texture);
        AssetUtils.ReplaceTextureColor(copiedImage, colorButtonTextureColorToReplace, color);
        buttonImage.sprite = Sprite.Create(copiedImage, new Rect(0, 0, copiedImage.width, copiedImage.height), new Vector2(0.5f, 0.5f));
    
        return colorButton;
    }

    public void SetColorButtonActive(Color color)
    {
        if (!colorButtons.ContainsKey(color))
            return;

        SetColorButtonActive(colorButtons[color], colorButtons);
    }

    protected void SetColorButtonActive(Button buttonToActivate, Dictionary<Color, Button> containingList)
    {
        //visually activate the given button
        Image background = buttonToActivate.GetComponent<Image>();
        background.color = colorButtonActiveColor;

        //visually deactivate all others
        foreach (Button colorButton in containingList.Values)
        {
            if (colorButton.Equals(buttonToActivate))
                continue;

            background = colorButton.GetComponent<Image>();
            background.color = colorButtonInactiveColor;
        }
    }

    public void SetOnGraphImageClickListener(UnityAction<PointerEventData, RectTransform> imageClickListener)
    {
        if (null == imageClickListener)
            return;

        onGraphImageClick = imageClickListener;
    }

    public void SetOnColorButtonClickListener(UnityAction<Color> colorClickListener)
    {
        if (null == colorClickListener)
            return;

        onColorButtonClick = colorClickListener;
    }

    public void SetOnClearButtonClickListener(UnityAction clearButtonListener)
    {
        if (null == clearButtonListener)
            return;

        onClearButtonClick = clearButtonListener;
    }

    public void SetOnEraseButtonClickListener(UnityAction eraseButtonListener)
    {
        if (null == eraseButtonListener)
            return;

        onEraseButtonClick = eraseButtonListener;
    }

    public void AddPauseButtonAction(UnityAction pauseButtonAction)
    {
        gameHeaderWithSliderUI.AddOnPauseAction(pauseButtonAction);
    }
    public void SetPauseButtonImage(Sprite image)
    {
        gameHeaderWithSliderUI.SetPauseButtonImage(image);
    }

    //linked to onClick event of image event trigger in unity editor
    public void OnImageClick(BaseEventData data)
    {
        if (onGraphImageClick == null)
            return;

        PointerEventData pointerData = (PointerEventData)data;
        RectTransform imageRect = graphImage.gameObject.GetComponent<RectTransform>();
        onGraphImageClick.Invoke(pointerData, imageRect);
    }

    public void SetLevelProgressText(int currentLevel, int maxLevel)
    {
        gameHeaderWithSliderUI.SetLevelCountText(string.Format("{0} / {1}", currentLevel, maxLevel));
    }

    public void SetGameSlider(UICustomSlider slider)
    {
        gameHeaderWithSliderUI.SetSlider(slider);
    }
}

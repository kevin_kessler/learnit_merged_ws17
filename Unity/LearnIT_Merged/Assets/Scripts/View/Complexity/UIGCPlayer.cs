﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UIGCPlayer : UIMPGamePlayer
{
    [SerializeField] private Text correctGraphsText;
    [SerializeField] private Text numOfColorizationsText;
    [SerializeField] private Text numOfMistakesText;
    [SerializeField] private Text usedTimeToSolveText;
    private int numOfGraphs;
    private int numOfCorrectGraphs;

    void Awake()
    {
        SetCorrectGraphsText(0,0);
        SetNumOfColorizationsText(0);
        SetNumOfMistakesText(0);
        SetUsedTimeToSolveText(0);
    }

    public void SetTotalNumberOfGraphs(int numOfGraphs)
    {
        this.numOfGraphs = numOfGraphs;
        this.numOfCorrectGraphs = 0;
        SetCorrectGraphsText(numOfCorrectGraphs, numOfGraphs);
    }

    public void SetNumOfCorrectGraphs(int numOfCorrectGraphs)
    {
        this.numOfCorrectGraphs = numOfCorrectGraphs;
        SetCorrectGraphsText(numOfCorrectGraphs, numOfGraphs);
    }

    public void IncreaseNumOfCorrectGraphs()
    {
        numOfCorrectGraphs++;
        SetCorrectGraphsText(numOfCorrectGraphs, numOfGraphs);
    }

    private void SetCorrectGraphsText(int numOfCorrectGraphs, int numOfGraphs)
    {
        this.correctGraphsText.text = string.Format("{0} / {1}", numOfCorrectGraphs, numOfGraphs);
    }

    public void SetNumOfColorizationsText(int num)
    {
        this.numOfColorizationsText.text = num.ToString();
    }

    public void SetNumOfMistakesText(int num)
    {
        this.numOfMistakesText.text = num.ToString();
    }

    public void SetUsedTimeToSolveText(float usedTime)
    {
        this.usedTimeToSolveText.text = Mathf.CeilToInt(usedTime).ToString() + " s";
    }
}

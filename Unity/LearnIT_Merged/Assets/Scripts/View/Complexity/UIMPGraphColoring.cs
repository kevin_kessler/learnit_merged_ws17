﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;
using System.Collections;
using UnityEngine.EventSystems;

public class UIMPGraphColoring : UIGraphColoring
{
    //Local Progress Area
    [SerializeField]private GameObject localProgressHolder;
    private UIProgressMarker[] localProgressMarkers;

    //Progress Area of Opponent
    [SerializeField] private GameObject remoteProgressHolder;
    private UIProgressMarker[] remoteProgressMarkers;

    [SerializeField]
    private Text localPlayerName;
    [SerializeField]
    private Text opponentName;
    [SerializeField]
    private Image localPlayerBackground;
    [SerializeField]
    private Image opponentBackground;

    public void UpdateLocalProgressMarker(int questionIndex, bool correct)
    {
        UpdateProgressMarker(questionIndex, correct, ref localProgressMarkers);
    }

    public void UpdateRemoteProgressMarker(int questionIndex, bool correct)
    {
        UpdateProgressMarker(questionIndex, correct, ref remoteProgressMarkers);
    }

    private void UpdateProgressMarker(int graphIndex, bool correct, ref UIProgressMarker[] progressMarkerCollection)
    {
        Debug.Assert(graphIndex >= 0 && graphIndex <= localProgressMarkers.Length);

        if (correct)
            progressMarkerCollection[graphIndex].SetCorrect();
        else
            progressMarkerCollection[graphIndex].SetWrong();
    }

    public void UpdateLocalProgressMarker(int questionIndex)
    {
        UpdateProgressMarker(questionIndex, ref localProgressMarkers);
    }

    public void UpdateRemoteProgressMarker(int questionIndex)
    {
        UpdateProgressMarker(questionIndex, ref remoteProgressMarkers);        
    }

    private void UpdateProgressMarker(int questionIndex, ref UIProgressMarker[] progressMarkerCollection)
    {
        Debug.Assert(questionIndex >= 0 && questionIndex <= remoteProgressMarkers.Length);
        progressMarkerCollection[questionIndex].SetNumber(questionIndex + 1);
    }

    public void SetupLocalPlayerVisuals(MPGamePlayer localPlayer, int amountOfGraphs)
    {
        localPlayerName.text = localPlayer.GetPlayerName();
        localPlayerBackground.color = localPlayer.GetPlayerColor();

        localProgressMarkers = new UIProgressMarker[amountOfGraphs];
        for (int i = 0; i < amountOfGraphs; i++)
        {
            localProgressMarkers[i] = UIElement.Instantiate<UIProgressMarker>(localProgressHolder.transform);
            localProgressMarkers[i].SetNumber(i + 1);
        }
    }

    public void SetupRemotePlayerVisuals(MPGamePlayer remotePlayer, int amountOfGraphs)
    {
        opponentName.text = remotePlayer.GetPlayerName();
        opponentBackground.color = remotePlayer.GetPlayerColor();

        remoteProgressMarkers = new UIProgressMarker[amountOfGraphs];
        for (int i = 0; i < amountOfGraphs; i++)
        {
            remoteProgressMarkers[i] = UIElement.Instantiate<UIProgressMarker>(remoteProgressHolder.transform);
            remoteProgressMarkers[i].SetNumber(i + 1);
        }
    }

    public void RemovePauseButton()
    {
        gameHeaderWithSliderUI.RemovePauseButton();
    }
}

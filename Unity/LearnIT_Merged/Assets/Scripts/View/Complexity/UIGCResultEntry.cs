﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UIGCResultEntry : UIElement
{
    [SerializeField] private Text graphNumber;
    [SerializeField] private Image graphImage;
    [SerializeField] private Button solutionButton;
    [SerializeField] private Text solutionButtonText;
    [SerializeField] private GameObject progressMarkerListHolder;
    [SerializeField] private GameObject markerHolderPrefab;
    private UIProgressMarker[] progressMarkers;

    private UnityAction<int> onSolutionButtonClick; //int param = graphIndex

    public void SetGraph(int graphIndex, Sprite graphImage, int numOfMarkers)
    {
        this.graphNumber.text = (graphIndex + 1).ToString();
        this.graphImage.sprite = graphImage;
        this.solutionButton.onClick.RemoveAllListeners();
        this.solutionButton.onClick.AddListener(() => { if (null != onSolutionButtonClick) onSolutionButtonClick(graphIndex); });

        progressMarkers = new UIProgressMarker[numOfMarkers];
        for(int i=0; i<progressMarkers.Length; i++)
        {
            progressMarkers[i] = CreateMarker(graphIndex);
        }
    }

    public void SetOnSolutionButtonHandler(UnityAction<int> solutionButtonHandler)
    {
        if (null == solutionButtonHandler)
            return;
        onSolutionButtonClick = solutionButtonHandler;
    }

    private UIProgressMarker CreateMarker(int graphIndex)
    {
        GameObject markerHolder = Instantiate(markerHolderPrefab);
        markerHolder.transform.SetParent(progressMarkerListHolder.transform, false);
        markerHolder.SetActive(true);

        UIProgressMarker progressMarker = UIElement.Instantiate<UIProgressMarker>(markerHolder.transform);
        progressMarker.SetNumber(graphIndex + 1);

        return progressMarker;
    }

    public void UpdateMarker(int markerIndex, int graphIndex, bool success)
    {
        UpdateMarker(progressMarkers[markerIndex], graphIndex, success);
    }

    private void UpdateMarker(UIProgressMarker marker, int questionIndex, bool success)
    {
        if (success)
            marker.SetCorrect();
        else
            marker.SetWrong();
    }

    public void ShowSolution()
    {
        solutionButton.gameObject.SetActive(true);
    }

    public void HideSolution()
    {
        solutionButton.gameObject.SetActive(false);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIModulo : UIElement {

	[SerializeField]
	private GameObject headerholder;
	[SerializeField]
	private GameObject numpadholder;
	[SerializeField]
	private AutoGridLayout grid;
	[SerializeField]
	private Button submitButton;
    [SerializeField]
    private Text textModulo;
    
    private UIScoreBar slider;
	private UINumPad numpad;
	private UIGameHeaderWithSlider header;
	private UIImageFlasher screenFlasher;

    public void SetModuloText(string text)
    {
        textModulo.text = text;
    }

	/**
	 * @returns the underlying grid that contains the inputfields
	 */
	public AutoGridLayout GetGrid(){
		return grid;
	}

	public UINumPad GetNumpad(){
		return numpad;
	}

   public UIScoreBar Slider{
		get{
			return slider;
		}
		set{
			this.slider = value;
		}
	}

	public UIGameHeaderWithSlider GetHeader(){
		return header;
	}

	public UINumPad Numpad{
		get{
			return numpad;
		}
		set{
			this.numpad = value;
		}
	}

	public UIImageFlasher GetScreenFlasher()
	{
		return screenFlasher;
	}

	public void setSlider(UIScoreBar slider)
	{
		this.slider = slider;
		header.SetSlider(this.slider);
	}

    public Button GetSubmitButton()
    {
        return submitButton;
    }

	void Awake()
	{
		header = UIElement.Instantiate<UIGameHeaderWithSlider> (headerholder.transform);
		header.SetTitleText (Global.MODULO_VS_TITEL_TEXT);

		screenFlasher = UIElement.Instantiate<UIImageFlasher>(this.transform);
		screenFlasher.transform.SetAsLastSibling();

        header.SetPauseButtonImage(Resources.Load<Sprite>(Global.PATH_ICON_EXIT));
		CreateNumPad ();

	}

    public void AddPauseButtonAction(UnityAction pauseButtonAction)
    {
        header.AddOnPauseAction(pauseButtonAction);
    }

	private void CreateNumPad()
	{
		numpad = UIElement.Instantiate<UINumPad>(numpadholder.transform);
	}

	public void setNumpadInputField(InputField field){

		numpad.SetInputField (field);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;
using System.Collections;
using UnityEngine.EventSystems;

public class UITheory : UIElement
{
    //Header
    [SerializeField] private GameObject headerHolder;
    protected UIGameMenuHeader header;

    //Content
    [SerializeField] private GameObject leftPageHolder;
    [SerializeField] private GameObject rightPageHolder;

    //Footer
    [SerializeField] private Button leftButton;
    [SerializeField] private GameObject progressSliderHolder;
    [SerializeField] private Button rightButton;
    private UnityAction onLeftButtonClick;
    private UnityAction onRightButtonClick;
    private UnityAction onBackButtonClick;
    private UnityAction onPauseButtonClick;

    //Page Elements
    [SerializeField] private Text headingPrefab;
    [SerializeField] private Text captionPrefab;
    [SerializeField] private Text textPrefab;
    [SerializeField] private Image imagePrefab;

    void Awake()
    {
        //footer
        rightButton.onClick.AddListener(() => { if (null != onRightButtonClick) onRightButtonClick(); });
        leftButton.onClick.AddListener(() => { if (null != onLeftButtonClick) onLeftButtonClick(); });

        //header
        header = UIElement.Instantiate<UIGameMenuHeader>(headerHolder.transform);
        header.HidePlayerName();
        Button pauseButton = header.GetRightButton();
        Button backButton = header.GetLeftButton();
        pauseButton.onClick.RemoveAllListeners();
        backButton.onClick.RemoveAllListeners();
        pauseButton.onClick.AddListener(() => { if (null != onPauseButtonClick) onPauseButtonClick(); });
        backButton.onClick.AddListener(() => { if (null != onBackButtonClick) onBackButtonClick(); });

        //change right header button sprite to the pause sprite
        Sprite pauseSprite = Resources.Load<Sprite>(Global.PATH_ICON_PAUSE); ;
        Image[] imageComponents = pauseButton.GetComponentsInChildren<Image>();
        foreach (Image img in imageComponents)
        {
            if (pauseButton.transform.Equals(img.gameObject.transform.parent))
            {
                img.sprite = pauseSprite;
            }
        }
    }

    public void SetTitleText(string text)
    {
        header.SetTitleText(text);
    }

    public void SetOnRightButtonClickListener(UnityAction rightButtonListener)
    {
        if (null == rightButtonListener)
            return;

        onRightButtonClick = rightButtonListener;
    }

    public void SetOnLeftButtonClickListener(UnityAction leftButtonListener)
    {
        if (null == leftButtonListener)
            return;

        onLeftButtonClick = leftButtonListener;
    }

    public void SetOnPauseButtonClickListener(UnityAction pauseButtonListener)
    {
        if (null == pauseButtonListener)
            return;

        onPauseButtonClick = pauseButtonListener;
    }

    public void SetOnBackButtonClickListener(UnityAction backButtonListener)
    {
        if (null == backButtonListener)
            return;

        onBackButtonClick = backButtonListener;
    }

    public void SetProgressSlider(UICustomSlider slider)
    {
        slider.transform.SetParent(progressSliderHolder.transform, false);
    }

    public void SetLeftButtonInteractable(bool interactable){
        leftButton.interactable = interactable;
    }

    public void SetRightButtonInteractable(bool interactable)
    {
        rightButton.interactable = interactable;
    }

    public void SetPages(TheoryPage leftPage, TheoryPage rightPage)
    {
        SetPage(leftPageHolder, leftPage);
        SetPage(rightPageHolder, rightPage);
    }

    private void SetPage(GameObject pageHolder, TheoryPage page)
    {
        //clear children
        foreach (Transform child in pageHolder.transform)
        {
            Destroy(child.gameObject);
        }

        if (null == page)
            return;

        foreach (PageElement element in page.GetElements())
        {
            //insantiate prefab by type
            MonoBehaviour elementInstance = null;
            switch(element.GetElementType())
            {
                case PageElement.ElementType.CAPTION:
                    elementInstance = Instantiate(captionPrefab);
                    break;
                case PageElement.ElementType.HEADING:
                    elementInstance = Instantiate(headingPrefab);
                    break;
                case PageElement.ElementType.TEXT:
                    elementInstance = Instantiate(textPrefab);
                    break;
                case PageElement.ElementType.IMAGE:
                    elementInstance = Instantiate(imagePrefab);
                    break;
            }

            //set element content
            if (elementInstance is Text)
            {
                Text textInstance = elementInstance as Text;
                textInstance.text = element.GetContent();
            }
            else if(elementInstance is Image)
            {
                //load image texture, apply it and adjust bounds to texture
                Image imageInstance = elementInstance as Image;
                imageInstance.sprite = TheoryLoader.LoadPageImageFromFile(element.GetContent());

                PageImage imageElement = element as PageImage;
                StartCoroutine(AdjustImageBounds(imageInstance, imageElement.GetWidthScale()));
            }

            //put on page
            elementInstance.transform.SetParent(pageHolder.transform, false);
        }
    }

    private IEnumerator AdjustImageBounds(Image image, float widthScale)
    {
        Texture2D texture = image.sprite.texture;
        float ratio = texture.width > texture.height ? (float)texture.width / texture.height : (float)texture.height / texture.width;
        if (widthScale > 0 && widthScale < 1)
            ratio = ratio / widthScale;

        AspectRatioFitter arf = image.gameObject.GetComponent<AspectRatioFitter>();
        arf.aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
        arf.aspectRatio = ratio;

        //apply ratio
        yield return null;

        LayoutElement le = image.gameObject.GetComponent<LayoutElement>();
        RectTransform imageBounds = (RectTransform)image.gameObject.transform;
        le.preferredHeight = imageBounds.rect.height;
    }
}

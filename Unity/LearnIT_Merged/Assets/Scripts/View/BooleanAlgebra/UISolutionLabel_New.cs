﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UISolutionLabel_New : UIElement {

	public void SetText(string text)
    {
        GetComponentInChildren<Text>().text = text;
    }

    public string GetText()
    {
        return GetComponentInChildren<Text>().text;
    }
}

﻿using UnityEngine;
using System.Collections.Generic;

public class UIBooleanEquationHistoryItem : UIElement
{
    [SerializeField] private GameObject headerHolder;
    [SerializeField] private GameObject triesHolder;
    private UIBooleanEquationSolution headerEquation;
    private List<UIBooleanEquation> userEquations;

    void Start()
    {
       
    }

    public void AddUserEquation(string equation, bool correct)
    {
        if(userEquations == null)
            userEquations = new List<UIBooleanEquation>();

        UIBooleanEquation userEquation = UIElement.Instantiate<UIBooleanEquation>(triesHolder.transform);
        userEquation.SetEquation(equation);

        if (correct)
        {
            userEquation.SetCorrectColor(Global.COLOR_FLASH_GREEN);
        }
        else
        {
            userEquation.SetCorrectColor(Global.COLOR_FLASH_RED);
        }

        userEquations.Add(userEquation);
    }

    public void SetHeaderEquation(string equation, int number)
    {
        headerEquation = UIElement.Instantiate<UIBooleanEquationSolution>(headerHolder.transform);
        headerEquation.SetEquation(equation);
        headerEquation.SetNumber(number);
    }

    public void SetMasterEquation(string equation)
    {
        headerEquation.SetMasterEquation(equation);
    }

    public void EnableMasterEquation()
    {
        headerEquation.EnableMasterEquation();
    }
}

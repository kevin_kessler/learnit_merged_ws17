﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UIBooleanEquationSolution : UIElement
{
    [SerializeField] private Text number;
    [SerializeField] private Text equation;
    [SerializeField] private Text masterEquation;

    void Start()
    {

    }

    public void SetNumber(int number)
    {
        this.number.text = "" + number + ".";
    }

    public void SetEquation(string equation)
    {
        this.equation.text = equation;
    }

    public void SetMasterEquation(string equation)
    {
        this.masterEquation.color = Global.COLOR_FLASH_GREEN;
        this.masterEquation.text = equation;
        this.masterEquation.enabled = false;
    }

    public void EnableMasterEquation()
    {
        masterEquation.enabled = true;
    }
}

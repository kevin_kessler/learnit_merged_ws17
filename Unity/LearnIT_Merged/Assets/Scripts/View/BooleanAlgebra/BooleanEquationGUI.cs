﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public abstract class BooleanEquationGUI : UIElement{

    public abstract void GenerateGUI(BooleanEquation equation);
    public abstract BooleanEquation GetTerm();
    public abstract Button GetSubmitButton();
    public abstract bool CanSubmit();
    public abstract void SetCanSubmit(bool value);
    public abstract void SetHeaderTitle(string title);
    public abstract void SetSlider(UICustomSlider slider);
    public abstract UICustomSlider GetSlider();
    public abstract bool LabelsClickedControl();
    public abstract void FlashCorrect();
    public abstract void FlashCorrect(int time);
    public abstract void FlashWrong();
    public abstract void FlashWrong(int time);
    public abstract void ShowHistory();
    public abstract void HideHistory();
    public abstract void AddOnSliderFinishedAction(UnityAction finish);
    public abstract void AddSubmitAction(UnityAction click);
    public abstract void AddSkipAction(UnityAction click);
    public abstract void AddHistoryAction(UnityAction click);
    public abstract void AddHistoryBackAction(UnityAction click);
    public abstract void AddPauseButtonAction(UnityAction action);
    public abstract void AddClickLabelAction(UnityAction action);
    public abstract void SetPoints(int points);
    public abstract void AddHistoryItem(BooleanEquation equation);
    public abstract void AddUserEquation(BooleanEquation equation, bool correct);
    public abstract void EnableMasterEquation();
    public abstract void ResetGui();

}

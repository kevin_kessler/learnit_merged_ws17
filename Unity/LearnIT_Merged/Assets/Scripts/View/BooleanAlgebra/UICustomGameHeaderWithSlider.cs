﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class UICustomGameHeaderWithSlider : UIElement
{
    [SerializeField] private Text titleText;
    [SerializeField] private Button pauseButton;
    [SerializeField] private Text levelText;
    [SerializeField] private Text levelCountText;

    [SerializeField] private GameObject sliderHolder;
    private UICustomSlider slider;

    public void SetOnSliderFinished(UnityAction action)
    {
        slider.OnFinished += action;
    }

    public void SetSlider(UICustomSlider slider)
    {
        this.slider = slider;
        this.slider.transform.SetParent(sliderHolder.transform, false);
    }

    public void SetTitleText(string text)
    {
        titleText.text = text;
    }

    public void SetLevelText(string text)
    {
        levelText.text = text;
    }

    public void SetLevelCountText(string text)
    {
        levelCountText.text = text;
    }

    public void AddOnPauseAction(UnityAction onPauseAction)
    {
        if (null != pauseButton && null != onPauseAction)
            pauseButton.onClick.AddListener(onPauseAction);
    }

    public void RemovePauseButton()
    {
        Destroy(pauseButton.gameObject);
    }


}

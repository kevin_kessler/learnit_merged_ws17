﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIOperatorLabel_New : UIElement {

    public void SetOperator(string op)
    {
        GetComponentInChildren<Text>().text = op;
    }

    public string GetOperator()
    {
        return GetComponentInChildren<Text>().text;
    }
}

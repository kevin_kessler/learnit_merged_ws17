﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class BooleanEquationGUI_Terme : BooleanEquationGUI {

    [SerializeField] private GameObject headerHolder;
    [SerializeField] private GameObject termHolder;
    [SerializeField] private Button submitButton;
    [SerializeField] private Button skipButton;
    [SerializeField] private Button historyButton;

    private UICustomGameHeaderWithSlider gameHeaderWithSliderUI;
    private UICustomSlider slider;
    private UIBooleanEquationHistory historyView;

    private UIOperatorLabel[] operators;
    private UIClickLabel[] values;
    private UIClickLabel[] constants;
    private UISolutionLabel solution;
    private UIImageFlasher solutionFlasher;
    private UITextFlasher eventFlasher;

    private bool canSubmit;

    void Start()
    {
        solutionFlasher = UIElement.Instantiate<UIImageFlasher>();
        eventFlasher = UIElement.Instantiate<UITextFlasher>();
        eventFlasher.SetStayDuration(0.1f);

        gameHeaderWithSliderUI = UIElement.Instantiate<UICustomGameHeaderWithSlider>(headerHolder.transform);
        gameHeaderWithSliderUI.SetLevelText("Gelöst");
        gameHeaderWithSliderUI.SetLevelCountText("0");

        historyView = UIElement.Instantiate<UIBooleanEquationHistory>();
        historyView.Init();
        historyView.Hide();
    }

    public override void SetHeaderTitle(string title)
    {
        gameHeaderWithSliderUI.SetTitleText(title);
    }

    public override void GenerateGUI(BooleanEquation equation)
    {
        canSubmit = false;
        GetSubmitButton().GetComponent<Image>().color = Global.COLOR_DISABLED_GRAY;

        if (values != null)
        {
            foreach(UIClickLabel value in values)
            {
                if (value != null)
                Destroy(value.gameObject);
            }
            foreach (UIClickLabel con in constants)
            {
                if (con != null)
                    Destroy(con.gameObject);
            }
            foreach (UIOperatorLabel op in operators)
            {
                if (op != null)
                    Destroy(op.gameObject);
            }
            Destroy(solution.gameObject);
        }

        constants = new UIClickLabel[equation.GetOperands().Length];
        values = new UIClickLabel[equation.GetOperands().Length];
        for (int i = 0; i < values.Length; i++)
        {
            if (equation.GetOperands()[i] == BoolOperand.ONE || equation.GetOperands()[i] == BoolOperand.ZERO)
            {
                constants[i] = UIElement.Instantiate<UIClickLabel>();
                constants[i].GetComponent<Image>().color = Global.COLOR_DISABLED_GRAY;
                constants[i].SetText(BooleanAlgebra.ToString(equation.GetOperands()[i]));
                Destroy(constants[i].GetComponent<Button>());
            }
            else
            {
                values[i] = UIElement.Instantiate<UIClickLabel>();
                values[i].SetText("");
            }
        } 
        operators = new UIOperatorLabel[equation.GetOperators().Length];
        for (int i = 0; i < equation.GetOperators().Length; i++)
        {
            operators[i] = UIElement.Instantiate<UIOperatorLabel>();
            operators[i].SetOperator(BooleanAlgebra.ToString(equation.GetOperators()[i]));
        }
       
        for(int i = 0; i < values.Length; i++)
        {
            if (values[i] == null)
            {
                constants[i].transform.SetParent(termHolder.transform);
            }
            else
            {
                values[i].transform.SetParent(termHolder.transform);
            }

            if (i < equation.GetOperators().Length)
                operators[i].transform.SetParent(termHolder.transform);
        }

        solution = UIElement.Instantiate<UISolutionLabel>(termHolder.transform);
        solution.SetText("= " + BooleanAlgebra.ToString(equation.GetSolution()));
    }

    public override BooleanEquation GetTerm()
    {
        string[] term = new string[values.Length+operators.Length];
        for (int i = 0, v = 0, o = 0; i < term.Length; i++)
        {
            if (i % 2 == 0)
            {
                if (values[v] != null)
                    term[i] = values[v++].GetText();
                else
                    term[i] = constants[v++].GetText();
            }
            else
            {
                term[i] = operators[o++].GetOperator();
            }
        }

        return new BooleanEquation(term);
    }

    public override Button GetSubmitButton()
    {
        return submitButton;
    }

    public UIClickLabel[] GetValues()
    {
        return values;
    }

    public override bool CanSubmit()
    {
        return canSubmit;
    }

    public override void SetCanSubmit(bool value)
    {
        canSubmit = value;
    }

    public override void SetSlider(UICustomSlider slider)
    {
        this.slider = slider;
        gameHeaderWithSliderUI.SetSlider(slider);
    }

    public override UICustomSlider GetSlider()
    {
        return slider;
    }

    public override void FlashCorrect()
    {
        StopAllCoroutines();
        StartCoroutine(solutionFlasher.Flash(Global.COLOR_FLASH_GREEN));
        StartCoroutine(eventFlasher.Flash(Global.COLOR_FLASH_GREEN, ""));
    }

    public override void FlashCorrect(int time)
    {
        StopAllCoroutines();
        StartCoroutine(solutionFlasher.Flash(Global.COLOR_FLASH_GREEN));
        StartCoroutine(eventFlasher.Flash(Global.COLOR_FLASH_GREEN, "+"+time+" Sekunden"));
    }

    public override void FlashWrong()
    {
        StopAllCoroutines();
        StartCoroutine(solutionFlasher.Flash(Global.COLOR_FLASH_RED));
        StartCoroutine(eventFlasher.Flash(Global.COLOR_FLASH_RED, ""));
    }

    public override void FlashWrong(int time)
    {
        StopAllCoroutines();
        StartCoroutine(solutionFlasher.Flash(Global.COLOR_FLASH_RED));
        StartCoroutine(eventFlasher.Flash(Global.COLOR_FLASH_RED, ""+time+" Sekunden"));
    }

    public override void ShowHistory()
    {
        StopAllCoroutines();
        historyView.Show();
    }

    public override void HideHistory()
    {
        historyView.Hide();
    }

    public override void AddOnSliderFinishedAction(UnityAction finish)
    {
        gameHeaderWithSliderUI.SetOnSliderFinished(finish);
    }

    public override void AddSubmitAction(UnityAction click)
    {
        submitButton.onClick.AddListener(() => {
            if (canSubmit)
                click();
        });
    }

    public override void AddSkipAction(UnityAction click)
    {
        skipButton.onClick.AddListener(click);
    }

    public override void AddHistoryAction(UnityAction click)
    {
       historyButton.onClick.AddListener(click);
    }

    public override void AddHistoryBackAction(UnityAction click)
    {
        historyView.SetBackButtonAction(click);
    }

    public override void AddPauseButtonAction(UnityAction action)
    {
        gameHeaderWithSliderUI.AddOnPauseAction(action);
    }

    public override void AddClickLabelAction(UnityAction action)
    {
        foreach (UIClickLabel label in values)
        {
            if (label != null)
            {
                label.AddModifyListener();
                label.GetComponent<Button>().onClick.AddListener(action);
            }
        }
    }

    public override void SetPoints(int points)
    {
        gameHeaderWithSliderUI.SetLevelCountText(points.ToString());
    }

    public override void AddHistoryItem(BooleanEquation equation)
    {
        historyView.AddItem(equation);
    }

    public override void AddUserEquation(BooleanEquation equation, bool correct)
    {
        historyView.AddUserEquation(equation.ToString(), correct);
    }

    public override void EnableMasterEquation()
    {
        historyView.EnableMasterEquation();
    }

    public override void ResetGui()
    {
        gameHeaderWithSliderUI.SetLevelCountText("0");

        historyView = UIElement.Instantiate<UIBooleanEquationHistory>();
        historyView.Init();
        historyView.Hide();
    }

    public override bool LabelsClickedControl()
    {
        for (int i = 0; i < values.Length; i++)
            if (values[i] != null && !values[i].isClicked())
                return false;
        return true;
    }
}

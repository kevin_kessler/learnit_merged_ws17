﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;

public class BooleanEquationGUI_Schaltnetz : BooleanEquationGUI {

    [SerializeField] private GameObject headerHolder;
    [SerializeField] private GameObject termHolder;
    [SerializeField] private Button submitButton;
    [SerializeField] private Button skipButton;
    [SerializeField] private Button historyButton;

    private UICustomGameHeaderWithSlider gameHeaderWithSliderUI;
    private UICustomSlider slider;
    private UIBooleanEquationHistory historyView;

    private UIOperatorLabel_New[] operators;
    private UIClickLabel_New[] values;
    private UIClickLabel_New[] constants;
    private UISolutionLabel_New solution;
    private UIImageFlasher solutionFlasher;
    private UITextFlasher eventFlasher;

    private bool canSubmit;

    private enum Operator
    {
        OR = 1, AND = 2, NOR = 3, NAND = 4
    }

    private static Dictionary<string, Operator> ops = new Dictionary<string, Operator>() {
        { "OR", Operator.OR }, { "AND", Operator.AND }, { "NOR", Operator.NOR }, { "NAND", Operator.NAND }
    };    

    void Start()
    {
        solutionFlasher = UIElement.Instantiate<UIImageFlasher>();
        eventFlasher = UIElement.Instantiate<UITextFlasher>();
        eventFlasher.SetStayDuration(0.1f);

        gameHeaderWithSliderUI = UIElement.Instantiate<UICustomGameHeaderWithSlider>(headerHolder.transform);
        gameHeaderWithSliderUI.SetLevelText("Gelöst");
        gameHeaderWithSliderUI.SetLevelCountText("0");

        historyView = UIElement.Instantiate<UIBooleanEquationHistory>();
        historyView.Init();
        historyView.Hide();
    }

    public override void SetHeaderTitle(string title)
    {
        gameHeaderWithSliderUI.SetTitleText(title);
    }

    public override void GenerateGUI(BooleanEquation equation)
    {

        canSubmit = false;
        GetSubmitButton().GetComponent<Image>().color = Global.COLOR_DISABLED_GRAY;

        if (termHolder.transform.childCount != 0)
        {
            (termHolder.transform.GetChild(0).GetComponent<UIBooleanEquationPart>()).DeleteTree();
            Destroy(solution.gameObject);
        }
        
        if (values != null)
        {
            foreach (UIClickLabel_New value in values)
            {
                if (value != null)
                    Destroy(value.gameObject);
            }
            foreach (UIClickLabel_New con in constants)
            {
                if (con != null)
                    Destroy(con.gameObject);
            }
            foreach (UIOperatorLabel_New op in operators)
            {
                if (op != null)
                    Destroy(op.gameObject);
            }

        }
        
        constants = new UIClickLabel_New[equation.GetOperands().Length];
        values = new UIClickLabel_New[equation.GetOperands().Length];
        for (int i = 0; i < values.Length; i++)
        {
            if (equation.GetOperands()[i] == BoolOperand.ONE || equation.GetOperands()[i] == BoolOperand.ZERO)
            {
                constants[i] = UIElement.Instantiate<UIClickLabel_New>();
                Destroy(constants[i].GetComponent<Button>());
            }
            else
            {
                values[i] = UIElement.Instantiate<UIClickLabel_New>();
            }
        }

        // Werden noch zum lösen der Gleichung benötigt
        operators = new UIOperatorLabel_New[equation.GetOperators().Length];
        for (int i = 0; i < equation.GetOperators().Length; i++)
        {
            operators[i] = UIElement.Instantiate<UIOperatorLabel_New>();
            operators[i].SetOperator(BooleanAlgebra.ToString(equation.GetOperators()[i]));
        }

        BuildTermTree(equation);

    }

    private void BuildTermTree(BooleanEquation equation)
    {
        //Debug.Log("Before RPN: " + string.Join(" ", equation.ToStringArray()));
        string[] rpn = ToReversePolishNotation(equation.ToStringArray());
        //Debug.Log("After RPN: " + string.Join(" ", rpn));

        Stack tree = new Stack();
        int i = 0;
        foreach (string s in rpn)
        {
            if (s.Equals("X") || s.Equals("!X"))
            {
                values[i].SetText("");
                tree.Push(values[i++]);
            }
            else if (s.Equals("0") || s.Equals("1"))
            {
                constants[i].SetText(s);
                constants[i].GetComponent<Image>().color = Global.COLOR_DISABLED_GRAY;
                tree.Push(constants[i++]);
            }
            else
            {
                UIBooleanEquationPart tmp = Instantiate<UIBooleanEquationPart>();
                UIOperatorLabel_New otmp = Instantiate<UIOperatorLabel_New>();
                otmp.SetOperator(s);
                tmp.SetOperator(otmp);
                tmp.SetNextTop((UIElement)tree.Pop());
                tmp.SetNextBot((UIElement)tree.Pop());
                tree.Push(tmp);
            }
        }

        ((UIElement)tree.Pop()).transform.SetParent(termHolder.transform, false);

        solution = UIElement.Instantiate<UISolutionLabel_New>(termHolder.transform);
        solution.SetText("= " + BooleanAlgebra.ToString(equation.GetSolution()));

    }

    // Wandelt den Term in die Umgekehrte Polnische Notation
    private string[] ToReversePolishNotation(string[] str)
    {

        Stack operatorStack = new Stack();
        List<string> output = new List<string>();

        foreach (string s in str)
        {
            if(ops.ContainsKey(s))
            {
                while(operatorStack.Count > 0 && isHeigherPrec(s, (string)operatorStack.Peek()))
                {
                    output.Add((string)operatorStack.Pop());
                }
                operatorStack.Push(s);
            }
            else
            {
                output.Add(s);
            }
        }

        while (operatorStack.Count > 0)
        {
            output.Add((string) operatorStack.Pop());
        }

        return output.ToArray();
    }

    private bool isHeigherPrec(string op, string sub)
    {
        return (ops.ContainsKey(sub) && ops[sub] >= ops[op] );
    }

    public override BooleanEquation GetTerm()
    {
        string[] term = new string[values.Length+operators.Length];
        for (int i = 0, v = 0, o = 0; i < term.Length; i++)
        {
            if (i % 2 == 0)
            {
                if (values[v] != null)
                    term[i] = values[v++].GetText();
                else
                    term[i] = constants[v++].GetText();
            }
            else
            {
                term[i] = operators[o++].GetOperator();
            }
        }

        return new BooleanEquation(term);
    }

    public override Button GetSubmitButton()
    {
        return submitButton;
    }

    public UIClickLabel_New[] GetValues()
    {
        return values;
    }

    public override bool CanSubmit()
    {
        return canSubmit;
    }

    public override void SetCanSubmit(bool value)
    {
        canSubmit = value;
    }

    public override void SetSlider(UICustomSlider slider)
    {
        this.slider = slider;
        gameHeaderWithSliderUI.SetSlider(slider);
    }

    public override UICustomSlider GetSlider()
    {
        return slider;
    }

    public override void FlashCorrect()
    {
        StopAllCoroutines();
        StartCoroutine(solutionFlasher.Flash(Global.COLOR_FLASH_GREEN));
        StartCoroutine(eventFlasher.Flash(Global.COLOR_FLASH_GREEN, ""));
    }

    public override void FlashCorrect(int time)
    {
        StopAllCoroutines();
        StartCoroutine(solutionFlasher.Flash(Global.COLOR_FLASH_GREEN));
        StartCoroutine(eventFlasher.Flash(Global.COLOR_FLASH_GREEN, "+"+time+" Sekunden"));
    }

    public override void FlashWrong()
    {
        StopAllCoroutines();
        StartCoroutine(solutionFlasher.Flash(Global.COLOR_FLASH_RED));
        StartCoroutine(eventFlasher.Flash(Global.COLOR_FLASH_RED, ""));
    }

    public override void FlashWrong(int time)
    {
        StopAllCoroutines();
        StartCoroutine(solutionFlasher.Flash(Global.COLOR_FLASH_RED));
        StartCoroutine(eventFlasher.Flash(Global.COLOR_FLASH_RED, ""+time+" Sekunden"));
    }

    public override void ShowHistory()
    {
        StopAllCoroutines();
        historyView.Show();
    }

    public override void HideHistory()
    {
        historyView.Hide();
    }

    public override void AddOnSliderFinishedAction(UnityAction finish)
    {
        gameHeaderWithSliderUI.SetOnSliderFinished(finish);
    }

    public override void AddSubmitAction(UnityAction click)
    {
        submitButton.onClick.AddListener(() => {
            if (canSubmit)
                click();
        });
    }

    public override void AddSkipAction(UnityAction click)
    {
        skipButton.onClick.AddListener(click);
    }

    public override void AddHistoryAction(UnityAction click)
    {
       historyButton.onClick.AddListener(click);
    }

    public override void AddHistoryBackAction(UnityAction click)
    {
        historyView.SetBackButtonAction(click);
    }

    public override void AddPauseButtonAction(UnityAction action)
    {
        gameHeaderWithSliderUI.AddOnPauseAction(action);
    }

    public override void AddClickLabelAction(UnityAction action)
    {
        foreach (UIClickLabel_New label in values)
        {
            if (label != null)
            {
                label.AddModifyListener();
                label.GetComponent<Button>().onClick.AddListener(action);
            }
        }
    }

    public override void SetPoints(int points)
    {
        gameHeaderWithSliderUI.SetLevelCountText(points.ToString());
    }

    public override void AddHistoryItem(BooleanEquation equation)
    {
        historyView.AddItem(equation);
    }

    public override void AddUserEquation(BooleanEquation equation, bool correct)
    {
        historyView.AddUserEquation(equation.ToString(), correct);
    }

    public override void EnableMasterEquation()
    {
        historyView.EnableMasterEquation();
    }

    public override void ResetGui()
    {
        gameHeaderWithSliderUI.SetLevelCountText("0");

        historyView = UIElement.Instantiate<UIBooleanEquationHistory>();
        historyView.Init();
        historyView.Hide();
    }

    public override bool LabelsClickedControl()
    {
        for (int i = 0; i < values.Length; i++)
            if (values[i] != null && !values[i].isClicked())
                return false;
        return true;
    }
}

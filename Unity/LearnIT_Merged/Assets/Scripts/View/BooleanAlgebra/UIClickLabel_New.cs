﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIClickLabel_New : UIElement{
    
    private bool clicked;

    // Use this for initialization
    void Start () {
        clicked = false;
    }

    private void OnClick()
    {
        clicked = true;
        if (GetComponentInChildren<Text>().text == "")
            GetComponentInChildren<Text>().text = BooleanAlgebra.ToString(BoolOperand.X);
        else
            GetComponentInChildren<Text>().text = BooleanAlgebra.Negate(GetComponentInChildren<Text>().text);
    }

    public void AddModifyListener()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void SetText(string text)
    {
        GetComponentInChildren<Text>().text = text;
    }

    public string GetText()
    {
        return GetComponentInChildren<Text>().text;
    }

    public void setClicked(bool value)
    {
        clicked = value;
    }

    public bool isClicked()
    {
        return clicked;
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIRound : UIElement {

    [SerializeField] private Text roundNumberText;
    [SerializeField] private GameObject roundMarkerHolderOne;
    [SerializeField] private GameObject roundMarkerHolderTwo;
    private UIProgressMarker playerOneRoundMarker;
    private UIProgressMarker playerTwoRoundMarker;

    // Use this for initialization
    void Start () {
	}
	
    public void SetRoundNumber(int roundNumber)
    {
        roundNumberText.text = "" + roundNumber;
    }

    public void SetRoundWon(UIProgressMarker playerOneWin, UIProgressMarker playerTwoWin)
    {
        playerOneRoundMarker = UIElement.Instantiate<UIProgressMarker>(playerOneWin.gameObject, roundMarkerHolderOne.transform);
        if (playerOneWin.IsCorrect())
            playerOneRoundMarker.SetCorrect();
        else
            playerOneRoundMarker.SetWrong();

        playerTwoRoundMarker = UIElement.Instantiate<UIProgressMarker>(playerTwoWin.gameObject, roundMarkerHolderTwo.transform);
        if (playerTwoWin.IsCorrect())
            playerTwoRoundMarker.SetCorrect();
        else
            playerTwoRoundMarker.SetWrong();
    }
}

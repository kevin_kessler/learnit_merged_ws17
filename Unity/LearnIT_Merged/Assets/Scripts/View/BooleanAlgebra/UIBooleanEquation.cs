﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UIBooleanEquation : UIElement
{
    [SerializeField] private Image colorImage;
    [SerializeField] private Text equation;

    void Start()
    {
        
    }

    public void SetEquation(string equation)
    {
        this.equation.text = equation;
    }

    public void SetCorrectColor(Color color)
    {
        colorImage.GetComponent<Image>().color = color;
    }
}

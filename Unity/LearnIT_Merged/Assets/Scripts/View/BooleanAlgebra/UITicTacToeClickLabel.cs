﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UITicTacToeClickLabel : UIElement {

    private int index;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => {
            if(GetText() != "")
                GetComponent<Image>().color = Global.COLOR_DISABLED_GRAY;
        });
    }

    public void SetText(string text)
    {
        GetComponentInChildren<Text>().text = text;
    }

    public string GetText()
    {
        return GetComponentInChildren<Text>().text;
    }

    public void SetIndex(int i)
    {
        index = i;
    }

    public int GetIndex()
    {
        return index;
    }

    public bool isClicked()
    {
        if(GetText() != "")
            return true;

        return false;
    }
}

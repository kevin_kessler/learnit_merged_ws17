﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class MPTicTacToeGUI : UIElement {

    [SerializeField] private GameObject headerHolder;
    [SerializeField] private GameObject[] rows;
    [SerializeField] private UIProgressMarker[] playerOneScores;
    [SerializeField] private UIProgressMarker[] playerTwoScores;
    [SerializeField] private Image[] solutionIndicatorLines;
    [SerializeField] private Image playerOneTurnIndicator;
    [SerializeField] private Image playerTwoTurnIndicator;
    [SerializeField] private Image playerOneColor;
    [SerializeField] private Image playerTwoColor;
    [SerializeField] private Text playerOneName;
    [SerializeField] private Text playerTwoName;
    [SerializeField] private Text playerOneOperand;
    [SerializeField] private Text playerTwoOperand;
    [SerializeField] private GameObject playerOneWaitingIndicator;
    [SerializeField] private GameObject playerTwoWaitingIndicator;
    [SerializeField] private Button nextButton;
    [SerializeField] private GameObject nextButtonHolder;
    [SerializeField] private GameObject waitingIndicator;
    [SerializeField] private GameObject playerOneProgressHolder;
    [SerializeField] private GameObject playerTwoProgressHolder;
    private UITicTacToeHeader header;
    private UICustomSlider slider;
    private UIImageFlasher solutionFlasher;
    private UICountSlider playerOneProgress;
    private UICountSlider playerTwoProgress;

    private UIOperatorLabel[] operators;
    private UITicTacToeClickLabel[] values;

    void Start () {
        header = UIElement.Instantiate<UITicTacToeHeader>(headerHolder.transform);
        playerTwoTurnIndicator.color = new Color32(0x00, 0x00, 0x00, 0x00);
        playerOneTurnIndicator.color = new Color32(0x00, 0x00, 0x00, 0x00);

        solutionFlasher = UIElement.Instantiate<UIImageFlasher>();

        playerOneProgress = UIElement.Instantiate<UICountSlider>(playerOneProgressHolder.transform);
        playerOneProgress.Reset(8);
        playerOneProgress.SetPrependingText("Terme:");
        playerOneProgress.SetAppendingText("/8");
        playerOneProgress.SetIsCountingDown(false);

        playerTwoProgress = UIElement.Instantiate<UICountSlider>(playerTwoProgressHolder.transform);
        playerTwoProgress.Reset(8);
        playerTwoProgress.SetPrependingText("Terme:");
        playerTwoProgress.SetAppendingText("/8");
        playerTwoProgress.SetIsCountingDown(false);
        Hide();
    }

    public void SetHeaderTitle(string title)
    {
        header.SetTitleText(title);
    }

    public void GenerateGUI(BoolOperator[] ops, MPTicTacToePlayer playerOne, MPTicTacToePlayer playerTwo, int roundIndex)
    {
        if (values != null)
        {
            foreach (UITicTacToeClickLabel label in values)
            {
                Destroy(label.gameObject);
            }
            foreach (UIOperatorLabel label in operators)
            {
                Destroy(label.gameObject);
            }
        }


        values = new UITicTacToeClickLabel[9];
        for (int i = 0; i < values.Length; i++)
        {
            values[i] = UIElement.Instantiate<UITicTacToeClickLabel>();
            values[i].SetText("");
            values[i].SetIndex(i);
        }
        operators = new UIOperatorLabel[ops.Length];
        for (int i = 0; i < ops.Length; i++)
        {
            operators[i] = UIElement.Instantiate<UIOperatorLabel>();
            operators[i].SetOperator(ops[i].ToString());
        }

        for (int i = 0, v = 0, o = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
            {
                if (i % 2 == 0 && j % 2 == 0)
                    values[v++].transform.SetParent(rows[i].transform);
                else
                    operators[o++].transform.SetParent(rows[i].transform);
            }

        foreach(Image line in solutionIndicatorLines)
        {
            line.color = Global.COLOR_DISABLED_GRAY;
        }

        MPTicTacToeRound round = playerOne.GetController().GetRound(playerOne.GetController().GetCurrentRoundIndex());

        playerOneColor.color = playerOne.GetPlayerColor();
        playerOneName.text = playerOne.GetPlayerName();
        playerOneOperand.text = BooleanAlgebra.ToString(round.GetOperandByPlayerID(playerOne.netId.Value));
        playerOneProgress.SetCurrentValue(0);

        playerTwoColor.color = playerTwo.GetPlayerColor();
        playerTwoName.text = playerTwo.GetPlayerName();
        playerTwoOperand.text = BooleanAlgebra.ToString(round.GetOperandByPlayerID(playerTwo.netId.Value));
        playerTwoProgress.SetCurrentValue(0);

        if (round.GetPlayerIDByOperand(round.GetStartOperand()) == playerOne.netId.Value)
            ActivatePlayerOneTurnIndicator();
        else
            ActivatePlayerTwoTurnIndicator();

        nextButton.GetComponent<Image>().color = Global.COLOR_DISABLED_GRAY;
        nextButton.gameObject.SetActive(true);
        nextButtonHolder.gameObject.SetActive(true);
        waitingIndicator.gameObject.SetActive(false);
        playerOneWaitingIndicator.gameObject.SetActive(false);
        playerTwoWaitingIndicator.gameObject.SetActive(false);
        playerOneProgressHolder.SetActive(true);
        playerTwoProgressHolder.SetActive(true);

        StartCoroutine(DrawLines());
    }

    public IEnumerator DrawLines()
    {
        for(int i = 0; i < 10; i++)
            yield return new WaitForEndOfFrame();

        Vector3 pos;
        Vector3 start;
        Vector3 end;
        Vector3 differenceVector;
        float angle;
        int lineIndex = 0;
        
        //Horizontals
        for (int i = 0; i < 3; i++)
        {
            pos = values[i * 3].GetComponent<RectTransform>().position;
            start = values[i * 3].GetComponent<RectTransform>().localPosition;
            end = values[i * 3 + 2].GetComponent<RectTransform>().localPosition;
            differenceVector = end - start;

            solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().sizeDelta = new Vector2(differenceVector.magnitude, 6);
            solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
            solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().position = pos;
            angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
            solutionIndicatorLines[lineIndex++].GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, angle);
        }

        //Verticals
        for (int i = 0; i < 3; i++)
        {
            pos = values[i].GetComponent<RectTransform>().position;
            start = rows[0].GetComponent<RectTransform>().localPosition;
            end = rows[4].GetComponent<RectTransform>().localPosition;
            differenceVector = end - start;

            solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().sizeDelta = new Vector2(differenceVector.magnitude, 6);
            solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
            solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().position = pos;
            angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
            solutionIndicatorLines[lineIndex++].GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, angle);
        }

        //Diagonals
        pos = values[0].GetComponent<RectTransform>().position;
        start = rows[0].GetComponent<RectTransform>().localPosition;
        end = new Vector3(values[8].GetComponent<RectTransform>().localPosition.x*2, rows[4].GetComponent<RectTransform>().localPosition.y, rows[4].GetComponent<RectTransform>().localPosition.z);
        differenceVector = end - start;

        solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().sizeDelta = new Vector2(differenceVector.magnitude, 6);
        solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
        solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().position = pos;
        angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
        solutionIndicatorLines[lineIndex++].GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, angle);

        pos = values[6].GetComponent<RectTransform>().position;
        start = rows[4].GetComponent<RectTransform>().localPosition;
        end = new Vector3(values[2].GetComponent<RectTransform>().localPosition.x*2, rows[0].GetComponent<RectTransform>().localPosition.y, rows[0].GetComponent<RectTransform>().localPosition.z);
        differenceVector = end - start;

        solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().sizeDelta = new Vector2(differenceVector.magnitude, 6);
        solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
        solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().position = pos;
        angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
        solutionIndicatorLines[lineIndex].GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, angle);

    }

    public void SetButtonActions(UnityAction<int> clickAction, UnityAction next)
    {
        for (int i = 0; i < values.Length; i++)
        {
            UITicTacToeClickLabel label = values[i];
            label.GetComponent<Button>().onClick.AddListener(() => {
                if (!label.isClicked())
                    clickAction(label.GetIndex());
            });
        }

        nextButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (nextButton.GetComponent<Image>().color != Global.COLOR_DISABLED_GRAY)
            {
                nextButton.gameObject.SetActive(false);
                nextButtonHolder.gameObject.SetActive(false);
                waitingIndicator.gameObject.SetActive(true);
                next();
            }
        });
    }

    public void AddOnExitAction(UnityAction action)
    {
        header.GetRightButton().onClick.RemoveAllListeners();
        header.GetRightButton().onClick.AddListener(action);
    }

    public void SetPlayerOneColor(Color color)
    {
        playerOneColor.color = color;
    }

    public void SetPlayerTwoColor(Color color)
    {
        playerTwoColor.color = color;
    }

    public void SetPlayerOneName(string name)
    {
        playerOneName.text = name;
    }

    public void SetPlayerTwoName(string name)
    {
        playerTwoName.text = name;
    }

    public void SetPlayerOneOperand(string operand)
    {
        playerOneOperand.text = operand;
    }

    public void SetPlayerTwoOperand(string operand)
    {
        playerTwoOperand.text = operand;
    }

    public void ActivatePlayerOneTurnIndicator()
    {
        playerOneTurnIndicator.color = new Color32(0xFB, 0xFF, 0x00, 0x99);
        playerTwoTurnIndicator.color = new Color32(0x00, 0x00, 0x00, 0x00);
    }

    public void ActivatePlayerTwoTurnIndicator()
    {
        playerTwoTurnIndicator.color = new Color32(0xFB, 0xFF, 0x00, 0x99);
        playerOneTurnIndicator.color = new Color32(0x00, 0x00, 0x00, 0x00);
    }

    public void SetOperand(int index, BoolOperand op)
    {
        this.values[index].SetText(BooleanAlgebra.ToString(op));
        this.values[index].GetComponent<Image>().color = Global.COLOR_DISABLED_GRAY;
    }

    public void SetSolutionIndicatorLineColor(int index, Color color)
    {
        if (solutionIndicatorLines[index].color == Global.COLOR_DISABLED_GRAY) {
            StartCoroutine(solutionFlasher.Flash(color));
            solutionIndicatorLines[index].GetComponent<Image>().color = color;
            if (color == playerOneColor.color)
                playerOneProgress.Increment();
            else
                playerTwoProgress.Increment();
        }
    }

    public void SetPlayerOneProgress(int roundIndex, bool win)
    {
        if (win)
            playerOneScores[roundIndex].SetCorrect();
        else
            playerOneScores[roundIndex].SetWrong();
    }

    public void SetPlayerTwoProgress(int roundIndex, bool win)
    {
        if (win)
            playerTwoScores[roundIndex].SetCorrect();
        else
            playerTwoScores[roundIndex].SetWrong();
    }

    public void EnableNextButton()
    {
        nextButton.GetComponent<Image>().color = new UnityEngine.Color32(0x00, 0x45, 0x95, 0xFF);
    }

    public string GetPlayerOneName()
    {
        return playerOneName.text;
    }

    public string GetPlayerTwoName()
    {
        return playerTwoName.text;
    }

    public Color GetPlayerOneColor()
    {
        return playerOneColor.color;
    }

    public Color GetPlayerTwoColor()
    {
        return playerTwoColor.color;
    }

    public UIProgressMarker[] GetPlayerOneRounds()
    {
        return playerOneScores;
    }

    public UIProgressMarker[] GetPlayerTwoRounds()
    { 
        return playerTwoScores;
    }

    public void ActivatePlayerWaiting(int playerId)
    {
        if(playerId <= 0)
        {
            playerOneWaitingIndicator.gameObject.SetActive(true);
            playerOneProgressHolder.SetActive(false);
        }
        else
        {
            playerTwoWaitingIndicator.gameObject.SetActive(true);
            playerTwoProgressHolder.SetActive(false);
        }
        
    }
}

﻿using UnityEngine;
using System.Collections;

public class UIBooleanEquationPart : UIElement {

    [SerializeField]
    public GameObject op;

    [SerializeField]
    public GameObject nextTop;

    [SerializeField]
    public GameObject nextBot;

    public void SetOperator(UIElement op)
    {
        op.transform.SetParent(this.op.transform, false);
    }

    public void SetNextTop(UIElement nextTop)
    {
        nextTop.transform.SetParent(this.nextTop.transform, false);
    }

    public void SetNextBot(UIElement nextBot)
    {
        nextBot.transform.SetParent(this.nextBot.transform, false);
    }

    public void DeleteTree()
    {
        Destroy(gameObject);
    }
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class UIBooleanEquationHistory : UIElement
{

    [SerializeField] private Text title;
    [SerializeField] private Text colTextColor;
    [SerializeField] private Text colTextName;
    [SerializeField] protected Text backToLobbyIndicationText;
    [SerializeField] private Button backButton;
    [SerializeField] private GameObject playerListHolder;
    private List<UIBooleanEquationHistoryItem> historyItems;
    private int currentItemNumber;

    void Start()
    {
      
    }

    public void Init()
    {
        historyItems = new List<UIBooleanEquationHistoryItem>();
        currentItemNumber = 0;
    }

    public void SetBackButtonAction(UnityAction buttonAction)
    {
        if (null == buttonAction)
            return;

        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(buttonAction);
    }

    public override void Show()
    {
        base.Show();
        backButton.gameObject.SetActive(true);
    }

    public override void Hide()
    {
        base.Hide();
        backButton.gameObject.SetActive(false);
    }

    public void AddItem(BooleanEquation equation)
    {
        UIBooleanEquationHistoryItem item = UIElement.Instantiate<UIBooleanEquationHistoryItem>(playerListHolder.transform);
        item.SetHeaderEquation(equation.ToBlankEquation(), ++currentItemNumber);
        item.SetMasterEquation(equation.ToString());
        if (currentItemNumber % 2 == 0)
        {
            float greyValue = 160f / 255f;
            item.GetComponent<Image>().color = new Color(greyValue, greyValue, greyValue);
        }

        historyItems.Add(item);
    }

    public void AddUserEquation(string equation, bool correct)
    {
        UIBooleanEquationHistoryItem item = historyItems.ToArray()[currentItemNumber - 1];
        item.AddUserEquation(equation, correct);
    }

    public void EnableMasterEquation()
    {
        historyItems.ToArray()[currentItemNumber - 1].EnableMasterEquation();
    }
}

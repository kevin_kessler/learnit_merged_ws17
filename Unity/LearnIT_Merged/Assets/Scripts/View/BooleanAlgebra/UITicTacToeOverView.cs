﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UITicTacToeOverView : UIGameOverview {

    [SerializeField] private GameObject roundHolder;
    [SerializeField] private Text playerOneName;
    [SerializeField] private Text playerTwoName;
    List<UIRound> rounds;

	// Use this for initialization
	void Start () {
	}

    public void AddRoundUI(UIRound round)
    {
        if(rounds == null)
            rounds = new List<UIRound>();

        round.transform.SetParent(roundHolder.transform);
        rounds.Add(round);
    }

    public override void Show()
    {
        if (rounds == null)
            rounds = new List<UIRound>();

        base.Show();
        foreach (UIRound r in rounds)
            r.Show();
    }

    public override void Hide()
    {
        if (rounds == null)
            rounds = new List<UIRound>();

        base.Hide();
        foreach (UIRound r in rounds)
            r.Hide();
    }

    public void SetPlayeOneName(string name)
    {
        playerOneName.text = name;
    }

    public void SetPlayeTwoName(string name)
    {
        playerTwoName.text = name;
    }
}

﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UITicTacToeHeader : UIElement
{
    [SerializeField] private Button btnRight;
    [SerializeField] private Text txtTitle;

    public Button GetRightButton()
    {
        return btnRight;
    }

    public void SetTitleText(string text)
    {
        txtTitle.text = text;
    }
}

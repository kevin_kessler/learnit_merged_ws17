﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof (CanvasGroup))]
public class UITextFlasher : UICanvasGroupFlasher {

    [SerializeField] private Text textOfCanvasGroupToBeFlashed;

    public void SetFontSize(int fontSize)
    {
        textOfCanvasGroupToBeFlashed.fontSize = fontSize;
    }

    public IEnumerator Flash(Color colorToFlashTextIn)
    {
        Color tmp = textOfCanvasGroupToBeFlashed.color;
        textOfCanvasGroupToBeFlashed.color = colorToFlashTextIn;
        yield return StartCoroutine(base.Flash());
        textOfCanvasGroupToBeFlashed.color = tmp;
    }

    public IEnumerator Flash(Color colorToFlashTextIn, string textToFlash)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(Flash(colorToFlashTextIn));
        textOfCanvasGroupToBeFlashed.text = tmp;
    }

    public IEnumerator Flash(string textToFlash)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(base.Flash());
        textOfCanvasGroupToBeFlashed.text = tmp;
    }

}

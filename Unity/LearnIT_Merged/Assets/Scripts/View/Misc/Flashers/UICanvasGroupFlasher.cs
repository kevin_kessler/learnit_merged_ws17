﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Canvas)), RequireComponent(typeof(CanvasGroup))]
public abstract class UICanvasGroupFlasher : UIElement {

    [SerializeField] private float fadeSpeed;
    [SerializeField] private float stayDuration;

    private CanvasGroup groupToFlash;

    void Start()
    {
        InitGroup();
    }

    private void InitGroup()
    {
        groupToFlash = GetComponent<CanvasGroup>();
        groupToFlash.alpha = 0;

        //display flashing canvas in front of other canvases
        Canvas renderingCanvas = GetComponent<Canvas>();
        renderingCanvas.sortingOrder = 1;
    }

    public void SetFadeSpeed(float speed)
    {
        this.fadeSpeed = speed;
    }

    public void SetStayDuration(float duration)
    {
        this.stayDuration = duration;
    }

    public void SetBlockRaycasts(bool block)
    {
        if (null == groupToFlash)
        {
            InitGroup();
        }

        groupToFlash.blocksRaycasts = block;
    }

    public IEnumerator Flash()
    {
        if (null == groupToFlash)
        {
            InitGroup();
        }

        groupToFlash.transform.SetAsLastSibling();
        yield return StartCoroutine(FadeIn());
        yield return new WaitForSeconds(stayDuration);
        yield return StartCoroutine(FadeOut());
    }

    private IEnumerator FadeIn()
    {
        while (groupToFlash.alpha < 1f)
        {
            groupToFlash.alpha += Time.deltaTime * fadeSpeed;
            yield return null;
        }
    }

    private IEnumerator FadeOut()
    {
        while (groupToFlash.alpha > 0f)
        {
            groupToFlash.alpha -= Time.deltaTime * fadeSpeed;
            yield return null;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIDialogHeader : UIElement {
    [SerializeField] private Button btnClose;
    [SerializeField] private Text txtTitle;

    public Button GetCloseButton()
    {
        return btnClose;
    }

    public void SetTitleText(string text)
    {
        txtTitle.text = text;
    }

    public Text GetTitle()
    {
        return txtTitle;
    }
}

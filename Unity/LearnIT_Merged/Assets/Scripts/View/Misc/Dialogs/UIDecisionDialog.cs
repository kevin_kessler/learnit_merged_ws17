﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;
using UnityEngine.Events;

public class UIDecisionDialog : UIInfoDialog
{

    [SerializeField] protected Button btnAccept;
    [SerializeField] protected Text btnTextAccept;
    [SerializeField] protected Text btnTextDismiss;
    [SerializeField] protected Image btnImgAccept;
    [SerializeField] protected Image btnImgDismiss;

    void Update()
    {
        if (!allowKeyboardInput || !IsShowing())
            return;

        KeyCode pressedKey = InputUtils.GetPressedKey();
        switch (pressedKey)
        {
            case KeyCode.Return:
            case KeyCode.KeypadEnter:
                btnAccept.onClick.Invoke();
                break;
            case KeyCode.Escape:
                btnDismiss.onClick.Invoke();
                break;
        }
    }

    public void AddOnAcceptAction(UnityEngine.Events.UnityAction onAcceptCallback)
    {
        if (null != onAcceptCallback)
            btnAccept.onClick.AddListener(onAcceptCallback);
    }

    public void ClearOnAcceptActions()
    {
        btnAccept.onClick.RemoveAllListeners();
    }

    public void Setup(string dialogTitle, string infoText, Sprite infoSprite = null, UnityAction onDismissCallback = null, UnityAction onAcceptCallback = null)
    {
        SetDialogTitle(dialogTitle);
        SetInfoText(infoText);
        SetInfoIcon(infoSprite);

        ClearOnCloseActions();
        AddOnCloseAction(onDismissCallback);
        AddOnCloseAction(Hide);

        ClearOnAcceptActions();
        AddOnAcceptAction(onAcceptCallback);
        AddOnAcceptAction(Hide);
    }


    public void SetAcceptButtonText(string text)
    {
        btnImgAccept.gameObject.SetActive(false);
        btnTextAccept.gameObject.SetActive(true);
        btnTextAccept.text = text;
    }

    public void SetDismissButtonText(string text)
    {
        btnImgDismiss.gameObject.SetActive(false);
        btnTextDismiss.gameObject.SetActive(true);
        btnTextDismiss.text = text;
    }
}

﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class UIInfoDialog : UIDialog
{
    [SerializeField] private Image imgInfoIcon;
    [SerializeField] private Text txtInfoText;
    [SerializeField] protected Button btnDismiss;

    public void SetInfoText(string infoText)
    {
        txtInfoText.text = infoText;
    }

    protected override void Awake()
    {
        base.Awake();
    }

    void Update()
    {
        if (!allowKeyboardInput || !IsShowing())
            return;

        KeyCode pressedKey = InputUtils.GetPressedKey();
        switch (pressedKey)
        {
            case KeyCode.Return:
            case KeyCode.KeypadEnter:
            case KeyCode.Escape:
                btnDismiss.onClick.Invoke();
                break;
        }
    }

    public override void AddOnCloseAction(UnityEngine.Events.UnityAction onCloseCallback)
    {
        base.AddOnCloseAction(onCloseCallback);

        if (null != onCloseCallback)
            btnDismiss.onClick.AddListener(onCloseCallback);
    }

    public override void ClearOnCloseActions()
    {
        base.ClearOnCloseActions();
        btnDismiss.onClick.RemoveAllListeners();
    }

    public void Setup(string dialogTitle, string infoText, Sprite infoSprite = null, UnityAction onCloseCallback = null)
    {
        SetDialogTitle(dialogTitle);
        SetInfoText(infoText);
        SetInfoIcon(infoSprite);

        ClearOnCloseActions();
        AddOnCloseAction(onCloseCallback);
        AddOnCloseAction(Hide);
    }

    public void SetInfoIcon(Sprite infoSprite = null)
    {
        //show default icon, if none was specified
        if (null == infoSprite){
            infoSprite = Resources.Load<Sprite>(Global.PATH_ICON_INFO);
        }

        imgInfoIcon.sprite = infoSprite;
    }
}

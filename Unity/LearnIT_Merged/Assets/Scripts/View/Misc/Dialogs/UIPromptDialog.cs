﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;
using UnityEngine.Events;

public class UIPromptDialog : UIDecisionDialog
{

    [SerializeField] private InputField input;
    [SerializeField] private Text placeholderText;

    public void AddOnAcceptAction(UnityEngine.Events.UnityAction<string> onAcceptCallback)
    {
        if (null != onAcceptCallback)
            btnAccept.onClick.AddListener(() => { onAcceptCallback(input.text); });
    }

    public void SetInputText(string text)
    {
        input.text = text;
    }

    public void SetPlaceholderText(string text)
    {
        placeholderText.text = text;
    }

    public string GetInputText()
    {
        return input.text;
    }

    public void SetInputCharacterLimit(int characterLimit)
    {
        input.characterLimit = characterLimit;
    }
}

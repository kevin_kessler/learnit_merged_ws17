﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class UISimpleListGroup : UIElement
{
    [SerializeField] protected Text listTitle;
    [SerializeField] protected bool alignScrollbarRight;
    [SerializeField] protected GameObject itemHolder;
    [SerializeField] protected Scrollbar scrollbar;

    protected virtual void Awake()
    {
        SetAlignmentOfScrollbar(alignScrollbarRight);
    }

    public void SetListTitle(string listTitle)
    {
        this.listTitle.text = listTitle;
    }

    public void SetAlignmentOfScrollbar(bool alignRight)
    {
        this.alignScrollbarRight = alignRight;
        if (alignRight)
            scrollbar.transform.SetAsLastSibling();
        else
            scrollbar.transform.SetAsFirstSibling();
    }

    public void AddItem(GameObject item)
    {
        item.transform.SetParent(itemHolder.transform, false);
    }

    public GameObject GetItemHolder()
    {
        return itemHolder;
    }

    public virtual void Clear()
    {
        if (null == itemHolder)
            return;

        foreach (Transform child in itemHolder.transform)
        {
            Destroy(child.gameObject);
        }
    }
}

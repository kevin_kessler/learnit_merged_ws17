﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UICountSlider : UICustomSlider {

    private bool isCountingDown = true;

    //whether the counter counts from 0 to max or from max to 0
    public void SetIsCountingDown(bool countingDown){
        isCountingDown = countingDown;
        Reset();
    }

    public void Decrement()
    {
        if (finished)
            return;

        currentValue--;
        UpdateSlider();

        CheckForEnd();
    }

    public void Increment()
    {
        if (finished)
            return;

        currentValue++;
        UpdateSlider();

        CheckForEnd();
    }

    private void CheckForEnd()
    {
        //check wether countdown reached end
        bool reachedEnd = isCountingDown ? currentValue <= 0 : currentValue >= maxValue;
        if (reachedEnd)
        {
            finished = true;
            currentValue = isCountingDown ? 0 : maxValue;
            if (null != onFinishedHandler)
                NotifyAboutSliderFinished();
            else
                Debug.Log("Counter finished. No OnFinishedListeners set.");
        }
    }

    public override void Reset()
    {
        finished = false;
        currentValue = isCountingDown ? maxValue : 0;
        UpdateSlider();
    }

    public void SetCurrentValue(int value)
    {
        currentValue = value;
        UpdateSlider();
    }
}

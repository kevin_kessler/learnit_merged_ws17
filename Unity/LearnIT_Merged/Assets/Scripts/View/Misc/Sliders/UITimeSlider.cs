﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UITimeSlider : UICustomSlider {

    private bool isPaused = false;

	// Update is called once per frame
	void Update () {
        if (finished || isPaused)
            return;

        //decrease timer
        currentValue -= Time.deltaTime;

        //check wether time is up
        if (currentValue <= 0)
        {
            finished = true;
            currentValue = 0;
            if (null!=onFinishedHandler)
                NotifyAboutSliderFinished();
            else
                Debug.Log("Timer finished. No OnFinishedListeners set.");
        }

        UpdateSlider();
	}

    public void AddTime(float seconds)
    {
        float newTime = currentValue + seconds;
        currentValue = newTime > maxValue ? maxValue : newTime;
    }

    public void SetTime(float seconds)
    {
        currentValue = seconds > maxValue ? maxValue : seconds;
    }

    protected override void UpdateText()
    {
        //update text
        valueText.text = string.Format("{0}{1:N2}{2}", prependingText, currentValue, appendingText);
    }

    public void Pause()
    {
        isPaused = true;
    }

    public void Continue()
    {
        isPaused = false;
    }
}

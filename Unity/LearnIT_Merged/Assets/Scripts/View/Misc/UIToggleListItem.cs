﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System.Linq;

public class UIToggleListItem : UIElement
{
    [SerializeField]
    private Image itemImage;

    [SerializeField]
    private ToggleText toggleText;

    [SerializeField]
    private Toggle toggle;

    public Toggle GetToggle()
    {
        return toggle;
    }

    public void SetItemImage(Sprite sprite)
    {
        itemImage.sprite = sprite;

        if (null == sprite)
            HideItemImage();
        else
            ShowItemImage();
    }

    public void SetText(string text)
    {
        toggleText.GetComponent<Text>().text = text;
    }
    
    public void SetGroup(ToggleGroup group){
        toggle.group = group;
    }

    public void SetInteractable(bool interactable)
    {
        toggle.interactable = interactable;
    }

    public void AddOnValueChangedHandler(UnityAction<bool> handler)
    {
        toggle.onValueChanged.AddListener(handler);
    }

    public void ShowItemImage()
    {
        this.itemImage.gameObject.SetActive(true);
    }

    public void HideItemImage()
    {
        this.itemImage.gameObject.SetActive(false);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class ToggleText : MonoBehaviour {
        
    [SerializeField] private Color activeColor;
    [SerializeField] private Color inactiveColor;
    [SerializeField] private Toggle toggle;

    private Text textToChange;

    void Start()
    {
        textToChange = GetComponent<Text>();
        toggle.onValueChanged.AddListener(SwitchTextColor);
        SwitchTextColor(toggle.isOn);
    }

    public void SwitchTextColor(bool active)
    {
        if(null!=textToChange)
            textToChange.color = active ? activeColor : inactiveColor;
    }
}

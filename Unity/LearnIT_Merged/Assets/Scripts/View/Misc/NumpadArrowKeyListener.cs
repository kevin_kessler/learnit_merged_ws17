﻿using UnityEngine;
using System.Collections;

public interface NumpadArrowKeyListener
{
	void UpEvent();
	void DownEvent();
	void LeftEvent();
	void RightEvent();
}
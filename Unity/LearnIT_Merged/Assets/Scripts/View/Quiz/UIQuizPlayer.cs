﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UIQuizPlayer : UIMPGamePlayer
{
    [SerializeField] private Text usedTimeToSolveText;
    [SerializeField] private Text correctAnswersText;
    private float usedTimeToSolve;
    private int numOfQuestions;
    private int numOfCorrectAnswers;

    void Awake()
    {
        SetUsedTimeToSolveText(0);
    }

    public void SetTotalNumberOfQuestions(int numOfQuestions)
    {
        this.numOfQuestions = numOfQuestions;
        this.numOfCorrectAnswers = 0;
        SetCorrectAnswersText(numOfCorrectAnswers, numOfQuestions);
    }

    public void SetNumberOfCorrectAnswers(int numOfCorrectAnswers)
    {
        this.numOfCorrectAnswers = numOfCorrectAnswers;
        SetCorrectAnswersText(numOfCorrectAnswers, numOfQuestions);
    }

    public void IncreaseNumOfCorrectAnswers()
    {
        numOfCorrectAnswers++;
        SetCorrectAnswersText(numOfCorrectAnswers, numOfQuestions);
    }

    private void SetCorrectAnswersText(int numOfCorrectAnswers, int numOfQuestions)
    {
        this.correctAnswersText.text = string.Format("{0} / {1}", numOfCorrectAnswers, numOfQuestions);
    }

    public void SetUsedTimeToSolveText(float timeToSolve)
    {
        this.usedTimeToSolve = timeToSolve;
        this.usedTimeToSolveText.text = Mathf.CeilToInt(timeToSolve).ToString() + " s";
    }

    public float GetUsedTimeToSolve()
    {
        return usedTimeToSolve;
    }
}

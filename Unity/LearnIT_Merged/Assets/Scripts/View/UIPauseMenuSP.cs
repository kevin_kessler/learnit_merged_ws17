﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class UIPauseMenuSP : UIDialog
{
    [SerializeField] private Button quitButton;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button resumeButton;
    [SerializeField] private Button helpButton;
    [SerializeField] private Button settingsButton;

    public void AddOnQuitAction(UnityAction onQuitAction)
    {
        if (null != onQuitAction)
            quitButton.onClick.AddListener(onQuitAction);
    }

    public void ClearOnQuitActions()
    {
        quitButton.onClick.RemoveAllListeners();
    }

    public void AddOnHelpAction(UnityAction onHelpAction)
    {
        if (null != onHelpAction)
            helpButton.onClick.AddListener(onHelpAction);
    }

    public void ClearOnHelpActions()
    {
        helpButton.onClick.RemoveAllListeners();
    }

    public void AddOnSettingsAction(UnityAction onSettingsAction)
    {
        if (null != onSettingsAction)
            settingsButton.onClick.AddListener(onSettingsAction);
    }

    public void ClearOnSettingsActions()
    {
        settingsButton.onClick.RemoveAllListeners();
    }

    public void AddOnRestartAction(UnityAction onRestartAction)
    {
        if (null != onRestartAction)
            restartButton.onClick.AddListener(onRestartAction);
    }

    public void ClearOnRestartActions()
    {
        restartButton.onClick.RemoveAllListeners();
    }

    public void AddOnResumeAction(UnityAction onResumeAction)
    {
        if (null != onResumeAction)
            resumeButton.onClick.AddListener(onResumeAction);
    }

    public void ClearOnResumeActions()
    {
        resumeButton.onClick.RemoveAllListeners();
    }

    protected override void Awake()
    {
        base.Awake();

        header.SetTitleText(Global.STRING_PAUSE);
    }

    public void ShowHelpButton()
    {
        helpButton.gameObject.SetActive(true);
    }

    public void HideHelpButton()
    {
        helpButton.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExecuteOnCloseActions();
        }
    }
}
  

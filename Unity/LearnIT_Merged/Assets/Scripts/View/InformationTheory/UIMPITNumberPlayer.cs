﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UIMPITNumberPlayer : UIMPGamePlayer
{
    [SerializeField] private Text numOfGuesses;
    [SerializeField] private Text numOfCorrectGuesses;
    [SerializeField] private Text bonusTime;
    [SerializeField] private Text level;

    public void SetNumOfGuesses(int num)
    {
        this.numOfGuesses.text = num.ToString();
    }

    public void SetNumOfCorrectGuesses(int num)
    {
        this.numOfCorrectGuesses.text = num.ToString();
    }

    public void SetBonusTime(float bonusTime)
    {
        this.bonusTime.text = string.Format(" {0}s", Mathf.RoundToInt(bonusTime));
    }

    public void SetLevel(int lvl)
    {
        this.level.text = lvl.ToString();
    }
}

﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class UIMPGamePlayer : UIElement
{
    [SerializeField] private Image colorImage;
    [SerializeField] private Text playerName;
    [SerializeField] private Toggle backToLobbyToggle;

    void Start()
    {
        this.transform.localScale = Vector3.one;
        backToLobbyToggle.isOn = false;
        backToLobbyToggle.interactable = false;
    }

    public void SetReadyToggleState(bool readyState)
    {
        backToLobbyToggle.isOn = readyState;
    }

    private void SetReadyToggleCallback(UnityAction<bool> clbck)
    {
        if (null == clbck)
            return;

        backToLobbyToggle.onValueChanged.RemoveAllListeners();
        backToLobbyToggle.onValueChanged.AddListener(clbck);
    }

    public void SetPlayerName(string name)
    {
        this.playerName.text = name;
    }

    public string GetPlayerName()
    {
        return playerName.text;
    }

    public void SetPlayerColor(Color color)
    {
        colorImage.GetComponent<Image>().color = color;
    }

    public virtual void UseAsSinglePlayer()
    {
        this.colorImage.transform.parent.gameObject.SetActive(false);
        this.backToLobbyToggle.transform.parent.gameObject.SetActive(false);
    }
}

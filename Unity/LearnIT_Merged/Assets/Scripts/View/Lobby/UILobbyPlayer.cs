﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using Utils;

public class UILobbyPlayer : UIElement
{
    [SerializeField] private Text playerName;
    [SerializeField] private Button colorButton;
    [SerializeField] private Toggle readyToggle;
    private Color notInteractableColor;
    private Color interactableColor;

    void Start()
    {
        this.transform.localScale = Vector3.one;
        SetReadyToggleState(false);
    }

    //sets the toggle value without raising the event
    public void SetReadyToggleState(bool readyState)
    {
        readyToggle.Set(readyState, false);
    }

    private void SetReadyToggleCallback(UnityAction<bool> clbck)
    {
        if (null == clbck)
            return;

        readyToggle.onValueChanged.RemoveAllListeners();
        readyToggle.onValueChanged.AddListener(clbck);
    }

    private void SetColorButtonCallback(UnityAction clbck)
    {
        if (null == clbck)
            return;

        colorButton.onClick.RemoveAllListeners();
        colorButton.onClick.AddListener(clbck);
    }

    public void PreventInteraction()
    {
        readyToggle.interactable = false;
        ToggleColorButton(false);
        TogglePlayerName(false);
    }

    public void AllowInteraction()
    {
        readyToggle.interactable = true;
        ToggleColorButton(true);
        TogglePlayerName(true);
    }

    public void SetLocalCallbacks(UnityAction colorCallback, UnityAction<bool> readyCallback)
    {
        SetColorButtonCallback(colorCallback);
        SetReadyToggleCallback(readyCallback);
    }

    public void ToggleColorButton(bool interactable)
    {
        colorButton.interactable = interactable;
    }

    public void TogglePlayerName(bool interactable)
    {
        Color c = playerName.color;
        c.a = interactable ? 1 : 0.5f;
        playerName.color = c;
    }

    public void SetPlayerName(string name)
    {
        this.playerName.text = name;
    }

    public void SetPlayerColor(Color color)
    {
        colorButton.GetComponent<Image>().color = color;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

[RequireComponent(typeof(Canvas))]
public class UIGameOverview : UIElement {

    [SerializeField] private Text title;
    [SerializeField] private Text colTextColor;
    [SerializeField] private Text colTextName;
    [SerializeField] private Text colTextBackToLobby;
    [SerializeField] protected Text backToLobbyIndicationText;
    [SerializeField] private Button backButton;
    [SerializeField] private Text backButtonText;
    [SerializeField] private GameObject playerListHolder;
    protected List<UIMPGamePlayer> playerUIs;
    protected Canvas canvas;
    protected bool isSinglePlayerOverview = false;

    protected virtual void Awake()
    {
        playerUIs = new List<UIMPGamePlayer>();
        canvas = GetComponent<Canvas>();

        title.text = Global.STRING_OVERVIEW_TITLE;
        colTextColor.text = Global.STRING_OVERVIEW_COLOR;
        colTextName.text = Global.STRING_OVERVIEW_NAME;
        colTextBackToLobby.text = Global.STRING_OVERVIEW_BACK_TO_LOBBY;
        backButtonText.text = Global.STRING_OVERVIEW_BACK_TO_LOBBY;
    }

    public void SetTitle(string title)
    {
        this.title.text = title;
    }

    public virtual void AddPlayerUI(UIMPGamePlayer playerUI)
    {
        if (null == playerUI)
            return;

        playerUIs.Add(playerUI);
        playerUI.transform.SetParent(playerListHolder.transform, false);

        if (isSinglePlayerOverview)
            playerUI.UseAsSinglePlayer();
    }

    public virtual void RemovePlayerUI(UIMPGamePlayer playerUI)
    {
        if (null == playerUI || !playerUIs.Contains(playerUI))
            return;

        playerUIs.Remove(playerUI);
        playerUI.transform.SetParent(null);
    }

    public void SetBackToLobbyTimerText(int seconds)
    {
        backToLobbyIndicationText.text = string.Format(Global.STRING_OVERVIEW_RETURNING_TO_LOBBY, seconds.ToString());
    }

    public void SetBackButtonAction(UnityAction buttonAction)
    {
        if (null == buttonAction)
            return;

        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(buttonAction);
    }

    public override void Show()
    {
        base.Show();
        backToLobbyIndicationText.gameObject.SetActive(!isSinglePlayerOverview);
        backButton.gameObject.SetActive(true);
    }

    public virtual void ShowWithoutFooter()
    {
        base.Show();
        backToLobbyIndicationText.gameObject.SetActive(false);
        backButton.gameObject.SetActive(false);
    }

    public virtual void UseAsSinglePlayerOverview()
    {
        colTextColor.gameObject.SetActive(false);
        colTextBackToLobby.gameObject.SetActive(false);
        backToLobbyIndicationText.gameObject.SetActive(false);
        backButtonText.text = "OK";

        foreach (UIMPGamePlayer ui in playerUIs)
            ui.UseAsSinglePlayer();

        isSinglePlayerOverview = true;
    }
}

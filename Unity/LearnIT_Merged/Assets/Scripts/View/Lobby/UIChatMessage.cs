﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIChatMessage : UIElement {

    [SerializeField] private Text senderName;
    [SerializeField] private Text messageText;
    [SerializeField] private Text timeStampText;

    public void Setup(string senderName, string messageText)
    {
        this.senderName.text = senderName;
        this.messageText.text = messageText;
        this.timeStampText.text = System.DateTime.Now.ToString("hh:mm");
    }

}

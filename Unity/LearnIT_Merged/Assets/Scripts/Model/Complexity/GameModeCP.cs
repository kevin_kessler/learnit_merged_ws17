﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.IO;

public class GameModeCP : GameMode {
    private static System.Random rng = new System.Random();

    private List<GCGraphData> graphData;

    public GameModeCP(List<GCGraphData> graphData, string name, string description, string spritePath, string gameSceneName, string tutorialPath = null, string mpGamePlayerPrefabPath = null)
        :base(name, description, spritePath, gameSceneName, tutorialPath, mpGamePlayerPrefabPath)
    {
        this.graphData = graphData;
    }

    public List<GCGraphData> GetGraphData()
    {
        return graphData;
    }

    public List<GCGraphData> GetGraphData(int amountOfGraphs)
    {
        //if random mode, choose randomly among all graphdata
        if (null == graphData)
            return GCGraphData.GetRandomSelectionOfGraphData(amountOfGraphs);

        //else choose randomly among the assigned graphdata by copying the list and removing entries randomly until amount is reached
        List<GCGraphData> dataToChooseFrom = new List<GCGraphData>(GetGraphData());
        while (dataToChooseFrom.Count > amountOfGraphs)
            dataToChooseFrom.RemoveAt(rng.Next(0, dataToChooseFrom.Count));
        return dataToChooseFrom;
    }
}

﻿using UnityEngine;
using System.Collections;

public class GameModeQuiz : GameMode {

    private string quizXml;
    private Quiz quiz;

    public GameModeQuiz(string quizXml, string name, string description, string spritePath, string gameSceneName, string tutorialPath = null, string mpGamePlayerPrefabPath = null)
        :base(name, description, spritePath, gameSceneName, tutorialPath, mpGamePlayerPrefabPath)
    {
        this.quizXml = quizXml;
    }

    public Quiz GetQuiz()
    {
        if (null == quiz)
            quiz = QuizLoader.LoadQuizByName(quizXml);
        return quiz;
    }
}

﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class QuizQuestion {

    public enum AnswerState
    {
        NOT_ANSWERED,
        CORRECT_ANSWERED,
        WRONG_ANSWERED
    }

    private string questionText;
    private string questionImageName;
    private string[] answers;
    private int correctAnswer;
    private float solveDuration;
    private byte[] questionImageBytes;

    [System.NonSerialized]
    private int givenAnswer;
    [System.NonSerialized]
    private AnswerState answerState;

    public QuizQuestion(string questionText, string questionImageName, string answer0, string answer1, string answer2, string answer3, int correctAnswer, float solveDuration)
    {
        this.questionText = questionText;
        this.questionImageName = questionImageName;
        this.answers = new string[4];
        answers[0] = answer0;
        answers[1] = answer1;
        answers[2] = answer2;
        answers[3] = answer3;
        this.correctAnswer = correctAnswer;
        this.solveDuration = solveDuration;

        this.givenAnswer = -1;
        this.answerState = AnswerState.NOT_ANSWERED;
    }

    public void SetQuestionImageBytes(byte[] imgBytes)
    {
        this.questionImageBytes = imgBytes;
    }

    public bool IsImageQuestion()
    {
        return null != questionImageName || null != questionImageBytes;
    }

    public string GetQuestionText()
    {
        return questionText;
    }

    public string GetQuestionImageName()
    {
        return questionImageName;
    }

    public byte[] GetQuestionImageBytes()
    {
        return questionImageBytes;
    }

    public string GetPossibleAnswer(int index)
    {
        Debug.Assert(index >= 0 && index < answers.Length);

        return answers[index];
    }

    public string GetCorrectAnswer() {
        return GetPossibleAnswer(correctAnswer);
    }

    public int GetCorrectAnswerIndex()
    {
        return correctAnswer;
    }

    public int GetGivenAnswerIndex()
    {
        return givenAnswer;
    }

    public float GetSolveDuration()
    {
        return solveDuration;
    }

    public void SetSolveDuration(float solveDuration)
    {
        this.solveDuration = solveDuration;
    }

    public AnswerState GetAnswerState(){
        return answerState;
    }

    public void GiveAnswer(int answerIndex)
    {
        if (IsAnswered())
            return;

        this.givenAnswer = answerIndex;
        this.answerState = givenAnswer == correctAnswer ? AnswerState.CORRECT_ANSWERED : AnswerState.WRONG_ANSWERED;
    }

    public bool IsAnswered()
    {
        return answerState != AnswerState.NOT_ANSWERED;
    }

    public void Reset()
    {
        this.givenAnswer = -1;
        this.answerState = AnswerState.NOT_ANSWERED;
    }
}

﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.IO;

public class Game {

    private static List<Game> ALL_GAMES = new List<Game>();
    private static int idCounter = 0;

    public static ReadOnlyCollection<Game> GetAllGames()
    {
        return ALL_GAMES.AsReadOnly();
    }

    private int id;
    private string name;
    private List<GameMode> modes;

    public Game(string name)
    {
        this.id = idCounter++;
        this.name = name;
        this.modes = new List<GameMode>();
        ALL_GAMES.Add(this);
    }

    public Game(string name, List<GameMode> modes)
        :this(name)
    {
        if (modes == null)
            this.modes = new List<GameMode>();
        else
            this.modes = modes;
    }

    public void AddGameMode(GameMode gameMode)
    {
        this.modes.Add(gameMode);
    }

    public int GetId()
    {
        return id;
    }

    public string GetName()
    {
        return name;
    }

    public ReadOnlyCollection<GameMode> GetModes()
    {
        if (null == modes)
            return null;
        return modes.AsReadOnly();
    }
}

﻿using UnityEngine;
using System.Collections;

public class GameModeSubnet : GameMode
{
    public enum Difficulty
    {
        EASY,
        MASTER
    }

    public enum TurnMode
    {
        NULL,
        TIME,
        TRIES

    }

    public enum PlayMode
    {
        SINGLEPLAYER,
        PVP,
        COOP
    }

    private Difficulty activeDifficulty;
    private TurnMode activeTurnMode;
    private PlayMode activePlayMode;

    private Subnet subnet;

    public GameModeSubnet( TurnMode turnMode, Difficulty difficulty, PlayMode playMode, int questionCount, int ipPerQuestion, string name, string description, string spritePath, string gameSceneName, string tutorialPath = null, string mpGamePlayerPrefabPath = null) : base(name, description, spritePath, gameSceneName, tutorialPath, mpGamePlayerPrefabPath)
    {
        activeTurnMode = turnMode;
        activeDifficulty = difficulty;
        activePlayMode = playMode;
        subnet = new Subnet(questionCount, ipPerQuestion, difficulty);
    }

    public Subnet GetSubnet()
    {
        return subnet;
    }

    public Difficulty GetActiveDifficulty()
    {
        return activeDifficulty;
    }

    public TurnMode GetActiveTurnMode()
    {
        return activeTurnMode;
    }

    public PlayMode GetActivePlayMode()
    {
        return activePlayMode;
    }
}
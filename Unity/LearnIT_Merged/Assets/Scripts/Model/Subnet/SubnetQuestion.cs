﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class SubnetQuestion
{
    public enum AnswerState
    {
        CORRECT_ANSWERED,
        WRONG_ANSWERED,
        NOT_ANSWERED
    }

    private AnswerState questionState;

    private bool[] answeredPart = new bool[4];

    private string[] questionText;
    private string resultQuestionText;
    private IPAddress questionIP;
    private IPAddress correctIP;
    private IPAddress[] ipaddresses;

    private int[] subnetmask = new int[4];

    private int questionNumber;

    private System.Random rand;

    public SubnetQuestion(int numberOfIPs, int questionNumber, GameModeSubnet.Difficulty difficulty)
    {
        rand = new System.Random(GetHashCode());
        questionText = new string[2];
        this.questionNumber = questionNumber;
        questionState = AnswerState.NOT_ANSWERED;

        GenerateSubnet(difficulty);
        GenerateIPs(numberOfIPs);
        SetQuestionText();
    }

    private void GenerateIPs(int numberOfIPs)
    {
        ipaddresses = new IPAddress[numberOfIPs];
        questionIP = new IPAddress();

        correctIP = IPAddress.GenerateMatchingIP(questionIP, subnetmask);
        ipaddresses[0] = correctIP;

        for (int i = 1; i < ipaddresses.Length; i++)
        {
            IPAddress newIPA = new IPAddress();
            if (newIPA.Equals(questionIP) || HasDoubleIPPart(newIPA, i))
            {
                i--;
                continue;
            }
            ipaddresses[i] = newIPA;
        }
    }

    private bool HasDoubleIPPart(IPAddress ipa, int ipCount)
    {
        for (int i = 0; i < ipCount; i++)
        {
            for (int j = 0; j < ipa.GetAddress().Length; j++)
            {
                if (ipaddresses[i].GetAddress()[j] == ipa.GetAddress()[j])
                {
                    return true;
                }
            }
        }

        return false;
    }

    private void GenerateSubnet(GameModeSubnet.Difficulty diff)
    {
        int[] bits = new int[32];
        List<int> bitGroups = new List<int>();
        int prefixBits = 0;
        string bitString;
        switch (diff)
        {
            case GameModeSubnet.Difficulty.EASY:
                for (int i = 1; i < 30; i++)
                {
                    bitGroups.Add(i);
                }
                break;
            case GameModeSubnet.Difficulty.MASTER:
                for (int i = 1; i < 30; i++)
                {
                    if (i % 8 == 0)
                    {
                        continue;
                    }
                    bitGroups.Add(i);
                }
                break;
            default:
                break;
        }

        prefixBits = bitGroups[rand.Next(bitGroups.Count)];

        //Debug.Log("Prefix-Bits: " + prefixBits + " Difficulty: " + diff);
        
        for (int i = 0; i < 32; i++)
        {
            if (i < prefixBits)
                bits[i] = 1;
            else
                bits[i] = 0;
        }

        for (int i = 0; i < 4; i++)
        {
            bitString = "";

            for (int j = 0; j < 8; j++)
            {
                bitString += bits[(i * 8) + j];
            }

            subnetmask[i] = Convert.ToInt32(bitString, 2);
        }

        //Debug.Log("SNM: " + SubnetmaskToString());
    }

    private string SubnetmaskToString()
    {
        return subnetmask[0] + "." + subnetmask[1] + "." + subnetmask[2] + "." + subnetmask[3];
    }

    private void SetQuestionText()
    {
        questionText[0] = "IP-Adresse" + Environment.NewLine + questionIP.ToString();
        questionText[1] = "Subnetzmaske" + Environment.NewLine + SubnetmaskToString();
        resultQuestionText = "IP-Adresse: "  + questionIP.ToString() + " Subnetzmaske: " + SubnetmaskToString();
    }

    private void IsQuestionAnswered()
    {
        foreach (bool b in answeredPart)
        {
            if (!b) return;
        }
        questionState = AnswerState.CORRECT_ANSWERED;
    }

    public void WrongAnswered()
    {
        questionState = AnswerState.WRONG_ANSWERED;
    }

    public string GetResultQuestionText()
    {
        return resultQuestionText;
    }

    public string[] GetQuestionText()
    {
        return questionText;
    }

    public IPAddress GetCorrectIPAddress()
    {
        return correctIP;
    }

    public IPAddress[] GetIPAddresses()
    {
        IPAddress[] ipaOut = new IPAddress[ipaddresses.Length];
        for (int i = 0; i < ipaOut.Length; i++)
        {
            ipaOut[i] = ipaddresses[i];
        }
        return ipaOut;
    }

    public bool IsValidInput(int value, int part)
    {
        if (questionIP.GetAddress()[part] == value && subnetmask[part] != 255)
        {
            return false;
        }

        int v1 = value & subnetmask[part];
        //Debug.Log("v1: " + v1);
        int v2 = questionIP.GetAddress()[part] & subnetmask[part];
        //Debug.Log("v2: " + v2);

        bool outB = v1 == v2;
        answeredPart[part] = outB;

        IsQuestionAnswered();

        return outB;
    }

    public AnswerState GetAnsweredState()
    {
        return questionState;
    }

    public int GetQuestionNumber()
    {
        return questionNumber;
    }

    internal void Reset()
    {
        questionState = AnswerState.NOT_ANSWERED;
        answeredPart[0] = false;
        answeredPart[1] = false;
        answeredPart[2] = false;
        answeredPart[3] = false;
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Subnet
{
    private int questionCount;
    private int ipPerQuestion;
    private GameModeSubnet.Difficulty difficulty;

    private SubnetQuestion[] questions;
    private int activeQuestion = 0;

    private string inputIP;

    public Subnet(int questionCount, int ipPerQuestion, GameModeSubnet.Difficulty difficulty)
    {
        this.questionCount = questionCount;
        this.ipPerQuestion = ipPerQuestion;
        this.difficulty = difficulty;

        questions = new SubnetQuestion[questionCount];
        GenerateQuestions(questionCount, ipPerQuestion, difficulty);
    }

    public void Reset()
    {
        questions = new SubnetQuestion[questionCount];
        activeQuestion = 0;
        GenerateQuestions(questionCount, ipPerQuestion, difficulty);
    }

    public int GetQuestionCount()
    {
        return questionCount;
    }

    public int GetIPPerQuestion()
    {
        return ipPerQuestion;
    }

    public string[] GetActiveQuestionText()
    {
        return questions[activeQuestion].GetQuestionText();
    }

    public SubnetQuestion[] GetAllQuestions()
    {
        return questions;
    }

    public SubnetQuestion GetActiveQuestion()
    {
        return questions[activeQuestion];
    }

    public int ActiveQuestionIndex()
    {
        return activeQuestion;
    }

    private void GenerateQuestions(int numberOfQuestions, int ipPerQuestion, GameModeSubnet.Difficulty difficulty)
    {
        for (int i = 0; i < numberOfQuestions; i++)
        {
            questions[i] = new SubnetQuestion(ipPerQuestion , i, difficulty);
        }
    }

    public bool IsValidInput(int value, int part)
    {
        return questions[activeQuestion].IsValidInput(value, part);
    }

    public bool IsValidInput(int value, int part, SubnetQuestion question)
    {
        return question.IsValidInput(value, part);
    }

    public void NextQuestion()
    {
        activeQuestion++;
    }

}
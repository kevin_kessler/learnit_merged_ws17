﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class IPAddress
{
    private int[] ipAddressBytes;

    private System.Random rand;

    public IPAddress()
    {
        rand = new System.Random(GetHashCode());

        GenerateIP();
    }

    public static IPAddress GenerateMatchingIP(IPAddress ipaddress, int[] subnetmask)
    {
        IPAddress outIP = new IPAddress();

        for (int i = 0; i < subnetmask.Length; i++)
        {
            if (subnetmask[i] == 0)
                break;
           
            if (subnetmask[i] == 255)
            {
                outIP.ipAddressBytes[i] = ipaddress.GetAddress()[i];
                continue;
            }

            outIP.ipAddressBytes[i] = outIP.GenerateSNMIPPart(ipaddress.GetAddress()[i], subnetmask[i]);
            break;
        }
                 
        return outIP;
    }

    private void GenerateIP()
    {
        ipAddressBytes = new int[4];

        for (int i = 0; i < 4; i++)
        {
            ipAddressBytes[i] = rand.Next(1, 254);
        }
    }

    private int GenerateSNMIPPart(int ipPart, int snmPart)
    {
        int activeBit = 0;
        int outI = 0;
        char[] snm = Convert.ToString(snmPart, 2).ToCharArray();
        char[] ip = Convert.ToString(ipPart, 2).PadLeft(8, '0').ToCharArray();

        for (int i = 0; i < snm.Length; i++)
        {
            if (snm[i] == '0')
            {
                activeBit = i;
                break;
            }
        }

        do
        {
            for (int i = 0; i < ip.Length; i++)
            {
                if (i < activeBit)
                {
                    ip[i] = ip[i];
                    continue;
                }
                if (rand.Next(2) >= 1)
                {
                    ip[i] = '1';
                }

                else
                {
                    ip[i] = '0';
                }
            }

            outI = Convert.ToInt32(new string(ip), 2);
        } while ( outI == ipPart);


        return outI;
    }

    public int[] GetAddress()
    {
        return ipAddressBytes;
    }

    public override bool Equals(object obj)
    {
        if (obj == null) return false;
        if (!(obj is IPAddress)) return false;
        if (obj == this) return true;

        for (int i = 0; i < ipAddressBytes.Length; i++)
        {
            if (((IPAddress) obj).GetAddress()[i] != ipAddressBytes[i])
            {
                return false;
            }
        }
        return true;
    }

    public override string ToString()
    {
        return ipAddressBytes[0] + "." + ipAddressBytes[1] + "." + ipAddressBytes[2] + "." + ipAddressBytes[3];
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}

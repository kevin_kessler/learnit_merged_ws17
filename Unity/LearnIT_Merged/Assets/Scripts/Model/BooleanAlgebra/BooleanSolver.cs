﻿

using System.Collections.Generic;
using UnityEngine;

public class BooleanSolver
{

    public static BoolOperand Evaluate(BooleanEquation boolEq)
    {
        foreach(BoolOperand op in boolEq.GetOperands())        
            if (op == BoolOperand.NULL)
                return BoolOperand.NULL;        

        BooleanEquation tmp = new BooleanEquation(boolEq.GetOperands(), boolEq.GetOperators());

        tmp = EvaluateNOT(tmp);
        tmp = EvaluateOperator(tmp, BoolOperator.AND);
        tmp = EvaluateOperator(tmp, BoolOperator.OR);

        return tmp.GetOperands()[0];
    }

    private static BooleanEquation EvaluateNOT(BooleanEquation boolEq)
    {
        BoolOperand[] operands = new BoolOperand[boolEq.GetOperands().Length];
        BoolOperator[] operators = new BoolOperator[boolEq.GetOperators().Length];
        System.Array.Copy(boolEq.GetOperands(), operands, boolEq.GetOperands().Length);
        System.Array.Copy(boolEq.GetOperators(), operators, boolEq.GetOperators().Length);
        //Debug.Log(boolEq.ToString());
        //Debug.Log("Init operands = " + operands.Length);


        int startChain = 0;
        List<BooleanEquation> chainsToEvaluate = new List<BooleanEquation>();
        List<int> chainStartIndices = new List<int>();
        for (int i = 0; i < operators.Length; i++)
        {
            if (operators[i] == BoolOperator.NAND | operators[i] == BoolOperator.NOR)
            {
                startChain = i;
                List<BoolOperator> opts = new List<BoolOperator>();
                List<BoolOperand> ops = new List<BoolOperand>();
                for(int j = i, chainCounter = 0; j < operators.Length; j++, i++, chainCounter++)
                {
                    opts.Add(operators[j]);
                    if(chainCounter == 0)
                        ops.Add(operands[j]);
                    ops.Add(operands[j + 1]);
                    if (j + 1 >= operators.Length || (operators[j + 1] != BoolOperator.NAND & operators[j + 1] != BoolOperator.NOR))
                    {
                        break;
                    }
                }

                chainStartIndices.Add(startChain);
                chainsToEvaluate.Add(new BooleanEquation(ops.ToArray(), opts.ToArray()));
                Debug.Log(new BooleanEquation(ops.ToArray(), opts.ToArray()).ToString());
            }
        }

        int e = 0;
        foreach(BooleanEquation equation in chainsToEvaluate)
        {
            for(int i = 0; i < equation.GetOperators().Length; i++)
            {
                int negate = (equation.GetOperators().Length - 1 - i) % 2;

                if(i == equation.GetOperators().Length - 1)
                {
                    operators[chainStartIndices.ToArray()[e] + i] = BooleanAlgebra.Negate(equation.GetOperators()[i]);
                    if (i == 0)
                    {
                        operands[chainStartIndices.ToArray()[e] + i] = BooleanAlgebra.Negate(equation.GetOperands()[i]);
                    }
                    operands[chainStartIndices.ToArray()[e] + i + 1] = BooleanAlgebra.Negate(equation.GetOperands()[i + 1]);
                }
                else if(negate == 1)
                {
                    operators[chainStartIndices.ToArray()[e] + i] = BooleanAlgebra.Identity(equation.GetOperators()[i]);
                }
                else
                {
                    operators[chainStartIndices.ToArray()[e] + i] = BooleanAlgebra.Negate(equation.GetOperators()[i]);
                    if (i == 0)
                    {
                        operands[chainStartIndices.ToArray()[e] + i] = BooleanAlgebra.Negate(equation.GetOperands()[i]);
                    }
                    operands[chainStartIndices.ToArray()[e] + i + 1] = BooleanAlgebra.Negate(equation.GetOperands()[i + 1]);
                }
            }

            e++;
        }

        return new BooleanEquation(operands, operators);
    }

    private static BooleanEquation EvaluateOperator(BooleanEquation boolEq, BoolOperator op)
    {
        BoolOperand[] operands = new BoolOperand[boolEq.GetOperands().Length];
        BoolOperator[] operators = new BoolOperator[boolEq.GetOperators().Length];
        System.Array.Copy(boolEq.GetOperands(), operands, boolEq.GetOperands().Length);
        System.Array.Copy(boolEq.GetOperators(), operators, boolEq.GetOperators().Length);

        for (int i = 0; i < operators.Length; i++)
        {
            if (operators[i] == op)
            {
                operands[i] = EvaluateTriple(operands[i], operators[i], operands[i + 1]);

                // Überflüssige Variablen = NULL
                operands[i + 1] = BoolOperand.NULL;
                operators[i] = BoolOperator.NULL;

                BoolOperand[] tmpOperands = new BoolOperand[operands.Length - 1];
                BoolOperator[] tmpOperators = new BoolOperator[operators.Length - 1];

                // Operanden-Array um NULL reduzieren
                for (int j = 0, k = 0; j < operands.Length; j++)
                {
                    if(operands[j] != BoolOperand.NULL)
                        tmpOperands[k++] = operands[j];
                }

                // Operatoren-Array um NULL reduzieren
                for (int j = 0, k = 0; j < operators.Length; j++)
                {
                    if (operators[j] != BoolOperator.NULL)
                        tmpOperators[k++] = operators[j];
                }

                operands = tmpOperands;
                operators = tmpOperators;
                i = -1;
            }
        }
        return new BooleanEquation(operands, operators);
    }

    private static BoolOperand EvaluateTriple(BoolOperand op1, BoolOperator op, BoolOperand op2)
    {
        switch (op)
        {
            case BoolOperator.NAND:
                return EvaluateOR(BooleanAlgebra.Negate(op1), BooleanAlgebra.Negate(op2));
            case BoolOperator.NOR:
                return EvaluateAND(BooleanAlgebra.Negate(op1), BooleanAlgebra.Negate(op2));
            case BoolOperator.AND:
                return EvaluateAND(op1, op2);
            case BoolOperator.OR:
                return EvaluateOR(op1, op2);
            default:
                return BoolOperand.NULL;
        }
    }

    private static BoolOperand EvaluateAND(BoolOperand op1, BoolOperand op2)
    {
        if (op1 == BoolOperand.ZERO | op2 == BoolOperand.ZERO)
            return BoolOperand.ZERO;
        else if (op1 == BoolOperand.ONE && op1 != op2)
            return op2;
        else if (op2 == BoolOperand.ONE && op1 != op2)
            return op1;
        else if (op1 == BoolOperand.ONE | op2 == BoolOperand.ONE)
            return BoolOperand.ONE;
        else if (op1 != op2)
            return BoolOperand.ZERO;
        else if (op1 == op2)
            return op1;
        else
            return BoolOperand.NULL;
    }

    private static BoolOperand EvaluateOR(BoolOperand op1, BoolOperand op2)
    {
        if (op1 == BoolOperand.ONE | op2 == BoolOperand.ONE)
            return BoolOperand.ONE;
        else if ((op1 == BoolOperand.X && op2 == BoolOperand.NOTX) | (op1 == BoolOperand.NOTX && op2 == BoolOperand.X))
            return BoolOperand.ONE;
        else if (op1 == BoolOperand.ZERO && op2 != BoolOperand.ZERO)
            return op2;
        else if (op2 == BoolOperand.ZERO && op1 != BoolOperand.ZERO)
            return op1;
        else if (op1 == op2)
            return op1;
        else
            return BoolOperand.NULL;
    }
}

﻿using UnityEngine;
using System.Collections;

public class MPTicTacToeModel
{
    private static MPTicTacToeRound[] rounds = new MPTicTacToeRound[MPTicTacToeController.NUMBER_OF_MAX_ROUNDS];

    private static uint player0Id;
    private static uint player1Id;

    /*
     * Wertet alle acht bool'schen Gleichungen des Spielfelds aus und liefert die Lösungen als BoolEquation-Array zurück
     */
    public static BooleanEquation[] EvaluateRound(MPTicTacToeRound round)
    {
        BoolOperand[] operands = round.GetOperands();
        BoolOperator[] operators = round.GetOperators();

        BooleanEquation[] eq = new BooleanEquation[8];

        /* 0: links oben nach rechts oben
         * X X X
         * - - -
         * - - -
         */
        eq[0] = new BooleanEquation(new BoolOperand[] { operands[0], operands[1], operands[2] }, new BoolOperator[] { operators[0], operators[1] });

        /* 1: links mitte nach rechts mitte
         * - - -
         * X X X
         * - - -
         */
        eq[1] = new BooleanEquation(new BoolOperand[] { operands[3], operands[4], operands[5] }, new BoolOperator[] { operators[7], operators[8] });

        /* 2: links unten nach rechts unten
         * - - -
         * - - -
         * X X X
         */
        eq[2] = new BooleanEquation(new BoolOperand[] { operands[6], operands[7], operands[8] }, new BoolOperator[] { operators[14], operators[15] });

        /* 3: links oben nach links unten
         * X - -
         * X - -
         * X - -
         */
        eq[3] = new BooleanEquation(new BoolOperand[] { operands[0], operands[3], operands[6] }, new BoolOperator[] { operators[2], operators[9] });

        /* 4: mitte oben nach mitte unten
         * - X -
         * - X -
         * - X -
         */
        eq[4] = new BooleanEquation(new BoolOperand[] { operands[1], operands[4], operands[7] }, new BoolOperator[] { operators[4], operators[11] });

        /* 5: rechts oben nach rechts unten
         * - - X
         * - - X
         * - - X
         */
        eq[5] = new BooleanEquation(new BoolOperand[] { operands[2], operands[5], operands[8] }, new BoolOperator[] { operators[6], operators[13] });

        /* 6: links oben nach rechts unten
         * X - -
         * - X -
         * - - X
         */
        eq[6] = new BooleanEquation(new BoolOperand[] { operands[0], operands[4], operands[8] }, new BoolOperator[] { operators[3], operators[12] });

        /* 7: rechts oben nach links unten
         * - - X
         * - X -
         * X - -
         */
        eq[7] = new BooleanEquation(new BoolOperand[] { operands[2], operands[4], operands[6] }, new BoolOperator[] { operators[5], operators[10] });

        return eq;
    }

    /*
     * Initialisiert die Player-Referenzen, lässt die benötigten Runden-Objekte generieren und liefert diese als Array zurück
     */
    public static void InitMatch(uint player0Id, uint player1Id, Difficulty difficulty)
    {
        MPTicTacToeModel.player0Id = player0Id;
        MPTicTacToeModel.player1Id = player1Id;

        for (int i = 0; i < MPTicTacToeModel.rounds.Length; i++)
        {
            BoolOperator[] bo = BooleanGenerator.GetRandomEquation(MPTicTacToeController.NUMBER_OF_OPERATORS, difficulty, true).GetOperators();
            GenerateNewRound(bo);
        }
    }

    /*
     * Fabrikmethode für neues Runden-Objekt, fügt das neue Objekt auch zu MPTicTacToeRound.rounds hinzu
     */
    private static MPTicTacToeRound GenerateNewRound(BoolOperator[] operators)
    {
        MPTicTacToeRound round = new MPTicTacToeRound(operators, MPTicTacToeModel.player0Id, MPTicTacToeModel.player1Id);
        MPTicTacToeModel.AddRound(round);
        return round;
    }

    public static MPTicTacToeRound[] GetRounds()
    {
        return MPTicTacToeModel.rounds;
    }

    public static MPTicTacToeRound GetRound(int index)
    {
        return MPTicTacToeModel.rounds[index];
    }

    /*
     * Fügt eine Runde der Rundenliste MPTicTacToeRound.rounds hinzu
     */
    private static void AddRound(MPTicTacToeRound round)
    {
        for (int i = 0; i < MPTicTacToeModel.rounds.Length; i++)
            if (MPTicTacToeModel.rounds[i] == null)
            {
                MPTicTacToeModel.rounds[i] = round;
                break;
            }
    }

    /*
     * Gibt die Spieler-ID mit dem BoolOperand 0 oder 1 über den Rundenindex zurück
     */
    public static uint GetPlayerIDByOperand(int roundIndex, BoolOperand zeroOrOne)
    {
        return MPTicTacToeModel.rounds[roundIndex].GetPlayerIDByOperand(zeroOrOne);
    }

    public static BoolOperand GetOperandByPlayerID(int roundIndex, uint playerId)
    {
        return MPTicTacToeModel.rounds[roundIndex].GetOperandByPlayerID(playerId);
    }

    /*
     * Gibt die Byteanzahl eines Rundenojektes zurück
     */
    public static int GetBytesPerRound()
    {
        return Utils.ObjectSerializationExtension.SerializeToByteArray(MPTicTacToeModel.GenerateNewRound(null)).Length;
    }

    /*
     * Setzt den Operanden in einer Runde in einem Feld über die Spieler-ID
     */
    public static void SetOperand(int roundIndex, int index, uint playerId)
    {
        MPTicTacToeModel.rounds[roundIndex].SetOperand(index, playerId);
    }

    public static BoolOperand[] GetRoundsOperands(int roundNumber)
    {
        return MPTicTacToeModel.rounds[roundNumber].GetOperands();
    }

    public static BoolOperator[] GetRoundsOperators(int roundNumber)
    {
        return MPTicTacToeModel.rounds[roundNumber].GetOperators();
    }

    public static void Reset()
    {
        rounds = new MPTicTacToeRound[MPTicTacToeController.NUMBER_OF_MAX_ROUNDS];
    }
}

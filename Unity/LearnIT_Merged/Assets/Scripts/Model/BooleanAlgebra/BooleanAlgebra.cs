﻿/**
 * Global enums to work with
 */ 
public enum BoolOperator { NULL, AND, OR, NAND, NOR }
public enum BoolOperand { NULL, ZERO, ONE, X, NOTX }

/**
 * This class provides public static methods to convert string and enum values and to negate operands
 */ 
public class BooleanAlgebra {

	public static string ToString(BoolOperand op)
    {
        switch (op)
        {
            case BoolOperand.ZERO:
                return "0";
            case BoolOperand.ONE:
                return "1";
            case BoolOperand.X:
                return "X";
            case BoolOperand.NOTX:
                return "!X";
            default:
                return "";
        }
    }

    public static string ToString(BoolOperator op)
    {
        switch (op)
        {
            case BoolOperator.AND:
                return "AND";
            case BoolOperator.NAND:
                return "NAND";
            case BoolOperator.OR:
                return "OR";
            case BoolOperator.NOR:
                return "NOR";
            default:
                return "";
        }
    }

    public static BoolOperator OperatorToEnumValue(string op)
    {
        switch (op)
        {
            case "AND":
                return BoolOperator.AND;
            case "NAND":
                return BoolOperator.NAND;
            case "OR":
                return BoolOperator.OR;
            case "NOR":
                return BoolOperator.NOR;
            default:
                return BoolOperator.NULL;
        }
    }

    public static BoolOperand OperandToEnumValue(string op)
    {
        switch (op)
        {
            case "0":
                return BoolOperand.ZERO;
            case "ZERO":
                return BoolOperand.ZERO;
            case "1":
                return BoolOperand.ONE;
            case "ONE":
                return BoolOperand.ONE;
            case "X":
                return BoolOperand.X;
            case "x":
                return BoolOperand.X;
            case "!X":
                return BoolOperand.NOTX;
            case "!x":
                return BoolOperand.NOTX;
            default:
                return BoolOperand.NULL;
        }
    }

    public static BoolOperand Negate(BoolOperand op)
    {
        switch (op)
        {
            case BoolOperand.ZERO:
                return BoolOperand.ONE;
            case BoolOperand.ONE:
                return BoolOperand.ZERO;
            case BoolOperand.X:
                return BoolOperand.NOTX;
            case BoolOperand.NOTX:
                return BoolOperand.X;
            default:
                return BoolOperand.NULL;
        }
    }

    public static BoolOperator Negate(BoolOperator op)
    {
        switch (op)
        {
            case BoolOperator.AND:
                return BoolOperator.OR;
            case BoolOperator.OR:
                return BoolOperator.AND;
            case BoolOperator.NAND:
                return BoolOperator.OR;
            case BoolOperator.NOR:
                return BoolOperator.AND;
            default:
                return BoolOperator.NULL;
        }
    }

    public static BoolOperator Identity(BoolOperator op)
    {
        switch (op)
        {
            case BoolOperator.NAND:
                return BoolOperator.AND;
            case BoolOperator.NOR:
                return BoolOperator.OR;
            default:
                return op;
        }
    }

    public static string Negate(string op)
    {
        return ToString(Negate(OperandToEnumValue(op)));
    }
        
}

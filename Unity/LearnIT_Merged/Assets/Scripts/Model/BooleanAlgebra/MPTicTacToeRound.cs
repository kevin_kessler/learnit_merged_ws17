﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MPTicTacToeRound
{
    private BoolOperand[] operands;
    private BoolOperator[] operators;

    private BoolOperand startOperand = BooleanGenerator.GetRandomConstant();

    private uint opZero; // playerID mit Operand 0
    private uint opOne; // playerID mit Operand 1

    /*
     * Konstruktor, dem 16 Operatoren in einem Array sowie die IDs der beiden Spieler übergeben werden
     */
    public MPTicTacToeRound(BoolOperator[] operators, uint player0_Id, uint player1_Id)
    {
        // operands mit NULLs füllen
        this.operands = new BoolOperand[9];
        for (int i = 0; i < this.operands.Length; i++)
            this.operands[i] = BoolOperand.NULL;

        this.operators = operators;

        if (new System.Random().Next(2) == 1)
        {
            this.opZero = player1_Id;
            this.opOne = player0_Id;
        }
        else
        {
            this.opZero = player0_Id;
            this.opOne = player1_Id;
        }
    }

    /*
     * Setzt einen einzelnen Operanden für einen Spieler
     */
    public void SetOperand(int index, uint playerId)
    {
        if (this.opZero == playerId)
            operands[index] = BoolOperand.ZERO;
        if (this.opOne == playerId)
            operands[index] = BoolOperand.ONE;
    }

    public uint GetPlayerIDByOperand(BoolOperand zeroOrOne)
    {
        switch (zeroOrOne)
        {
            case BoolOperand.ZERO:
                return this.opZero;
            case BoolOperand.ONE:
                return this.opOne;
            default:
                return 0;
        }
    }

    public BoolOperand GetOperandByPlayerID(uint playerId)
    {
        if (playerId == opZero)
            return BoolOperand.ZERO;
        if (playerId == opOne)
            return BoolOperand.ONE;
        return BoolOperand.NULL;
    }

    /*
     * Liefert ein int-Array mit den bisher ungenutzten Indices zurück
     * leer lassen
     */
    public int[] GetUnusedIndices()
    {
        // Anzahl ungenutzter Indices ermitteln
        int count = 0;
        foreach (BoolOperand bo in this.operands)
        {
            if (bo == BoolOperand.NULL)
                count++;
        }

        if (count == 0)
            return new int[0];

        // Array mit Indices füllen
        int[] indices = new int[count];
        for (int i = 0; i < this.operands.Length; i++)
        {
            if (this.operands[i] != BoolOperand.NULL)
                indices[i] = i;
        }

        return indices;
    }

    public BoolOperand[] GetOperands()
    {
        return this.operands;
    }

    public BoolOperator[] GetOperators()
    {
        return this.operators;
    }

    public BoolOperand GetStartOperand()
    {
        return startOperand;
    }

    public bool IsEnd()
    {
        foreach (BoolOperand bo in this.operands)
            if (bo == BoolOperand.NULL)
                return false;
        return true;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BooleanHistory {

    private List<string> userEquations;
    private List<string> solutionEquations;

	void Start () {
        userEquations = new List<string>();
        solutionEquations = new List<string>();
	}

    public void addUserEquation(string equation)
    {
        userEquations.Add(equation);
    }

    public List<string> getUserEquations()
    {
        return new List<string>(userEquations);
    }

    public void addSolutionEquation(string equation)
    {
        userEquations.Add(equation);
    }

    public List<string> getSolutionEquations()
    {
        return new List<string>(solutionEquations);
    }
}

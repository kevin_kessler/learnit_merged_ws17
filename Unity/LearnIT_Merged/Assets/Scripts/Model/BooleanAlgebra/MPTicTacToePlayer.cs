﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class MPTicTacToePlayer : MPGamePlayer
{
    [SyncVar]
    private bool imReady = false;

    private bool waitingActivated = false;

    private MPTicTacToeController myController;

    protected override void InitClientSide()
    {

    }

    public void Decide(int index)
    {
        CmdDecide(index);
    }

    public void ReadyForNextRound()
    {
        CmdReadyForNextRound();
    }

    public void CheckOtherPlayerReadyForNextRound()
    {
        CmdCheckOtherPlayerReadyForNextRound();
    }

    [Command]
    public void CmdCheckOtherPlayerReadyForNextRound()
    {
        GetController().CheckPlayerReadyForNextRound(netId.Value);
    }

    [Command]
    public void CmdReadyToStart()
    {
        isReady(true);
        GetController().CheckForStart();
    }

    [Command]
    private void CmdDecide(int index)
    {
        GetController().Decide( this.netId.Value, index );
    }

    [Command]
    private void CmdReadyForNextRound()
    {
        isReady(true);
        GetController().CheckForNextRound();
    }

    [Command]
    public void CmdLeaveGame()
    {
        GetController().LeaveGame();
    }

    public MPTicTacToeController GetController()
    {
        if(myController == null)
            myController = (MPTicTacToeController)gameController;
        return myController;
    }

    public bool isReady()
    {
        return imReady;
    }

    public void isReady(bool ready)
    {
        imReady = ready;
    }

    public bool isWaiting()
    {
        return waitingActivated;
    }

    public void isWaiting(bool waiting)
    {
        waitingActivated = waiting;
    }
}

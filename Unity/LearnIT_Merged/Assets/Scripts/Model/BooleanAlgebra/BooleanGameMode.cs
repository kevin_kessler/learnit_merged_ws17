﻿using UnityEngine;
using System.Collections;

public enum Difficulty { EASY, MODERATE, HARD }

public class BooleanGameMode : GameMode
{
    private Difficulty difficulty;
    private Variant variant;

    public BooleanGameMode(string name, string description, string spritePath, string gameSceneName, Difficulty diff, string tutorialPath, string mpGamePlayerPrefabPath) : base(name, description, spritePath, gameSceneName, tutorialPath, mpGamePlayerPrefabPath)
    {
        this.difficulty = diff;
    }

    public BooleanGameMode(string name, string description, string spritePath, string gameSceneName, Difficulty diff, Variant variant, string tutorialPath, string mpGamePlayerPrefabPath) : base(name, description, spritePath, gameSceneName, tutorialPath, mpGamePlayerPrefabPath)
    {
        this.difficulty = diff;
        this.variant = variant;
    }

    public Difficulty GetDifficulty()
    {
        return this.difficulty;
    }

    public Variant getVariant()
    {
        return this.variant;
    }
}

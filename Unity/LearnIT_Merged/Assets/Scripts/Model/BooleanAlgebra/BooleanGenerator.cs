﻿using UnityEngine;
/**
 *This singleton class provides a method to get a random boolean term, filled with a random number of constants and a valid solution
 */
public class BooleanGenerator
{
    private static System.Random rnd = new System.Random();
    
    /**
     * public method to provide getting a random BoolEquation
     */   
    public static BooleanEquation GetRandomEquation(int numberOfOperators, Difficulty degree, bool fair)
    {
        BooleanEquation result = new BooleanEquation(GetRandomOperators(numberOfOperators, degree, fair));
        FillConstants(result);
        SetValidSolution(result);

        return result;
    }   

    /**
     * helper method to get a random OperandArray filled with X or !X to set a valid solution
     */
    private static BoolOperand[] GetRandomOperands(int length)
    {
        BoolOperand[] result = new BoolOperand[length];
        for (int i = 0; i < length; i++)
            result[i] = GetRandomOperand();
        return result;
    }

    private static BoolOperator[] GetRandomOperators(int length, Difficulty degree, bool fair)
    {
        BoolOperator[] result = new BoolOperator[length];
        if (fair)
        {
            int index = 0;
            while(index < length)
            {
                BoolOperator rnd = GetRandomOperator(degree);

                int max = length / GetBaseQuantity(degree).Length;

                if (length % GetBaseQuantity(degree).Length != 0)
                    max++;
                
                if(GetNumberOfOperator(rnd, result) < max)                
                    result[index++] = rnd;

            }
        }
        else        
            for (int i = 0; i < length; i++)
                result[i] = GetRandomOperator(degree);

        return result;
    }

    private static int GetNumberOfOperator(BoolOperator op, BoolOperator[] toCount)
    {
        int counter = 0;
        for (int i = 0; i < toCount.Length; i++)
            if (toCount[i] == op)
                counter++;
        return counter;
    }

    /**
     * helper method to get a quantity of operands depending on difficulty
     */
    private static BoolOperator[] GetBaseQuantity( Difficulty degree )
    {
        switch (degree)
        {
            case Difficulty.EASY:
                return new BoolOperator[] { BoolOperator.AND, BoolOperator.OR };
            case Difficulty.MODERATE:
                return new BoolOperator[] { BoolOperator.NAND, BoolOperator.NOR };
            case Difficulty.HARD:
                return new BoolOperator[] { BoolOperator.AND, BoolOperator.OR, BoolOperator.NAND, BoolOperator.NOR };
            default:
                return new BoolOperator[] { BoolOperator.NULL };
        }
    }

    /**
     * helper method to fill a random number of 1 and 0 in the given equation 
     */
    private static BooleanEquation FillConstants(BooleanEquation equation)
    {
        int max = equation.GetOperators().Length/2;
        int number = GetRandom(max);
        for (int i = 0; i < number; i++)
            equation.AddOperand(GetRandom(equation.GetOperands().Length), GetRandomConstant());
        return equation;
    }

    /**
     * helper method to set a valid solution
     */
    private static void SetValidSolution(BooleanEquation equation)
    {
        equation.AddOperands(GetRandomOperands(equation.GetNumberOfFreeOperandPlaces()));
        equation.SetSolution(BooleanSolver.Evaluate(equation));
    }

    /**
     * returns 0 or 1
     */
    public static BoolOperand GetRandomConstant()
    {
        if (rnd.NextDouble() < 0.5)
            return BoolOperand.ZERO;
        else
            return BoolOperand.ONE;
    }

    /**
     * returns X or !X
     */
    private static BoolOperand GetRandomOperand()
    {
        if (rnd.NextDouble() < 0.5)
            return BoolOperand.X;
        else
            return BoolOperand.NOTX;
    }

    private static BoolOperator GetRandomOperator(Difficulty degree)
    {
        BoolOperator[] domain = GetBaseQuantity(degree);
        return domain[GetRandom(domain.Length)];
    }

    /**
     * returns a random integer between 0 and the given maximum value
     */
    public static int GetRandom(int max)
    {
        return (int)(rnd.NextDouble() * max);
    }

}

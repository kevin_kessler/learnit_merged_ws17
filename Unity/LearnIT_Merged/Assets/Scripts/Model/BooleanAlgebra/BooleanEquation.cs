﻿
using System.Collections.Generic;
using UnityEngine;
/**
* This class represents a boolean term with valid solution and everything you need to work with this datatype
*/
public class BooleanEquation
{
    private BoolOperator[] operators;
    private BoolOperand[] operands;
    private BoolOperand solution = BoolOperand.NULL;

    public BooleanEquation(BoolOperand[] operands, BoolOperator[] operators)
    {
        this.operands = operands;
        this.operators = operators;
    }

    public BooleanEquation(BoolOperator[] operators) : this(new BoolOperand[operators.Length + 1], operators)
    {

    }

    public BooleanEquation(string[] term) : this(new BoolOperator[term.Length / 2])
    {
        for (int i = 0, v = 0, o = 0; i < term.Length; i++)
        {
            if (i % 2 == 0)
                operands[v++] = BooleanAlgebra.OperandToEnumValue(term[i]);
            else
                operators[o++] = BooleanAlgebra.OperatorToEnumValue(term[i]);
        }
    }

    public void AddOperand(int position, BoolOperand operand)
    {
        if (position < operands.Length)
            this.operands[position] = operand;
    }

    public void AddOperand(int position, string operand)
    {
        AddOperand(position, BooleanAlgebra.OperandToEnumValue(operand));
    }

    /**
     * Opportunity to fill operands to free places. Make sure you checked the GetNumberOfFreeOperandPlaces() for the parameter array!
     */
    public void AddOperands(BoolOperand[] operands)
    {
        if (operands.Length <= GetNumberOfFreeOperandPlaces())
            for (int i = 0, k = 0; i < this.operands.Length; i++)
                if (this.operands[i] != BoolOperand.ZERO & this.operands[i] != BoolOperand.ONE)
                {
                    this.operands[i] = operands[k++];
                }
    }

    public int GetNumberOfFreeOperandPlaces()
    {
        int size = 0;
        for (int i = 0; i < operands.Length; i++)
            if (operands[i] != BoolOperand.ZERO & operands[i] != BoolOperand.ONE & operands[i] != BoolOperand.X & operands[i] != BoolOperand.NOTX)
                size++;
        return size;
    }

    public void ResetOperandsButConstants()
    {
        for (int i = 0; i < operands.Length; i++)
            if (operands[i] != BoolOperand.ZERO & operands[i] != BoolOperand.ONE)
                operands[i] = BoolOperand.NULL;
    }

    public int GetTermLength()
    {
        return this.operands.Length + this.operators.Length;
    }

    public BoolOperator[] GetOperators()
    {
        return this.operators;
    }

    public BoolOperand[] GetOperands()
    {
        return this.operands;
    }

    public BoolOperand GetSolution()
    {
        return this.solution;
    }

    public void SetSolution(BoolOperand solution)
    {
        this.solution = solution;
    }

    public void SetSolution(string solution)
    {
        SetSolution(BooleanAlgebra.OperandToEnumValue(solution));
    }

    public string ToBlankEquation()
    {
        string output = "";

        for (int i = 0; i < operands.Length; i++)
        {
            if(operands[i] == BoolOperand.X || operands[i] == BoolOperand.NOTX)
                output += "_ ";
            else
                output += BooleanAlgebra.ToString(operands[i]) + " ";
            if (i < operators.Length)
                output += BooleanAlgebra.ToString(operators[i]) + " ";
        }
        output += "= " + BooleanAlgebra.ToString(solution);

        return output;
    }

    public string[] ToStringArray()
    {
        List<string> output = new List<string>();

        for (int i = 0; i < operands.Length; i++)
        {
            output.Add(BooleanAlgebra.ToString(operands[i]));
            if (i < operators.Length)
                output.Add(BooleanAlgebra.ToString(operators[i]));
        }

        return output.ToArray();
    }

    public override string ToString()
    {
        string output = "";

        for (int i = 0; i < operands.Length; i++)
        {
            output += BooleanAlgebra.ToString(operands[i]) + " ";
            if (i < operators.Length)
                output += BooleanAlgebra.ToString(operators[i]) + " ";
        }
        output += "= " + BooleanAlgebra.ToString(solution);

        return output;
    }
}
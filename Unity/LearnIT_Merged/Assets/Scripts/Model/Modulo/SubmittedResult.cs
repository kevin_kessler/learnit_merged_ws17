﻿using UnityEngine;
using System.Collections;

public class SubmittedResult
{
	private uint submitter;
	private string result;
	private int position;

	public SubmittedResult(uint submitter, string result, int position)
	{
		this.submitter = submitter;
		this.result = result;
		this.position = position;
	}

	public uint GetSubmitter()
	{
		return this.submitter;
	}

	public string GetResult()
	{
		return result;
	}

	public int GetPosition()
	{
		return position;
	}
}
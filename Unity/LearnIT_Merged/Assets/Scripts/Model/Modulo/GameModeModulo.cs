﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.IO;

public class GameModeModulo : GameMode {

    private int roundsToWin;

    public GameModeModulo(int roundsToWin, string name, string description, string spritePath, string gameSceneName, string tutorialPath = null, string mpGamePlayerPrefabPath = null)
        :base(name, description, spritePath, gameSceneName, tutorialPath, mpGamePlayerPrefabPath)
    {
        this.roundsToWin = roundsToWin;
    }

    public int GetRoundsToWin()
    {
        return roundsToWin;
    }
}

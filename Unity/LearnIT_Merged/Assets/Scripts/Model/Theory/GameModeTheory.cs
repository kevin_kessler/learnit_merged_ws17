﻿using UnityEngine;
using System.Collections;

public class GameModeTheory : GameMode {

    private string theoryXml;
    private TheoryBook theoryBook;

    public GameModeTheory(string theoryXml, string name, string description, string spritePath, string gameSceneName, string tutorialPath = null, string mpGamePlayerPrefabPath = null)
        :base(name, description, spritePath, gameSceneName, tutorialPath, mpGamePlayerPrefabPath)
    {
        this.theoryXml = theoryXml;
    }

    public TheoryBook GetTheoryBook()
    {
        if (null == theoryBook)
            theoryBook = TheoryLoader.LoadTheoryBookByName(theoryXml);
        return theoryBook;
    }
}

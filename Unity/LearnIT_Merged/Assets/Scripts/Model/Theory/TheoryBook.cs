﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Utils;

public class TheoryBook {
    private List<TheoryPage> pages;

    public TheoryBook()
    {
        this.pages = new List<TheoryPage>();
    }

    public void AddPage(TheoryPage page)
    {
        if (null == page)
            return;

        pages.Add(page);
    }

    public TheoryPage[] GetPages()
    {
        return pages.ToArray();
    }

}

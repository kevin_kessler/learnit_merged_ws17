﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PageImage : PageElement
{
    private float widthScale;
    public PageImage(string imageName, float widthScale)
        : base(PageElement.ElementType.IMAGE, imageName)
    {
        this.widthScale = widthScale;
    }

    public PageImage(string imageName, float widthScale, Dictionary<string, string> attributes)
        : base(PageElement.ElementType.IMAGE, imageName, attributes)
    {
        this.widthScale = widthScale;
    }

    public float GetWidthScale()
    {
        return widthScale;
    }
}

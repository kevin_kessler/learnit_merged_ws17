﻿using UnityEngine;
using System.Collections.Generic;

public class TheoryPage {

    private List<PageElement> pageElements;

    public TheoryPage()
    {
        pageElements = new List<PageElement>();
    }

    public void AddElement(PageElement element)
    {
        pageElements.Add(element);
    }

    public List<PageElement> GetElements()
    {
        return pageElements;
    }
}

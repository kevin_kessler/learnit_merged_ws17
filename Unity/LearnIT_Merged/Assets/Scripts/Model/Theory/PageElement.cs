﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PageElement
{
    public enum ElementType
    {
        NONE = -1,
        TEXT = 0,
        IMAGE = 1,
        HEADING = 2,
        CAPTION = 3
    }

    private string content;
    private Dictionary<string, string> attributes;
    private ElementType type;

    public PageElement(ElementType type, string content)
    {
        this.type = type;
        this.content = content;
        this.attributes = new Dictionary<string, string>();
    }

    public PageElement(ElementType type, string content, Dictionary<string, string> attributes)
        :this(type, content)
    {
        if(null!=attributes)
            this.attributes = attributes;
    }

    public void AddAttribute(string key, string value)
    {
        if (attributes.ContainsKey(key))
            return;
        attributes.Add(key, value);
    }

    public string GetContent()
    {
        return content;
    }

    public ElementType GetElementType()
    {
        return type;
    }

    public string GetAttributeValue(string key)
    {
        if (!attributes.ContainsKey(key))
            return null;
        return attributes[key];
    }
}

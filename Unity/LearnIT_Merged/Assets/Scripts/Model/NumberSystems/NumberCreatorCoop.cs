﻿using UnityEngine;
using System.Collections;

/**
 * Special numbercreator adapted for coop.
 * Generates 3 representations of a number.
 */
public class NumberCreatorCoop : NumberCreator
{
	private int sysDecimal = 10;

	public NumberCreatorCoop(int left, int right, int top) : base(left, right)
	{
		this.sysDecimal = top;
	}

	public NumberCreatorCoop(int left, int right) : base(left, right)
	{
	}

	public override string[] createNumbers(int digits)
	{
		string[] result = new string[3];
		int low;
		if (digits == 1) {
			low = 0;
		} else {
			low = (int)Mathf.Pow (sysDecimal, digits - 1);
		}
		int high = (int)Mathf.Pow (sysDecimal, digits);
		int number = new System.Random ().Next (low, high);
		result [0] = createNumber (getTopSystem(), number);
		result [1] = createNumber (getLeftSystem (), number);
		result [2] = createNumber (getRightSystem (), number);
		Debug.LogError ("low: " + low + " high: " + high);
		return result;
	}

	public int getTopSystem()
	{
		return sysDecimal;
	}
}
﻿using UnityEngine;
using System.Collections;
using System;

/**
 * This numbercreator is designed to output two representations for a random number.
 * Supports positive and negative numbers.
 */
public class NumberCreator
{
	private int leftSystem;
	private int rightSystem;
	private bool negative = false;

	public NumberCreator(int left, int right)
	{
		if (left < 2)
		{
			throw new ArgumentOutOfRangeException ("left", left, "Must be greater than 1!");
		}
		if (right < 2)
		{
			throw new ArgumentOutOfRangeException ("right", right, "Must be greater than 1!");
		}
		leftSystem = left;
		rightSystem = right;
	}

	public void setModeToNegative (bool negative)
	{
		this.negative = negative;
	}

    public virtual string[] createNumbers(int digits)
    {
    	string[] result = new string[2];
		int low;
		if (digits == 1) {
			low = 0;
		} else {
			low = (int)Mathf.Pow (leftSystem, digits - 1);
		}
		int high = (int)Mathf.Pow (leftSystem, digits);
		int number = new System.Random().Next(low, high);
		if (negative)
		{
			number = -number;
		}
    	result [0] = createNumber (leftSystem, number);
    	result [1] = createNumber (rightSystem, number);
		Debug.Log("low: " + low + " high: " + high);
    	return result;
    }

    protected string createNumber(int system, int number)
	{
		return Convert.ToString (number, system);
	}

	public int getLeftSystem()
	{
		return leftSystem;
	}

	public int getRightSystem()
	{
		return rightSystem;
	}
}
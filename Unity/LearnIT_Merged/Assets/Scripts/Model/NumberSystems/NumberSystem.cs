﻿using UnityEngine;
using System.Collections;

/**
 * DataStructure used to store a numbersystems name and the number describing it.
 * 
 * e.g.
 * 		(2, Binary)
 * 		(8, Octal)
 * 		(10, Decimal)
 * 		(16, Hexadecimal)
 */
public class NumberSystem
{
	private int number;
	private string name;

	public NumberSystem(int number, string name)
	{
		this.number = number;
		this.name = name;
	}

	public int Number
	{
		get
		{ 
			return number;
		}
	}

	public string Name
	{
		get
		{
			return name;
		}
	}
}
﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.IO;

public class GameModeNumberSystem : GameMode {

    public struct NumberSystemData
    {
        private int numberSystemLeft;
        private int numberSystemRight;
        private float difficultyScaling;
        private float stretchScaling;
        private string numberSystemLeftName;
        private string numberSystemRightName;

        public NumberSystemData(int numberSystemLeft, int numberSystemRight, string numberSystemLeftName, string numberSystemRightName, float difficultyScaling, float stretchScaling){
            this.numberSystemLeft = numberSystemLeft;
            this.numberSystemRight = numberSystemRight;
            this.difficultyScaling = difficultyScaling;
            this.stretchScaling = stretchScaling;
            this.numberSystemLeftName = numberSystemLeftName;
            this.numberSystemRightName = numberSystemRightName;
        }

        public int GetNumberSystemLeft()
        {
            return this.numberSystemLeft;
        }

        public int GetNumberSystemRight()
        {
            return this.numberSystemRight;
        }

        public string GetNumberSystemLeftName()
        {
            return this.numberSystemLeftName;
        }

        public string GetNumberSystemRightName()
        {
            return this.numberSystemRightName;
        }

        public float GetDifficultyScaling()
        {
            return this.difficultyScaling;
        }

        public float GetStretchScaling()
        {
            return this.stretchScaling;
        }
    }

    NumberSystemData numberSystem;

    public GameModeNumberSystem(NumberSystemData numberSystem,
                                string name, string description, string spritePath, string gameSceneName, string tutorialPath = null, string mpGamePlayerPrefabPath = null)
        :base(name, description, spritePath, gameSceneName, tutorialPath, mpGamePlayerPrefabPath)
    {
        this.numberSystem = numberSystem;
    }

    public NumberSystemData GetNumberSystem()
    {
        return this.numberSystem;
    }
}

<TheoryBook>
  <!--!!! Make sure the xml is encoded in UTF-8 !!!-->
  <!-- A Page-Side (left or right) can contain different elements: 
  
         *The <Text>-Tag represents a simple text paragraph. 
         
         *The <Image>-Tag displays the image found under the given imagename (with extension). 
          ##The "width" attribute defines how to scale the image according to the pages width and must be between 0 and 1.
          ##Example: width=0.75 will cause the image to take up to 75% of the page width. (Default is 1.0, meaning 100% width)
          ##The width also affects the image's height, since its aspect ratio will be kept.
          
         *The <Heading>-Tag shows a headline (bigger text, left aligned)
         
         *The <Caption>-Tag shows a caption (e.g. for image description) (smaller text, centered)
         
       The page elements will appear in the same order as specified here-->

  <!-- Notes: 
       *The text elements have fix font-sizes. So if too much text is at the same page, it will overflow vertically. 
       *The same applies for images. If too many images and / or too much text is on a page, the content will overflow.-->

  <Page>
    <Heading>Das Färbungsproblem</Heading>
    <Text>In der Informatik gibt es Probleme, die sehr schwer zu lösen sind. Damit ist nicht gemeint, dass das Konzept dahinter schwer zu verstehen ist, sondern dass es sehr lange dauert um sie zu lösen.</Text>
    <Text>Ein Beispiel für so ein Problem ist das sogenannte Färbungsproblem. Dabei geht es darum die Gebiete einer Landkarte so einzufärben, dass angrenzende Gebiete nie die gleiche Farbe besitzen.</Text>
    <Image width="0.4">small_map.png</Image>
  </Page>

  <Page>
    <Text>Beim Einfärben spielt es keine Rolle welche Farbe man für eine Fläche benutzt, solange sie anders ist als die Farbe der benachbarten Flächen. Wenn wir zum Beispiel "Nordland" rot färben, dann dürfen "West-" und "Ostland" nicht rot sein.</Text>
    <Text>"Südland" hingegen darf rot sein, weil sich die Flächen nur an einem einzigen Punkt berühren und dieser nicht als Grenze zählt. Wenn man nun "West-" und "Ostland" grün einfärbt, erhält man folgende Karte.</Text>
    <Image width="0.4">small_map_coloured.png</Image>
  </Page>

  <Page>
    <Text>In Beispiel brauchen wir also nur zwei verschiedene Farben um die komplette Karte einfärben zu können.</Text>
    <Heading>Vier-Farben-Satz</Heading>
    <Text>Im Zusammenhang mit dem Färbungsproblem gibt es den sogenannten Vier-Farben-Satz. Er besagt, dass jede beliebige Landkarte höchstens vier Farben benötigt um entsprechend eingefärbt werden zu können. Die Anzahl der tatsächlich benötigten Farben, wird "chromatische Zahl" X(G) genannt.</Text>
    <Image width="0.9">fourcoloursmap.png</Image>
  </Page>

  <Page>
    <Heading>Graphen Färben</Heading>
    <Text>Das Färbungsproblem bezieht sich im Allgemeinen eigentlich eher auf Graphen. Das ist nicht weiter kompliziert, denn die Graphen können einfach als Abstraktion der Landkarten gesehen werden, wobei die Länder den Knoten und die Grenzen den Kanten entsprechen.</Text>
    <Image width="0.6">map_graph.png</Image>
    <Caption>Die Beispielkarte als Graph</Caption>
    <Text>Dadurch lässt sich das Färbungsproblem auch auf andere Gebiete als nur das Einfärben von Karten übertragen.</Text>
  </Page>

  <Page>
    <Text>Das Problem tritt beispielsweise beim Erstellen eines Stundenplans auf. Kurse, die vom gleichen Schüler besucht werden, müssen zu unterschiedlichen Zeitspannen stattfinden, damit der Schüler beide besuchen kann. Dabei werden Kurse dann als Knoten und "Wird vom gleichen Schüler besucht" als Kanten dargestellt.</Text>
    <Image width="0.6">school_graph.png</Image>
    <Text>Statt unterschiedlichen Farben werden hier nun unterschiedliche Zeitspannen gesucht. Das Problem bleibt aber das gleiche.</Text>
  </Page>

  <Page>
    <Heading>Komplexität des Problems</Heading>
    <Text>Bisher scheint das Problem nocht recht gut lösbar, oder? Die anfangs gezeigten Karten hättest du sicherlich schnell lösen können. Was aber, wenn eine Karte aus 10, 100 oder gar 1000 Bereichen besteht?</Text>
    <Image width="0.8">complex_map.png</Image>
    <Text>Vermutlich denkst du jetzt: "Dann lasse ich das eben einen Computer für mich machen", aber sogar die haben ihre Schwierigkeiten damit. Warum ist das so?</Text>
  </Page>

  <Page>
    <Text>Das hängt damit zusammen, dass die Berechnungsdauer zum Lösen des Problems exponentiell zur Anzahl der Flächen ansteigt. Was heißt das?</Text>
    <Text>Angenommen wir lassen den Computer einfach alle möglichen Farbkombinationen ausprobieren bis eine passende gefunden wurde. Wir wissen, dass wir maximal vier unterschiedliche Farben dafür brauchen.</Text>
    <Text>Bei einer Karte mit n Flächen haben wir also 4ⁿ mögliche Kombinationen. Das heißt, die Anzahl an Kombinationen verfierfacht sich jedes mal, wenn eine weitere Fläche hinzu kommt.</Text>
    <Image width="1">combinations.png</Image>
  </Page>

  <Page>
    <Text>Selbst wenn man einen Computer erfindet, der das Problem für 50 Flächen in einer Stunde lösen könnte, würde er für 51 Flächen schon vier Stunden und für 60 Flächen schon mehr als ein Jahr lang brauchen.</Text>
    <Image width="0.5">exhausted.png</Image>
    <Text>Das zeigt, dass diese Art von Problem nicht einfach durch das Erfinden schnellerer Computer gelöst werden werden kann. Man bräuchte einen Algorithmus, der das Problem effizient lösen könnte.</Text>
  </Page>

  <Page>
    <Text>Doch solch ein Algorithmus wurde noch nicht entdeckt. Es ist nicht einmal sicher, dass es überhaupt eine effiziente Lösung für das Problem gibt. Damit gehört das Problem zur Klasse der NP-Vollständigen Probleme.</Text>
  </Page>

</TheoryBook>
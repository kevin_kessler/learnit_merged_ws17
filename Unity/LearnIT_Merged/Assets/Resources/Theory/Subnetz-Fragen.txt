﻿Frage: Wie nennt man die einzelnen Blöcke einer Subnetzmaske oder einer IP-Adresse?
	Antworten: Oktett, Quartett, Menuett, Silhouett
	Erklärung: Da die einzelnen Blöcke einer IP-Adresse oder einer Subnetzmaske aus 8 bits besteht, nennt man diese Oktett.

Frage: Was ist die höchst mögliche Zahl die einem Block einer IP-Adresse oder Subnetzmaske zugeordnet werden kann?
	Antworten: 255, 256, 512, 128
	Erklärung: Es sind zwar 256 Adressen, jedoch zählen wir ab 0 anstatt 1.
	
Frage: Was ist die niedrigste Zahl die einem Block einer IP-Adresse oder Subnetzmaske zugeordnet werden kann?
	Antworten: 0, 1, 2, 3
	Erklärung: Informatiker zählen ab 0. Deswegen lautet die Antwort auch 0.
	
xFrage: Wie berechnet sich die Größe in eines Subnetzes?
	Antworten: 2 hoch der Anzahl der nicht gesetzten Bits, 2 mal der Anzahl der gesetzte Bits, 2 hoch der Anzahl der gesetzte Bits, 2 mal der Anzahl der nicht gesetzte Bits
	Erklärung: Die Größe eines Subnetzes wird als: 2 hoch der Anzahl der Bits die 0 sind (nicht gesetzte Bits).
	
XFrage: Wie viele Hosts passen in folgenden Subnetz: 255.255.254.0?
	Antworten: 510, 512, 256, 254
	Erklärung: Da wir eine Adresse an die Broadcastadresse und eine an die Netzwerkadresse verlieren ist die Anzahl der möglichen Hosts = Subnetzgröße - 2.
				In diesem Beispiel also 510.
	
XFrage: Wie viele Adressen sind pro Subnetz reserviert?
	Antworten: 2, 0, 1, alle
	Erklärung: Zwei Adressen sind pro Subnetz immer reserviert für die Broadcastadresse und Netzwerkadresse.
	
XFrage: Wie groß sich die Anzahl der Hosts eines Subnetzes?
	Antworten: Subnetzgröße - 2, Subnetzgröße, Subnetzgröße - 1, Subnetzgröße + 2
	Erklärung: Da wir eine Adresse an die Broadcastadresse und eine an die Netzwerkadresse verlieren ist die Anzahl der möglichen Hosts = Subnetzgröße - 2.

XFrage: Welche ist das kleinste, mögliche und anwendbare Subnetz?
	Antworten: 255.255.255.252, 0.0.0.0, 255.255.255.255, 255.255.255.128
	Erklärung: Ein Subnetz mit nur einem Host ist sinnlos, deswegen sollte die mindest Anzahl der Hosts 2 sein. Zwei + Broadcastadresse + Netzwerkadresse = 4;
	
xFrage: Welches Subnetz sollte für Anforderung verwendet werden: 4 Gruppen mit je 128 Nutzer?
	Antworten: 255.255.255.0, 255.255.255.128, 255.255.254.0, 255.255.128.0
	Erklärung: Die Anzahl der Gruppen ist für die Größe des Subnetzes irrelevant. Ein Subnetz sollte nur so groß sein, dass alle Hosts reinpassen.
				Da wir 2 Adressen zusätzlich brauchen für die Broadcastadresse und Netzwerkadresse, muss die Anzahl der Adressen 130 sein.
				Diese passt nur in ein Subnetz der größe 256.
	
XFrage: Welche Adresse hat die Broadcastadresse in einem Subnetz?
	Antworten: Die letzte Adresse, Die erste Adresse, Muss eingestellt werden, Die Broadcastadresse wird nicht verwendet
	Erklärung: Die höchste bzw letzte Adresse ist immer die Broadcastadresse.
	
XFrage: Welche Adresse hat die Netzwerkadresse in einem Subnetz?
	Antworten: Die erste Adresse, Die letzte Adresse, Muss eingestellt werden, Die Netzwerkadresse wird nicht verwendet
	Erklärung: Die kleinste bzw erste Adresse ist immer die Netzwerkadresse.
	
XFrage: Wie wird die Subnetzmaske abgekürzt angegeben?
	Antworten: /Anzahl der gesetzten Bits, :Anzahl der gesetzten Bits, /Anzahl der nicht gesetzten Bits, :Anzahl der gesetzten Bits
	Erklärung: Die Slash-Notation wird verwendet um das Subnetz einer IP-Adresse verkürzt anzugeben. Bsp: 192.168.1.0 /24
	
XFrage: Um welchen Faktor muss ein Subnetz vergrößert werden, wenn die Anzahl der Hosts die Anzahl der möglichen Adressen übersteigt?
	Antworten: 2, 4, 3, die überschüssigen kommen in ihr eigenes Subnetz
	Erklärung: Da die Subnetzmaske binär berechnet wird ist unser Vergrößerungsfaktor immer 2.
	

Generierbare Fragen:
Frage: Wie lautet das richtige Subnetz für folgende Anforderung: x Gruppen mit jeweils y Hosts?
Frage: Wie viele Hosts passen in folgenden Subnetz: x.x.x.x?
Frage: Wie lautet die Subnetzmaske für die folgende Slash-Notation: /x?
